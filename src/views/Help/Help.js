import React, {Component} from 'react';
import Container from 'react-bootstrap/Container';
import Breadcrumb from '../../containers/Layout/Breadcrumb';
import TabRating from './Tab/Rating';
import breadcrumbs from '../../breadcrumbs';
import Tab from 'react-bootstrap/Tab';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Nav from 'react-bootstrap/Nav';
import {connect} from 'react-redux';
import seo from '../../lib/seo';

class Help extends Component {
    _isMounted = false;
    state = {};

    static propTypes = {};

    constructor(props) {
        super(props);
        this.state = {
            tab: props.location.pathname,
        };
    }

    componentDidMount() {
        const {tab} = this.state;
        if (tab === '/help') {
            seo.help();
        }
    }

    render() {
        const {tab} = this.state;
        return (
            <React.Fragment>
                <section>
                    <Container>
                        <Breadcrumb push={breadcrumbs.help} />
                    </Container>
                </section>
                <section>
                    <Container className={`mb-5`} style={{minHeight: '500px'}}>
                        <Tab.Container defaultActiveKey={tab}>
                            <Row>
                                <Col xs={12} md={3} className={`aw-tabs-nav-pills`}>
                                    <Nav variant="pills" className="flex-column mb-3" title={``}>
                                        {/*<Nav.Link eventKey="report" className={`nav-item ${tab === '/about' ? 'active' : ''}`} onSelect={() => this.onSelect(`report`)}>*/}
                                        {/*    Обратная связь*/}
                                        {/*</Nav.Link>*/}
                                        <Nav.Link eventKey="rating" className={`nav-item ${tab === '/help' ? 'active' : ''}`} onSelect={() => this.onSelect(`rating`)}>
                                            Обратная связь
                                        </Nav.Link>
                                        {/*<Nav.Link eventKey="offer" className={`nav-item`} onSelect={() => this.onSelect(`offer`)}>*/}
                                        {/*    Сотрудничество*/}
                                        {/*</Nav.Link>*/}
                                    </Nav>
                                </Col>
                                <Col xs={12} md={9}>
                                    <Tab.Content>
                                        {/*<Tab.Pane eventKey="report">*/}
                                        {/*    <TabReport />*/}
                                        {/*</Tab.Pane>*/}
                                        <Tab.Pane eventKey="rating" className={`${tab === '/help' ? 'show active' : ''}`}>
                                            <TabRating />
                                        </Tab.Pane>
                                        {/*<Tab.Pane eventKey="offer">*/}
                                        {/*    <TabOffer />*/}
                                        {/*</Tab.Pane>*/}
                                    </Tab.Content>
                                </Col>
                            </Row>
                        </Tab.Container>
                    </Container>
                </section>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Help);
