import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import ReactRating from 'react-rating';
import {getHelpRatingPending, getHelpRatingSuccess} from '../../../store/reducers/Help';
import {fetchHelpRating, fetchHelpRatingSuccess} from '../../../store/actions/Help';
import {connect} from 'react-redux';
import * as PropTypes from 'prop-types';

class Rating extends React.Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        pending: PropTypes.bool,
        success: PropTypes.bool,
        fetchHelpRating: PropTypes.func,
        fetchHelpRatingSuccess: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.state = {
            description: '',
            email: '',
            rating: 0,
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        const {success, fetchHelpRatingSuccess} = this.props;
        if (success) {
            this.setState({
                email: '',
                description: '',
                rating: 0,
            });
            fetchHelpRatingSuccess(false);
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    handleChange(e) {
        this.setState({
            [e.currentTarget.name]: e.currentTarget.value,
        });
    }

    handleSubmit(e) {
        e.preventDefault();
        const {fetchHelpRating} = this.props;
        const {description, rating, email} = this.state;
        const data = {
            description: description,
            rating: rating,
            email: email,
        };
        fetchHelpRating(data);
    }

    rateChange(rate) {
        this.setState({
            rating: rate,
        });
    }

    render() {
        const {description, rating, email} = this.state;
        const {pending} = this.props;
        if (!this.shouldComponentRender()) return null;
        return (
            <React.Fragment>
                <h3>Обратная связь</h3>
                <Row>
                    <Col xs={12} md={12}>
                        <p className={`text-muted`}>
                            Оставьте нам сообщение и мы вам ответим в течении 1-2х недель.
                            Так же можете поставь нашему сервису оценку и замечания, если такие есть.
                            Мы каждый день стараемся улучшить наш сервис и вы очень поможете.
                        </p>
                    </Col>
                    <Col xs={12} md={12}>
                        <Form onSubmit={this.handleSubmit}>
                            <Form.Group>
                                <Form.Label>
                                    Ваш Email <i className="r">*</i>
                                </Form.Label>
                                <Form.Control type="email" placeholder="Ваш Email" value={email} name="email" onChange={this.handleChange} required />
                            </Form.Group>
                            <Form.Group>
                                <Form.Label>
                                    Текст <i className="r">*</i>
                                </Form.Label>
                                <Form.Control
                                    as="textarea"
                                    rows="5"
                                    type="text"
                                    placeholder="Описание"
                                    value={description}
                                    name="description"
                                    onChange={this.handleChange}
                                    required
                                />
                            </Form.Group>
                            <Form.Group>
                                <Form.Label>
                                    Ваша оценка
                                </Form.Label>
                                <div>
                                    <ReactRating
                                        emptySymbol="far fa-star"
                                        fullSymbol="fas fa-star"
                                        initialRating={rating}
                                        onChange={rate => this.rateChange(rate)}
                                        readonly={false}
                                    />
                                </div>
                            </Form.Group>

                            <Button variant="primary" type="submit" className={`${pending ? 'is-loading' : ''} `}>
                                Отправить
                            </Button>
                        </Form>
                    </Col>
                    <Col xs={12} md={12} className={`mt-3`}>
                        <p>
                            Вводимые ваши данные конфиденциальные, поля с <i className="r">*</i> обязательны к заполнению.
                        </p>
                    </Col>
                </Row>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    pending: getHelpRatingPending(state),
    success: getHelpRatingSuccess(state),
});

const mapDispatchToProps = dispatch => ({
    fetchHelpRating: data => dispatch(fetchHelpRating(data)),
    fetchHelpRatingSuccess: success => dispatch(fetchHelpRatingSuccess(success)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Rating);
