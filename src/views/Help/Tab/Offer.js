import React from 'react';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import {getHelpOfferPending} from '../../../store/reducers/Help';
import {fetchHelpOffer} from '../../../store/actions/Help';
import {connect} from 'react-redux';
import * as PropTypes from 'prop-types';

class Offer extends React.Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        pending: PropTypes.bool,
        fetchHelpOffer: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.state = {
            description: '',
            radio: '',
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    handleChange(e) {
        this.setState({
            [e.currentTarget.name]: e.currentTarget.value,
        });
    }

    handleSubmit(e) {
        e.preventDefault();
        const {fetchHelpOffer} = this.props;
        const {description, radio} = this.state;
        const data = {
            description: description,
            radio: radio,
        };
        fetchHelpOffer(data);
    }
    render() {
        const {description, radio} = this.state;
        const {pending} = this.props;
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <h3>Сотрудничество</h3>
                <Row>
                    <Col xs={12} md={12}>
                        <p className={`text-muted`}>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt dicta distinctio enim eos, fuga harum labore libero maiores
                            maxime molestiae nemo, neque nihil rerum, saepe sapiente sequi totam voluptas voluptate?
                        </p>
                    </Col>
                    <Col xs={12} md={12}>
                        <Form onSubmit={this.handleSubmit}>
                            <Form.Group controlId="exampleForm.ControlSelect1">
                                {/*<Form.Label>Я</Form.Label>*/}
                                <Form.Check
                                    custom
                                    type={`radio`}
                                    label={`Организатор`}
                                    checked={radio === 1}
                                    onChange={this.handleChange}
                                    value={1}
                                    name={`radio`}
                                />
                                <Form.Check
                                    custom
                                    type={`radio`}
                                    label={`По поводу Рекламы`}
                                    checked={radio === 2}
                                    onChange={this.handleChange}
                                    value={2}
                                    name={`radio`}
                                />
                            </Form.Group>

                            <Form.Group>
                                <Form.Label>Описание</Form.Label>
                                <Form.Control
                                    as="textarea"
                                    rows="5"
                                    type="text"
                                    placeholder="Описание"
                                    value={description}
                                    name="description"
                                    onChange={this.handleChange}
                                />
                            </Form.Group>

                            <Button variant="primary" type="submit" className={`${pending ? 'is-loading' : ''} `}>
                                Отправить
                            </Button>
                        </Form>
                    </Col>
                    <Col xs={12} md={12} className={`mt-3`}>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt dicta distinctio enim eos, fuga harum labore libero maiores
                            maxime molestiae nemo, neque nihil rerum, saepe sapiente sequi totam voluptas voluptate?
                        </p>
                    </Col>
                </Row>
            </React.Fragment>
        );
    }
}
const mapStateToProps = state => ({
    pending: getHelpOfferPending(state),
});

const mapDispatchToProps = dispatch => ({
    fetchHelpOffer: data => dispatch(fetchHelpOffer(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Offer);
