import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import {connect} from 'react-redux';
import {getHelpReportPending} from '../../../store/reducers/Help';
import {fetchHelpReport} from '../../../store/actions/Help';
import * as PropTypes from 'prop-types';

class Report extends React.Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        pending: PropTypes.bool,
        fetchHelpReport: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.state = {
            description: '',
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    handleChange(e) {
        this.setState({
            [e.currentTarget.name]: e.currentTarget.value,
        });
    }

    handleSubmit(e) {
        e.preventDefault();
        const {fetchHelpReport} = this.props;
        const {description} = this.state;
        const data = {
            description: description,
        };
        fetchHelpReport(data);
    }

    render() {
        const {description} = this.state;
        const {pending} = this.props;
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <h3>Ошибка на сайте</h3>
                <Row>
                    <Col xs={12} md={12}>
                        <p className={`text-muted`}>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt dicta distinctio enim eos, fuga harum labore libero maiores
                            maxime molestiae nemo, neque nihil rerum, saepe sapiente sequi totam voluptas voluptate?
                        </p>
                    </Col>
                    <Col xs={12} md={12}>
                        <Form onSubmit={this.handleSubmit}>
                            {/*<Form.Group controlId="exampleForm.ControlSelect1">*/}
                            {/*    <Form.Label>Уровень критичности</Form.Label>*/}
                            {/*    <Form.Control as="select">*/}
                            {/*        <option>Информативный</option>*/}
                            {/*        <option>Опасный</option>*/}
                            {/*        <option>Критичный</option>*/}
                            {/*    </Form.Control>*/}
                            {/*</Form.Group>*/}

                            <Form.Group>
                                <Form.Label>Описание</Form.Label>
                                <Form.Control
                                    as="textarea"
                                    rows="5"
                                    type="text"
                                    placeholder="Описание"
                                    value={description}
                                    name="description"
                                    onChange={this.handleChange}
                                />
                            </Form.Group>

                            <Button variant="primary" type="submit" className={`${pending ? 'is-loading' : ''} `}>
                                Отправить
                            </Button>
                        </Form>
                    </Col>
                    <Col xs={12} md={12} className={`mt-3`}>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt dicta distinctio enim eos, fuga harum labore libero maiores
                            maxime molestiae nemo, neque nihil rerum, saepe sapiente sequi totam voluptas voluptate?
                        </p>
                    </Col>
                </Row>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    pending: getHelpReportPending(state),
});

const mapDispatchToProps = dispatch => ({
    fetchHelpReport: data => dispatch(fetchHelpReport(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Report);
