import React, {Component} from 'react';
import Container from 'react-bootstrap/Container';
import Breadcrumb from '../../containers/Layout/Breadcrumb';
import breadcrumbs from '../../breadcrumbs';
import {connect} from 'react-redux';
import seo from '../../lib/seo';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import {scrollTo} from '../../lib/scrollTo';
import * as PropTypes from 'prop-types';
import {paginationTypes} from '../../prop/Types/Pagination';
import {TITLE_DIRECTIONS_ATTRACTIONS} from "../../lib/titles";
import TitleMain from "../../containers/Layout/TitleMain";
import {getAttractions, getAttractionsPagination, getAttractionsPending} from "../../store/reducers/Attractions";
import {fetchAttractions} from "../../store/actions/Attractions";
import {attractionsTypes} from "../../prop/Types/Attraction";
import SkeletonAttraction from '../../containers/Skeleton/Attraction/Card';
import AttractionCard from "../../containers/Attraction/Card/Card";
import AwPagination from "../../containers/Layout/AwPagination";
import {fetchDirection} from "../../store/actions/Direction";
import {getDirection, getDirectionPending} from "../../store/reducers/Direction";
import Skeleton from "react-loading-skeleton";
import {directionTypes} from "../../prop/Types/Direction";

class DirectionAttractions extends Component {
    _isMounted = false;

    static propTypes = {
        attractions: attractionsTypes,
        pending: PropTypes.bool,
        pagination: paginationTypes,
        fetchAttractions: PropTypes.func,
        fetchDirection: PropTypes.func,
        direction: directionTypes,
        directionLoading: PropTypes.bool,
    };

    constructor(props) {
        super(props);
        this.state = {};
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.onChangePage = this.onChangePage.bind(this);
    }

    componentDidMount() {
        const {fetchDirection} = this.props;
        fetchDirection(this.props.match.params.id);
        this._isMounted = true;
        seo.attractions();
        const {location} = this.props;
        let searchUrl = location.search;
        let urlData = new URLSearchParams(searchUrl);
        const page = urlData.get('page');
        this.onChangePage(page);
    }

    componentWillMount() {
        const {direction} = this.props;
        console.log(direction);
    }

    onChangePage(page) {
        const {fetchAttractions} = this.props;
        scrollTo.selectorFast('.breadcrumb');
        const direction_id = this.props.match.params.id;
        fetchAttractions({
            page: page !== undefined ? page : 1,
            direction_id: direction_id !== undefined ? direction_id : null,
        });
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {pending, attractions, pagination, direction, directionLoading} = this.props;
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <section>
                    {directionLoading &&
                    <Container>
                        <Row>
                            <Col xs={12}>
                                <div className={`mt-2 mb-2`}>
                                    <Skeleton height={`25px`} width={`200px`} />
                                </div>
                            </Col>
                        </Row>
                    </Container>
                    }
                    {!directionLoading && (
                        <Container>
                            <Breadcrumb push={breadcrumbs.attractions} parent={{to: `/direction/${direction.id}/${direction.name}`, title: direction.title}}/>
                        </Container>
                    )}
                </section>
                <section>
                    {directionLoading &&
                    <Container className={`mb-5`}>
                        <TitleMain title={`Достопримечательности`} model={`${TITLE_DIRECTIONS_ATTRACTIONS}`}/>
                    </Container>
                    }
                    {!directionLoading && (
                        <Container className={`mb-5`}>
                            <TitleMain title={`Достопримечательности`} model={`${TITLE_DIRECTIONS_ATTRACTIONS}`} item={direction}/>
                        </Container>
                    )}
                </section>
                {pending && (
                    <section className={`h-500`}>
                        <Container>
                            <Row>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`3`}>
                                    <SkeletonAttraction/>
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`3`}>
                                    <SkeletonAttraction/>
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`3`}>
                                    <SkeletonAttraction/>
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`3`}>
                                    <SkeletonAttraction/>
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`3`}>
                                    <SkeletonAttraction/>
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`3`}>
                                    <SkeletonAttraction/>
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`3`}>
                                    <SkeletonAttraction/>
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`3`}>
                                    <SkeletonAttraction/>
                                </Col>
                            </Row>
                        </Container>
                    </section>
                )}
                {!pending && (
                    <section>
                        <Container>
                            <Row>
                                {attractions.length !== 0 &&
                                attractions.map((attraction, key) => (
                                    <Col xs={`12`} sm={`12`} md={`6`} lg={3} className={`mb-3`} key={key}>
                                        <AttractionCard attraction={attraction}/>
                                    </Col>
                                ))}
                            </Row>
                        </Container>
                        {pagination !== null && pagination.total > pagination.limit && (
                            <Container className="d-flex justify-content-center mt-5">
                                {/* eslint-disable-next-line react/jsx-no-bind */}
                                <AwPagination pagination={pagination} onChangePage={page => this.onChangePage(page)} location={this.props.location} />
                            </Container>
                        )}
                    </section>
                )}
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    attractions: getAttractions(state),
    pagination: getAttractionsPagination(state),
    pending: getAttractionsPending(state),

    directionLoading: getDirectionPending(state),
    direction: getDirection(state),
});

const mapDispatchToProps = dispatch => ({
    fetchAttractions: data => dispatch(fetchAttractions(data)),
    fetchDirection: id => dispatch(fetchDirection(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(DirectionAttractions);
