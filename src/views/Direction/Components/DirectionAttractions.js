import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import {connect} from 'react-redux';
import {TITLE_ATTRACTIONS_DIRECTION} from '../../../lib/titles';
import TitleMain from '../../../containers/Layout/TitleMain';
import Card from "../../../containers/Attraction/Card/Card";
import {directionTypes} from "../../../prop/Types/Direction";
import {Link} from "react-router-dom";

class DirectionAttractions extends React.Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        direction: directionTypes,
    };

    constructor(props) {
        super(props);
        this.state = {
            direction: props.direction,
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {direction} = this.props;
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                {direction.attractions.data.length !== 0 && (
                    <Container>
                        <Row className={`mb-5 mt-5`}>
                            <TitleMain title={`Достопримечательности`} model={TITLE_ATTRACTIONS_DIRECTION} role={`h2`} />
                        </Row>
                        <Row>
                            {direction.attractions.data.map((attraction, key) => (
                                <Col xs={12} sm={6} md={6} lg={4} xl={3} key={key}>
                                    <Card attraction={attraction}/>
                                </Col>
                            ))}
                        </Row>
                        {direction.attractions.data.length === 3 && (
                            <Row className={`mt-3`}>
                                <Col xs={12} className={`text-right`}>
                                    <Link to={`/direction/${direction.id}/attractions`} className={`btn`} variant="light">
                                        Все достопримечательности ({direction.attractions.total})
                                    </Link>
                                </Col>
                            </Row>
                        )}
                        {/*<AttractionCardModal />*/}
                    </Container>
                )}
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({});
export default connect(mapStateToProps)(DirectionAttractions);
