import React, {Component} from 'react';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import {directionTypes} from "../../../prop/Types/Direction";
import {
    FullscreenControl,
    GeolocationControl,
    Map,
    Placemark,
    RouteButton,
    TypeSelector, YMaps,
    ZoomControl
} from "react-yandex-maps";

class DirectionMap extends Component {
    static propTypes = {
        direction: directionTypes,
    };
    constructor(props) {
        super(props);
        this.state = {
            direction: props.direction,
        };
    }

    render() {
        const {direction} = this.state;
        return (
            <React.Fragment>
                {direction.location !== null &&
                <Row>
                    <Col xs={12} md={12} style={{marginTop: '30px'}}>
                        <YMaps>
                            <div>
                                <Map
                                    defaultState={{
                                        center: direction.location.point,
                                        zoom: direction.location.zoom,
                                        controls: [],
                                        //behaviors: ['ScrollZoom'],
                                    }}
                                    width={`100%`}
                                >
                                    <Placemark geometry={direction.location.point} />
                                    <ZoomControl options={{float: 'right'}} />
                                    <TypeSelector options={{float: 'right'}} />
                                    <GeolocationControl options={{float: 'left'}} />
                                    <RouteButton options={{float: 'right'}} />
                                    <FullscreenControl />
                                </Map>
                            </div>
                        </YMaps>
                    </Col>
                </Row>
                }
            </React.Fragment>
        );
    }
}

export default DirectionMap;
