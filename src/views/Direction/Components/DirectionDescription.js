import React, {Component} from 'react';
import Col from 'react-bootstrap/Col';
import ReactMarkdown from '../../../plugins/ReactMarkdown';
import Row from 'react-bootstrap/Row';
import {directionTypes} from "../../../prop/Types/Direction";

class DirectionDescription extends Component {
    static propTypes = {
        direction: directionTypes,
    };
    constructor(props) {
        super(props);
        this.state = {
            direction: props.direction,
        };
    }

    render() {
        const {direction} = this.state;
        return (
            <React.Fragment>
                <Row className={`mt-5 mb-5`}>
                    <Col xs={12} md={12}>
                        <h3>Описание</h3>
                        <ReactMarkdown source={direction.description} />
                    </Col>
                </Row>
            </React.Fragment>
        );
    }
}

export default DirectionDescription;
