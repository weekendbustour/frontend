import React, {Component} from 'react';
import 'react-dates/initialize';
import {connect} from 'react-redux';
import {isMobile} from 'react-device-detect';
import FsLightbox from 'fslightbox-react';
import {fsLightboxHelper} from '../../../lib/fsLightboxHelper';
import {directionTypes} from "../../../prop/Types/Direction";
import DirectionMedia from "../../../containers/Direction/Components/Social/Media";
import Rating from '../../../components/Rating/RatingDefault';

class Main extends Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        direction: directionTypes,
    };

    constructor(props) {
        super(props);
        this.state = {
            toggleBanner: false,
            direction: props.direction,
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.setToggleImageBanner = this.setToggleImageBanner.bind(this);
        this.handleClearTitles = this.handleClearTitles.bind(this);
    }

    setToggleImageBanner() {
        const {toggleBanner} = this.state;
        this.setState({toggleBanner: !toggleBanner});
    }
    handleClearTitles() {
        fsLightboxHelper.clearTitles();
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {direction, toggleBanner} = this.state;
        if (!this.shouldComponentRender()) return null;
        return (
            <React.Fragment>
                <div className={`page__flip`}>
                    <div className="page__flip--img">
                        <div className={`page__flip--background`} style={{backgroundImage: 'url(' + direction.image.banner + ')'}} onClick={this.setToggleImageBanner}/>
                        <div className={`page__flip--socials __centered __100px`}>
                            <DirectionMedia direction={direction} variant={`light-dark`} role={`page`} size={isMobile ? `md` : `md`} />
                        </div>
                    </div>
                    <div className={`mt-3`} style={{zIndex: '-1'}}>
                        <h2 className={`text-center`}>{direction.title}</h2>
                        <p className="text-center delta cardo regular italic">{direction.preview_description}</p>
                        <div className={`text-center page__flip--rating`}>
                            <Rating rating={direction.rating} readonly={true} />
                        </div>
                    </div>
                </div>
                <div className={`flight-box-custom ${isMobile ? 'is-mobile' : 'is-desktop'}`}>
                    <FsLightbox
                        type="image"
                        toggler={toggleBanner}
                        sources={[direction.image.banner]}
                        slide={0}
                        thumbs={[direction.image.banner]}
                        onOpen={this.handleClearTitles}
                    />
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Main);
