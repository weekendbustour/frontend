import React, {Component} from 'react';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import {DayPickerSingleDateController} from 'react-dates';
import {isMobile} from 'react-device-detect';
import isInclusivelyAfterDay from 'react-dates/src/utils/isInclusivelyAfterDay';
import Button from 'react-bootstrap/Button';
import SkeletonTour from '../../../containers/Skeleton/Tour/Card';
import TourCard from '../../../containers/Tour/Card/Card';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {scrollTo} from '../../../lib/scrollTo';
import * as PropTypes from 'prop-types';
import {toursTypes} from '../../../prop/Types/Tour';
import {directionTypes} from '../../../prop/Types/Direction';
import {fetchDirectionTours} from '../../../store/actions/Direction';
import {getDirectionTours, getDirectionToursDate, getDirectionToursPending} from '../../../store/reducers/Direction';
import TitleDefault from "../../../components/Title/TitleDefault";
const moment = require('moment');
require('moment/locale/ru');

class DirectionCalendar extends Component {
    static propTypes = {
        direction: directionTypes,
        tours: toursTypes,
        toursPending: PropTypes.bool,
        fetchDirectionTours: PropTypes.func,
        toursDate: PropTypes.string,
    };

    constructor(props) {
        super(props);
        this.state = {
            direction: props.direction,
            date: moment().locale('ru'),
            dateChanged: false,
            datesActive: props.direction.dates.active ?? [],
            datesSale: props.direction.dates.sale ?? [],
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.dateChange = this.dateChange.bind(this);
        this.dateToggle = this.dateToggle.bind(this);
    }

    componentWillMount(): void {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    dateChange(date) {
        const {fetchDirectionTours} = this.props;
        const {direction} = this.state;
        this.setState({
            dateChanged: true,
            date: date,
        });
        fetchDirectionTours(direction.id, date.format('DD.MM.YYYY'));
        setTimeout(function() {
            scrollTo.selector('.row-date-title');
        }, 100);
    }

    dateToggle() {
        this.setState({
            dateChanged: false,
            date: moment().locale('ru'),
        });
    }

    render() {
        const {toursPending, tours, toursDate} = this.props;
        const {direction, dateChanged, date, datesActive} = this.state;
        if (!this.shouldComponentRender()) return null;
        console.log(tours);
        return (
            <React.Fragment>
                <Row>
                    <Col xs={12}>
                        <div className={`text-muted text-center`}>{datesActive.length !== 0 ? 'Выберите дату' : 'Активных туров нет'}</div>
                    </Col>
                    <Col
                        xs={12}
                        md={{
                            span: 6,
                            offset: 3,
                        }}
                        className={`d-flex justify-content-center mt-3`}
                        style={{marginTop: '30px'}}
                    >
                        <DayPickerSingleDateController
                            date={date}
                            onDateChange={date => this.dateChange(date)}
                            firstDayOfWeek={1}
                            numberOfMonths={isMobile ? 1 : 2}
                            keepOpenOnDateSelect={true}
                            hideKeyboardShortcutsPanel={true}
                            isOutsideRange={day => {
                                return !isInclusivelyAfterDay(day, moment());
                            }}
                            isDayBlocked={day => {
                                return !direction.dates.active.includes(day.format('DD.MM.YYYY'));
                            }}
                            isDayHighlighted={day => {
                                return direction.dates.sales.includes(day.format('DD.MM.YYYY'));
                            }}
                        />
                    </Col>
                </Row>
                {/* Когда дата выбрана */}
                {dateChanged && (
                    <Row className={`pt-3 mt-2 mb-3 row-date-title`}>
                        <Col xs={12}>
                            <TitleDefault role={`h2`} title={toursDate}/>
                        </Col>
                        <Col xs={12} className={`text-center mt-3`}>
                            <Button className={`btn-light-dark`} size={`sm`} variant={`light`} onClick={() => this.dateToggle()}>
                                <i className="fas fa-times" />
                                &nbsp;&nbsp;Сбросить дату
                            </Button>
                        </Col>
                    </Row>
                )}
                {/* Прелоадер */}
                {dateChanged && toursPending && (
                    <Row className={`awesome-card cards`}>
                        <Col xs={`12`} sm={`12`} md={`6`} lg={`4`} className={`mb-3`}>
                            <SkeletonTour />
                        </Col>
                        <Col xs={`12`} sm={`12`} md={`6`} lg={`4`} className={`mb-3`}>
                            <SkeletonTour />
                        </Col>
                        <Col xs={`12`} sm={`12`} md={`6`} lg={`4`} className={`mb-3`}>
                            <SkeletonTour />
                        </Col>
                        <Col xs={`12`} sm={`12`} md={`6`} lg={`4`} className={`mb-3`}>
                            <SkeletonTour />
                        </Col>
                        <Col xs={`12`} sm={`12`} md={`6`} lg={`4`} className={`mb-3`}>
                            <SkeletonTour />
                        </Col>
                        <Col xs={`12`} sm={`12`} md={`6`} lg={`4`} className={`mb-3`}>
                            <SkeletonTour />
                        </Col>
                    </Row>
                )}
                {/* Вывод туров */}
                {dateChanged && !toursPending && (
                    <Row className={`awesome-card cards`}>
                        {tours.length !== 0 &&
                            tours.map((tour, key) => (
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`4`} className={`mb-3`} key={key}>
                                    <TourCard tour={tour} />
                                </Col>
                            ))}
                        <Col xs={`12`} sm={`12`} md={`12`} lg={`12`} className={`mb-3 mt-3`}>
                            <p className={`text-center`}>
                                Так же вы можете выбрать ближайщие туры или найти туры на&#160;<Link to={`/search`}>странице поиска</Link>
                            </p>
                        </Col>
                    </Row>
                )}
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    tours: getDirectionTours(state),
    toursPending: getDirectionToursPending(state),
    toursDate: getDirectionToursDate(state),
});
const mapDispatchToProps = dispatch => ({
    fetchDirectionTours: (id, date) => dispatch(fetchDirectionTours(id, date)),
});

export default connect(mapStateToProps, mapDispatchToProps)(DirectionCalendar);
