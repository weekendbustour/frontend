import React, {Component} from 'react';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import {directionTypes} from "../../../prop/Types/Direction";

class DirectionWeather extends Component {
    static propTypes = {
        direction: directionTypes,
    };
    constructor(props) {
        super(props);
        this.state = {
            direction: props.direction,
        };
    }

    render() {
        return (
            <React.Fragment>
                <Row className={`mt-5`}>
                    <Col xs={12}>
                        <h3>Погода</h3>
                    </Col>
                    <Col xs={12} md={12}>
                        <div className="card__weather">
                            <div className="details">
                                <div className="temp">+20<span>c</span></div>
                                <div className="right">
                                    <div className="date">
                                        Понедельник <br/>
                                        22 Августа</div>
                                    <div className="summary">Снег</div>
                                </div>
                            </div>
                            <div className="details d-block">
                                <div>
                                    Ощущается как: <div className="temp temp-sm">+24<span>c</span></div>
                                </div>
                                <div>Ветер: 1.2 м/с</div>
                                <div>Давление: 866 мм рт. ст.</div>
                                <div>Вероятность выпадения осадков: 20%</div>
                            </div>
                        </div>
                    </Col>
                </Row>
            </React.Fragment>
        );
    }
}

export default DirectionWeather;
