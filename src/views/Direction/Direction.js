import React, {Component} from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Breadcrumb from '../../containers/Layout/Breadcrumb';
import 'react-dates/initialize';
import breadcrumbs from '../../breadcrumbs';
import {getDirection, getDirectionPending} from '../../store/reducers/Direction';
import {fetchDirection} from '../../store/actions/Direction';
import SkeletonPage from '../../containers/Skeleton/Direction/Page';
import {connect} from 'react-redux';
import * as PropTypes from 'prop-types';
import {directionTypes} from '../../prop/Types/Direction';

import Main from './Components/Main';
import DirectionSlider from './Components/DirectionSlider';
import DirectionDescription from './Components/DirectionDescription';
import DirectionMap from './Components/DirectionMap';
import DirectionComments from './Components/DirectionComments';
import Title from '../../containers/Layout/Title';
import ResultFound from '../../containers/Tour/Components/ResultFound';
import TourCard from '../../containers/Tour/Card/Card';
import {Link} from 'react-router-dom';
import DirectionSubscription from './Components/DirectionSubscription';
import DirectionCalendar from './Components/DirectionCalendar';
import DirectionAttractions from './Components/DirectionAttractions';

class Direction extends Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        direction: directionTypes,
        directionLoading: PropTypes.bool,
        fetchDirection: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.state = {};
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentDidMount() {
        const {fetchDirection} = this.props;
        if (window.page !== undefined && window.page.direction !== undefined) {
            fetchDirection(window.page.direction.id);
        } else {
            fetchDirection(this.props.match.params.id);
        }
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {direction, directionLoading} = this.props;
        if (!this.shouldComponentRender()) return null;
        return (
            <React.Fragment>
                {directionLoading && (
                    <section>
                        <SkeletonPage />
                    </section>
                )}
                {/* Хлебные крошки */}
                <section>
                    {!directionLoading && (
                        <Container>
                            <Breadcrumb parent={breadcrumbs.directions} push={{title: direction.title}} />
                        </Container>
                    )}
                </section>
                {/* Баннер */}
                <section>
                    {!directionLoading && (
                        <Container className={`mb-2 mt-0`}>
                            <Row>
                                <Col xs={12}>
                                    <Main direction={direction} />
                                </Col>
                            </Row>
                        </Container>
                    )}
                </section>
                {/* Слайдер */}
                <section>{!directionLoading && <DirectionSlider direction={direction} />}</section>
                {/* Описание */}
                <section>
                    {!directionLoading && direction.description !== null && (
                        <Container>
                            <DirectionDescription direction={direction} />
                        </Container>
                    )}
                </section>
                {/* Календарь */}
                <section>
                    {!directionLoading && (
                        <Container className={`mb-5 direction--date ${direction.tours.length === 0 ? '__empty' : ''}`}>
                            <DirectionCalendar direction={direction} />
                            {direction.tours.length === 0 && <DirectionSubscription direction={direction} />}
                        </Container>
                    )}
                </section>
                {/* Ближайщие туры */}
                <section>
                    {!directionLoading && (
                        <Container>
                            {/* Загрузить еще */}
                            {direction.tours !== undefined && direction.tours.length !== 0 && (
                                <Row className={`mt-5`}>
                                    <Col xs={12}>
                                        <Title role={`h2`} title={`Ближайшие`} />
                                    </Col>
                                    <Col xs={`12`}>
                                        <ResultFound total={direction.tours.length} />
                                    </Col>
                                </Row>
                            )}
                            {/* Ближайщие туры */}
                            {direction.tours !== undefined && direction.tours.length !== 0 && (
                                <Row className={`awesome-card cards`}>
                                    {direction.tours.length !== 0 &&
                                        direction.tours.map((tour, key) => (
                                            <Col xs={`12`} sm={`12`} md={`6`} lg={`4`} className={`mb-3`} key={key}>
                                                <TourCard tour={tour} />
                                            </Col>
                                        ))}
                                </Row>
                            )}
                            {direction.tours.length === 4 && (
                                <Row className={`mb-5`}>
                                    <Col xs={12} className={`text-right`}>
                                        <Link to={`/search?direction_id=${direction.id}&scroll=1`} className={`btn`} variant="light">
                                            Посмотреть все Туры в этом Направлении
                                        </Link>
                                    </Col>
                                </Row>
                            )}
                        </Container>
                    )}
                </section>
                {/*/!* Достопримечательности *!/*/}
                {/*<section>*/}
                {/*    {!directionLoading && direction.attractions.length !== 0 && (*/}
                {/*        <Container>*/}
                {/*            <DirectionAttractions direction={direction} />*/}
                {/*        </Container>*/}
                {/*    )}*/}
                {/*</section>*/}
                {/*/!* Погода *!/*/}
                {/*<section>*/}
                {/*    {!directionLoading && (*/}
                {/*        <Container>*/}
                {/*            <DirectionWeather direction={direction} />*/}
                {/*        </Container>*/}
                {/*    )}*/}
                {/*</section>*/}
                {/* Карта */}
                <section>
                    {!directionLoading && (
                        <Container>
                            <DirectionMap direction={direction} />
                        </Container>
                    )}
                </section>
                {/* Достопримечательности */}
                <section>
                    {!directionLoading && direction.attractions.length !== 0 && (
                        <Container>
                            <DirectionAttractions direction={direction} />
                        </Container>
                    )}
                </section>
                {/* Комментарии */}
                <section>
                    {!directionLoading && (
                        <Container>
                            <DirectionComments direction={direction} />
                        </Container>
                    )}
                </section>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    directionLoading: getDirectionPending(state),
    direction: getDirection(state),
});

const mapDispatchToProps = dispatch => ({
    fetchDirection: id => dispatch(fetchDirection(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Direction);
