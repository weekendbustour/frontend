import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import {connect} from 'react-redux';
import ArticleCard from '../../../containers/Article/Card/Card';
import {TITLE_ATTRACTIONS_ARTICLES} from '../../../lib/titles';
import TitleMain from '../../../containers/Layout/TitleMain';
import {attractionTypes} from "../../../prop/Types/Attraction";

class AttractionArticles extends React.Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        attraction: attractionTypes,
    };

    constructor(props) {
        super(props);
        this.state = {
            attraction: props.attraction,
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {attraction} = this.props;
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                {attraction.articles.length !== 0 && (
                    <Container>
                        <Row className={`mb-5 mt-5`}>
                            <TitleMain title={`СТАТЬИ`} model={TITLE_ATTRACTIONS_ARTICLES} role={`h2`} />
                        </Row>
                        <Row>
                            {attraction.articles.map((article, key) => (
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`4`} className={`mb-3`} key={key}>
                                    <ArticleCard article={article} />
                                </Col>
                            ))}
                        </Row>
                    </Container>
                )}
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({});
export default connect(mapStateToProps)(AttractionArticles);
