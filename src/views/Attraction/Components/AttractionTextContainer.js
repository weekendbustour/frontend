import React, {Component} from 'react';
import {isMobile} from "react-device-detect";
import ReactMarkdown from "../../../plugins/ReactMarkdown";
import {attractionTypes} from "../../../prop/Types/Attraction";
import AttractionStatistic from "../../../containers/Attraction/Components/ArticleStatistic";
import {Link} from "react-router-dom";

class AttractionTextContainer extends Component {
    static propTypes = {
        attraction: attractionTypes,
    };
    constructor(props) {
        super(props);
        this.state = {
            attraction: props.attraction,
        };
    }
    render() {
        const {attraction} = this.state;
        return (
            <React.Fragment>
                <div className="page__polo--text-container">
                    <div className={`d-flex align-items-center`}>
                        {/*<div className="text-muted">{attraction.published_at_format}</div>*/}
                        <div className="page__polo--socials">
                            <AttractionStatistic attraction={attraction} variant={`light-dark`} size={isMobile ? `md` : `md`}/>
                        </div>
                    </div>
                    <h2 className={`text-left`}>
                        {attraction.title}
                    </h2>
                    <ReactMarkdown source={attraction.description} />
                    {attraction.direction !== null && (
                        <div>
                            Встречается по Направлению:&#160;
                            <Link to={`/direction/${attraction.direction.id}/${attraction.direction.name}`}>
                                {attraction.direction.title}
                            </Link>
                        </div>
                    )}
                </div>
            </React.Fragment>
        );
    }
}

export default AttractionTextContainer;
