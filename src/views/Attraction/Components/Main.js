import React, {Component} from 'react';
import 'react-dates/initialize';
import {connect} from 'react-redux';
import {isMobile} from 'react-device-detect';
import FsLightbox from 'fslightbox-react';
import {fsLightboxHelper} from '../../../lib/fsLightboxHelper';
import {attractionTypes} from "../../../prop/Types/Attraction";

class Main extends Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        attraction: attractionTypes,
    };

    constructor(props) {
        super(props);
        this.state = {
            toggleBanner: false,
            attraction: props.attraction,
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.setToggleImageBanner = this.setToggleImageBanner.bind(this);
        this.handleClearTitles = this.handleClearTitles.bind(this);
    }

    setToggleImageBanner() {
        const {toggleBanner} = this.state;
        this.setState({toggleBanner: !toggleBanner});
    }
    handleClearTitles() {
        fsLightboxHelper.clearTitles();
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {attraction, toggleBanner} = this.state;
        if (!this.shouldComponentRender()) return null;
        return (
            <React.Fragment>
                <div className="page__polo--img">
                    <div className={`page__polo--background`} style={{backgroundImage: 'url(' + attraction.image.banner + ')'}} onClick={this.setToggleImageBanner}/>
                </div>
                <div className={`flight-box-custom ${isMobile ? 'is-mobile' : 'is-desktop'}`}>
                    <FsLightbox
                        type="image"
                        toggler={toggleBanner}
                        sources={[attraction.image.banner]}
                        slide={0}
                        thumbs={[attraction.image.banner]}
                        onOpen={this.handleClearTitles}
                    />
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Main);
