import React, {Component} from 'react';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import {FullscreenControl, GeolocationControl, Map, Placemark, RouteButton, TypeSelector, YMaps, ZoomControl} from 'react-yandex-maps';
import {attractionTypes} from "../../../prop/Types/Attraction";

/**
 * https://tech.yandex.com/maps/jsapi/doc/2.1/ref/reference/ObjectManager-docpage/
 *
 * https://tech.yandex.com/maps/jsapi/doc/2.1/ref/reference/Placemark-docpage/
 * balloonContentHeader
 * balloonContentBody
 * balloonContentFooter
 */
class AttractionMap extends Component {
    static propTypes = {
        attraction: attractionTypes,
    };
    constructor(props) {
        super(props);
        this.state = {
            attraction: props.attraction,
        };
    }

    render() {
        const {attraction} = this.state;
        return (
            <React.Fragment>
                <Row>
                    <Col xs={12} md={12} style={{marginTop: '30px'}}>
                        <YMaps>
                            <div>
                                <Map
                                    defaultState={{
                                        center: attraction.location.point,
                                        zoom: attraction.location.zoom,
                                        controls: [],
                                        //behaviors: ['ScrollZoom'],
                                    }}
                                    width={`100%`}
                                >
                                    <Placemark geometry={attraction.location.point} />
                                    <ZoomControl options={{float: 'right'}} />
                                    <TypeSelector options={{float: 'right'}} />
                                    <GeolocationControl options={{float: 'left'}} />
                                    <RouteButton options={{float: 'right'}} />
                                    <FullscreenControl />
                                </Map>
                            </div>
                        </YMaps>
                    </Col>
                </Row>
            </React.Fragment>
        );
    }
}

export default AttractionMap;
