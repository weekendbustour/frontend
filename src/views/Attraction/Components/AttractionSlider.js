import React, {Component} from 'react';
import FsLightbox from 'fslightbox-react';
import Flickity from 'react-flickity-component';
import {isMobile} from 'react-device-detect';
import {fsLightboxHelper} from '../../../lib/fsLightboxHelper';
import {attractionTypes} from "../../../prop/Types/Attraction";
const flickityOptions = {
    //asNavFor: '.carousel-main',
    contain: true,
    // pageDots: false,
    setGallerySize: false,
    fullscreen: true,
    lazyLoad: 2,
    groupCells: 3,
};
class AttractionSlider extends Component {
    static propTypes = {
        attraction: attractionTypes,
    };

    constructor(props) {
        super(props);
        this.state = {
            article: props.attraction,
            toggle: false,
            slide: 1,
            images: props.attraction.image.images || [],
        };
        this.setToggle = this.setToggle.bind(this);
        this.handleClearTitles = this.handleClearTitles.bind(this);
    }

    setToggle(state, key) {
        this.setState({
            toggle: state,
            slide: key + 1,
        });
    }

    handleClearTitles() {
        const {article} = this.state;
        fsLightboxHelper.clearTitles();
        fsLightboxHelper.addCaption(article.image.images_captions);
    }

    render() {
        const {toggle, images, slide} = this.state;
        return (
            <React.Fragment>
                {images.length !== 0 && (
                    <div>
                        <div className={`flight-box-custom ${isMobile ? 'is-mobile' : 'is-desktop'}`}>
                            <FsLightbox type="image" toggler={toggle} sources={images} slide={slide} thumbs={images} onOpen={this.handleClearTitles} />
                        </div>
                        <Flickity
                            className={'carousel'} // default ''
                            elementType={'div'} // default 'div'
                            options={flickityOptions} // takes flickity options {}
                            disableImagesLoaded={false} // default false
                            reloadOnUpdate // default false
                            static // default false
                        >
                            {images.map((image, key) => (
                                <img key={key} src={image} onClick={() => this.setToggle(!toggle, key)} alt="" />
                            ))}
                        </Flickity>
                    </div>
                )}
            </React.Fragment>
        );
    }
}

export default AttractionSlider;
