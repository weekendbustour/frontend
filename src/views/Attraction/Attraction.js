import React, {Component} from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Breadcrumb from '../../containers/Layout/Breadcrumb';
import 'react-dates/initialize';
import breadcrumbs from '../../breadcrumbs';
import SkeletonPage from '../../containers/Skeleton/Attraction/Page';
import {connect} from 'react-redux';
import * as PropTypes from 'prop-types';
import {getAttraction, getAttractionPending} from "../../store/reducers/Attraction";
import {fetchAttraction} from "../../store/actions/Attraction";
import {attractionTypes} from "../../prop/Types/Attraction";
import AttractionSlider from "./Components/AttractionSlider";
import AttractionMap from "./Components/AttractionMap";
import AttractionComments from "./Components/AttractionComments";
import Main from "./Components/Main";
import AttractionTextContainer from "./Components/AttractionTextContainer";
import AttractionArticles from "./Components/AttractionArticles";

class Attraction extends Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        attraction: attractionTypes,
        attractionLoading: PropTypes.bool,
        fetchAttraction: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.state = {};
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentDidMount() {
        const {fetchAttraction} = this.props;
        fetchAttraction(this.props.match.params.id);
        this._isMounted = true;
    }

    componentDidUpdate(prevProps) {
        const {attraction, attractionLoading, match, fetchAttraction} = this.props;
        const id = parseInt(match.params.id);
        if (attraction.id !== id && !attractionLoading) {
            fetchAttraction(id);
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {attraction, attractionLoading} = this.props;
        if (!this.shouldComponentRender()) return null;
        return (
            <React.Fragment>
                {attractionLoading && (
                    <section>
                        <SkeletonPage />
                    </section>
                )}
                {/* Хлебные крошки */}
                <section>
                    {!attractionLoading && (
                        <Container>
                            <Breadcrumb parent={breadcrumbs.attractions} push={{
                                title: `${attraction.title}`,
                            }} />
                        </Container>
                    )}
                </section>
                {/* Баннер */}
                <section>
                    {!attractionLoading && (
                        <Container className={`mb-2 mt-0 page__polo`}>
                            {attraction.with_banner &&
                            <Row>
                                <Col xs={12}>
                                    <Main attraction={attraction} />
                                </Col>
                            </Row>
                            }
                            <Row>
                                <Col xs={12} md={12} lg={{span: 8, offset: 2}} className={`mt-3`}>
                                    <AttractionTextContainer attraction={attraction} />
                                </Col>
                            </Row>
                        </Container>
                    )}
                </section>
                {/* Видео */}
                {/*<section>{!articleLoading && <ArticleVideo article={article} />}</section>*/}
                {/* Слайдер */}
                <section>{!attractionLoading && <AttractionSlider attraction={attraction} />}</section>
                {/* Карта */}
                <section>
                    {!attractionLoading && attraction.location !== null && (
                        <Container>
                            <AttractionMap attraction={attraction} />
                        </Container>
                    )}
                </section>
                {/* Статьи */}
                <section>
                    {!attractionLoading && attraction.articles.length !== 0 && (
                        <Container>
                            <AttractionArticles attraction={attraction} />
                        </Container>
                    )}
                </section>
                {/* Комментарии */}
                <section>
                    {!attractionLoading && (
                        <Container>
                            <Row className={`--container-attraction-comments`}>
                                <Col xs={12}>
                                    <AttractionComments attraction={attraction} />
                                </Col>
                            </Row>
                        </Container>
                    )}
                </section>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    attraction: getAttraction(state),
    attractionLoading: getAttractionPending(state),
});

const mapDispatchToProps = dispatch => ({
    fetchAttraction: id => dispatch(fetchAttraction(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Attraction);
