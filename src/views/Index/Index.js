import React from 'react';
import Banner from './../../containers/Layout/Banner';
import {connect} from 'react-redux';
import ToursContainer from './Container/Tours';
import OrganizationsContainer from './Container/Organizations';
import DirectionsContainer from './Container/Directions';
import AboutContainer from './Container/About';
import PromotionSecondContainer from './Container/PromotionSecond';
import NewsContainer from './Container/News';
import ArticlesContainer from './Container/Articles';
import FaqContainer from './Container/Faq';
import seo from '../../lib/seo';
import {getAppSeo} from '../../store/reducers/App/Seo';
import TourAppShare from "../Tour/Components/TourAppShare";

class Home extends React.Component {
    _isMounted = false;
    state = {};

    constructor(props) {
        super(props);
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentDidMount() {
        seo.index();
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <section>
                    <Banner />
                </section>
                <section>
                    <ToursContainer />
                </section>
                <section>
                    <PromotionSecondContainer />
                </section>
                <section>
                    <OrganizationsContainer />
                </section>
                <section>
                    <NewsContainer />
                </section>
                {/*/!*<section>*!/*/}
                {/*/!*    <CategoriesContainer/>*!/*/}
                {/*/!*</section>*!/*/}
                <section>
                    <DirectionsContainer />
                </section>
                <section>
                    <AboutContainer />
                </section>
                <section>
                    <ArticlesContainer />
                </section>
                <section>
                    <FaqContainer />
                </section>
                <section>
                    <TourAppShare />
                </section>
                {/*<section>*/}
                {/*    <SubscriptionContainer />*/}
                {/*</section>*/}
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    seo: getAppSeo(state),
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
