import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import {connect} from 'react-redux';
import {getDataNews, getDataNewsPending} from '../../../store/reducers/Data';
import NewsCard from '../../../containers/News/Card/Card';
import {Link} from 'react-router-dom';
import SkeletonNewsBanner from '../../../containers/Skeleton/Promotion/Default';
import * as PropTypes from 'prop-types';
import {newsTypes} from '../../../prop/Types/News';
import {TITLE_NEWS_OTHER} from "../../../lib/titles";
import TitleMain from "../../../containers/Layout/TitleMain";

class News extends React.Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        news: newsTypes,
        newsPending: PropTypes.bool,
    };

    constructor(props) {
        super(props);
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {news, newsPending} = this.props;
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                {newsPending && <SkeletonNewsBanner />}
                {!newsPending &&
                    news.length !== 0 &&
                    news.map(
                        (newsItem, key) =>
                            newsItem.position === 'banner' && (
                                <div className="grid--news-container is-news shadow-banner--" style={{backgroundImage: `url(${newsItem.image.banner})`}} key={key}>
                                    <Container>
                                        <div className={`banner`}>
                                            <div className="banner-text">
                                                <h3>{newsItem.title}</h3>
                                                <p>{newsItem.preview_description}</p>
                                                <Link className={`btn btn-primary`} to={`/news/${newsItem.id}/${newsItem.name}`}>
                                                    Читать полностью
                                                </Link>
                                            </div>
                                        </div>
                                    </Container>
                                </div>
                            )
                    )}
                {!newsPending && news.length !== 0 && news.length > 1 && (
                    <Container>
                        <Row className={`mb-5 mt-5`}>
                            <TitleMain title={`ДРУГИЕ НОВОСТИ`} model={TITLE_NEWS_OTHER}/>
                        </Row>
                        <Row>
                            {news.map(
                                (newsItem, key) =>
                                    newsItem.position !== 'banner' && (
                                        <Col xs={`12`} sm={`12`} md={`6`} lg={`4`} className={`mb-3`} key={key}>
                                            <NewsCard news={newsItem} />
                                        </Col>
                                    )
                            )}
                        </Row>
                        {news.length === 4 && (
                            <Row className={`mt-3`}>
                                <Col xs={12} className={`text-right`}>
                                    <Link to={`/news`} className={`btn`} variant="light">
                                        Все новости
                                    </Link>
                                </Col>
                            </Row>
                        )}
                    </Container>
                )}
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    news: getDataNews(state),
    newsPending: getDataNewsPending(state),
});
export default connect(mapStateToProps)(News);
