import React from 'react';
import {connect} from 'react-redux';
import SkeletonPromotionDefault from '../../../containers/Skeleton/Promotion/Default';
import {getDataPromotionsPending, getDataPromotions} from '../../../store/reducers/Data';
import * as PropTypes from 'prop-types';
import {promotionsTargetDefaultTypes} from '../../../prop/Types/Promotion';
import Banner from '../../../containers/Promotion/Banner';

class Promotion extends React.Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        promotions: promotionsTargetDefaultTypes,
        pending: PropTypes.bool,
    };

    constructor(props) {
        super(props);
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {promotions, pending} = this.props;
        if (!this.shouldComponentRender()) return '';
        const promotionDefault = promotions !== null && promotions.default !== undefined ? promotions.default : null;
        const promotionTarget = promotions !== null && promotions.target !== undefined ? promotions.target : null;
        return (
            <React.Fragment>
                {pending && <SkeletonPromotionDefault />}
                {!pending && promotions !== null && promotionTarget !== null && <Banner promotion={promotionTarget} />}
                {!pending && promotions !== null && promotionTarget === null && promotionDefault !== null && <Banner promotion={promotionDefault} />}
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    promotions: getDataPromotions(state),
    pending: getDataPromotionsPending(state),
});
export default connect(mapStateToProps)(Promotion);
