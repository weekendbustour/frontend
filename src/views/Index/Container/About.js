import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Title from '../../../containers/Layout/Title';
import {connect} from 'react-redux';
import {getDataStatistic, getDataStatisticPending, getDataTitles} from '../../../store/reducers/Data';
import {helpers} from '../../../lib/helpers/helpers';
import {statisticDataTypes} from '../../../prop/Types/Statistic';
import * as PropTypes from 'prop-types';
import {titlesTypes} from "../../../prop/Types/Title";
import ReactMarkdown from "../../../plugins/ReactMarkdown";
import Skeleton from "react-loading-skeleton";

class About extends React.Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        titles: titlesTypes,
        statistic: statisticDataTypes,
        statisticPending: PropTypes.bool,
    };

    constructor(props) {
        super(props);
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {statisticPending, statistic, titles} = this.props;
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <div className={`bg-light mb-5 mt-5`}>
                    <Container>
                        <Title title={process.env.REACT_APP_NAME} description={process.env.REACT_APP_NAME_DESCRIPTION} />
                    </Container>
                    <Container>
                        <Row>
                            <Col xs={12} md={12} lg={{span: 6, offset: 3}} className={`text-center mt-3 mb-3`}>
                                {titles.about !== undefined ? (
                                    <div>
                                        <ReactMarkdown source={titles.about.description}/>
                                    </div>
                                ) : (
                                    <div>
                                        <Skeleton height={`21px`} width={`100%`} count={2}/>
                                    </div>
                                )}
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={12} md={12} lg={{span: 6, offset: 3}}>
                                <div className={`d-flex justify-content-center align-items-center`}>
                                    <div
                                        className={`m-1 shadow-sm p-3 mb-5 bg-white rounded d-flex align-items-stretch justify-content-center flex-column text-center`}
                                        style={{flex: '1 1 100px', height: '120px'}}
                                    >
                                        <span className={`text-primary`} style={{fontSize: '2rem'}}>
                                            <i className="fas fa-bus" />
                                        </span>
                                        {!statisticPending && (
                                            <div>
                                                <span>
                                                    <h4 className={`mb-0`}>{statistic.tours.count}</h4>
                                                </span>
                                                <span style={{lineHeight: 1}}>
                                                    {helpers.units(statistic.tours.count, ['Активный тур', 'Активных тура', 'Активных туров'])}
                                                </span>
                                            </div>
                                        )}
                                    </div>
                                    <div
                                        className={`m-1 shadow-sm p-3 mb-5 bg-white rounded d-flex align-items-stretch justify-content-center flex-column text-center`}
                                        style={{flex: '1 1 100px', height: '120px'}}
                                    >
                                        <span className={`text-primary`} style={{fontSize: '2rem'}}>
                                            <i className="fas fa-map-signs" />
                                        </span>
                                        {!statisticPending && (
                                            <div>
                                                <span>
                                                    <h4 className={`mb-0`}>{statistic.directions.count}</h4>
                                                </span>
                                                <span style={{lineHeight: 1}}>
                                                    {helpers.units(statistic.directions.count, ['Направление', 'Направления', 'Направлений'])}
                                                </span>
                                            </div>
                                        )}
                                    </div>
                                    <div
                                        className={`m-1 shadow-sm p-3 mb-5 bg-white rounded d-flex align-items-stretch justify-content-center flex-column text-center`}
                                        style={{flex: '1 1 100px', height: '120px'}}
                                    >
                                        <span className={`text-primary`} style={{fontSize: '2rem'}}>
                                            <i className="fas fa-route" />
                                        </span>
                                        {!statisticPending && (
                                            <div>
                                                <span>
                                                    <h4 className={`mb-0`}>{statistic.organizations.count}</h4>
                                                </span>
                                                <span style={{lineHeight: 1}}>
                                                    {helpers.units(statistic.organizations.count, ['Организатор', 'Организатора', 'Организаторов'])}
                                                </span>
                                            </div>
                                        )}
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    titles: getDataTitles(state),
    statistic: getDataStatistic(state),
    statisticPending: getDataStatisticPending(state),
});
export default connect(mapStateToProps)(About);
