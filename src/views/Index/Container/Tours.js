import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import {Link} from 'react-router-dom';
import {getDataTitles, getDataTitlesPending, getDataTours, getDataToursPending} from '../../../store/reducers/Data';
import TourCard from '../../../containers/Tour/Card/Card';
import SkeletonTour from '../../../containers/Skeleton/Tour/Card';
import {connect} from 'react-redux';
import {getUser} from "../../../store/reducers/User";
import * as PropTypes from "prop-types";
import {toursTypes} from "../../../prop/Types/Tour";
import TitleMain from "../../../containers/Layout/TitleMain";
import {TITLE_TOURS_NEAR} from "../../../lib/titles";

class Tours extends React.Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        tours: toursTypes,
        toursPending: PropTypes.bool,
        title: PropTypes.string,
    };

    constructor(props) {
        super(props);
        this.state = {
            title: props.title || `БЛИЖАЙЩИЕ ТУРЫ`,
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {title} = this.state;
        const {tours, toursPending} = this.props;
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <Container className={`mb-5 mt-5`}>
                    <TitleMain title={title} model={TITLE_TOURS_NEAR}/>
                </Container>
                {toursPending && (
                    <Container>
                        <Row className={`awesome-card cards`}>
                            <Col xs={`12`} sm={`12`} md={`6`} lg={`4`} className={`mb-3`}>
                                <SkeletonTour />
                            </Col>
                            <Col xs={`12`} sm={`12`} md={`6`} lg={`4`} className={`mb-3`}>
                                <SkeletonTour />
                            </Col>
                            <Col xs={`12`} sm={`12`} md={`6`} lg={`4`} className={`mb-3`}>
                                <SkeletonTour />
                            </Col>
                            <Col xs={`12`} sm={`12`} md={`6`} lg={`4`} className={`mb-3`}>
                                <SkeletonTour />
                            </Col>
                            <Col xs={`12`} sm={`12`} md={`6`} lg={`4`} className={`mb-3`}>
                                <SkeletonTour />
                            </Col>
                            <Col xs={`12`} sm={`12`} md={`6`} lg={`4`} className={`mb-3`}>
                                <SkeletonTour />
                            </Col>
                        </Row>
                    </Container>
                )}
                {!toursPending && (
                    <Container>
                        <Row className={`awesome-card cards`}>
                            {tours.length !== 0 &&
                                tours.map((tour, key) => (
                                    <Col xs={`12`} sm={`12`} md={`6`} lg={`4`} className={`mb-3`} key={key}>
                                        <TourCard tour={tour}/>
                                    </Col>
                                ))}
                        </Row>
                        {tours.length === 6 && (
                            <Row className={`mb-5`}>
                                <Col xs={12} className={`text-right`}>
                                    <Link to={`/tours`} className={`btn`} variant="light">
                                        Все туры
                                    </Link>
                                </Col>
                            </Row>
                        )}
                    </Container>
                )}
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: getUser(state),
    tours: getDataTours(state),
    toursPending: getDataToursPending(state),
    titles: getDataTitles(state),
    titlesPending: getDataTitlesPending(state),
});
export default connect(mapStateToProps)(Tours);
