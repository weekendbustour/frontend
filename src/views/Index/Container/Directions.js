import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import {Link} from 'react-router-dom';
import {
    getDataDirections,
    getDataDirectionsPending,
} from '../../../store/reducers/Data';
import {connect} from 'react-redux';
import SkeletonDirection from '../../../containers/Skeleton/Direction/Card';
import DirectionCard from '../../../containers/Direction/Card/Card';
import {directionsTypes} from '../../../prop/Types/Direction';
import * as PropTypes from 'prop-types';
import {TITLE_DIRECTIONS} from "../../../lib/titles";
import TitleMain from "../../../containers/Layout/TitleMain";

class Directions extends React.Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        directions: directionsTypes,
        directionsPending: PropTypes.bool,
    };

    constructor(props) {
        super(props);
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {directions, directionsPending} = this.props;
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <Container className={`mb-5 mt-5`}>
                    <TitleMain title={`НАПРАВЛЕНИЯ`} model={TITLE_DIRECTIONS}/>
                </Container>
                {directionsPending && (
                    <Container>
                        <Row>
                            <Col xs={`12`} sm={`12`} md={`6`} lg={`6`} className={`mb-3`}>
                                <SkeletonDirection />
                            </Col>
                            <Col xs={`12`} sm={`12`} md={`6`} lg={`6`} className={`mb-3`}>
                                <SkeletonDirection />
                            </Col>
                            <Col xs={`12`} sm={`12`} md={`6`} lg={`6`} className={`mb-3`}>
                                <SkeletonDirection />
                            </Col>
                            <Col xs={`12`} sm={`12`} md={`6`} lg={`6`} className={`mb-3`}>
                                <SkeletonDirection />
                            </Col>
                        </Row>
                    </Container>
                )}
                {!directionsPending && (
                    <Container>
                        <Row>
                            {directions.length !== 0 &&
                                directions.map((direction, key) => (
                                    <Col xs={`12`} sm={`12`} md={`6`} lg={`6`} className={`mb-3`} key={key}>
                                        <DirectionCard direction={direction} />
                                    </Col>
                                ))}
                        </Row>
                        {directions.length === 8 && (
                            <Row className={`mt-3`}>
                                <Col xs={12} className={`text-right`}>
                                    <Link to={`/directions`} className={`btn`} variant="light">
                                        Все направления
                                    </Link>
                                </Col>
                            </Row>
                        )}
                    </Container>
                )}
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    directions: getDataDirections(state),
    directionsPending: getDataDirectionsPending(state),
});
export default connect(mapStateToProps)(Directions);
