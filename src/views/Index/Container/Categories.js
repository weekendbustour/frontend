import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Badge from 'react-bootstrap/Badge';
import Title from '../../../containers/Layout/Title';
import {connect} from 'react-redux';

class Categories extends React.Component {
    _isMounted = false;
    state = {};

    constructor(props) {
        super(props);
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <Container className={`mb-5 mt-5`}>
                    <Title title={`Теги`} description={`Wow, what a fine cofee flavor`} />
                </Container>
                <Container>
                    <Row>
                        <Col xs={12} className={`mb-2`}>
                            <div className={`text-center`}>
                                <Badge variant="default" className={`lg mr-1`}>
                                    <a href="/#">Экстрим</a>
                                </Badge>
                                <Badge variant="default" className={`lg mr-1`}>
                                    <a href="/">Экскурсии</a>
                                </Badge>
                                <Badge variant="default" className={`lg mr-1`}>
                                    <a href="/#">Походы</a>
                                </Badge>
                                <Badge variant="default" className={`lg mr-1`}>
                                    <a href="/#">Однодневные туры</a>
                                </Badge>
                                <Badge variant="default" className={`lg mr-1`}>
                                    <a href="/#">Новогодние</a>
                                </Badge>
                                <Badge variant="default" className={`lg mr-1`}>
                                    <a href="/#">Горнолыжные</a>
                                </Badge>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({});
export default connect(mapStateToProps)(Categories);
