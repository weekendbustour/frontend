import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import {connect} from 'react-redux';
import Subscribe from '../../../containers/Layout/Subscribe';
import {getUser} from '../../../store/reducers/User';
import {userTypes} from '../../../prop/Types/User';

class Subscription extends React.Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        user: userTypes,
    };

    constructor(props) {
        super(props);
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {user} = this.props;
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                {user === null || !user.subscriber ? (
                    <Container className={`mb-5 mt-5`}>
                        <Row>
                            <Col xs={12} md={12} lg={{span: 6, offset: 3}} className={`mb-2`}>
                                <div className={`d-flex justify-content-center`}>
                                    <Subscribe />
                                </div>
                            </Col>
                        </Row>
                    </Container>
                ) : (
                    ''
                )}
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: getUser(state),
});
export default connect(mapStateToProps)(Subscription);
