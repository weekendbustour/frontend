import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import Accordion from 'react-bootstrap/Accordion';
import Title from '../../../containers/Layout/Title';
import {connect} from 'react-redux';
import {getDataQuestions, getDataQuestionsPending} from "../../../store/reducers/Data";
import * as PropTypes from "prop-types";
import ReactMarkdown from "../../../plugins/ReactMarkdown";
import SkeletonQuestion from "../../../containers/Skeleton/Question/Item";

class Faq extends React.Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        questions: PropTypes.array,
        questionsPending: PropTypes.bool,
    };

    constructor(props) {
        super(props);
        this.state = {
            open: 0,
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.setOpen = this.setOpen.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    setOpen(key) {
        const {open} = this.state;
        this.setState({
            open: open !== key ? key : 0,
        });
    }

    render() {
        const {open} = this.state;
        const {questions, questionsPending} = this.props;
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <div className={`bg-light mb-5 mt-5`}>
                    <Container>
                        <Title title={`FAQ`} description={`Часто задаваемые вопросы`} />
                    </Container>
                    <Container>
                        {questionsPending && <SkeletonQuestion />}
                        {!questionsPending &&
                        <Row>
                            <Col xs={12} md={{span: 8, offset: 2}} className={`mt-4`}>
                                <Accordion defaultActiveKey={open} className={`is-faq`}>
                                    {questions.map((question, key) => (
                                        <Card key={key}>
                                            <Card.Header className={`bg-white shadow-sm border-0 ${open === question.id ? 'is-open' : ''}`} style={{position: 'relative'}}>
                                                <Accordion.Toggle as={Button} variant="link" eventKey={question.id} className={``} onClick={() => this.setOpen(question.id)}>
                                                <span>
                                                    {question.question}
                                                </span>
                                                    <span className={`collapse-icon`}>
                                                    <i className="fas fa-times"/>
                                                </span>
                                                </Accordion.Toggle>
                                            </Card.Header>
                                            <Accordion.Collapse eventKey={question.id}>
                                                <Card.Body>
                                                    <ReactMarkdown source={question.answer} />
                                                </Card.Body>
                                            </Accordion.Collapse>
                                        </Card>
                                    ))}
                                </Accordion>
                            </Col>
                        </Row>
                        }
                    </Container>
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    questions: getDataQuestions(state),
    questionsPending: getDataQuestionsPending(state),
});
export default connect(mapStateToProps)(Faq);
