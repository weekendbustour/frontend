import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import {Link} from 'react-router-dom';
import {
    getDataOrganizations,
    getDataOrganizationsPending,
} from '../../../store/reducers/Data';
import {connect} from 'react-redux';
import SkeletonOrganization from '../../../containers/Skeleton/Organization/Card';
import OrganizationCard from '../../../containers/Organization/Card/Card';
import {organizationsTypes} from '../../../prop/Types/Organization';
import * as PropTypes from 'prop-types';
import {TITLE_ORGANIZATIONS} from "../../../lib/titles";
import TitleMain from "../../../containers/Layout/TitleMain";

class Organizations extends React.Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        organizations: organizationsTypes,
        organizationsPending: PropTypes.bool,
    };

    constructor(props) {
        super(props);
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {organizations, organizationsPending} = this.props;
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <Container className={`mb-5 mt-5`}>
                    <TitleMain title={`ОРГАНИЗАТОРЫ`} model={TITLE_ORGANIZATIONS}/>
                </Container>
                {organizationsPending && (
                    <Container>
                        <Row>
                            <Col xs={`12`} sm={`12`} md={`6`} lg={`6`} className={`mb-3`}>
                                <SkeletonOrganization />
                            </Col>
                            <Col xs={`12`} sm={`12`} md={`6`} lg={`6`} className={`mb-3`}>
                                <SkeletonOrganization />
                            </Col>
                            <Col xs={`12`} sm={`12`} md={`6`} lg={`6`} className={`mb-3`}>
                                <SkeletonOrganization />
                            </Col>
                            <Col xs={`12`} sm={`12`} md={`6`} lg={`6`} className={`mb-3`}>
                                <SkeletonOrganization />
                            </Col>
                        </Row>
                    </Container>
                )}
                {!organizationsPending && (
                    <Container>
                        <Row>
                            {organizations.length !== 0 &&
                                organizations.map((organization, key) => (
                                    <Col xs={`12`} sm={`12`} md={`6`} lg={`6`} className={`mb-3`} key={key}>
                                        <OrganizationCard organization={organization} />
                                    </Col>
                                ))}
                        </Row>
                        {organizations.length === 6 && (
                            <Row className={`mt-3`}>
                                <Col xs={12} className={`text-right`}>
                                    <Link to={`/organizations`} className={`btn`} variant="light">
                                        Все организаторы
                                    </Link>
                                </Col>
                            </Row>
                        )}
                    </Container>
                )}
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    organizations: getDataOrganizations(state),
    organizationsPending: getDataOrganizationsPending(state),
});
export default connect(mapStateToProps)(Organizations);
