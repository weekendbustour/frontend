import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import {connect} from 'react-redux';
import {
    getDataArticles,
    getDataArticlesPending,
} from '../../../store/reducers/Data';
import ArticleCard from '../../../containers/Article/Card/Card';
import {Link} from 'react-router-dom';
import SkeletonArticleBanner from '../../../containers/Skeleton/Promotion/Default';
import * as PropTypes from 'prop-types';
import {articlesTypes} from "../../../prop/Types/Article";
import {TITLE_ARTICLES_OTHER} from "../../../lib/titles";
import TitleMain from "../../../containers/Layout/TitleMain";

class Articles extends React.Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        articles: articlesTypes,
        articlesPending: PropTypes.bool,
    };

    constructor(props) {
        super(props);
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {articles, articlesPending} = this.props;
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                {articlesPending && <SkeletonArticleBanner />}
                {!articlesPending &&
                articles.length !== 0 &&
                articles.map(
                        (article, key) =>
                            article.position === 'banner' && (
                                <div className="grid--news-container shadow-banner--" style={{backgroundImage: `url(${article.image.banner})`}} key={key}>
                                    <Container>
                                        <div className={`banner`}>
                                            <div className="banner-text">
                                                <h3>{article.title}</h3>
                                                <p>{article.preview_description}</p>
                                                <Link className={`btn btn-primary`} to={`/article/${article.id}/${article.name}`}>
                                                    Читать полностью
                                                </Link>
                                            </div>
                                        </div>
                                    </Container>
                                </div>
                            )
                    )}
                {!articlesPending && articles.length !== 0 && articles.length > 1 && (
                    <Container>
                        <Row className={`mb-5 mt-5`}>
                            <TitleMain title={`ДРУГИЕ СТАТЬИ`} model={TITLE_ARTICLES_OTHER}/>
                        </Row>
                        <Row>
                            {articles.map(
                                (article, key) =>
                                    article.position !== 'banner' && (
                                        <Col xs={`12`} sm={`12`} md={`6`} lg={`4`} className={`mb-3`} key={key}>
                                            <ArticleCard article={article} />
                                        </Col>
                                    )
                            )}
                        </Row>
                        {articles.length === 4 && (
                            <Row className={`mt-3`}>
                                <Col xs={12} className={`text-right`}>
                                    <Link to={`/articles`} className={`btn`} variant="light">
                                        Все статьи
                                    </Link>
                                </Col>
                            </Row>
                        )}
                    </Container>
                )}
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    articles: getDataArticles(state),
    articlesPending: getDataArticlesPending(state),
});
export default connect(mapStateToProps)(Articles);
