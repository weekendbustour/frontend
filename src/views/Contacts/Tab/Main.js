import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import {connect} from 'react-redux';

class Main extends React.Component {
    _isMounted = false;
    state = {};

    static propTypes = {};

    constructor(props) {
        super(props);
        this.state = {};
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <h3>Основные контактые</h3>
                <Row>
                    <Col xs={12} md={12}>
                        <p className={`text-muted`}>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt dicta distinctio enim eos, fuga harum labore libero maiores
                            maxime molestiae nemo, neque nihil rerum, saepe sapiente sequi totam voluptas voluptate?
                        </p>
                    </Col>
                </Row>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Main);
