import React, {Component} from 'react';
import Container from 'react-bootstrap/Container';
import Breadcrumb from '../../containers/Layout/Breadcrumb';
import breadcrumbs from '../../breadcrumbs';
import {connect} from 'react-redux';
import seo from '../../lib/seo';
import Tab from 'react-bootstrap/Tab';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Nav from 'react-bootstrap/Nav';
import TabMain from './Tab/Main';

class Contacts extends Component {
    _isMounted = false;
    state = {};

    static propTypes = {};

    constructor(props) {
        super(props);
        this.state = {
            tab: 'main',
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.onSelect = this.onSelect.bind(this);
    }

    onSelect(name) {
        const {history} = this.props;
        let urlData = new URLSearchParams({
            tab: name,
        });
        history.push({
            pathname: '/contacts',
            search: '?' + urlData.toString(),
        });
    }

    componentWillMount() {
        const {location} = this.props;
        this._isMounted = true;
        let searchUrl = location.search;
        let urlData = new URLSearchParams(searchUrl);
        if (urlData.has('tab')) {
            this.setState({tab: urlData.get('tab')});
        }
    }

    componentDidMount(): void {
        seo.contacts();
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {tab} = this.state;
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <section>
                    <Container>
                        <Breadcrumb push={breadcrumbs.contacts} />
                    </Container>
                </section>
                <section>
                    <Container className={`mb-5`} style={{minHeight: '500px'}}>
                        <Tab.Container defaultActiveKey={tab}>
                            <Row>
                                <Col xs={12} md={3} className={`aw-tabs-nav-pills`}>
                                    <Nav variant="pills" className="flex-column mb-3" title={``}>
                                        <Nav.Link eventKey="main" className={`nav-item`} onSelect={() => this.onSelect(`main`)}>
                                            Основные контакты
                                        </Nav.Link>
                                    </Nav>
                                </Col>
                                <Col xs={12} md={9}>
                                    <Tab.Content>
                                        <Tab.Pane eventKey="main">
                                            <TabMain />
                                        </Tab.Pane>
                                    </Tab.Content>
                                </Col>
                            </Row>
                        </Tab.Container>
                    </Container>
                </section>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({});

export default connect(mapStateToProps)(Contacts);
