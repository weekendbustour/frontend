import React, {Component} from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Breadcrumb from '../../containers/Layout/Breadcrumb';
import 'react-dates/initialize';
import breadcrumbs from '../../breadcrumbs';
import SkeletonPage from '../../containers/Skeleton/Category/Page';
import {connect} from 'react-redux';
import * as PropTypes from 'prop-types';
import {categoryTypes} from "../../prop/Types/Category";
import {getCategory, getCategoryPending} from "../../store/reducers/Category";
import {fetchCategory} from "../../store/actions/Category";
import ArticleCard from "../../containers/Article/Card/Card";

class Category extends Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        category: categoryTypes,
        categoryLoading: PropTypes.bool,
        fetchCategory: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.state = {};
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentDidMount() {
        const {fetchCategory} = this.props;
        fetchCategory(this.props.match.params.id);
        this._isMounted = true;
    }

    componentDidUpdate(prevProps) {
        const {category, categoryLoading, match, fetchCategory} = this.props;
        const id = parseInt(match.params.id);
        if (category.id !== id && !categoryLoading) {
            fetchCategory(id);
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {category, categoryLoading} = this.props;
        if (!this.shouldComponentRender()) return null;
        return (
            <React.Fragment>
                {categoryLoading && (
                    <section>
                        <SkeletonPage />
                    </section>
                )}
                {/* Хлебные крошки */}
                <section>
                    {!categoryLoading && (
                        <Container>
                            <Breadcrumb parent={breadcrumbs.articleCategories} push={{title: category.title}} />
                        </Container>
                    )}
                </section>
                {/* Баннер */}
                {!categoryLoading && (
                    <div className="grid--news-container shadow-banner-- mt-0 mb-0" style={{backgroundImage: `url(${category.image.banner})`}}>
                        <Container>
                            <div className={`banner`}>
                                <div className="banner-text">
                                    <h3>{category.title}</h3>
                                    <p>{category.description}</p>
                                </div>
                            </div>
                        </Container>
                    </div>
                )}
                <section>
                    {!categoryLoading && category.articles.length !== 0 && (
                        <Container className={`mt-3`}>
                            <Row>
                                {category.articles.map((item, key) => (
                                    <Col xs={`12`} sm={`12`} md={`6`} lg={`${[1, 5, 11, 15].indexOf(key) !== -1 ? '8' : '4'}`} className={`mb-3`} key={key}>
                                        <ArticleCard article={item} type={[1, 5, 11, 15].indexOf(key) !== -1 ? 'long' : 'short'} />
                                    </Col>
                                ))}
                            </Row>
                        </Container>
                    )}
                </section>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    category: getCategory(state),
    categoryLoading: getCategoryPending(state),
});

const mapDispatchToProps = dispatch => ({
    fetchCategory: id => dispatch(fetchCategory(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Category);
