import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Redirect, Route} from 'react-router';
import routes from '../../routes';

class Name extends Component {
    _isMounted = false;
    state = {};

    constructor(props) {
        super(props);
        this.state = {
            organization: window.page !== undefined && window.page.organization !== undefined ? window.page.organization : null,
            direction: window.page !== undefined && window.page.direction !== undefined ? window.page.direction : null,
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentDidMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {organization, direction} = this.state;
        let route = null;
        if (organization !== null) {
            route = routes.filter(route => route.name === 'Organization')[0];
        }
        if (direction !== null) {
            route = routes.filter(route => route.name === 'Direction')[0];
        }
        return (
            <React.Fragment>
                {route !== null && organization !== null && (
                    <Route path={`*`} exact={route.exact} name={route.name} render={props => <route.component {...props} />} />
                )}
                {route !== null && direction !== null && (
                    <Route path={`*`} exact={route.exact} name={route.name} render={props => <route.component {...props} />} />
                )}
                {route === null && <Redirect from="/" to="/" />}
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({});
export default connect(mapStateToProps, mapDispatchToProps)(Name);
