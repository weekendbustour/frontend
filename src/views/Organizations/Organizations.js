import React, {Component} from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import OrganizationCard from '../../containers/Organization/Card/Card';
import Breadcrumb from '../../containers/Layout/Breadcrumb';
import breadcrumbs from '../../breadcrumbs';
import {getOrganizations, getOrganizationsPending} from '../../store/reducers/Organizations';
import {fetchOrganizations} from '../../store/actions/Organizations';
import SkeletonOrganization from '../../containers/Skeleton/Organization/Card';
import seo from '../../lib/seo';
import {connect} from 'react-redux';
import {organizationsTypes} from "../../prop/Types/Organization";
import * as PropTypes from 'prop-types';
import {TITLE_ORGANIZATIONS} from "../../lib/titles";
import TitleMain from "../../containers/Layout/TitleMain";

class Organizations extends Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        organizations: organizationsTypes,
        pending: PropTypes.bool,
        fetchOrganizations: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentDidMount() {
        const {fetchOrganizations} = this.props;
        seo.organizations();
        fetchOrganizations();
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {organizations, pending} = this.props;
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <section>
                    <Container>
                        <Breadcrumb push={breadcrumbs.organizations} />
                    </Container>
                </section>
                <section>
                    <Container className={`mb-5`}>
                        <TitleMain title={`Организаторы`} model={TITLE_ORGANIZATIONS}/>
                    </Container>
                </section>
                {pending && (
                    <section>
                        <Container>
                            <Row>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`6`} className={`mb-3`}>
                                    <SkeletonOrganization />
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`6`} className={`mb-3`}>
                                    <SkeletonOrganization />
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`6`} className={`mb-3`}>
                                    <SkeletonOrganization />
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`6`} className={`mb-3`}>
                                    <SkeletonOrganization />
                                </Col>
                            </Row>
                        </Container>
                    </section>
                )}
                {!pending && (
                    <section>
                        <Container>
                            <Row>
                                {organizations.length !== 0 &&
                                    organizations.map((organization, key) => (
                                        <Col xs={`12`} sm={`12`} md={`6`} lg={`6`} className={`mb-3`} key={key}>
                                            <OrganizationCard organization={organization} />
                                        </Col>
                                    ))}
                            </Row>
                        </Container>
                    </section>
                )}
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    organizations: getOrganizations(state),
    pending: getOrganizationsPending(state),
});

const mapDispatchToProps = dispatch => ({
    fetchOrganizations: () => dispatch(fetchOrganizations()),
});
export default connect(mapStateToProps, mapDispatchToProps)(Organizations);
