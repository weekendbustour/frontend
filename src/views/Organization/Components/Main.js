import React, {Component} from 'react';
import Badge from 'react-bootstrap/Badge';
import 'react-dates/initialize';
import {connect} from 'react-redux';
import {isMobile} from 'react-device-detect';
import {fsLightboxHelper} from '../../../lib/fsLightboxHelper';

import OrganizationDepartures from "../../../containers/Organization/Components/Departures";

import OrganizationMedia from "../../../containers/Organization/Components/Social/Media";
import OrganizationSocial from "../../../containers/Organization/Components/Social/Social";
import Rating from "../../../components/Rating/RatingDefault";
import {organizationTypes} from "../../../prop/Types/Organization";
import FsLightbox from "fslightbox-react";

class Main extends Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        organization: organizationTypes,
    };

    constructor(props) {
        super(props);
        this.state = {
            toggleBanner: false,
            organization: props.organization,
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.setToggleImageBanner = this.setToggleImageBanner.bind(this);
        this.handleClearTitles = this.handleClearTitles.bind(this);
    }

    setToggleImageBanner() {
        const {toggleBanner} = this.state;
        this.setState({toggleBanner: !toggleBanner});
    }
    handleClearTitles() {
        const {organization} = this.state;
        fsLightboxHelper.clearTitles();
        fsLightboxHelper.addCaption([organization.image.banner_caption]);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {organization, toggleBanner} = this.state;
        if (!this.shouldComponentRender()) return null;
        return (
            <React.Fragment>
                <div className="page__movie--img">
                    <div className={`page__movie--background`} style={{backgroundImage: 'url(' + organization.image.banner + ')'}} onClick={this.setToggleImageBanner}>
                        <div className="blur"/>
                    </div>
                    <div className={`page__movie--background--size-vk-container`}>
                        <div className={`page__movie--background--size-vk`} style={{backgroundImage: 'url(' + organization.image.banner + ')'}} onClick={this.setToggleImageBanner}/>
                    </div>
                    {/*<div className="card--social">*/}
                    {/*    <div className={`card--socials`}>*/}
                    {/*        <OrganizationMedia organization={organization} variant={`light`} role={`page`}/>*/}
                    {/*    </div>*/}
                    {/*</div>*/}
                    <div className={`page__movie--logo __centered __100px`}>
                        <div className={`__logo`}>
                            <img src={organization.image.logo} alt="" />
                        </div>
                        {/*<img src={organization.image.default} alt={organization.title} title={`Изображение`} />*/}
                    </div>
                    <div className={`page__movie--socials __centered __100px`}>
                        <OrganizationMedia organization={organization} variant={`light-dark`} role={`page`} size={isMobile ? `md` : `md`} />
                    </div>
                </div>
                <div className={`mt-5 pt-3`}>
                    <h2 className={`text-center`}>{organization.title}</h2>
                    {organization.departures.length !== 0 && (
                        <div className={`text-center text-muted`}>
                            <OrganizationDepartures organization={organization} />
                        </div>
                    )}
                    {organization.preview_description !== null && (
                        <p className="text-center delta cardo regular italic mb-1">{organization.preview_description}</p>
                    )}
                    <div className={`d-flex justify-content-center mb-1`}>
                        <OrganizationSocial organization={organization} variant={`light-dark`} size={`lg`} />
                    </div>
                    <div className={`text-center`}>
                        <Rating rating={organization.rating} readonly={true} />
                    </div>
                    <div className={`text-center`}>
                        {organization.categories.length !== 0 &&
                        organization.categories.map((category, key) => (
                            <Badge
                                variant="default"
                                className={[`mr-1`, category.color ? `colored` : ``]}
                                key={key}
                                style={{borderColor: category.color, color: category.color}}
                            >
                                {category.title}
                            </Badge>
                        ))}
                    </div>
                </div>
                <div className={`flight-box-custom ${isMobile ? 'is-mobile' : 'is-desktop'}`}>
                    <FsLightbox
                        type="image"
                        toggler={toggleBanner}
                        sources={[organization.image.banner]}
                        slide={0}
                        thumbs={[organization.image.banner]}
                        onOpen={this.handleClearTitles}
                    />
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Main);
