import React, {Component} from 'react';
import {organizationTypes} from '../../../prop/Types/Organization';
import {connect} from 'react-redux';
import * as PropTypes from 'prop-types';
import {getUserSubscriptions, getUserSubscriptionsPendingArray} from '../../../store/reducers/User/Subscription';
import {fetchUserSubscribeModel} from '../../../store/actions/User/Subscription';
import {userSubscriptionsTypes} from '../../../prop/Types/User';

class OrganizationSubscription extends Component {
    static propTypes = {
        organization: organizationTypes,
        subscriptions: userSubscriptionsTypes,
        subscriptionsPending: PropTypes.array,
        fetchUserSubscribeModel: PropTypes.func,
    };
    constructor(props) {
        super(props);
        this.state = {
            organization: props.organization,
        };
    }

    setSubscription(item) {
        const {fetchUserSubscribeModel} = this.props;
        fetchUserSubscribeModel('organization', item);
        this.forceUpdate();
    }

    render() {
        const {organization} = this.state;
        const {subscriptions, subscriptionsPending} = this.props;
        const subscribed = subscriptions !== null ? subscriptions.organizations.indexOf(organization.id) !== -1 : false;
        return (
            <React.Fragment>
                <div className={`direction--date-placeholder`}>
                    <div className={`text-center pt-5 pb-5`}>
                        <h5>Пока от этого Организатора нет активных туров</h5>
                        {subscribed ? (
                            <div>
                                <p>Вы подписаны на новости, мы вас оповестим, когда туры появятся</p>
                                <button
                                    type="button"
                                    className={`btn btn-primary ${subscriptionsPending.indexOf('organization-' + organization.id) !== -1 ? 'is-loading' : ''}`}
                                    onClick={() => this.setSubscription(organization)}
                                >
                                    Отписаться
                                </button>
                            </div>
                        ) : (
                            <div>
                                <p>Подпишись на новости и будешь в курсе, когда туры появятся</p>
                                <div>
                                    <button
                                        type="button"
                                        className={`btn btn-primary ${subscriptionsPending.indexOf('organization-' + organization.id) !== -1 ? 'is-loading' : ''}`}
                                        onClick={() => this.setSubscription(organization)}
                                    >
                                        Подписаться
                                    </button>
                                </div>
                            </div>
                        )}
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    subscriptions: getUserSubscriptions(state),
    subscriptionsPending: getUserSubscriptionsPendingArray(state),
});
const mapDispatchToProps = dispatch => ({
    fetchUserSubscribeModel: (model, data) => dispatch(fetchUserSubscribeModel(model, data)),
});
export default connect(mapStateToProps, mapDispatchToProps)(OrganizationSubscription);
