import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import {connect} from 'react-redux';
import ArticleCard from '../../../containers/Article/Card/Card';
import {Link} from 'react-router-dom';
import {TITLE_ARTICLES_ORGANIZATION} from '../../../lib/titles';
import TitleMain from '../../../containers/Layout/TitleMain';
import {organizationTypes} from '../../../prop/Types/Organization';

class OrganizationArticles extends React.Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        organization: organizationTypes,
    };

    constructor(props) {
        super(props);
        this.state = {
            organization: props.organization,
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {organization} = this.props;
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                {organization.articles.data.length !== 0 && (
                    <Container>
                        <Row className={`mb-5 mt-5`}>
                            <TitleMain title={`СТАТЬИ`} model={TITLE_ARTICLES_ORGANIZATION} role={`h2`} />
                        </Row>
                        <Row>
                            {organization.articles.data.map((article, key) => (
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`4`} className={`mb-3`} key={key}>
                                    <ArticleCard article={article} />
                                </Col>
                            ))}
                        </Row>
                        {organization.articles.data.length === 3 && (
                            <Row className={`mt-3`}>
                                <Col xs={12} className={`text-right`}>
                                    <Link to={`/organization/${organization.id}/articles`} className={`btn`} variant="light">
                                        Все статьи ({organization.articles.total})
                                    </Link>
                                </Col>
                            </Row>
                        )}
                    </Container>
                )}
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({});
export default connect(mapStateToProps)(OrganizationArticles);
