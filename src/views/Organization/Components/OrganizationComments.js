import React, {Component} from 'react';
import {organizationTypes} from '../../../prop/Types/Organization';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import SkeletonCommentItem from '../../../containers/Skeleton/Comment/Item';
import CommentsList from '../../../components/Comments/List';
import Rating from '../../../components/Rating/RatingDefault';
import {helpers} from '../../../lib/helpers/helpers';
import Button from 'react-bootstrap/Button';
import {MODAL_COMMENT_RATE_CREATE} from '../../../lib/modal';
import CommentCreateModal from '../../../components/Comments/Modal/CreateRateable';
import CommentLimitModal from '../../../components/Comments/Modal/Limit';
import {getAuth} from '../../../store/reducers/Auth';
import {fetchModal, getModal} from '../../../store/reducers/Modal';
import {getUserCommentPending} from '../../../store/reducers/User/Comment';
import {fetchComments, getCommentsOrganization, getCommentsPending} from '../../../store/reducers/Comments';
import {getUserCommentsOrganizations} from '../../../store/reducers/User/Comments';
import {connect} from 'react-redux';
import {commentsTypes} from '../../../prop/Types/Comment';
import * as PropTypes from 'prop-types';

class OrganizationComments extends Component {
    static propTypes = {
        organization: organizationTypes,
        comments: commentsTypes,
        commentsPending: PropTypes.bool,
        fetchModal: PropTypes.func,
        fetchComments: PropTypes.func,
    };
    constructor(props) {
        super(props);
        this.state = {
            organization: props.organization,
        };
    }

    componentDidMount() {
        this._isMounted = true;
        const {fetchComments} = this.props;
        fetchComments(this.props.organization.id, 'organization');
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    setShow(value) {
        const {fetchModal} = this.props;
        fetchModal(value);

        // const {fetchModal, userComments, auth} = this.props;
        // const {organization} = this.state;
        // if (!auth.guest && userComments.indexOf(organization.id) !== -1) {
        //     //fetchModal(MODAL_COMMENT_LIMIT);
        //     //toastError('Вы уже оставляли отзыв этому Организатора');
        //     fetchModal(value);
        // } else {
        //     fetchModal(value);
        // }
    }

    render() {
        const {comments, commentsPending} = this.props;
        const {organization} = this.state;
        return (
            <React.Fragment>
                <Row className={`mt-5 mb-5 --container-organization-comments`}>
                    <Col xs={12}>
                        <h3>Комментарии ({comments.filter(comment => comment.anon !== 1).length})</h3>
                    </Col>
                    <Col xs={12}>
                        {commentsPending && (
                            <Row className={`mt-3`}>
                                <Col xs={12} md={8}>
                                    <SkeletonCommentItem />
                                </Col>
                            </Row>
                        )}
                        {!commentsPending && (
                            <Row className={`mt-3 page-model-comments`}>
                                <Col xs={12} md={8} className={`mb-5`}>
                                    {comments.length !== 0 && <CommentsList comments={comments} item={organization} model={`organization`} />}
                                    {comments.length === 0 && <div>Комментарий нет</div>}
                                </Col>
                                <Col xs={12} md={4} className={`mb-5`}>
                                    <h5 className={`text-center`}>Средний рейтинг</h5>
                                    <div className={`text-center`} style={{fontSize: '18px'}}>
                                        <div className={`d-flex justify-content-between`}>
                                            <div>
                                                <Rating readonly={true} rating={helpers.average(comments)} />
                                            </div>
                                            <div>
                                                {helpers.average(comments)} / 5 ({comments.length})
                                            </div>
                                        </div>
                                        <hr />
                                        <div>
                                            <Button variant="primary" block={true} onClick={() => this.setShow(MODAL_COMMENT_RATE_CREATE)}>
                                                Написать отзыв
                                            </Button>
                                        </div>
                                    </div>
                                </Col>
                            </Row>
                        )}
                    </Col>
                    <CommentCreateModal item={organization} model={'organization'} />
                    <CommentLimitModal />
                </Row>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    auth: getAuth(state),
    modal: getModal(state),
    pending: getUserCommentPending(state),
    comments: getCommentsOrganization(state),
    commentsPending: getCommentsPending(state),
    userComments: getUserCommentsOrganizations(state),
});

const mapDispatchToProps = dispatch => ({
    fetchModal: name => dispatch(fetchModal(name)),
    fetchComments: (id, model) => dispatch(fetchComments(id, model)),
});

export default connect(mapStateToProps, mapDispatchToProps)(OrganizationComments);
