import React, {Component} from 'react';
import FsLightbox from 'fslightbox-react';
import placeholder from '../../../assets/img/youtube-placeholder-2.png';
import {organizationTypes} from "../../../prop/Types/Organization";

class OrganizationVideo extends Component {
    static propTypes = {
        organization: organizationTypes,
    };

    constructor(props) {
        super(props);
        this.state = {
            organization: props.organization,
            toggle: false,
            slide: 1,
        };
    }

    setToggle(state, key) {
        this.setState({
            toggle: state,
            slide: key + 1,
        });
    }

    render() {
        const {organization} = this.state;
        const {toggle} = this.state;
        return (
            <React.Fragment>
                {organization.parameters.video_youtube !== undefined && (
                    <div className={`tour-video`}>
                        <div className={`__preview`}>
                            <img
                                src={organization.parameters.video_youtube.meta.thumbnail_url}
                                onClick={() =>
                                    this.setState({
                                        toggle: !toggle,
                                    })
                                }
                                alt={`Видео`}
                                title={`Посмотреть видео`}
                            />
                        </div>
                        <div className={`__preview __placeholder`}>
                            <img
                                src={placeholder}
                                onClick={() =>
                                    this.setState({
                                        toggle: !toggle,
                                    })
                                }
                                title={`Посмотреть видео`}
                                alt=""
                            />
                        </div>
                        <FsLightbox toggler={toggle} sources={[organization.parameters.video_youtube.value]} />
                        {/*<iframe src={tour.parameters.video_youtube.embed} width="100%" height="400px" frameBorder="0" allow="autoplay; fullscreen" allowFullScreen />*/}
                    </div>
                )}
            </React.Fragment>
        );
    }
}

export default OrganizationVideo;
