import React, {Component} from 'react';
import {organizationTypes} from '../../../prop/Types/Organization';
import Col from 'react-bootstrap/Col';
import ReactMarkdown from '../../../plugins/ReactMarkdown';
import Row from 'react-bootstrap/Row';

class OrganizationDescription extends Component {
    static propTypes = {
        organization: organizationTypes,
    };
    constructor(props) {
        super(props);
        this.state = {
            organization: props.organization,
        };
    }

    render() {
        const {organization} = this.state;
        return (
            <React.Fragment>
                <Row className={`mt-5`}>
                    <Col xs={12} md={12}>
                        <h3>Описание</h3>
                        <ReactMarkdown source={organization.description} />
                    </Col>
                </Row>
            </React.Fragment>
        );
    }
}

export default OrganizationDescription;
