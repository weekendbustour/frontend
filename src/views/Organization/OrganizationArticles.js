import React, {Component} from 'react';
import Container from 'react-bootstrap/Container';
import Breadcrumb from '../../containers/Layout/Breadcrumb';
import breadcrumbs from '../../breadcrumbs';
import {connect} from 'react-redux';
import Subscription from '../../containers/Article/Subscription';
import seo from '../../lib/seo';
import {fetchArticles} from '../../store/actions/Articles';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import SkeletonArticle from '../../containers/Skeleton/Article/Card';
import ArticleCard from '../../containers/Article/Card/Card';
import AwPagination from '../../containers/Layout/AwPagination';
import {scrollTo} from '../../lib/scrollTo';
import * as PropTypes from 'prop-types';
import {paginationTypes} from '../../prop/Types/Pagination';
import {getArticles, getArticlesPagination, getArticlesPending} from "../../store/reducers/Articles";
import {articlesTypes} from "../../prop/Types/Article";
import {TITLE_ARTICLES_ORGANIZATION} from "../../lib/titles";
import TitleMain from "../../containers/Layout/TitleMain";
import {fetchOrganization, getOrganization, getOrganizationPending} from "../../store/reducers/Organization";
import {organizationTypes} from "../../prop/Types/Organization";
import Skeleton from "react-loading-skeleton";

class OrganizationArticles extends Component {
    _isMounted = false;

    static propTypes = {
        articles: articlesTypes,
        organization: organizationTypes,
        pending: PropTypes.bool,
        organizationLoading: PropTypes.bool,
        pagination: paginationTypes,
        fetchArticles: PropTypes.func,
        fetchOrganization: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.state = {};
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.onChangePage = this.onChangePage.bind(this);
    }

    componentDidMount() {
        const {fetchOrganization} = this.props;
        fetchOrganization(this.props.match.params.id);
        this._isMounted = true;
        seo.articles();
        const {location} = this.props;
        let searchUrl = location.search;
        let urlData = new URLSearchParams(searchUrl);
        const page = urlData.get('page');
        this.onChangePage(page);
    }

    onChangePage(page) {
        const {fetchArticles} = this.props;
        scrollTo.selectorFast('.breadcrumb');
        const organization_id = this.props.match.params.id;
        fetchArticles({
            page: page !== undefined ? page : 1,
            organization_id: organization_id !== undefined ? organization_id : null,
        });
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {pending, articles, pagination, organizationLoading, organization} = this.props;
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <section>
                    {organizationLoading &&
                    <Container>
                        <Row>
                            <Col xs={12}>
                                <div className={`mt-2 mb-2`}>
                                    <Skeleton height={`25px`} width={`200px`} />
                                </div>
                            </Col>
                        </Row>
                    </Container>
                    }
                    {!organizationLoading && (
                        <Container>
                            <Breadcrumb push={breadcrumbs.articles} parent={{to: `/organization/${organization.id}/${organization.name}`, title: organization.title}}/>
                        </Container>
                    )}
                </section>
                <section>
                    {organizationLoading &&
                    <Container className={`mb-5`}>
                        <TitleMain title={`Статьи`} model={`${TITLE_ARTICLES_ORGANIZATION}`}/>
                    </Container>
                    }
                    {!organizationLoading && (
                        <Container className={`mb-5`}>
                            <TitleMain title={`Статьи`} model={`${TITLE_ARTICLES_ORGANIZATION}`} item={organization}/>
                        </Container>
                    )}
                </section>
                {pending && (
                    <section className={`h-500`}>
                        <Container>
                            <Row className={`awesome-card cards`}>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`4`} className={`mb-3`}>
                                    <SkeletonArticle />
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`8`} className={`mb-3`}>
                                    <SkeletonArticle />
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`4`} className={`mb-3`}>
                                    <SkeletonArticle />
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`4`} className={`mb-3`}>
                                    <SkeletonArticle />
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`4`} className={`mb-3`}>
                                    <SkeletonArticle />
                                </Col>
                                {/*<Col xs={`12`} sm={`12`} md={`6`} lg={`4`} className={`mb-3`}>*/}
                                {/*    <SkeletonNews />*/}
                                {/*</Col>*/}
                            </Row>
                        </Container>
                    </section>
                )}
                {!pending && (
                    <section>
                        {/*{loading &&*/}
                        {/*<Container className={`mb-5 mt-5 d-flex justify-content-center`}>*/}
                        {/*    <RotateSpinner size={30} color="#41b882" loading={loading} />*/}
                        {/*</Container>*/}
                        {/*}*/}
                        <Container>
                            <Row>
                                {articles.length !== 0 &&
                                articles.map((item, key) => (
                                        <Col xs={`12`} sm={`12`} md={`6`} lg={`${[1, 5, 11, 15].indexOf(key) !== -1 ? '8' : '4'}`} className={`mb-3`} key={key}>
                                            {/*<TourItem tour={tour}/>*/}
                                            <ArticleCard article={item} type={[1, 5, 11, 15].indexOf(key) !== -1 ? 'long' : 'short'} />
                                        </Col>
                                    ))}
                            </Row>
                        </Container>
                        {pagination !== null && pagination.total > pagination.limit && (
                            <Container className="d-flex justify-content-center mt-5">
                                {/* eslint-disable-next-line react/jsx-no-bind */}
                                <AwPagination pagination={pagination} onChangePage={page => this.onChangePage(page)} location={this.props.location} />
                            </Container>
                        )}
                        {articles.length === 0 && (
                            <Container>
                                <div className="d-flex align-items-center justify-content-center" style={{height: '200px'}}>
                                    <Subscription organization={{}} />
                                </div>
                            </Container>
                        )}
                    </section>
                )}
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    articles: getArticles(state),
    pagination: getArticlesPagination(state),
    pending: getArticlesPending(state),

    organizationLoading: getOrganizationPending(state),
    organization: getOrganization(state),
});

const mapDispatchToProps = dispatch => ({
    fetchArticles: data => dispatch(fetchArticles(data)),
    fetchOrganization: id => dispatch(fetchOrganization(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(OrganizationArticles);
