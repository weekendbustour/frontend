import React, {Component} from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Breadcrumb from '../../containers/Layout/Breadcrumb';
import 'react-dates/initialize';
import breadcrumbs from '../../breadcrumbs';
import SkeletonPage from '../../containers/Skeleton/Organization/Page';
import OrganizationSlider from './Components/OrganizationSlider';
import {fetchOrganization, getOrganization, getOrganizationPending} from '../../store/reducers/Organization';
import {connect} from 'react-redux';
import {tourTypes} from '../../prop/Types/Tour';
import * as PropTypes from 'prop-types';
import OrganizationMain from './Components/Main';
import OrganizationDescription from './Components/OrganizationDescription';
import OrganizationComments from './Components/OrganizationComments';
import OrganizationCalendar from './Components/OrganizationCalendar';
import Title from '../../containers/Layout/Title';
import ResultFound from '../../containers/Tour/Components/ResultFound';
import TourCard from '../../containers/Tour/Card/Card';
import {Link} from 'react-router-dom';
import OrganizationSubscription from './Components/OrganizationSubscription';
import OrganizationVideo from './Components/OrganizationVideo';
import OrganizationArticles from "./Components/OrganizationArticles";

class Organization extends Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        organization: tourTypes,
        organizationLoading: PropTypes.bool,
        fetchOrganization: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.state = {};
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentDidMount() {
        const {fetchOrganization} = this.props;
        if (window.page !== undefined && window.page.organization !== undefined) {
            fetchOrganization(window.page.organization.id);
        } else {
            fetchOrganization(this.props.match.params.id);
        }
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {organization, organizationLoading} = this.props;
        if (!this.shouldComponentRender()) return null;
        return (
            <React.Fragment>
                {organizationLoading && (
                    <section>
                        <SkeletonPage />
                    </section>
                )}
                {/* Хлебные крошки */}
                <section>
                    {!organizationLoading && (
                        <Container>
                            <Breadcrumb parent={breadcrumbs.organizations} push={{title: organization.title}} />
                        </Container>
                    )}
                </section>
                {/* Баннер */}
                <section>
                    {!organizationLoading && (
                        <Container className={`mb-2 mt-0 page__movie`}>
                            <Row>
                                <Col xs={12}>
                                    <OrganizationMain organization={organization} />
                                </Col>
                            </Row>
                        </Container>
                    )}
                </section>
                {/* Видео */}
                <section>{!organizationLoading && <OrganizationVideo organization={organization} />}</section>
                {/* Слайдер */}
                <section>{!organizationLoading && <OrganizationSlider organization={organization} />}</section>
                {/* Календарь */}
                <section>
                    {!organizationLoading && (
                        <Container className={`mb-5 direction--date ${organization.tours.length === 0 ? '__empty' : ''}`}>
                            <OrganizationCalendar organization={organization} />
                            {organization.tours.length === 0 && <OrganizationSubscription organization={organization} />}
                        </Container>
                    )}
                </section>
                {/* Описание */}
                <section>
                    {!organizationLoading && organization.description !== null && (
                        <Container>
                            <OrganizationDescription organization={organization} />
                        </Container>
                    )}
                </section>
                <section>
                    {!organizationLoading && (
                        <Container>
                            {/* Загрузить еще */}
                            {organization.tours !== undefined && organization.tours.length !== 0 && (
                                <Row className={`mt-5`}>
                                    <Col xs={12}>
                                        <Title role={`h2`} title={`Ближайшие`} />
                                    </Col>
                                    <Col xs={`12`}>
                                        <ResultFound total={organization.tours.length} />
                                    </Col>
                                </Row>
                            )}
                            {/* Ближайщие туры */}
                            {organization.tours !== undefined && organization.tours.length !== 0 && (
                                <Row className={`awesome-card cards`}>
                                    {organization.tours.length !== 0 &&
                                        organization.tours.map((tour, key) => (
                                            <Col xs={`12`} sm={`12`} md={`6`} lg={`4`} className={`mb-3`} key={key}>
                                                <TourCard tour={tour} />
                                            </Col>
                                        ))}
                                </Row>
                            )}
                            {organization.tours.length === 4 && (
                                <Row className={`mb-5`}>
                                    <Col xs={12} className={`text-right`}>
                                        <Link to={`/search?organization_id=${organization.id}&scroll=1`} className={`btn`} variant="light">
                                            Посмотреть все Туры от этого Организатора
                                        </Link>
                                    </Col>
                                </Row>
                            )}
                        </Container>
                    )}
                </section>
                {/* Статьи */}
                <section>
                    {!organizationLoading && (
                        <Container>
                            <OrganizationArticles organization={organization} />
                        </Container>
                    )}
                </section>
                {/* Комментарии */}
                <section>
                    {!organizationLoading && (
                        <Container>
                            <OrganizationComments organization={organization} />
                        </Container>
                    )}
                </section>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    organizationLoading: getOrganizationPending(state),
    organization: getOrganization(state),
});

const mapDispatchToProps = dispatch => ({
    fetchOrganization: id => dispatch(fetchOrganization(id)),
});
export default connect(mapStateToProps, mapDispatchToProps)(Organization);
