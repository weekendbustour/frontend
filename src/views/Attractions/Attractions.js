import React, {Component} from 'react';
import Container from 'react-bootstrap/Container';
import Breadcrumb from '../../containers/Layout/Breadcrumb';
import breadcrumbs from '../../breadcrumbs';
import {connect} from 'react-redux';
import seo from '../../lib/seo';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import {scrollTo} from '../../lib/scrollTo';
import * as PropTypes from 'prop-types';
import {paginationTypes} from '../../prop/Types/Pagination';
import {TITLE_ATTRACTIONS} from "../../lib/titles";
import TitleMain from "../../containers/Layout/TitleMain";
import {getAttractions, getAttractionsPagination, getAttractionsPending} from "../../store/reducers/Attractions";
import {fetchAttractions} from "../../store/actions/Attractions";
import {attractionsTypes} from "../../prop/Types/Attraction";
import SkeletonAttraction from '../../containers/Skeleton/Attraction/Card';
import AttractionCard from "../../containers/Attraction/Card/Card";
import AwPagination from "../../containers/Layout/AwPagination";

class Attractions extends Component {
    _isMounted = false;

    static propTypes = {
        attractions: attractionsTypes,
        pending: PropTypes.bool,
        pagination: paginationTypes,
        fetchAttractions: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.state = {};
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.onChangePage = this.onChangePage.bind(this);
    }

    componentDidMount() {
        this._isMounted = true;
        seo.attractions();
        const {location} = this.props;
        let searchUrl = location.search;
        let urlData = new URLSearchParams(searchUrl);
        const page = urlData.get('page');
        this.onChangePage(page);
    }

    onChangePage(page) {
        const {fetchAttractions} = this.props;
        scrollTo.selectorFast('.breadcrumb');
        fetchAttractions({
            page: page !== undefined ? page : 1,
        });
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {pending, attractions, pagination} = this.props;
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <section>
                    <Container>
                        <Breadcrumb push={breadcrumbs.attractions} />
                    </Container>
                </section>
                <section>
                    <Container className={`mb-5`}>
                        <TitleMain title={`Достопримечательности`} model={TITLE_ATTRACTIONS}/>
                    </Container>
                </section>
                {pending && (
                    <section className={`h-500`}>
                        <Container>
                            <Row>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`3`}>
                                    <SkeletonAttraction/>
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`3`}>
                                    <SkeletonAttraction/>
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`3`}>
                                    <SkeletonAttraction/>
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`3`}>
                                    <SkeletonAttraction/>
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`3`}>
                                    <SkeletonAttraction/>
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`3`}>
                                    <SkeletonAttraction/>
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`3`}>
                                    <SkeletonAttraction/>
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`3`}>
                                    <SkeletonAttraction/>
                                </Col>
                            </Row>
                        </Container>
                    </section>
                )}
                {!pending && (
                    <section>
                        <Container>
                            <Row>
                                {attractions.length !== 0 &&
                                attractions.map((attraction, key) => (
                                    <Col xs={`12`} sm={`12`} md={`6`} lg={3} className={`mb-3`} key={key}>
                                        <AttractionCard attraction={attraction}/>
                                    </Col>
                                ))}
                            </Row>
                        </Container>
                        {pagination !== null && pagination.total > pagination.limit && (
                            <Container className="d-flex justify-content-center mt-5">
                                {/* eslint-disable-next-line react/jsx-no-bind */}
                                <AwPagination pagination={pagination} onChangePage={page => this.onChangePage(page)} location={this.props.location} />
                            </Container>
                        )}
                    </section>
                )}
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    attractions: getAttractions(state),
    pagination: getAttractionsPagination(state),
    pending: getAttractionsPending(state),
});

const mapDispatchToProps = dispatch => ({
    fetchAttractions: data => dispatch(fetchAttractions(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Attractions);
