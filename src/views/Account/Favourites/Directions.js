import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Alert from 'react-bootstrap/Alert';
import Breadcrumb from '../../../containers/Layout/Breadcrumb';
import breadcrumbs from '../../../breadcrumbs';
import {fetchUserFavourites, getUserFavouritesDirections, getUserFavouritesPending} from '../../../store/reducers/User/Favourites';
import {getUserFavouriteDirections, getUserFavourites} from '../../../store/reducers/User/Favourite';
import {getUser} from '../../../store/reducers/User';
import {connect} from 'react-redux';
import SkeletonDirection from '../../../containers/Skeleton/Direction/Card';
import DirectionCard from '../../../containers/Direction/Card/Card';
import Nav from 'react-bootstrap/Nav';
import {Link} from 'react-router-dom';
import seo from '../../../lib/seo';
import * as PropTypes from 'prop-types';
import {userFavouritesTypes, userTypes} from '../../../prop/Types/User';
import {directionsTypes} from '../../../prop/Types/Direction';

class Directions extends React.Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        user: userTypes,
        directions: directionsTypes,
        pending: PropTypes.bool,
        fetchUserFavourites: PropTypes.func,
        userFavourites: userFavouritesTypes,
    };

    constructor(props) {
        super(props);
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentDidMount() {
        const {user, fetchUserFavourites} = this.props;
        this._isMounted = true;
        seo.favourites();
        if (this._isMounted && user.id !== undefined) {
            fetchUserFavourites();
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {directions, pending, userFavourites} = this.props;
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <section>
                    <Container>
                        <Breadcrumb push={breadcrumbs.favouriteDirections} parent={breadcrumbs.favourites} />
                    </Container>
                </section>
                <section>
                    <Container className={`mb-5`} style={{minHeight: '500px'}}>
                        <Row>
                            <Col xs={12} className={`aw-tabs-nav-line mb-5`}>
                                <Nav>
                                    <Link to={`/lk/favourites/tours`} className={`nav-item`}>
                                        Туры
                                    </Link>
                                    <Link to={`/lk/favourites/organizations`} className={`nav-item`}>
                                        Организаторы
                                    </Link>
                                    <span className={`nav-item active`}>Направления</span>
                                </Nav>
                            </Col>
                        </Row>
                        {pending || userFavourites === null ? (
                            <Row>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`6`} className={`mb-3`}>
                                    <SkeletonDirection />
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`6`} className={`mb-3`}>
                                    <SkeletonDirection />
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`6`} className={`mb-3`}>
                                    <SkeletonDirection />
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`6`} className={`mb-3`}>
                                    <SkeletonDirection />
                                </Col>
                            </Row>
                        ) : (
                            ''
                        )}
                        {!pending && userFavourites !== null && (
                            <Row>
                                {directions.length !== 0 &&
                                    directions.map(
                                        (direction, key) =>
                                            userFavourites !== null &&
                                            userFavourites.directions.indexOf(direction.id) !== -1 && (
                                                <Col xs={`12`} sm={`12`} md={`6`} lg={`6`} className={`mb-3`} key={key}>
                                                    <DirectionCard direction={direction} />
                                                </Col>
                                            )
                                    )}
                                {directions.length === 0 && (
                                    <Col xs={`12`} sm={`12`} md={`12`} lg={`12`}>
                                        <Alert variant="info">
                                            <Alert.Heading>Список пуст</Alert.Heading>
                                            <p className={`mb-0`}>Найдите ваше любимое Направление и получайте рассылку на новости.</p>
                                        </Alert>
                                    </Col>
                                )}
                            </Row>
                        )}
                    </Container>
                </section>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: getUser(state),
    pending: getUserFavouritesPending(state),
    userFavourites: getUserFavourites(state),
    directions: getUserFavouritesDirections(state),
    directionsShort: getUserFavouriteDirections(state),
});

const mapDispatchToProps = dispatch => ({
    fetchUserFavourites: () => dispatch(fetchUserFavourites()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Directions);
