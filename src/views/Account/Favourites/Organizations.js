import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Breadcrumb from '../../../containers/Layout/Breadcrumb';
import breadcrumbs from '../../../breadcrumbs';
import {fetchUserFavourites, getUserFavouritesOrganizations, getUserFavouritesPending} from '../../../store/reducers/User/Favourites';
import {getUserFavourites} from '../../../store/reducers/User/Favourite';
import {getUser} from '../../../store/reducers/User';
import {connect} from 'react-redux';
import SkeletonOrganization from '../../../containers/Skeleton/Organization/Card';
import OrganizationCard from '../../../containers/Organization/Card/Card';
import Nav from 'react-bootstrap/Nav';
import {Link} from 'react-router-dom';
import Alert from 'react-bootstrap/Alert';
import seo from '../../../lib/seo';
import * as PropTypes from 'prop-types';
import {userFavouritesTypes, userTypes} from '../../../prop/Types/User';
import {organizationsTypes} from '../../../prop/Types/Organization';

class Organizations extends React.Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        user: userTypes,
        organizations: organizationsTypes,
        pending: PropTypes.bool,
        fetchUserFavourites: PropTypes.func,
        userFavourites: userFavouritesTypes,
    };

    constructor(props) {
        super(props);
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentDidMount() {
        const {user, fetchUserFavourites} = this.props;
        this._isMounted = true;
        seo.favourites();
        if (this._isMounted && user.id !== undefined) {
            fetchUserFavourites();
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {organizations, pending, userFavourites} = this.props;
        if (!pending && !this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <section>
                    <Container>
                        <Breadcrumb push={breadcrumbs.favouriteOrganizations} parent={breadcrumbs.favourites} />
                    </Container>
                </section>
                <section>
                    <Container className={`mb-5`} style={{minHeight: '500px'}}>
                        <Row>
                            <Col xs={12} className={`aw-tabs-nav-line mb-5`}>
                                <Nav>
                                    <Link to={`/lk/favourites/tours`} className={`nav-item`}>
                                        Туры
                                    </Link>
                                    <span className={`nav-item active`}>Организаторы</span>
                                    <Link to={`/lk/favourites/directions`} className={`nav-item`}>
                                        Направления
                                    </Link>
                                </Nav>
                            </Col>
                        </Row>
                        {pending || userFavourites === null ? (
                            <Row>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`6`} className={`mb-3`}>
                                    <SkeletonOrganization />
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`6`} className={`mb-3`}>
                                    <SkeletonOrganization />
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`6`} className={`mb-3`}>
                                    <SkeletonOrganization />
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`6`} className={`mb-3`}>
                                    <SkeletonOrganization />
                                </Col>
                            </Row>
                        ) : (
                            ''
                        )}
                        {!pending && userFavourites !== null && (
                            <Row>
                                {organizations.length !== 0 &&
                                    organizations.map(
                                        (organization, key) =>
                                            userFavourites !== null &&
                                            userFavourites.organizations.indexOf(organization.id) !== -1 && (
                                                <Col xs={`12`} sm={`12`} md={`6`} lg={`6`} className={`mb-3`} key={key}>
                                                    <OrganizationCard organization={organization} />
                                                </Col>
                                            )
                                    )}
                                {organizations.length === 0 && (
                                    <Col xs={`12`} sm={`12`} md={`12`} lg={`12`}>
                                        <Alert variant="info">
                                            <Alert.Heading>Список пуст</Alert.Heading>
                                            <p className={`mb-0`}>Найдите своего любимого Организатора и получайте рассылку на новости.</p>
                                        </Alert>
                                    </Col>
                                )}
                            </Row>
                        )}
                    </Container>
                </section>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: getUser(state),
    pending: getUserFavouritesPending(state),
    userFavourites: getUserFavourites(state),
    organizations: getUserFavouritesOrganizations(state),
});

const mapDispatchToProps = dispatch => ({
    fetchUserFavourites: () => dispatch(fetchUserFavourites()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Organizations);
