import React from 'react';
import Row from 'react-bootstrap/Row';
import Nav from 'react-bootstrap/Nav';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Breadcrumb from '../../../containers/Layout/Breadcrumb';
import breadcrumbs from '../../../breadcrumbs';
import {getUser} from '../../../store/reducers/User';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {fetchUserSubscriptions, getUserSubscriptionsOrganizations, getUserSubscriptionsPending} from '../../../store/reducers/User/Subscriptions';
import {getUserSubscriptions} from '../../../store/reducers/User/Subscription';
import SkeletonOrganization from '../../../containers/Skeleton/Organization/Card';
import OrganizationCard from '../../../containers/Organization/Card/Card';
import Alert from 'react-bootstrap/Alert';
import * as PropTypes from 'prop-types';
import {organizationsTypes} from '../../../prop/Types/Organization';
import {userTypes} from '../../../prop/Types/User';
import {helpers} from "../../../lib/helpers/helpers";

class Organizations extends React.Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        organizations: organizationsTypes,
        user: userTypes,
        pending: PropTypes.bool,
        fetchUserSubscriptions: PropTypes.func,
        userSubscriptions: PropTypes.array,
    };

    constructor(props) {
        super(props);
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentDidMount() {
        const {user, fetchUserSubscriptions} = this.props;
        this._isMounted = true;
        if (this._isMounted && user.id !== undefined) {
            fetchUserSubscriptions(user);
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {organizations, pending, userSubscriptions, user} = this.props;
        if (!pending && !this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <section>
                    <Container>
                        <Breadcrumb push={breadcrumbs.subscriptions} />
                    </Container>
                </section>
                <section>
                    <Container className={`mb-5`} style={{minHeight: '500px'}}>
                        <Row>
                            <Col xs={12} className={`aw-tabs-nav-line mb-5`}>
                                <Nav>
                                    <Link to={`/lk/subscriptions/directions`} className={`nav-item`}>
                                        Направления
                                    </Link>
                                    <span className={`btn nav-item active`}>Организаторы</span>
                                </Nav>
                            </Col>
                            {!helpers.validateEmail(user.email) &&
                            <Col xs={12} className="mb-2">
                                <Alert variant="info">
                                    <p className={`mb-0`}>
                                        Чтобы получать рассылку - измените ваш email на корректный. Текущий email: <b>{user.email}</b>
                                    </p>
                                </Alert>
                            </Col>
                            }
                        </Row>
                        {pending || userSubscriptions === null ? (
                            <Row>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`6`} className={`mb-3`}>
                                    <SkeletonOrganization />
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`6`} className={`mb-3`}>
                                    <SkeletonOrganization />
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`6`} className={`mb-3`}>
                                    <SkeletonOrganization />
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`6`} className={`mb-3`}>
                                    <SkeletonOrganization />
                                </Col>
                            </Row>
                        ) : (
                            ''
                        )}
                        {!pending && userSubscriptions !== null && (
                            <Row>
                                {organizations.length !== 0 &&
                                    organizations.map(
                                        (organization, key) =>
                                            userSubscriptions !== null &&
                                            userSubscriptions.organizations.indexOf(organization.id) !== -1 && (
                                                <Col xs={`12`} sm={`12`} md={`6`} lg={`6`} className={`mb-3`} key={key}>
                                                    <OrganizationCard organization={organization} />
                                                </Col>
                                            )
                                    )}
                                {organizations.length === 0 && (
                                    <Col xs={`12`} sm={`12`} md={`12`} lg={`12`}>
                                        <Alert variant="info">
                                            <Alert.Heading>Список пуст</Alert.Heading>
                                            <p className={`mb-0`}>Найдите своего любимого Организатора и получайте рассылку на новости.</p>
                                        </Alert>
                                    </Col>
                                )}
                            </Row>
                        )}
                    </Container>
                </section>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: getUser(state),
    pending: getUserSubscriptionsPending(state),
    userSubscriptions: getUserSubscriptions(state),
    organizations: getUserSubscriptionsOrganizations(state),
});

const mapDispatchToProps = dispatch => ({
    fetchUserSubscriptions: () => dispatch(fetchUserSubscriptions()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Organizations);
