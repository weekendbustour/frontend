import React from 'react';
import Row from 'react-bootstrap/Row';
import Nav from 'react-bootstrap/Nav';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Breadcrumb from '../../../containers/Layout/Breadcrumb';
import breadcrumbs from '../../../breadcrumbs';
import {getUser} from '../../../store/reducers/User';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {fetchUserSubscriptions, getUserSubscriptionsDirections, getUserSubscriptionsPending} from '../../../store/reducers/User/Subscriptions';
import {getUserSubscriptions} from '../../../store/reducers/User/Subscription';
import SkeletonDirection from '../../../containers/Skeleton/Direction/Card';
import DirectionCard from '../../../containers/Direction/Card/Card';
import Alert from "react-bootstrap/Alert";
import * as PropTypes from "prop-types";
import {userTypes} from "../../../prop/Types/User";
import {directionsTypes} from "../../../prop/Types/Direction";
import {helpers} from "../../../lib/helpers/helpers";

class Directions extends React.Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        user: userTypes,
        directions: directionsTypes,
        pending: PropTypes.bool,
        fetchUserSubscriptions: PropTypes.func,
        userSubscriptions: PropTypes.array,
    };

    constructor(props) {
        super(props);
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentDidMount() {
        const {user, fetchUserSubscriptions} = this.props;
        this._isMounted = true;
        if (this._isMounted && user.id !== undefined) {
            fetchUserSubscriptions();
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {directions, pending, userSubscriptions, user} = this.props;
        if (!pending && !this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <section>
                    <Container>
                        <Breadcrumb push={breadcrumbs.subscriptions} />
                    </Container>
                </section>
                <section>
                    <Container className={`mb-5`} style={{minHeight: '500px'}}>
                        <Row>
                            <Col xs={12} className={`aw-tabs-nav-line mb-5`}>
                                <Nav>
                                    <span className={`btn nav-item active`}>Направления</span>
                                    <Link to={`/lk/subscriptions/organizations`} className={`nav-item`}>
                                        Организаторы
                                    </Link>
                                </Nav>
                            </Col>
                            {!helpers.validateEmail(user.email) &&
                            <Col xs={12} className="mb-2">
                                <Alert variant="info">
                                    <p className={`mb-0`}>
                                        Чтобы получать рассылку - измените ваш email на корректный. Текущий email: <b>{user.email}</b>
                                    </p>
                                </Alert>
                            </Col>
                            }
                        </Row>
                        {pending || userSubscriptions === null ? (
                            <Row>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`6`} className={`mb-3`}>
                                    <SkeletonDirection />
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`6`} className={`mb-3`}>
                                    <SkeletonDirection />
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`6`} className={`mb-3`}>
                                    <SkeletonDirection />
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`6`} className={`mb-3`}>
                                    <SkeletonDirection />
                                </Col>
                            </Row>
                        ) : (
                            ''
                        )}
                        {!pending && userSubscriptions !== null && (
                            <Row>
                                {directions.length !== 0 && directions.map(
                                    (direction, key) =>
                                        userSubscriptions !== null &&
                                        userSubscriptions.directions.indexOf(direction.id) !== -1 && (
                                            <Col xs={`12`} sm={`12`} md={`6`} lg={`6`} className={`mb-3`} key={key}>
                                                <DirectionCard direction={direction} />
                                            </Col>
                                        )
                                )}
                                {directions.length === 0 &&
                                <Col xs={`12`} sm={`12`} md={`12`} lg={`12`}>
                                    <Alert variant="info">
                                        <Alert.Heading>Список пуст</Alert.Heading>
                                        <p className={`mb-0`}>
                                            Найдите ваше любимое Направление и получайте рассылку на новости.
                                        </p>
                                    </Alert>
                                </Col>
                                }
                            </Row>
                        )}
                    </Container>
                </section>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: getUser(state),
    pending: getUserSubscriptionsPending(state),
    userSubscriptions: getUserSubscriptions(state),
    directions: getUserSubscriptionsDirections(state),
});


const mapDispatchToProps = dispatch => ({
    fetchUserSubscriptions: () => dispatch(fetchUserSubscriptions()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Directions);
