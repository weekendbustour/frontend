import React from 'react';
import Table from 'react-bootstrap/Table';
import {connect} from 'react-redux';
import {TooltipQuestion} from '../../../containers/Layout/Tooltip/Question';
import * as PropTypes from 'prop-types';

class ReservationTable extends React.Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        reservation: PropTypes.object,
    };

    constructor(props) {
        super(props);
        this.state = {
            reservation: props.reservation,
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {reservation} = this.props;
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <div className={`shadow-sm p-3 mb-3`}>
                    <Table size="sm" responsive className={`tr-table mb-0`}>
                        <tbody>
                            <tr>
                                <td><b>Бронирование № {reservation.id}</b></td>
                                <td />
                            </tr>
                            <tr>
                                <td>Код</td>
                                <td>
                                    {reservation.code}
                                    <TooltipQuestion title="Нужен будет для подтверждения" />
                                </td>
                            </tr>
                            <tr>
                                <td>Количество мест</td>
                                <td>{reservation.places}</td>
                            </tr>
                            <tr>
                                <td>Статус</td>
                                <td>{reservation.status.title}</td>
                            </tr>
                            <tr>
                                <td>Сумма</td>
                                <td>{reservation.amount} р.</td>
                            </tr>
                        </tbody>
                    </Table>
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(ReservationTable);
