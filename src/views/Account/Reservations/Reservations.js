import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import SkeletonTour from '../../../containers/Skeleton/Tour/Card';
import SkeletonTourReservation from '../../../containers/Skeleton/Tour/Reservation';
import Breadcrumb from '../../../containers/Layout/Breadcrumb';
import breadcrumbs from '../../../breadcrumbs';
import {getUser} from '../../../store/reducers/User';
import {connect} from 'react-redux';
import TourCard from '../../../containers/Tour/Card/Card';
import Alert from 'react-bootstrap/Alert';
import Badge from 'react-bootstrap/Badge';
import {fetchUserReservations, getUserReservationsPending, getUserReservationsTours} from '../../../store/reducers/User/Reservations';
import {
    fetchUserReservationCancel,
    getUserReservations, getUserReservationsPendingArray
} from '../../../store/reducers/User/Reservation';
import * as PropTypes from 'prop-types';
import {userTypes} from '../../../prop/Types/User';
import {toursTypes} from '../../../prop/Types/Tour';
import {TITLE_TOURS_RESERVATIONS} from "../../../lib/titles";
import TitleMain from "../../../containers/Layout/TitleMain";
import ReservationTable from "./ReservationTable";

class Reservations extends React.Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        user: userTypes,
        tours: toursTypes,
        pending: PropTypes.bool,
        pendingCancel: PropTypes.bool,
        fetchUserReservations: PropTypes.func,
        fetchUserReservationCancel: PropTypes.func,
        userReservations: PropTypes.array,
    };

    constructor(props) {
        super(props);
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }

    componentDidMount() {
        const {user, fetchUserReservations} = this.props;
        this._isMounted = true;
        if (this._isMounted && user.id !== undefined) {
            fetchUserReservations(user);
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    handleCancel(id) {
        const {fetchUserReservationCancel} = this.props;
        fetchUserReservationCancel(id);
    }

    render() {
        const {tours, pending, userReservations, pendingCancel} = this.props;
        if (!pending && !this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <section>
                    <Container>
                        <Breadcrumb push={breadcrumbs.reservations} />
                    </Container>
                </section>
                <section>
                    <Container className={`mb-5`} style={{minHeight: '500px'}}>
                        <Row className={`mb-3`}>
                            <Col xs={12}>
                                <TitleMain title={`Туры`} model={TITLE_TOURS_RESERVATIONS} role={`h3`}/>
                            </Col>
                        </Row>
                        {pending || userReservations === null ? (
                            <Row className={`awesome-card cards`}>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`4`} className={`mb-5`}>
                                    <SkeletonTour />
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`8`} className={`mb-5`}>
                                    <SkeletonTourReservation />
                                </Col>
                            </Row>
                        ) : (
                            ''
                        )}
                        {!pending &&
                            userReservations !== null &&
                            tours.length !== 0 &&
                            tours.map(
                                (tour, key) =>
                                    userReservations !== null &&
                                    userReservations.tours.indexOf(tour.id) !== -1 && (
                                        <Row className={`awesome-card cards mb-5`} key={key}>
                                            <Col xs={`12`} sm={`12`} md={`6`} lg={`4`} className={`mb-3`}>
                                                <TourCard tour={tour} />
                                            </Col>
                                            <Col xs={12} md={6} lg={8} className={`mb-3`}>
                                                <div className={`tour-reservation`}>
                                                    <div className={`tr-info`}>
                                                        <div>
                                                            {tour.reservation.status.description !== null ? (
                                                                <div className={`mb-3`}>
                                                                    <Badge variant={tour.reservation.status.type} className={`styled`}>
                                                                        {tour.reservation.status.title}
                                                                    </Badge>
                                                                    <p className={`text-muted mt-1`}>{tour.reservation.status.description}</p>
                                                                </div>
                                                            ) : (
                                                                <div className={`mb-3`}>
                                                                    <Badge variant={tour.reservation.status.type} className={`styled`}>
                                                                        {tour.reservation.status.title}
                                                                    </Badge>
                                                                </div>
                                                            )}
                                                        </div>
                                                        <div>
                                                            <Row>
                                                                <Col xs={12} md={12} className={``}>
                                                                    <h5>Данные о бронировании</h5>
                                                                    <p className={`text-muted`}>
                                                                        После оплаты бронирования с Вами свяжется организатор.
                                                                    </p>
                                                                </Col>
                                                                <Col xs={`12`} sm={`12`} md={`12`} lg={`12`}>
                                                                    <ReservationTable reservation={tour.reservation}/>
                                                                </Col>
                                                                <Col xs={`12`} sm={`12`} className={`mb-3`}>
                                                                    <div className={`d-flex justify-content-between`}>
                                                                        <Button variant="empty-danger-link" className={`${pendingCancel.indexOf(tour.reservation.id) !== -1 ? 'is-loading' : ''}`} onClick={() => this.handleCancel(tour.reservation.id)}>
                                                                            Отменить бронирование
                                                                        </Button>
                                                                        {tour.reservation.can_pay &&
                                                                        <Button variant="primary">
                                                                            Оплатить
                                                                        </Button>
                                                                        }
                                                                    </div>
                                                                    {/*{tour.reservation.payment !== null ? (*/}
                                                                    {/*    <span>12.03.2020 12:00:12</span>*/}
                                                                    {/*) : (*/}
                                                                    {/*    <Button variant="primary">Оплатить</Button>*/}
                                                                    {/*)}*/}
                                                                </Col>
                                                            </Row>
                                                        </div>
                                                    </div>
                                                    <div className={`tr-actions text-right`}>

                                                    </div>
                                                </div>
                                            </Col>
                                        </Row>
                                    )
                            )}
                        {!pending && tours.length === 0 && (
                            <Row>
                                <Col xs={`12`} sm={`12`} md={`12`} lg={`12`}>
                                    <Alert variant="info">
                                        <Alert.Heading>Список пуст</Alert.Heading>
                                        <p className={`mb-0`}>Забронируйте тур чтобы он появился в этом списке.</p>
                                    </Alert>
                                </Col>
                            </Row>
                        )}
                    </Container>
                </section>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: getUser(state),
    pending: getUserReservationsPending(state),
    userReservations: getUserReservations(state),
    tours: getUserReservationsTours(state),
    pendingCancel: getUserReservationsPendingArray(state),
});

const mapDispatchToProps = dispatch => ({
    fetchUserReservations: () => dispatch(fetchUserReservations()),
    fetchUserReservationCancel: (id) => dispatch(fetchUserReservationCancel(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Reservations);
