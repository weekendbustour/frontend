import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Tab from 'react-bootstrap/Tab';
import Breadcrumb from '../../containers/Layout/Breadcrumb';
import breadcrumbs from '../../breadcrumbs';
import ImageAccount from './Account/Image';
import ProfileAccount from './Account/Profile';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import SettingsAccount from './Account/Settings';
import seo from "../../lib/seo";

class Account extends React.Component {
    _isMounted = false;
    state = {};

    static propTypes = {};

    constructor(props) {
        super(props);
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
        seo.account();
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <section>
                    <Container>
                        <Breadcrumb push={breadcrumbs.account} />
                    </Container>
                </section>
                <section>
                    <Container className={`mb-5`}>
                        <Tab.Container id="left-tabs-example" defaultActiveKey="1">
                            <Row>
                                <Col sm={3}>
                                    <div className={`nav-pills`}>
                                        <Link to={`/lk/account`} className={`nav-link active`}>
                                            Личные данные
                                        </Link>
                                        {/*<hr />*/}
                                        {/*<Link to={`/lk/reservations`} className={`nav-link`}>*/}
                                        {/*    Забронированные туры*/}
                                        {/*</Link>*/}
                                        {/*<Link to={`/lk/favourites`} className={`nav-link`}>*/}
                                        {/*    Мои Избранные*/}
                                        {/*</Link>*/}
                                        {/*<Link to={`/lk/subscriptions`} className={`nav-link`}>*/}
                                        {/*    Мои Подписки*/}
                                        {/*</Link>*/}
                                    </div>
                                </Col>
                                <Col sm={9}>
                                    <Row>
                                        <Col xs={12}>
                                            <hr className={`mt-0`} />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <ImageAccount />
                                    </Row>
                                    <Row>
                                        <Col xs={12}>
                                            <hr />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <ProfileAccount />
                                    </Row>
                                    <Row>
                                        <Col xs={12}>
                                            <hr />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <SettingsAccount />
                                    </Row>
                                </Col>
                            </Row>
                        </Tab.Container>
                    </Container>
                </section>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
});
export default connect(mapStateToProps)(Account);
