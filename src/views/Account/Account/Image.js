import React from 'react';
import Col from 'react-bootstrap/Col';
import Image from 'react-bootstrap/Image';
import Button from 'react-bootstrap/Button';
import {getUser} from '../../../store/reducers/User';
import {connect} from 'react-redux';
import Modal from 'react-bootstrap/Modal';
import Row from 'react-bootstrap/Row';
import {MODAL_USER_ACCOUNT_IMAGE} from '../../../lib/modal';
import {fetchUserAccountImage, fetchUserAccountImageDelete} from '../../../store/actions/User/AccountProfile';
import {fetchModal, getModal} from '../../../store/reducers/Modal';
import {getUserAccountProfilePendingImage, getUserAccountProfilePendingImageDelete} from '../../../store/reducers/User/AccountProfile';
import * as PropTypes from 'prop-types';
import {userTypes} from '../../../prop/Types/User';

const Cropper = require('cropperjs');

class ImageAccount extends React.Component {
    _NAME = MODAL_USER_ACCOUNT_IMAGE;
    _isMounted = false;
    state = {};
    cropper = null;

    static propTypes = {
        user: userTypes,
        modal: PropTypes.string,
        pendingImage: PropTypes.bool,
        pendingImageDelete: PropTypes.bool,
        fetchUserAccountImage: PropTypes.func,
        fetchModal: PropTypes.func,
        fetchUserAccountImageDelete: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.state = {
            src: '',
            image: props.user.image,
            preview: null,

            file: null,
            cropData: {},
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.onCrop = this.onCrop.bind(this);
        this.onClose = this.onClose.bind(this);
        this.onShowModal = this.onShowModal.bind(this);
        this.onSaveCrop = this.onSaveCrop.bind(this);
        this.setShow = this.setShow.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentDidMount() {
        const self = this;
        //const image = document.getElementById('image');
        const input = document.getElementById('input');
        input.addEventListener('change', function(e) {
            const files = e.target.files;
            let done = function(url) {
                input.value = '';
                self.setState({
                    preview: url,
                });
                self.setShow(self._NAME);
            };
            let reader, file;
            if (files && files.length > 0) {
                file = files[0];
                self.setState({file});
                if (URL) {
                    done(URL.createObjectURL(file));
                } else if (FileReader) {
                    reader = new FileReader();
                    reader.onload = function(e) {
                        done(reader.result);
                    };
                    reader.readAsDataURL(file);
                }
            }
        });
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    onClose() {
        this.setState({preview: null});
    }

    onCrop(preview) {
        this.setState({preview});
    }

    onShowModal() {
        const self = this;
        const image = document.getElementById('image');
        self.cropper = new Cropper(image, {
            aspectRatio: 1,
            viewMode: 3,
            checkCrossOrigin: false,
            autoCropArea: 1,
            crop(event) {
                self.setState({
                    cropData: {
                        x: event.detail.x,
                        y: event.detail.y,
                        width: event.detail.width,
                        height: event.detail.height,
                        rotate: event.detail.rotate,
                        scaleX: event.detail.scaleX,
                        scaleY: event.detail.scaleY,
                    },
                });
            },
        });
    }

    onSaveCrop() {
        const {file, cropData} = this.state;
        const {fetchUserAccountImage} = this.props;
        fetchUserAccountImage(cropData, file);
    }

    setShow(value) {
        const {fetchModal} = this.props;
        fetchModal(value);
    }

    imageDelete() {
        const {fetchUserAccountImageDelete} = this.props;
        fetchUserAccountImageDelete();
    }

    render() {
        const {user, modal, pendingImage, pendingImageDelete} = this.props;
        const {preview} = this.state;
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <Col xs={12} md={4}>
                    <h5>Аватар</h5>
                    <p className={`text-muted`}>Вы можете изменить свой аватар или удалить и использовать аватар по умолчанию</p>
                </Col>
                <Col xs={12} md={4}>
                    <div className={`mb-3`}>
                        <Image src={user.image} roundedCircle width={`200px`} height={200} alt=""/>
                    </div>
                </Col>
                <Col xs={12} md={4}>
                    <h6>Загрузить новое изображение</h6>
                    <div>
                        <input type="file" id="input" name="image" accept="image/*" />
                        <label htmlFor="input" className="btn btn-primary">
                            <span>Выбрать изображение</span>
                        </label>
                    </div>
                    {/*<label className="label" data-toggle="tooltip" title="Выберите файл">*/}
                    {/*    <input type="file" className="btn btn-primary" id="input" name="image" accept="image/*" style={{fontSize: '12px', width: '100%'}}/>*/}
                    {/*</label>*/}
                    <p className={`text-muted mt-2`}>Максимальный размер файла 2 MB.</p>
                    <Button variant="danger" type="button" className={`${pendingImageDelete ? 'is-loading' : ''} `} onClick={() => this.imageDelete()}>
                        Удалить изображение
                    </Button>
                </Col>
                <Modal
                    size="lg"
                    show={modal === this._NAME}
                    onHide={() => this.setShow(null)}
                    onShow={this.onShowModal}
                    aria-labelledby="contained-modal-title-vcenter"
                    centered
                >
                    <Modal.Header closeButton>
                        <Modal.Title id="contained-modal-title-vcenter">Обрезать изображение</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Row>
                            <Col>
                                <div className="d-flex align-content-center justify-content-center">
                                    <img id="image" src={preview} width={`100%`} alt="" />
                                </div>
                            </Col>
                        </Row>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant={`default`} onClick={() => this.setShow(null)}>
                            Закрыть
                        </Button>
                        <Button variant={`primary`} onClick={this.onSaveCrop} className={`${pendingImage ? 'is-loading' : ''} `}>
                            Сохранить
                        </Button>
                    </Modal.Footer>
                </Modal>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: getUser(state),
    modal: getModal(state),
    pendingImage: getUserAccountProfilePendingImage(state),
    pendingImageDelete: getUserAccountProfilePendingImageDelete(state),
});

const mapDispatchToProps = dispatch => ({
    fetchUserAccountImage: (cropData, file) => dispatch(fetchUserAccountImage(cropData, file)),
    fetchUserAccountImageDelete: () => dispatch(fetchUserAccountImageDelete()),
    fetchModal: name => dispatch(fetchModal(name)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ImageAccount);
