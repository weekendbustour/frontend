import React from 'react';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import {fetchUserLogout} from '../../../store/reducers/User';
import {connect} from 'react-redux';
import * as PropTypes from 'prop-types';

class SettingsAccount extends React.Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        fetchUserLogout: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.authLogout = this.authLogout.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    authLogout() {
        const {fetchUserLogout} = this.props;
        fetchUserLogout();
    }

    render() {
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <Col xs={12} md={4}>
                    <h5>Аккаунт</h5>
                    <p className={`text-muted`}>Вы можете удалить свой аккаунт, ваши данные так же удаляться с сайта.</p>
                </Col>
                <Col xs={12} md={8}>
                    <Button variant="danger" type="submit" onClick={() => this.authLogout()}>
                        Выйти
                    </Button>
                    <hr />
                    <Button variant="outline-danger" type="submit">
                        Удалить профиль
                    </Button>
                    <p className={`text-muted mt-2`}>
                        Профиль полностью удалится после 24х часов с момента отправки запроса. Такое необходимо, если вдруг вы передумаете. Аккаунт можно будет
                        восстановить по ссылке в письме, которые мы вам отправим.
                    </p>
                </Col>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
    fetchUserLogout: () => dispatch(fetchUserLogout()),
});

export default connect(mapStateToProps, mapDispatchToProps)(SettingsAccount);
