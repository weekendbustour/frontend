import React from 'react';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import {getUser} from '../../../store/reducers/User';
import {connect} from 'react-redux';
import InputGroup from 'react-bootstrap/InputGroup';
import store from '../../../store';
import {fetchUserAccountProfile} from '../../../store/actions/User/AccountProfile';
import {getUserAccountProfileErrors, getUserAccountProfilePending} from '../../../store/reducers/User/AccountProfile';
import {Link} from 'react-router-dom';
import * as PropTypes from 'prop-types';
import {userTypes} from '../../../prop/Types/User';
const moment = require('moment');
require('moment/locale/ru');

class ProfileAccount extends React.Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        user: userTypes,
        pending: PropTypes.bool,
        first_name: PropTypes.string,
        last_name: PropTypes.string,
        second_name: PropTypes.string,
        email: PropTypes.string,
        phone: PropTypes.string,
        validation: PropTypes.object,
    };

    constructor(props) {
        super(props);
        this.state = {
            first_name: props.user.first_name,
            last_name: props.user.last_name,
            second_name: props.user.second_name,
            email: props.user.email,
            phone: props.user.phone,
            day: props.user.birthday_at !== null ? moment(props.user.birthday_at, 'DD.MM.YYYY').format('DD') : '',
            month: props.user.birthday_at !== null ? moment(props.user.birthday_at, 'DD.MM.YYYY').format('MM') : '',
            year: props.user.birthday_at !== null ? moment(props.user.birthday_at, 'DD.MM.YYYY').format('YYYY') : '',
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    handleChange(e) {
        this.setState({
            [e.currentTarget.name]: e.currentTarget.value,
        });
    }

    handleSubmit(e) {
        e.preventDefault();
        const {first_name, last_name, second_name, email, phone, day, month, year} = this.state;
        const data = {
            first_name: first_name,
            last_name: last_name,
            second_name: second_name,
            email: email,
            phone: phone,
            day: day,
            month: month,
            year: year,
        };
        store.dispatch(fetchUserAccountProfile(data));
    }

    render() {
        const {first_name, last_name, second_name, email, phone, day, month, year} = this.state;
        const {pending, validation} = this.props;
        console.log(validation);
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <Col xs={12} md={4}>
                    <h5>Данные</h5>
                    <p className={`text-muted`}>
                        Ваши личные данные, поля без <i className="r">*</i> не обязательны к заполнению. <br />
                        Ваши данные полностью конфиденциальны, подробности можно узнать на странице <Link to={`/about`}>Положением о конфиденциальности</Link>
                    </p>
                </Col>
                <Col xs={12} md={8}>
                    <Form onSubmit={this.handleSubmit}>
                        <Form.Group>
                            <Form.Label>
                                Имя <i className="r">*</i>
                            </Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Имя"
                                value={first_name}
                                name="first_name"
                                onChange={this.handleChange}
                                className={[validation !== null && validation.first_name !== undefined ? `is-invalid` : ``]}
                                required
                            />
                            {validation !== null && validation.first_name !== undefined && (
                                <Form.Control.Feedback type="invalid">{validation.first_name}</Form.Control.Feedback>
                            )}
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Фамилия</Form.Label>
                            <Form.Control placeholder="Фамилия" value={last_name} name="last_name" onChange={this.handleChange} />
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Отчество</Form.Label>
                            <Form.Control placeholder="Отчество" value={second_name} name="second_name" onChange={this.handleChange} />
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>
                                Email <i className="r">*</i>
                            </Form.Label>
                            <Form.Control
                                type="email"
                                placeholder="Email"
                                value={email}
                                name="email"
                                onChange={this.handleChange}
                                className={[validation !== null && validation.email !== undefined ? `is-invalid` : ``]}
                                required
                            />
                            {validation !== null && validation.email !== undefined && (
                                <Form.Control.Feedback type="invalid">{validation.email}</Form.Control.Feedback>
                            )}
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Телефон</Form.Label>
                            <Form.Control
                                placeholder="Телефон"
                                value={phone}
                                name="phone"
                                onChange={this.handleChange}
                                className={[validation !== null && validation.phone !== undefined ? `is-invalid` : ``, `cleave-mask--phone`]}
                            />
                            {validation !== null && validation.phone !== undefined && (
                                <Form.Control.Feedback type="invalid">{validation.phone}</Form.Control.Feedback>
                            )}
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Дата рождения</Form.Label>
                            <InputGroup>
                                <Form.Control
                                    type="text"
                                    className={`cleave-mask--number`}
                                    data-length={2}
                                    placeholder="День"
                                    name="day"
                                    value={day}
                                    onChange={this.handleChange}
                                />
                                <Form.Control
                                    type="text"
                                    className={`cleave-mask--number`}
                                    data-length={2}
                                    placeholder="Месяц"
                                    name="month"
                                    value={month}
                                    onChange={this.handleChange}
                                />
                                <Form.Control
                                    type="text"
                                    className={`cleave-mask--number`}
                                    data-length={4}
                                    placeholder="Год"
                                    name="year"
                                    value={year}
                                    onChange={this.handleChange}
                                />
                            </InputGroup>
                        </Form.Group>

                        <Button variant="primary" type="submit" className={`${pending ? 'is-loading' : ''} `}>
                            Сохранить
                        </Button>
                    </Form>
                </Col>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: getUser(state),
    pending: getUserAccountProfilePending(state),
    validation: getUserAccountProfileErrors(state),
});
export default connect(mapStateToProps)(ProfileAccount);
