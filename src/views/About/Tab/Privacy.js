import React from 'react';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import {getHelpOfferPending} from '../../../store/reducers/Help';
import {fetchHelpOffer} from '../../../store/actions/Help';
import {connect} from 'react-redux';
import * as PropTypes from 'prop-types';

class Privacy extends React.Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        pending: PropTypes.bool,
        fetchHelpOffer: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.state = {
            description: '',
            radio: '',
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    handleChange(e) {
        this.setState({
            [e.currentTarget.name]: e.currentTarget.value,
        });
    }

    handleSubmit(e) {
        e.preventDefault();
        const {fetchHelpOffer} = this.props;
        const {description, radio} = this.state;
        const data = {
            description: description,
            radio: radio,
        };
        fetchHelpOffer(data);
    }
    render() {
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <h3>Положение о конфиденциальности</h3>
                <Row>
                    <Col xs={12} md={12}>
                        <p>
                            Мы уделяем большое внимание конфиденциальности ваших данных.
                            Воспользовавшись услугами {process.env.REACT_APP_NAME_OFFICIAL}, вы оказали нам доверие, и мы это ценим.
                            Мы прилагаем все усилия, чтобы сохранить ваши личные данные в безопасности.
                            Мы действуем в интересах наших клиентов и не скрываем информации об обработке ваших персональных данных.
                        </p>
                        <p>
                            Если вы не согласны с этим Положением о конфиденциальности, мы просим вас воздержаться от пользования нашими услугами.
                            Если вы согласны с нашим Положением о конфиденциальности, тогда вы готовы организовать следующую свою Поездку с нашей помощью.
                        </p>
                        <h5>Какие персональные данные собирает {process.env.REACT_APP_NAME_OFFICIAL}?</h5>
                        <p>
                            Когда вы пользуетесь нашими услугами, мы запрашиваем у вас определенную информацию: ваше имя, предпочитаемые контактные данные.
                            Кроме того, мы собираем некоторые данные с вашего компьютера, телефона, планшета или иного устройства, которое вы используете для доступа к нашим услугами, например, IP-адрес, тип используемого браузера и языковые настройки.
                            Бывают ситуации, когда мы получаем информацию о вас от третьих сторон или автоматически собираем другую информацию.
                        </p>
                    </Col>
                </Row>
            </React.Fragment>
        );
    }
}
const mapStateToProps = state => ({
    pending: getHelpOfferPending(state),
});

const mapDispatchToProps = dispatch => ({
    fetchHelpOffer: data => dispatch(fetchHelpOffer(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Privacy);
