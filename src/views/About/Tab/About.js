import React from 'react';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import {connect} from 'react-redux';
import * as PropTypes from 'prop-types';
import {fetchModal, getModal} from "../../../store/reducers/Modal";

class About extends React.Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        modal: PropTypes.string,
        fetchModal: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.state = {
            description: '',
            radio: '',
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentWillMount() {
        const {modal, fetchModal} = this.props;
        this._isMounted = true;
        if (modal !== null) {
            fetchModal(null);
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <h3>Основное</h3>
                <Row>
                    <Col xs={12} md={12}>
                        <p>
                            Компания {process.env.REACT_APP_NAME_OFFICIAL}, основанная в 2020 году в г. Ростов-на-Дону, начинает путь свой с маленького стартапа, где будут собраны
                            все туры на Юг России.
                        </p>
                        <p>
                            Мы предлагаем пользователям качественных организаторов, потрясающие варианты досуга и лучшие варианты отдыха, начиная от
                            однодневных поездок и заканчивая 5ти дневными турами с походами и с незабываемыми приключениями.
                            Будучи начинающей туристической платформой, {process.env.REACT_APP_NAME_OFFICIAL} помогает организаторам искать клиентов, придумывать новые
                            развлекательные программы и совершать безопасные сделки.
                        </p>
                    </Col>
                </Row>
            </React.Fragment>
        );
    }
}
const mapStateToProps = state => ({
    modal: getModal(state),
});

const mapDispatchToProps = dispatch => ({
    fetchModal: name => dispatch(fetchModal(name)),
});

export default connect(mapStateToProps, mapDispatchToProps)(About);
