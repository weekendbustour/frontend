import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Tab from 'react-bootstrap/Tab';
import Nav from 'react-bootstrap/Nav';
import Breadcrumb from '../../containers/Layout/Breadcrumb';
import breadcrumbs from '../../breadcrumbs';
import seo from '../../lib/seo';
import {Link} from 'react-router-dom';
import AboutTab from './Tab/About';
import PrivacyTab from './Tab/Privacy';
import TermsTab from "./Tab/Terms";

class About extends React.Component {
    static propTypes = {};

    constructor(props) {
        super(props);
        this.state = {
            tab: props.location.pathname,
        };
    }

    componentDidMount() {
        const {tab} = this.state;
        if (tab === '/about') {
            seo.about();
        }
        if (tab === '/privacy') {
            seo.privacy();
        }
    }

    render() {
        const {tab} = this.state;
        return (
            <React.Fragment>
                <section>
                    <Container>
                        <Breadcrumb push={breadcrumbs.about} />
                    </Container>
                </section>
                <section>
                    <Container className={`mb-5`} style={{minHeight: '500px'}}>
                        <Tab.Container id="left-tabs-example">
                            <Row>
                                <Col sm={3} className={`aw-tabs-nav-pills`}>
                                    <Nav variant="pills" className="flex-column mb-3">
                                        <Link to={`/about`} className={`nav-item nav-link ${tab === '/about' ? 'active' : ''}`}>
                                            Основное
                                        </Link>
                                        <Link to={`/privacy`} className={`nav-item nav-link ${tab === '/privacy' ? 'active' : ''}`}>
                                            Положение о конфиденциальности
                                        </Link>
                                        <Link to={`/terms`} className={`nav-item nav-link ${tab === '/terms' ? 'active' : ''}`}>
                                            Пользовательское соглашение
                                        </Link>
                                    </Nav>
                                </Col>
                                <Col sm={9}>
                                    <Tab.Content>
                                        <Tab.Pane eventKey="about" className={`${tab === '/about' ? 'show active' : ''}`}>
                                            <AboutTab />
                                        </Tab.Pane>
                                        <Tab.Pane eventKey="privacy" className={`${tab === '/privacy' ? 'show active' : ''}`}>
                                            <PrivacyTab />
                                        </Tab.Pane>
                                        <Tab.Pane eventKey="terms" className={`${tab === '/terms' ? 'show active' : ''}`}>
                                            <TermsTab />
                                        </Tab.Pane>
                                    </Tab.Content>
                                </Col>
                            </Row>
                        </Tab.Container>
                    </Container>
                </section>
            </React.Fragment>
        );
    }
}

export default About;
