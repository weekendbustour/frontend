import React, {Component} from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Breadcrumb from '../../containers/Layout/Breadcrumb';
import 'react-dates/initialize';
import breadcrumbs from '../../breadcrumbs';
import SkeletonPage from '../../containers/Skeleton/Article/Page';
import {connect} from 'react-redux';
import * as PropTypes from 'prop-types';
import Main from './Components/Main';
import ArticleTextContainer from './Components/ArticleTextContainer';
import ArticleRecommended from './Components/ArticleRecommended';
import ArticleComments from "./Components/ArticleComments";
import {getArticle, getArticlePending} from "../../store/reducers/Article";
import {fetchArticle} from "../../store/actions/Article";
import {articleTypes} from "../../prop/Types/Article";
import ArticleSlider from "./Components/ArticleSlider";
import ArticleMap from "./Components/ArticleMap";
import ArticleAttractions from "./Components/ArticleAttractions";

class Article extends Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        article: articleTypes,
        articleLoading: PropTypes.bool,
        fetchArticle: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.state = {};
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentDidMount() {
        const {fetchArticle} = this.props;
        fetchArticle(this.props.match.params.id);
        this._isMounted = true;
    }

    componentDidUpdate(prevProps) {
        const {article, articleLoading, match, fetchArticle} = this.props;
        const id = parseInt(match.params.id);
        if (article.id !== id && !articleLoading) {
            fetchArticle(id);
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {article, articleLoading} = this.props;
        if (!this.shouldComponentRender()) return null;
        return (
            <React.Fragment>
                {articleLoading && (
                    <section>
                        <SkeletonPage />
                    </section>
                )}
                {/* Хлебные крошки */}
                <section>
                    {!articleLoading && (
                        <Container>
                            <Breadcrumb parent={breadcrumbs.articles} push={{
                                title: `${article.type === 'top' ? '#' + article.number + ' ' : ''} ${article.title}`,
                            }} />
                        </Container>
                    )}
                </section>
                {/* Баннер */}
                <section>
                    {!articleLoading && (
                        <Container className={`mb-2 mt-0 page__snipe`}>
                            {article.with_banner &&
                            <Row>
                                <Col xs={12}>
                                    <Main article={article} />
                                </Col>
                            </Row>
                            }
                            <Row>
                                <Col xs={12} md={12} lg={{span: 8, offset: 2}} className={`mt-3`}>
                                    <ArticleTextContainer article={article} />
                                </Col>
                            </Row>
                            <ArticleRecommended article={article} />
                        </Container>
                    )}
                </section>
                {/* Видео */}
                {/*<section>{!articleLoading && <ArticleVideo article={article} />}</section>*/}
                {/* Слайдер */}
                <section>{!articleLoading && <ArticleSlider article={article} />}</section>
                {/* Карта */}
                <section>
                    {!articleLoading && article.location !== null && (
                        <Container>
                            <ArticleMap article={article} />
                        </Container>
                    )}
                </section>
                {/* Статьи */}
                <section>
                    {!articleLoading && (
                        <Container>
                            <ArticleAttractions article={article} />
                        </Container>
                    )}
                </section>
                {/* Комментарии */}
                <section>
                    {!articleLoading && (
                        <Container>
                            <Row className={`--container-article-comments`}>
                                <Col xs={12}>
                                    <ArticleComments article={article} />
                                </Col>
                            </Row>
                        </Container>
                    )}
                </section>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    article: getArticle(state),
    articleLoading: getArticlePending(state),
});

const mapDispatchToProps = dispatch => ({
    fetchArticle: id => dispatch(fetchArticle(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Article);
