import React, {Component} from 'react';
import 'react-dates/initialize';
import {connect} from 'react-redux';
import {isMobile} from 'react-device-detect';
import FsLightbox from 'fslightbox-react';
import {fsLightboxHelper} from '../../../lib/fsLightboxHelper';
import {articleTypes} from "../../../prop/Types/Article";

class Main extends Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        article: articleTypes,
    };

    constructor(props) {
        super(props);
        this.state = {
            toggleBanner: false,
            article: props.article,
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.setToggleImageBanner = this.setToggleImageBanner.bind(this);
        this.handleClearTitles = this.handleClearTitles.bind(this);
    }

    setToggleImageBanner() {
        const {toggleBanner} = this.state;
        this.setState({toggleBanner: !toggleBanner});
    }
    handleClearTitles() {
        fsLightboxHelper.clearTitles();
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {article, toggleBanner} = this.state;
        if (!this.shouldComponentRender()) return null;
        return (
            <React.Fragment>
                <div className="page__snipe--img">
                    <div className={`page__snipe--background`} style={{backgroundImage: 'url(' + article.image.banner + ')'}} onClick={this.setToggleImageBanner}/>
                </div>
                <div className={`flight-box-custom ${isMobile ? 'is-mobile' : 'is-desktop'}`}>
                    <FsLightbox
                        type="image"
                        toggler={toggleBanner}
                        sources={[article.image.banner]}
                        slide={0}
                        thumbs={[article.image.banner]}
                        onOpen={this.handleClearTitles}
                    />
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Main);
