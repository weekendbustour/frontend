import React, {Component} from 'react';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import {FullscreenControl, GeolocationControl, Map, RouteButton, TypeSelector, YMaps, ZoomControl, ObjectManager} from 'react-yandex-maps';
import {articleTypes} from '../../../prop/Types/Article';

/**
 * https://tech.yandex.com/maps/jsapi/doc/2.1/ref/reference/ObjectManager-docpage/
 *
 * https://tech.yandex.com/maps/jsapi/doc/2.1/ref/reference/Placemark-docpage/
 * balloonContentHeader
 * balloonContentBody
 * balloonContentFooter
 */
class ArticleMap extends Component {
    static propTypes = {
        article: articleTypes,
    };
    constructor(props) {
        super(props);
        this.state = {
            article: props.article,
        };
    }

    render() {
        const {article} = this.state;
        let features = [];
        // eslint-disable-next-line
        article.locations.map((location, key) => {
            let properties = {};
            if (location.title !== null && location.description !== null) {
                properties.balloonContentBody = location.title;
                properties.balloonContentFooter = location.description;
            } else if (location.title !== null && location.description === null) {
                properties.balloonContentBody = location.title;
            } else if (location.title === null && location.description !== null) {
                properties.balloonContentFooter = location.description;
            }
            if (location.placeholder !== null) {
                properties.hintContent = location.placeholder;
            }
            features.push({
                type: 'Feature',
                id: key,
                geometry: {
                    type: 'Point',
                    coordinates: location.point,
                },
                properties: properties,
                // properties: {
                //     //hintContent: 'Popup hint text',
                //     balloonContent: 'Balloon content',
                //     //balloonContentBody: 'Body',
                //     //balloonContentFooter: 'Footer',
                //     //balloonContentHeader: 'Header',
                // },
            });
        });
        console.log(features);
        return (
            <React.Fragment>
                <Row>
                    <Col xs={12} md={12} style={{marginTop: '30px'}}>
                        <YMaps>
                            <div>
                                <Map
                                    defaultState={{
                                        center: article.location.point,
                                        zoom: article.location.zoom,
                                        controls: [],
                                        //behaviors: ['ScrollZoom'],
                                    }}
                                    width={`100%`}
                                >
                                    <ObjectManager
                                        options={{
                                            clusterize: false,
                                            gridSize: 32,
                                        }}
                                        objects={{
                                            openBalloonOnClick: true,
                                            //preset: 'islands#greenDotIcon',
                                        }}
                                        clusters={
                                            {
                                                //preset: 'islands#redClusterIcons',
                                            }
                                        }
                                        // filter={object => object.id % 2 === 0}
                                        defaultFeatures={features}
                                        modules={['objectManager.addon.objectsBalloon', 'objectManager.addon.objectsHint']}
                                    />
                                    {/*{article.locations.map((location, key) => (*/}
                                    {/*    <Placemark geometry={location.point} key={key}/>*/}
                                    {/*))}*/}
                                    <ZoomControl options={{float: 'right'}} />
                                    <TypeSelector options={{float: 'right'}} />
                                    <GeolocationControl options={{float: 'left'}} />
                                    <RouteButton options={{float: 'right'}} />
                                    <FullscreenControl />
                                </Map>
                            </div>
                        </YMaps>
                    </Col>
                </Row>
            </React.Fragment>
        );
    }
}

export default ArticleMap;
