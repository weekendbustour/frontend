import React, {Component} from 'react';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import {articleTypes} from "../../../prop/Types/Article";

class ArticleParameters extends Component {
    static propTypes = {
        article: articleTypes,
    };
    constructor(props) {
        super(props);
        this.state = {
            article: props.article,
        };
    }
    render() {
        const {article} = this.state;
        return (
            <React.Fragment>
                {article.parameters.distance_from_rostov !== undefined || article.parameters.distance_from_krasnodar !== undefined ? (
                    <Row className={`mt-3`}>
                        <Col xs={12}>
                            <h3>Расстояние</h3>
                        </Col>
                        <Col xs={12}>
                            {article.parameters.distance_from_rostov !== undefined &&
                            <p className={`mb-0`}>
                                {article.parameters.distance_from_rostov.title} - <b>{article.parameters.distance_from_rostov.value}</b>
                            </p>
                            }
                            {article.parameters.distance_from_krasnodar !== undefined &&
                            <p className={`mb-0`}>
                                {article.parameters.distance_from_krasnodar.title} - <b>{article.parameters.distance_from_krasnodar.value}</b>
                            </p>
                            }
                        </Col>
                    </Row>
                ) : ('')}
            </React.Fragment>
        );
    }
}

export default ArticleParameters;
