import React, {Component} from 'react';
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import {Link} from "react-router-dom";
import {articleTypes} from "../../../prop/Types/Article";

class ArticleRecommended extends Component {
    static propTypes = {
        article: articleTypes,
    };
    constructor(props) {
        super(props);
        this.state = {
            article: props.article,
        };
    }
    render() {
        const {article} = this.state;
        return (
            <React.Fragment>
                {article.recommended.previous !== null || article.recommended.next !== null ? (
                    <Row>
                        <Col xs={12} md={12} lg={{span: 8, offset: 2}} className={`mt-3`}>
                            <Row>
                                <Col xs={6} className={`text-left`}>
                                    {article.recommended.previous !== null && (
                                        <div>
                                            <div>Предыдущая статья</div>
                                            <div>
                                                <Link to={`/article/${article.recommended.previous.id}/${article.recommended.previous.name}`}>
                                                    {article.recommended.previous.type === 'top' && `#${article.recommended.previous.number} `}
                                                    {article.recommended.previous.title}
                                                </Link>
                                            </div>
                                        </div>
                                    )}
                                </Col>
                                <Col xs={6} className={`text-right`}>
                                    {article.recommended.next !== null && (
                                        <div>
                                            <div>Следующая статья</div>
                                            <div>
                                                <Link to={`/article/${article.recommended.next.id}/${article.recommended.next.name}`}>
                                                    {article.recommended.next.type === 'top' && `#${article.recommended.next.number} `}
                                                    {article.recommended.next.title}
                                                </Link>
                                            </div>
                                        </div>
                                    )}
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                ) : (
                    ''
                )}
            </React.Fragment>
        );
    }
}

export default ArticleRecommended;
