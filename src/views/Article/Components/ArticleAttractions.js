import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import {connect} from 'react-redux';
import {TITLE_ARTICLES_ATTRACTIONS} from '../../../lib/titles';
import TitleMain from '../../../containers/Layout/TitleMain';
import {articleTypes} from "../../../prop/Types/Article";
import Card from "../../../containers/Attraction/Card/Card";

class ArticleAttractions extends React.Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        article: articleTypes,
    };

    constructor(props) {
        super(props);
        this.state = {
            article: props.article,
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {article} = this.props;
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                {article.attractions.length !== 0 && (
                    <Container>
                        <Row className={`mb-5 mt-5`}>
                            <TitleMain title={`Достопримечательности`} model={TITLE_ARTICLES_ATTRACTIONS} role={`h2`} />
                        </Row>
                        <Row>
                            {article.attractions.map((attraction, key) => (
                                <Col xs={12} sm={6} md={6} lg={4} xl={3} key={key}>
                                    <Card attraction={attraction}/>
                                </Col>
                            ))}
                        </Row>
                        {/*<AttractionCardModal />*/}
                    </Container>
                )}
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({});
export default connect(mapStateToProps)(ArticleAttractions);
