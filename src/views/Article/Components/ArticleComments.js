import React, {Component} from 'react';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import SkeletonCommentItem from '../../../containers/Skeleton/Comment/Item';
import CommentsList from '../../../components/Comments/List';
import Button from 'react-bootstrap/Button';
import CommentLimitModal from '../../../components/Comments/Modal/Limit';
import {fetchModal} from '../../../store/reducers/Modal';
import {fetchUserComment, getUserCommentPending} from '../../../store/reducers/User/Comment';
import {fetchComments, getCommentsArticle, getCommentsPending} from '../../../store/reducers/Comments';
import {connect} from 'react-redux';
import {commentsTypes} from '../../../prop/Types/Comment';
import * as PropTypes from 'prop-types';
import Form from 'react-bootstrap/Form';
import store from '../../../store';
import {articleTypes} from "../../../prop/Types/Article";

class ArticleComments extends Component {
    static propTypes = {
        article: articleTypes,
        comments: commentsTypes,
        commentsPending: PropTypes.bool,
        commentPending: PropTypes.bool,
        fetchModal: PropTypes.func,
        fetchComments: PropTypes.func,
    };
    constructor(props) {
        super(props);
        this.state = {
            article: props.article,
            text: '',
            model: 'article',
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {
        this._isMounted = true;
        const {fetchComments, article} = this.props;
        fetchComments(article.id, 'article');
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    setShow(value) {
        const {fetchModal} = this.props;
        fetchModal(value);
    }

    handleChange(e) {
        this.setState({
            [e.currentTarget.name]: e.currentTarget.value,
        });
    }

    handleSubmit(e) {
        e.preventDefault();
        const {text, model, article} = this.state;
        const data = {
            id: article.id,
            text: text,
            model: model,
        };
        store.dispatch(fetchUserComment(data));
    }

    render() {
        const {comments, commentsPending, commentPending} = this.props;
        const {article, text} = this.state;
        return (
            <React.Fragment>
                <Row className={`mt-5 mb-5`}>
                    <Col xs={12} md={12} lg={{span: 8, offset: 2}}>
                        <h3>Комментарии ({comments.filter(comment => comment.anon !== 1).length})</h3>
                    </Col>
                    <Col xs={12}>
                        {commentsPending && (
                            <Row className={`mt-3`}>
                                <Col xs={12} md={12} lg={{span: 8, offset: 2}}>
                                    <SkeletonCommentItem />
                                </Col>
                            </Row>
                        )}
                        {!commentsPending && (
                            <Row className={`page-model-comments`}>
                                <Col xs={12} md={12} lg={{span: 8, offset: 2}} className={`mb-5`}>
                                    <Form onSubmit={this.handleSubmit}>
                                        <Form.Group>
                                            <Form.Label>Комментарий</Form.Label>
                                            <Form.Control
                                                as="textarea"
                                                rows="2"
                                                placeholder="Комментарий"
                                                name="text"
                                                value={text}
                                                onChange={this.handleChange}
                                            />
                                        </Form.Group>
                                        <Button variant="primary" type="submit" className={commentPending ? `is-loading` : ``}>
                                            Отправить
                                        </Button>
                                    </Form>
                                </Col>
                                <Col xs={12} md={12} lg={{span: 8, offset: 2}} className={`mb-5`}>
                                    {comments.length !== 0 && <CommentsList comments={comments} item={article} model={`article`} rateable={false} />}
                                    {comments.length === 0 && <div>Комментарий нет</div>}
                                </Col>
                            </Row>
                        )}
                    </Col>
                    <CommentLimitModal />
                </Row>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    comments: getCommentsArticle(state),
    commentsPending: getCommentsPending(state),
    commentPending: getUserCommentPending(state),
});

const mapDispatchToProps = dispatch => ({
    fetchModal: name => dispatch(fetchModal(name)),
    fetchComments: (id, model) => dispatch(fetchComments(id, model)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ArticleComments);
