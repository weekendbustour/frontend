import React, {Component} from 'react';
import {isMobile} from "react-device-detect";
import ReactMarkdown from "../../../plugins/ReactMarkdown";
import Badge from "react-bootstrap/Badge";
import ArticleStatistic from "../../../containers/Article/Components/ArticleStatistic";
import ArticleParameters from "./../Components/ArticleParameters";
import {articleTypes} from "../../../prop/Types/Article";
import {Link} from "react-router-dom";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import ArticleVideoInText from "./ArticleVideoInText";
import SourceDefault from "../../../components/Source/SourceDefault";

class ArticleTextContainer extends Component {
    static propTypes = {
        article: articleTypes,
    };
    constructor(props) {
        super(props);
        this.state = {
            article: props.article,
        };
    }
    render() {
        const {article} = this.state;
        return (
            <React.Fragment>
                <div className="page__snipe--text-container">
                    <div className={`d-flex align-items-center`}>
                        <div className="text-muted">{article.published_at_format}</div>
                        <div className="page__snipe--socials">
                            <ArticleStatistic article={article} variant={`light-dark`} size={isMobile ? `md` : `md`}/>
                        </div>
                    </div>
                    <h2 className={`text-left`}>
                        {article.type === 'top' && `#${article.number} `}
                        {article.title}
                    </h2>
                    <ArticleVideoInText article={article}/>
                    {/*<p className="text-left delta cardo regular italic">{article.preview_description}</p>*/}
                    <ReactMarkdown source={article.description} />
                    {article.categories.length !== 0 &&
                    <div className="page__snipe--category mb-3">
                        {article.categories.map((category, key) => (
                            <Link to={`/category/${category.id}/${category.name}`} title={`Перейти на страницу Категории`} key={key}>
                                <Badge
                                    variant="default"
                                    className={[``, category.color ? `colored` : ``]}
                                    style={{backgroundColor: category.color, borderColor: category.color}}
                                >
                                    {category.title}
                                </Badge>
                            </Link>
                        ))}
                    </div>
                    }
                    <div>
                        <ArticleParameters article={article}/>
                    </div>
                    {article.source !== null && (
                        <div>
                            <Row className={`mt-3`}>
                                <Col xs={12}>
                                    <h3>Источник</h3>
                                </Col>
                                <Col xs={12} className={`page__snipe--source`}>
                                    <SourceDefault source={article.source}/>
                                </Col>
                            </Row>
                        </div>
                    )}
                </div>
            </React.Fragment>
        );
    }
}

export default ArticleTextContainer;
