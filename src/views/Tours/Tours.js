import React, {Component} from 'react';
import Container from 'react-bootstrap/Container';
import Breadcrumb from '../../containers/Layout/Breadcrumb';
import breadcrumbs from '../../breadcrumbs';
import {getTours, getToursPagination, getToursPending} from '../../store/reducers/Tours';
import {fetchTours} from '../../store/actions/Tours';
import seo from '../../lib/seo';
import {connect} from 'react-redux';
import AwPagination from '../../containers/Layout/AwPagination';
import {scrollTo} from '../../lib/scrollTo';
import {toursTypes} from '../../prop/Types/Tour';
import * as PropTypes from 'prop-types';
import {paginationTypes} from '../../prop/Types/Pagination';
import {TITLE_TOURS} from "../../lib/titles";
import TitleMain from "../../containers/Layout/TitleMain";
import ViewButtons from "../../components/View/ViewButtons";
import {getAppViewTours} from "../../store/reducers/App/View";
import TourListPending from "../../containers/Tour/List/TourListPending";
import TourList from "../../containers/Tour/List/TourList";

class Tours extends Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        tours: toursTypes,
        pending: PropTypes.bool,
        toursView: PropTypes.string,
        pagination: paginationTypes,
        getTours: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.state = {
            tours: [],
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.onChangePage = this.onChangePage.bind(this);
    }

    componentDidMount() {
        seo.tours();
        const {location} = this.props;
        this._isMounted = true;
        let searchUrl = location.search;
        let urlData = new URLSearchParams(searchUrl);
        const page = urlData.get('page');
        this.onChangePage(page);
    }

    componentWillMount() {}

    onChangePage(page) {
        scrollTo.selectorFast('.breadcrumb');
        this.props.getTours({
            page: page !== undefined ? page : 1,
        });
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {pending, tours, pagination} = this.props;
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <section>
                    <Container>
                        <Breadcrumb push={breadcrumbs.tours} />
                    </Container>
                </section>
                <section>
                    <Container className={`mb-5 views-container`}>
                        <TitleMain title={`Туры`} model={TITLE_TOURS}/>
                        <ViewButtons/>
                    </Container>
                </section>
                {pending && (
                    <section className={`h-500`}>
                        <Container>
                            <TourListPending/>
                        </Container>
                    </section>
                )}
                {!pending && (
                    <section>
                        <Container>
                            <TourList tours={tours}/>
                        </Container>
                        {pagination !== null && pagination.total > pagination.limit && (
                            <Container className="d-flex justify-content-center mt-5">
                                {/* eslint-disable-next-line react/jsx-no-bind */}
                                <AwPagination pagination={pagination} onChangePage={page => this.onChangePage(page)}/>
                            </Container>
                        )}
                    </section>
                )}
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    tours: getTours(state),
    pagination: getToursPagination(state),
    pending: getToursPending(state),
    toursView: getAppViewTours(state),
});

const mapDispatchToProps = dispatch => ({
    getTours: data => dispatch(fetchTours(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Tours);
