import React, {Component} from 'react';
import 'react-dates/initialize';
import {fetchTour, getTour, getTourPending} from '../../../store/reducers/Tour';
import {connect} from 'react-redux';
import {fetchData} from '../../../store/actions/Data';
import {fetchModal} from '../../../store/reducers/Modal';
import * as PropTypes from 'prop-types';
import {tourTypes} from '../../../prop/Types/Tour';

import TourCard from "../../../containers/Tour/Card/Card";

class DevTourCard extends Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        tour: tourTypes,
        tourLoading: PropTypes.bool,
        getTour: PropTypes.func,
        //getData: PropTypes.func
    };

    constructor(props) {
        super(props);
        this.state = {
            toggle: false,
            canCheckTour: true,
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    setToggle(state) {
        this.setState({
            toggle: state,
        });
    }

    componentDidMount() {
        //const {getTour} = this.props;
        this.props.getTour(this.props.match.params.id);
        //getTour(this.props.match.params.id);
        this._isMounted = true;
    }
    componentDidUpdate(prevProps) {
        const {tour, tourLoading, match} = this.props;
        const id = parseInt(match.params.id);
        if (tour.id !== id && !tourLoading) {
            this.props.getTour(id);
        }
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {tour, tourLoading} = this.props;
        if (!this.shouldComponentRender()) return null;
        return (
            <React.Fragment>
                <div className={`d-flex align-items-center justify-content-center`} style={{margin: '10px', width: '350px'}}>
                    <div className={`d-flex align-items-center justify-content-center`} style={{width: '350px'}}>
                        {!tourLoading &&
                        <TourCard tour={tour} />
                        }
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    tourLoading: getTourPending(state),
    tour: getTour(state),
});
const mapDispatchToProps = dispatch => ({
    getTour: id => dispatch(fetchTour(id)),
    getData: () => dispatch(fetchData()),
    setModal: name => dispatch(fetchModal(name)),
});

export default connect(mapStateToProps, mapDispatchToProps)(DevTourCard);
