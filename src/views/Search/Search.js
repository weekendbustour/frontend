import React, {Component} from 'react';
import Container from 'react-bootstrap/Container';
import Image from 'react-bootstrap/Image';
import Breadcrumb from '../../containers/Layout/Breadcrumb';
import SearchForm from '../../containers/Layout/SearchFrom';
import breadcrumbs from '../../breadcrumbs';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import {getSearchTours, fetchSearch, getSearchPending, getSearchPagination} from '../../store/reducers/Search/index';
import {getDataSearchOrders} from '../../store/reducers/Data';
import {connect} from 'react-redux';
import Skeleton from 'react-loading-skeleton';
import Select from 'react-select';
import ToursContainer from '../Index/Container/Tours';
import imgEmpty from '../../assets/img/empty-vektor-search.jpg';
import {isMobile} from 'react-device-detect';
import Form from 'react-bootstrap/Form';
import ResultFound from '../../containers/Tour/Components/ResultFound';
import AwPagination from '../../containers/Layout/AwPagination';
import seo from '../../lib/seo';
import {scrollTo} from '../../lib/scrollTo';
import {toursTypes} from '../../prop/Types/Tour';
import * as PropTypes from 'prop-types';
import {paginationTypes} from '../../prop/Types/Pagination';
import ViewButtons from "../../components/View/ViewButtons";
import TourListPending from "../../containers/Tour/List/TourListPending";
import TourList from "../../containers/Tour/List/TourList";

class Search extends Component {
    _isMounted = false;
    state = {
        orderBy: 'nearest',
    };

    static propTypes = {
        tours: toursTypes,
        toursPending: PropTypes.bool,
        pagination: paginationTypes,
        searchOrders: PropTypes.arrayOf(
            PropTypes.shape({
                id: PropTypes.string,
                label: PropTypes.string,
            })
        ),
        fetchSearch: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.handleOrderChange = this.handleOrderChange.bind(this);
        this.handleOrderSelectChange = this.handleOrderSelectChange.bind(this);
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.onChangePage = this.onChangePage.bind(this);
    }

    componentDidMount() {
        const {location} = this.props;
        let searchUrl = location.search;
        let urlData = new URLSearchParams(searchUrl);
        if (urlData.has('scroll')) {
            scrollTo.selector('.breadcrumb');
        } else {
            scrollTo.selector('.jumbotron');
        }
        seo.search();
    }

    handleOrderSelectChange(e) {
        this.setState({[e.currentTarget.name]: e.currentTarget.value});
        this.actionSearchByOrder(e.currentTarget.value);
    }

    handleOrderChange(selected) {
        this.setState({
            orderBy: selected !== null ? selected.id : null,
        });
        this.actionSearchByOrder(selected.id);
    }

    actionSearchByOrder(value) {
        const {fetchSearch, location, history} = this.props;
        let searchUrl = location.search;
        let urlData = new URLSearchParams(searchUrl);
        urlData.set('order_by', value);
        const paras = '?' + urlData.toString();
        history.push({
            pathname: '/search',
            search: paras,
        });
        fetchSearch(paras);
    }

    actionSearchByPage(page) {
        const {fetchSearch, location, history} = this.props;
        let searchUrl = location.search;
        let urlData = new URLSearchParams(searchUrl);
        urlData.set('page', page);
        const paras = '?' + urlData.toString();
        history.push({
            pathname: '/search',
            search: paras,
        });
        fetchSearch(paras);
    }

    onChangePage(page) {
        scrollTo.selectorFast('.breadcrumb');
        this.actionSearchByPage(page);
    }

    componentWillMount() {
        this._isMounted = true;
        const {fetchSearch, location} = this.props;
        fetchSearch(location.search);
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {tours, toursPending, pagination, searchOrders} = this.props;
        const {orderBy} = this.state;
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <section>
                    <Jumbotron fluid className={`pt-3 pb-2`}>
                        <Container>
                            <SearchForm isSearch={true} />
                        </Container>
                    </Jumbotron>
                </section>
                <section>
                    <Container>
                        <Breadcrumb push={breadcrumbs.search} />
                    </Container>
                </section>
                {toursPending && (
                    <section>
                        <Container>
                            <Row>
                                <Col xs={`12`} sm={`12`} md={`12`} lg={`12`}>
                                    <p>
                                        <Skeleton count={1} width={`100px`} height={`15px`} />
                                    </p>
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`12`} lg={`4`} className={`mb-4`}>
                                    {isMobile ? (
                                        <Form.Control as="select" name={`orderBy`} value={orderBy} onChange={this.handleOrderSelectChange}>
                                            <option disabled={true} value={0}>
                                                Выберите
                                            </option>
                                            {searchOrders.map((order, idx) => (
                                                <option key={idx} value={order.id}>
                                                    {order.label}
                                                </option>
                                            ))}
                                        </Form.Control>
                                    ) : (
                                        <Select
                                            classNamePrefix={`react-select`}
                                            className={`react-select-container`}
                                            onChange={this.handleOrderChange}
                                            options={searchOrders}
                                            placeholder={`Выберите`}
                                            value={searchOrders.filter(value => {
                                                return value.id === orderBy;
                                            })}
                                        />
                                    )}
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`12`} lg={`8`} className={`mb-4 views-container`}>
                                    <ViewButtons/>
                                </Col>
                            </Row>
                        </Container>
                        <Container>
                            {toursPending &&
                                <TourListPending/>
                            }
                        </Container>
                    </section>
                )}
                {!toursPending && (
                    <section>
                        <Container>
                            <Row>
                                <Col xs={`12`} sm={`12`} md={`12`} lg={`12`}>
                                    <ResultFound total={pagination.total} />
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`12`} lg={`4`} className={`mb-4`}>
                                    {isMobile ? (
                                        <Form.Control as="select" name={`orderBy`} value={orderBy} onChange={this.handleOrderSelectChange}>
                                            <option disabled={true} value={0}>
                                                Выберите
                                            </option>
                                            {searchOrders.map((order, idx) => (
                                                <option key={idx} value={order.id}>
                                                    {order.label}
                                                </option>
                                            ))}
                                        </Form.Control>
                                    ) : (
                                        <Select
                                            classNamePrefix={`react-select`}
                                            className={`react-select-container`}
                                            onChange={this.handleOrderChange}
                                            options={searchOrders}
                                            placeholder={`Выберите`}
                                            value={searchOrders.filter(value => {
                                                return value.id === orderBy;
                                            })}
                                        />
                                    )}
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`12`} lg={`8`} className={`mb-4 views-container`}>
                                    <ViewButtons/>
                                </Col>
                            </Row>
                        </Container>
                        {tours.length !== 0 && (
                            <Container>
                                <TourList tours={tours}/>
                            </Container>
                        )}
                        {tours.length === 0 && (
                            <Container>
                                <Row className={`awesome-card cards --empty-search`}>
                                    <Col xs={`12`} sm={`12`} md={`6`} lg={`6`} className={`mb-5 __description`}>
                                        <h4 className={`mt-5 mb-5`}>Туров не найдено</h4>
                                        <p>
                                            Попробуйте выбрать другие параметры
                                            <br />
                                            или
                                            <br />
                                            посмотрите рекоммендации ниже
                                        </p>
                                    </Col>
                                    <Col xs={`12`} sm={`12`} md={`6`} lg={`6`} className={`mb-5 __image`}>
                                        <Image src={imgEmpty} height={200} alt="" />
                                    </Col>
                                </Row>
                            </Container>
                        )}
                        {pagination !== null && pagination.total > pagination.limit && (
                            <Container className="d-flex justify-content-center mt-5">
                                {/* eslint-disable-next-line react/jsx-no-bind */}
                                <AwPagination pagination={pagination} onChangePage={page => this.onChangePage(page)} location={this.props.location} />
                            </Container>
                        )}
                    </section>
                )}
                {!toursPending && tours.length === 0 && (
                    <section>
                        <ToursContainer />
                    </section>
                )}
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    tours: getSearchTours(state),
    pagination: getSearchPagination(state),
    toursPending: getSearchPending(state),
    searchOrders: getDataSearchOrders(state),
});

const mapDispatchToProps = dispatch => ({
    fetchSearch: data => dispatch(fetchSearch(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Search);
