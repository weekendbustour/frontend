import React, {Component} from 'react';
import Container from 'react-bootstrap/Container';
import Breadcrumb from '../../containers/Layout/Breadcrumb';
import breadcrumbs from '../../breadcrumbs';
import {connect} from 'react-redux';
import Subscription from '../../containers/News/Subscription';
import seo from '../../lib/seo';
import store from '../../store';
import {fetchNews} from '../../store/actions/News';
import {getNews, getNewsPagination, getNewsPending} from '../../store/reducers/News';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import SkeletonNews from '../../containers/Skeleton/News/Card';
import NewsCard from '../../containers/News/Card/Card';
import AwPagination from '../../containers/Layout/AwPagination';
import {scrollTo} from '../../lib/scrollTo';
import * as PropTypes from 'prop-types';
import {newsTypes} from '../../prop/Types/News';
import {paginationTypes} from '../../prop/Types/Pagination';
import {TITLE_NEWS} from "../../lib/titles";
import TitleMain from "../../containers/Layout/TitleMain";

class News extends Component {
    _isMounted = false;

    static propTypes = {
        news: newsTypes,
        pending: PropTypes.bool,
        pagination: paginationTypes,
    };

    constructor(props) {
        super(props);
        this.state = {};
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.onChangePage = this.onChangePage.bind(this);
    }

    componentDidMount() {
        this._isMounted = true;
        seo.news();
        const {location} = this.props;
        let searchUrl = location.search;
        let urlData = new URLSearchParams(searchUrl);
        const page = urlData.get('page');
        this.onChangePage(page);
    }

    onChangePage(page) {
        scrollTo.selectorFast('.breadcrumb');
        store.dispatch(
            fetchNews({
                page: page !== undefined ? page : 1,
            })
        );
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {pending, news, pagination} = this.props;
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <section>
                    <Container>
                        <Breadcrumb push={breadcrumbs.news} />
                    </Container>
                </section>
                <section>
                    <Container className={`mb-5`}>
                        <TitleMain title={`Новости`} model={TITLE_NEWS}/>
                    </Container>
                </section>
                {pending && (
                    <section className={`h-500`}>
                        <Container>
                            <Row className={`awesome-card cards`}>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`4`} className={`mb-3`}>
                                    <SkeletonNews />
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`8`} className={`mb-3`}>
                                    <SkeletonNews />
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`4`} className={`mb-3`}>
                                    <SkeletonNews />
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`4`} className={`mb-3`}>
                                    <SkeletonNews />
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`4`} className={`mb-3`}>
                                    <SkeletonNews />
                                </Col>
                                {/*<Col xs={`12`} sm={`12`} md={`6`} lg={`4`} className={`mb-3`}>*/}
                                {/*    <SkeletonNews />*/}
                                {/*</Col>*/}
                            </Row>
                        </Container>
                    </section>
                )}
                {!pending && (
                    <section>
                        {/*{loading &&*/}
                        {/*<Container className={`mb-5 mt-5 d-flex justify-content-center`}>*/}
                        {/*    <RotateSpinner size={30} color="#41b882" loading={loading} />*/}
                        {/*</Container>*/}
                        {/*}*/}
                        <Container>
                            <Row>
                                {news.length !== 0 &&
                                    news.map((item, key) => (
                                        <Col xs={`12`} sm={`12`} md={`6`} lg={`${key === 1 || key === 5 ? '8' : '4'}`} className={`mb-3`} key={key}>
                                            {/*<TourItem tour={tour}/>*/}
                                            <NewsCard news={item} type={key === 1 || key === 5 ? 'long' : 'short'} />
                                        </Col>
                                    ))}
                            </Row>
                        </Container>
                        {pagination !== null && pagination.total > pagination.limit && (
                            <Container className="d-flex justify-content-center mt-5">
                                {/* eslint-disable-next-line react/jsx-no-bind */}
                                <AwPagination pagination={pagination} onChangePage={page => this.onChangePage(page)} location={this.props.location} />
                            </Container>
                        )}
                        {news.length === 0 && (
                            <Container>
                                <div className="d-flex align-items-center justify-content-center" style={{height: '200px'}}>
                                    <Subscription organization={{}} />
                                </div>
                            </Container>
                        )}
                    </section>
                )}
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    news: getNews(state),
    pagination: getNewsPagination(state),
    pending: getNewsPending(state),
});

const mapDispatchToProps = dispatch => ({
    fetchNews: data => dispatch(fetchNews(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(News);
