import React, {Component} from 'react';
import 'react-dates/initialize';
import {connect} from 'react-redux';
import {isMobile} from 'react-device-detect';
import FsLightbox from 'fslightbox-react';
import {fsLightboxHelper} from '../../../lib/fsLightboxHelper';
import {newsItemTypes} from "../../../prop/Types/News";

class Main extends Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        news: newsItemTypes,
    };

    constructor(props) {
        super(props);
        this.state = {
            toggleBanner: false,
            news: props.news,
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.setToggleImageBanner = this.setToggleImageBanner.bind(this);
        this.handleOnOpenLightbox = this.handleOnOpenLightbox.bind(this);
    }

    setToggleImageBanner() {
        const {toggleBanner} = this.state;
        this.setState({toggleBanner: !toggleBanner});
    }
    handleOnOpenLightbox() {
        fsLightboxHelper.clearTitles();
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {news, toggleBanner} = this.state;
        if (!this.shouldComponentRender()) return null;
        return (
            <React.Fragment>
                <div className="page__snipe--img">
                    <div className={`page__snipe--background`} style={{backgroundImage: 'url(' + news.image.banner + ')'}} onClick={this.setToggleImageBanner}/>
                </div>
                <div className={`flight-box-custom ${isMobile ? 'is-mobile' : 'is-desktop'}`}>
                    <FsLightbox
                        type="image"
                        toggler={toggleBanner}
                        sources={[news.image.banner]}
                        slide={0}
                        thumbs={[news.image.banner]}
                        onOpen={this.handleOnOpenLightbox}
                    />
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Main);
