import React, {Component} from 'react';
import {newsItemTypes} from "../../../prop/Types/News";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import {Link} from "react-router-dom";

class NewsRecommended extends Component {
    static propTypes = {
        news: newsItemTypes,
    };
    constructor(props) {
        super(props);
        this.state = {
            news: props.news,
        };
    }
    render() {
        const {news} = this.state;
        return (
            <React.Fragment>
                {news.recommended.previous !== null || news.recommended.next !== null ? (
                    <Row>
                        <Col xs={12} md={12} lg={{span: 8, offset: 2}} className={`mt-3`}>
                            <Row>
                                <Col xs={6} className={`text-left`}>
                                    {news.recommended.previous !== null && (
                                        <div>
                                            <div>Предыдущая статья</div>
                                            <div>
                                                <Link to={`/news/${news.recommended.previous.id}/${news.recommended.previous.name}`}>
                                                    {news.recommended.previous.title}
                                                </Link>
                                            </div>
                                        </div>
                                    )}
                                </Col>
                                <Col xs={6} className={`text-right`}>
                                    {news.recommended.next !== null && (
                                        <div>
                                            <div>Следующая статья</div>
                                            <div>
                                                <Link to={`/news/${news.recommended.next.id}/${news.recommended.next.name}`}>
                                                    {news.recommended.next.title}
                                                </Link>
                                            </div>
                                        </div>
                                    )}
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                ) : (
                    ''
                )}
            </React.Fragment>
        );
    }
}

export default NewsRecommended;
