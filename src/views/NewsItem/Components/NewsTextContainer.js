import React, {Component} from 'react';
import {newsItemTypes} from "../../../prop/Types/News";
import {isMobile} from "react-device-detect";
import ReactMarkdown from "../../../plugins/ReactMarkdown";
import Badge from "react-bootstrap/Badge";
import NewsStatistic from "../../../containers/News/Components/NewsStatistic";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import SourceDefault from "../../../components/Source/SourceDefault";

class NewsTextContainer extends Component {
    static propTypes = {
        news: newsItemTypes,
    };
    constructor(props) {
        super(props);
        this.state = {
            news: props.news,
        };
    }
    render() {
        const {news} = this.state;
        return (
            <React.Fragment>
                <div className="page__snipe--text-container">
                    <div className={`d-flex align-items-center`}>
                        <div className="text-muted">{news.published_at_format}</div>
                        <div className="page__snipe--socials">
                            <NewsStatistic news={news} variant={`light-dark`} size={isMobile ? `md` : `md`}/>
                        </div>
                    </div>
                    <h2 className={`text-left`}>{news.title}</h2>
                    {/*<p className="text-left delta cardo regular italic">{news.preview_description}</p>*/}
                    <ReactMarkdown source={news.description} />
                    <div className="page__snipe--category">
                        {news.categories.map((category, key) => (
                            <Badge
                                key={key}
                                variant="default"
                                className={[``, category.color ? `colored` : ``]}
                                style={{backgroundColor: category.color, borderColor: category.color}}
                            >
                                {category.title}
                            </Badge>
                        ))}
                    </div>
                    {news.source !== null && (
                        <div>
                            <Row className={`mt-3`}>
                                <Col xs={12}>
                                    <h3>Источник</h3>
                                </Col>
                                <Col xs={12} className={`page__snipe--source`}>
                                    <SourceDefault source={news.source}/>
                                </Col>
                            </Row>
                        </div>
                    )}
                </div>
            </React.Fragment>
        );
    }
}

export default NewsTextContainer;
