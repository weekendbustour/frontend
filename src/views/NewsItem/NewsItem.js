import React, {Component} from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Breadcrumb from '../../containers/Layout/Breadcrumb';
import 'react-dates/initialize';
import breadcrumbs from '../../breadcrumbs';
import SkeletonPage from '../../containers/Skeleton/News/Page';
import {connect} from 'react-redux';
import {fetchNewsPage} from '../../store/actions/News';
import {getNewsPage, getNewsPagePending} from '../../store/reducers/NewsPage';
import NewsContainer from '../../views/Index/Container/News';
import * as PropTypes from 'prop-types';
import {newsItemTypes} from '../../prop/Types/News';
import Main from './Components/Main';
import NewsTextContainer from './Components/NewsTextContainer';
import NewsRecommended from './Components/NewsRecommended';
import NewsComments from "./Components/NewsComments";

class NewsItem extends Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        news: newsItemTypes,
        newsLoading: PropTypes.bool,
        fetchNewsPage: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.state = {};
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentDidMount() {
        const {fetchNewsPage} = this.props;
        fetchNewsPage(this.props.match.params.id);
        this._isMounted = true;
    }

    componentDidUpdate(prevProps) {
        const {news, newsLoading, match, fetchNewsPage} = this.props;
        const id = parseInt(match.params.id);
        if (news.id !== id && !newsLoading) {
            fetchNewsPage(id);
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {news, newsLoading} = this.props;
        if (!this.shouldComponentRender()) return null;
        return (
            <React.Fragment>
                {newsLoading && (
                    <section>
                        <SkeletonPage />
                    </section>
                )}
                {/* Хлебные крошки */}
                <section>
                    {!newsLoading && (
                        <Container>
                            <Breadcrumb parent={breadcrumbs.news} push={{title: news.title}} />
                        </Container>
                    )}
                </section>
                {/* Баннер */}
                <section>
                    {!newsLoading && (
                        <Container className={`mb-2 mt-0 page__snipe`}>
                            <Row>
                                <Col xs={12}>
                                    <Main news={news} />
                                </Col>
                            </Row>
                            <Row>
                                <Col xs={12} md={12} lg={{span: 8, offset: 2}} className={`mt-3`}>
                                    <NewsTextContainer news={news} />
                                </Col>
                            </Row>
                            <NewsRecommended news={news} />
                        </Container>
                    )}
                </section>
                {/* Похожие новости */}
                <section>
                    <NewsContainer />
                </section>
                {/* Комментарии */}
                <section>
                    {!newsLoading && (
                        <Container>
                            <Row className={`--container-news-comments`}>
                                <Col xs={12}>
                                    <NewsComments news={news} />
                                </Col>
                            </Row>
                        </Container>
                    )}
                </section>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    news: getNewsPage(state),
    newsLoading: getNewsPagePending(state),
});

const mapDispatchToProps = dispatch => ({
    fetchNewsPage: id => dispatch(fetchNewsPage(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(NewsItem);
