import React, {Component} from 'react';
import ForgetPasswordForm from '../../containers/Auth/Form/ForgetPassword';
import {Link} from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

class Reset extends Component {
    static propTypes = {};

    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        return (
            <React.Fragment>
                <section>
                    <Container>
                        <Row className={`mt-3`}>
                            <Col xs={12} md={{span: 4, offset: 4}}>
                                <div className={`auth-header`}>
                                    <div className={`back-button`}>
                                        <Link to="/">
                                            <i className="fas fa-chevron-left" />
                                            &#160; На главную
                                        </Link>
                                    </div>
                                </div>
                                <div className={`auth-form is-page`}>
                                    <div className={`auth-form-container`}>
                                        <div className="form-container sign-in-container">
                                            <ForgetPasswordForm />
                                        </div>
                                        {/*<div className={`d-flex justify-content-center`}>*/}
                                        {/*    <Link to="/auth/login">Страница входа</Link>*/}
                                        {/*</div>*/}
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </section>
            </React.Fragment>
        );
    }
}

export default Reset;
