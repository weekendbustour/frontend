import React, {Component} from 'react';
import LoginForm from '../../containers/Auth/Form/Login';
import {Link} from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import {getUser} from '../../store/reducers/User';
import {connect} from 'react-redux';
import {userTypes} from '../../prop/Types/User';

class Login extends Component {
    static propTypes = {
        user: userTypes,
    };

    constructor(props) {
        super(props);
        this.state = {};
        console.log('login');
    }

    componentDidMount() {
        const {user} = this.props;

        if (user !== null) {
            window.location.href = '/';
        }
    }

    render() {
        return (
            <React.Fragment>
                <section>
                    <Container>
                        <Row className={`mt-3`}>
                            <Col xs={12} md={{span: 4, offset: 4}}>
                                <div className={`auth-header`}>
                                    <div className={`back-button`}>
                                        <Link to="/">
                                            <i className="fas fa-chevron-left" />
                                            &#160; На главную
                                        </Link>
                                    </div>
                                </div>
                                <div className={`auth-form is-page`}>
                                    <div className={`auth-form-container`}>
                                        <div className="form-container sign-in-container">
                                            <LoginForm />
                                        </div>
                                        <div className={`d-flex justify-content-center`}>
                                            Нет аккаунта?&#160;<Link to="/auth/register">Создать</Link>
                                        </div>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </section>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: getUser(state),
});
export default connect(mapStateToProps)(Login);
