import React from 'react';
import Container from 'react-bootstrap/Container';
import Breadcrumb from '../../containers/Layout/Breadcrumb';
import breadcrumbs from '../../breadcrumbs';
import DirectionCard from '../../containers/Direction/Card/Card';
import SkeletonDirection from '../../containers/Skeleton/Direction/Card';
import {getDirections, getDirectionsPending} from '../../store/reducers/Directions';
import {fetchDirections} from '../../store/actions/Directions';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import seo from '../../lib/seo';
import {connect} from 'react-redux';
import * as PropTypes from 'prop-types';
import {directionsTypes} from '../../prop/Types/Direction';
import {TITLE_DIRECTIONS} from "../../lib/titles";
import TitleMain from "../../containers/Layout/TitleMain";

class Directions extends React.Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        directions: directionsTypes,
        pending: PropTypes.bool,
        fetchDirections: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentDidMount() {
        const {fetchDirections} = this.props;
        seo.directions();
        fetchDirections();
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {directions, pending} = this.props;
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <section>
                    <Container>
                        <Breadcrumb push={breadcrumbs.directions} />
                    </Container>
                </section>
                <section>
                    <Container className={`mb-5`}>
                        <TitleMain title={`Направления`} model={TITLE_DIRECTIONS}/>
                    </Container>
                </section>
                {pending && (
                    <section className={`h-500`}>
                        <Container>
                            <Row>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`6`} className={`mb-3`}>
                                    <SkeletonDirection />
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`6`} className={`mb-3`}>
                                    <SkeletonDirection />
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`6`} className={`mb-3`}>
                                    <SkeletonDirection />
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`6`} className={`mb-3`}>
                                    <SkeletonDirection />
                                </Col>
                            </Row>
                        </Container>
                    </section>
                )}
                {!pending && (
                    <section className={`h-500`}>
                        <Container>
                            <Row>
                                {directions.length !== 0 &&
                                    directions.map((direction, key) => (
                                        <Col xs={`12`} sm={`12`} md={`6`} lg={`6`} className={`mb-3`} key={key}>
                                            <DirectionCard direction={direction} />
                                        </Col>
                                    ))}
                            </Row>
                        </Container>
                    </section>
                )}
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    directions: getDirections(state),
    pending: getDirectionsPending(state),
});

const mapDispatchToProps = dispatch => ({
    fetchDirections: () => dispatch(fetchDirections()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Directions);
