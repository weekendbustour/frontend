import React, {Component} from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Breadcrumb from '../../containers/Layout/Breadcrumb';
import 'react-dates/initialize';
import breadcrumbs from '../../breadcrumbs';
import {fetchTour, getTour, getTourPending} from '../../store/reducers/Tour';
import SkeletonPage from '../../containers/Skeleton/Tour/Page';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import ToursContainer from '../Index/Container/Tours';
import {fetchData} from '../../store/actions/Data';
import TourPrice from '../../containers/Tour/Components/Price';
import TourMain from '../Tour/Components/Main';
import {Button} from 'react-bootstrap';
import {getTourTimelines, getTourTimelinesPending} from '../../store/reducers/Tour/Timelines';
import Skeleton from 'react-loading-skeleton';
import {fetchModal} from '../../store/reducers/Modal';
import {MODAL_TOUR_RESERVATION} from '../../lib/modal';
import * as PropTypes from 'prop-types';
import {tourTimelinesTypes, tourTypes} from '../../prop/Types/Tour';

import TourMap from './Components/TourMap';
import TourVideo from './Components/TourVideo';
import TourTimeline from './Components/TourTimeline/TourTimeline';
import TourSlider from './Components/TourSlider';
import TourCategories from './Components/TourCategories';
import TourDescription from './Components/TourDescription';
import TourIncludes from './Components/TourIncludes';
import TourDepartures from './Components/TourDepartures';
import TourSignature from './Components/TourSignature';
import TourParameters from './Components/TourParameters';
import TourDuplicates from './Components/TourDuplicates';
import TourAppShare from './Components/TourAppShare';
import TourReservation from "./Components/TourReservation";
import TourMultiples from "./Components/TourMultiples";
import TourPrepayment from "./Components/TourPrepayment";
import TourAttractions from "./Components/TourAttractions";
import TourOutfitsRent from "./Components/TourOutfitsRent";

class Tour extends Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        tour: tourTypes,
        timelines: tourTimelinesTypes,
        tourLoading: PropTypes.bool,
        timelinesPending: PropTypes.bool,
        setModal: PropTypes.func,
        getTour: PropTypes.func,
        //getData: PropTypes.func
    };

    constructor(props) {
        super(props);
        this.state = {
            toggle: false,
            canCheckTour: true,
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.onOpenReservation = this.onOpenReservation.bind(this);
    }

    setToggle(state) {
        this.setState({
            toggle: state,
        });
    }

    componentDidMount() {
        //const {getTour} = this.props;
        this.props.getTour(this.props.match.params.id);
        //getTour(this.props.match.params.id);
        this._isMounted = true;
    }
    componentDidUpdate(prevProps) {
        const {tour, tourLoading, match} = this.props;
        const id = parseInt(match.params.id);
        if (tour.id !== id && !tourLoading) {
            this.props.getTour(id);
        }
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    onOpenReservation(event) {
        this.props.setModal(MODAL_TOUR_RESERVATION);
    }

    render() {
        const {tour, tourLoading, timelinesPending, timelines} = this.props;
        if (!this.shouldComponentRender()) return null;
        return (
            <React.Fragment>
                {tourLoading && (
                    <section>
                        <SkeletonPage />
                    </section>
                )}
                <section>
                    {!tourLoading && (
                        <Container>
                            <Breadcrumb parent={breadcrumbs.tours} push={{title: tour.title}} />
                        </Container>
                    )}
                </section>
                {/* Баннер */}
                <section>
                    {!tourLoading && (
                        <Container className={`mt-0 page__tale ${tour.cancellation !== null ? 'is-cancellation' : ''}`}>
                            <Row>
                                <Col xs={12}>
                                    <TourMain tour={tour} />
                                </Col>
                            </Row>
                            <Row>
                                <Col xs={12} md={{span: 6, offset: 3}} className={`mt-5`}>
                                    <div className={`text-center justify-content-center mt-2 mb-1`}>
                                        Организатор:&#160;
                                        <Link to={`/organization/${tour.organization.id}/${tour.organization.name}`}>{tour.organization.title}</Link>
                                    </div>
                                </Col>
                                <Col xs={12} md={{span: 6, offset: 3}}>
                                    <h2 className={`text-center`}>{tour.title}</h2>
                                    {tour.meta_tag !== null &&
                                    <h5 className={`text-center`}>{tour.meta_tag.description}</h5>
                                    }
                                    <div className={`text-center mt-3 mb-3`}>
                                        <TourCategories tour={tour} />
                                    </div>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={{span: 6, offset: 3}}>
                                    <p className={`text-center text-muted mb-1 mt-1`}>{tour.preview_description}</p>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={{span: 6, offset: 3}}>
                                    <div className="page__tale--price">{tour.price !== null && <TourPrice price={tour.price} />}</div>
                                </Col>
                            </Row>
                            {process.env.REACT_APP_TOUR_RESERVATION_ENABLED === 'true' && (
                                <Row>
                                    <Col xs={12} md={{span: 6, offset: 3}} className={`text-center`}>
                                        <Button variant="primary" className={`mt-1`} onClick={this.onOpenReservation}>
                                            <i className="fas fa-bus" /> Отправить заявку на бронирование
                                        </Button>
                                    </Col>
                                </Row>
                            )}
                        </Container>
                    )}
                </section>
                {/* Видео */}
                <section>{!tourLoading && <TourVideo tour={tour} />}</section>
                {/* Слайдер */}
                <section>{!tourLoading && <TourSlider tour={tour} />}</section>
                {/* Описание */}
                {!tourLoading && (
                    <section>
                        {/* Описание */}
                        <Container>
                            <TourDescription tour={tour} />
                        </Container>
                        {/* Что входит */}
                        {tour.parameters.include !== undefined && (
                            <Container>
                                <TourIncludes tour={tour} />
                            </Container>
                        )}
                        {/* Развлечения */}
                        {/*{tour.entertainments.length !== 0 && (*/}
                        {/*    <Container>*/}
                        {/*        <TourEntertainments tour={tour} />*/}
                        {/*    </Container>*/}
                        {/*)}*/}
                        {/* Отправления */}
                        {tour.departures.length !== 0 && !tour.departures_empty && (
                            <Container>
                                <TourDepartures tour={tour} />
                            </Container>
                        )}
                        {tour.outfits_rent.length !== 0 && (
                            <Container>
                                <TourOutfitsRent tour={tour} />
                            </Container>
                        )}
                        {/* Предоплата */}
                        {tour.price.prepayment !== null && (
                            <Container>
                                <TourPrepayment tour={tour}/>
                            </Container>
                        )}
                        {/* Как забронировать */}
                        {tour.parameters.reservation !== undefined && (
                            <Container>
                                <TourReservation tour={tour} />
                            </Container>
                        )}
                        {/* Параметры */}
                        <Container>
                            <TourParameters tour={tour} />
                        </Container>
                        {/* Подпись */}
                        {tour.organization.signature !== null && (
                            <Container>
                                <TourSignature tour={tour} />
                            </Container>
                        )}
                    </section>
                )}
                {/* Расписание */}
                {!tourLoading && timelinesPending && (
                    <section>
                        <Skeleton height={`400px`} width={`100%`} />
                    </section>
                )}
                {!tourLoading && !timelinesPending && timelines.length !== 0 && (
                    <section style={{backgroundColor: '#F1F4F5'}}>
                        <Container>
                            <h3 className={`text-center pt-5`}>Тайминг путешествия</h3>
                            {tour.parameters.timeline_note !== undefined && <p className={`text-muted text-center`}>{tour.parameters.timeline_note.value}</p>}
                            <hr />
                            <TourTimeline timelines={timelines} />
                        </Container>
                    </section>
                )}
                {/* Карта */}
                {/*<section>*/}
                {/*    {!tourLoading && (*/}
                {/*        <Container>*/}
                {/*            <TourMap tour={tour} />*/}
                {/*        </Container>*/}
                {/*    )}*/}
                {/*</section>*/}
                {!tourLoading && (
                    <section>
                        <TourMap tour={tour} />
                    </section>
                )}
                {/* Достопримечательности */}
                <section>{!tourLoading && tour.attractions.length !== 0 && <TourAttractions tour={tour} />}</section>
                {/* Дубликаты */}
                <section>{!tourLoading && tour.duplicates.length !== 0 && <TourDuplicates tour={tour} />}</section>
                {/* Мультитуры */}
                <section>{!tourLoading && tour.multiples.length !== 0 && <TourMultiples tour={tour} />}</section>
                {/*/!* Реклама *!/*/}
                {/*<section>*/}
                {/*    <PromotionContainer />*/}
                {/*</section>*/}
                {/* Ближайшие туры */}
                <section>
                    <ToursContainer/>
                </section>
                {/* Рассказать о нас */}
                <section>
                    <TourAppShare />
                </section>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    tourLoading: getTourPending(state),
    tour: getTour(state),
    timelines: getTourTimelines(state),
    timelinesPending: getTourTimelinesPending(state),
});
const mapDispatchToProps = dispatch => ({
    getTour: id => dispatch(fetchTour(id)),
    getData: () => dispatch(fetchData()),
    setModal: name => dispatch(fetchModal(name)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Tour);
