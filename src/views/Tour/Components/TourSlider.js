import React, {Component} from 'react';
import FsLightbox from 'fslightbox-react';
import Flickity from 'react-flickity-component';
import {isMobile} from 'react-device-detect';
import {fsLightboxHelper} from '../../../lib/fsLightboxHelper';
import {tourTypes} from '../../../prop/Types/Tour';
const flickityOptions = {
    //asNavFor: '.carousel-main',
    contain: true,
    // pageDots: false,
    setGallerySize: false,
    fullscreen: true,
    lazyLoad: 2,
    groupCells: 3,
};
class TourSlider extends Component {
    static propTypes = {
        tour: tourTypes,
    };

    constructor(props) {
        super(props);
        this.state = {
            tour: props.tour,
            toggle: false,
            slide: 1,
            images: props.tour.image.images || [],
        };
        this.setToggle = this.setToggle.bind(this);
        this.handleClearTitles = this.handleClearTitles.bind(this);
    }

    setToggle(state, key) {
        this.setState({
            toggle: state,
            slide: key + 1,
        });
    }

    handleClearTitles() {
        const {tour} = this.state;
        fsLightboxHelper.clearTitles();
        fsLightboxHelper.addCaption(tour.image.images_captions);
    }

    render() {
        const {tour} = this.state;
        const {toggle, images, slide} = this.state;
        return (
            <React.Fragment>
                {/* Если есть баннер - выводится баннер, а ниже изображения */}
                {/* Если нет баннера и изображение одно - выводится изображение как баннер и без слайдера ниже */}
                {/* Если нет баннера и изображений два - выводится изображение как баннер и со слайдером ниже, повторяется главное изображение, в баннере и в слайдере */}
                {images.length === 0 || (images.length === 1 && !tour.image.banner_has) ? (
                    ''
                ) : (
                    <div>
                        <div className={`flight-box-custom ${isMobile ? 'is-mobile' : 'is-desktop'}`}>
                            <FsLightbox type="image" toggler={toggle} sources={images} slide={slide} thumbs={images} onOpen={this.handleClearTitles} />
                        </div>
                        <Flickity
                            className={'carousel'} // default ''
                            elementType={'div'} // default 'div'
                            options={flickityOptions} // takes flickity options {}
                            disableImagesLoaded={false} // default false
                            reloadOnUpdate // default false
                            static // default false
                        >
                            {images.map((image, key) => (
                                <img key={key} src={image} onClick={() => this.setToggle(!toggle, key)} alt="" />
                            ))}
                        </Flickity>
                    </div>
                )}
            </React.Fragment>
        );
    }
}

export default TourSlider;
