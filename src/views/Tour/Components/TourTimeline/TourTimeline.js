import React, {Component} from 'react';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import TimelineSlider from './TourTimelineSlider';
import {tourTimelinesTypes} from '../../../../prop/Types/Tour';

class TourTimeline extends Component {
    static propTypes = {
        timelines: tourTimelinesTypes,
    };

    constructor(props) {
        super(props);
        this.state = {
            timelines: props.timelines,
        };
    }

    render() {
        const {timelines} = this.state;
        return (
            <React.Fragment>
                {timelines.length !== 0 && (
                    <Row>
                        <Col className={`mt-3 mb-5 timelines-container`} xs={12}>
                            {Object.keys(timelines).map((i, key) => (
                                <ul className="timeline timeline-centered" key={key}>
                                    {timelines[i].map((timeline, key) => (
                                        <li
                                            className={`timeline-item ${timeline.position ? '__right' : '__left'} ${
                                                timeline.type === 'day_title' ? 'period' : ''
                                            } ${timeline.type === 'day_title' && timeline.description !== null ? '__with-description' : ''}`}
                                            key={key}
                                        >
                                            {timeline.type === 'day_title' && (
                                                <div>
                                                    <div className="timeline-info" />
                                                    <div className="timeline-marker" />
                                                    <div className="timeline-content">
                                                        <h2 className="timeline-title">
                                                            {timeline.title}
                                                            &#160;
                                                            {timeline.time_description !== null && `${timeline.time_description}`}
                                                        </h2>
                                                        {timeline.description !== null && <p className={`text-muted`}>{timeline.description}</p>}
                                                    </div>
                                                </div>
                                            )}
                                            {timeline.type !== 'day_title' && (
                                                <div>
                                                    <div className="timeline-info">
                                                        {timeline.start_at_time !== null && <span>{timeline.start_at_time}</span>}
                                                        {timeline.finish_at_time !== null && (
                                                            <span>
                                                                {' '}
                                                                - {timeline.finish_at_time} ({timeline.time_description})
                                                            </span>
                                                        )}
                                                    </div>
                                                    <div className="timeline-marker" />
                                                    <div className="timeline-content">
                                                        {timeline.type === 'primary_title' && <h3 className="timeline-title">{timeline.title}</h3>}
                                                        {timeline.type === 'secondary_title' && <h6 className={`timeline-title`}>{timeline.title}</h6>}
                                                        {timeline.description !== null && <p className={`text-muted`}>{timeline.description}</p>}
                                                    </div>
                                                </div>
                                            )}
                                        </li>
                                    ))}
                                    {timelines[i].map(
                                        (timeline, key) =>
                                            timeline.type === 'day_title' &&
                                            timeline.image.images.length !== 0 && (
                                                <li className={`timeline-item __image`} key={key}>
                                                    <div className="timeline-image-gallery-container">
                                                        <TimelineSlider images={timeline.image.images} />
                                                    </div>
                                                </li>
                                            )
                                    )}
                                </ul>
                            ))}
                        </Col>
                    </Row>
                )}
            </React.Fragment>
        );
    }
}

export default TourTimeline;
