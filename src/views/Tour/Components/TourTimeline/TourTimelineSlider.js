import React, {Component} from 'react';
import FsLightbox from 'fslightbox-react';
import Flickity from 'react-flickity-component';
import * as PropTypes from 'prop-types';
const flickityOptions = {
    //asNavFor: '.carousel-main',
    //cellSelector: '.carousel-cell',
    contain: true,
    // pageDots: false,
    setGallerySize: false,
    fullscreen: true,
    lazyLoad: 2,
    groupCells: 1,
    adaptiveHeight: true,
    //wrapAround: true,
    //pageDots: false,
};
class TourTimelineSlider extends Component {
    static propTypes = {
        images: PropTypes.array,
    };

    constructor(props) {
        super(props);
        this.state = {
            toggle: false,
            slide: 1,
            images: props.images || [],
        };
    }

    setToggle(state, key) {
        this.setState({
            toggle: state,
            slide: key + 1,
        });
    }

    render() {
        const {toggle, images, slide} = this.state;
        return (
            <React.Fragment>
                {images.length !== 0 && (
                    <div>
                        <FsLightbox type="image" toggler={toggle} sources={images} slide={slide} thumbs={images} />
                        <Flickity
                            className={'carousel'} // default ''
                            elementType={'div'} // default 'div'
                            options={flickityOptions} // takes flickity options {}
                            disableImagesLoaded={false} // default false
                            reloadOnUpdate // default false
                            static // default false
                        >
                            {images.map((image, key) => (
                                <img key={key} src={image} onClick={() => this.setToggle(!toggle, key)} alt="" />
                            ))}
                        </Flickity>
                    </div>
                )}
            </React.Fragment>
        );
    }
}

export default TourTimelineSlider;
