import React, {Component} from 'react';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import {tourTypes} from '../../../prop/Types/Tour';
import {TooltipQuestion} from "../../../containers/Layout/Tooltip/Question";

class TourParameters extends Component {
    static propTypes = {
        tour: tourTypes,
    };
    constructor(props) {
        super(props);
        this.state = {
            tour: props.tour,
        };
    }
    render() {
        const {tour} = this.state;
        return (
            <React.Fragment>
                <Row className={`mt-3`}>
                    <Col xs={12}>
                        <h3>Характеристики</h3>
                    </Col>
                    <Col xs={12} md={6}>
                        <div className="dot-options">
                            <p>
                                <span className={`is-title`}>Дата</span>
                                <span className={`is-content`}>{tour.start_at_date}</span>
                            </p>
                            <p>
                                <span className={`is-title`}>Организатор</span>
                                <span className={`is-content`}>{tour.organization.title}</span>
                            </p>
                            <p>
                                <span className={`is-title`}>Отправление из</span>
                                <span className={`is-content`}>
                                    {tour.departures.map((departure, key) => (
                                        <span key={key}>{departure.title}{Object.keys(tour.departures)[key + 1] !== undefined ? ', ' : ''}</span>
                                    ))}
                                </span>
                            </p>
                            <p>
                                <span className={`is-title`}>Направление</span>
                                <span className={`is-content`}>{tour.direction.title}</span>
                            </p>
                            <p>
                                <span className={`is-title`}>Вид</span>
                                <span className={`is-content`}>
                                    {tour.categories.map((category, key) => (
                                        <span key={key}>{category.title}{Object.keys(tour.categories)[key + 1] !== undefined ? ', ' : ''}</span>
                                    ))}
                                </span>
                            </p>
                        </div>
                    </Col>
                    <Col xs={12} md={6}>
                        <div className="dot-options">
                            {Object.keys(tour.parameters_view).map((i, key) => (
                                <p key={key}>
                                    <span className={`is-title`}>{tour.parameters[i].title}</span>
                                    <span className={`is-content`}>
                                        {tour.parameters[i].value}
                                        {i === 'places' &&
                                            <TooltipQuestion title={`Точное количество уточняйте у Организатора`}/>
                                        }
                                    </span>
                                </p>
                            ))}
                            <p>
                                <span className={`is-title`}>Добавлено</span>
                                <span className={`is-content`}>{tour.published_at_format}</span>
                            </p>
                        </div>
                    </Col>
                </Row>
            </React.Fragment>
        );
    }
}

export default TourParameters;
