import React, {Component} from 'react';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import {tourTypes} from '../../../prop/Types/Tour';
import ReactMarkdown from "../../../plugins/ReactMarkdown";

class TourIncludes extends Component {
    static propTypes = {
        tour: tourTypes,
    };
    constructor(props) {
        super(props);
        this.state = {
            tour: props.tour,
        };
    }
    render() {
        const {tour} = this.state;
        return (
            <React.Fragment>
                <Row className={`mb-3`}>
                    <Col xs={12}>
                        {tour.parameters.include !== undefined && (
                            <div className={`tour-include-block`}>
                                <h5>Что входит?</h5>
                                <ReactMarkdown source={tour.parameters.include.value} />
                            </div>
                        )}
                        {tour.parameters.not_include !== undefined && (
                            <div className={`tour-include-block`}>
                                <h5>Что не входит?</h5>
                                <ReactMarkdown source={tour.parameters.not_include.value} />
                            </div>
                        )}
                        {tour.parameters.more_include !== undefined && (
                            <div className={`tour-include-block`}>
                                <h5>Дополнительные расходы</h5>
                                <ReactMarkdown source={tour.parameters.more_include.value} />
                            </div>
                        )}
                    </Col>
                </Row>
            </React.Fragment>
        );
    }
}

export default TourIncludes;
