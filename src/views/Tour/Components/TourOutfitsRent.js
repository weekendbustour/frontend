import React, {Component} from 'react';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import {tourTypes} from '../../../prop/Types/Tour';
import {TooltipQuestion} from '../../../containers/Layout/Tooltip/Question';

class TourOutfitsRent extends Component {
    static propTypes = {
        tour: tourTypes,
    };
    constructor(props) {
        super(props);
        this.state = {
            tour: props.tour,
        };
    }
    render() {
        const {tour} = this.state;
        return (
            <React.Fragment>
                <Row className={`mb-3`}>
                    <Col xs={12} md={12}>
                        <h3>Аренда снаряжения</h3>
                    </Col>
                    <Col xs={12}>
                        {tour.outfits_rent.map((outfit, key) => (
                            <div key={key} className={`mb-1`}>
                                <div className={`d-flex`}>
                                    <div>
                                        <span className={`font-weight-bold`}>{outfit.title}</span>
                                        {outfit.tooltip !== null && <TooltipQuestion title={outfit.tooltip} />}
                                        &#160;-&#160;
                                        <span>{outfit.price}р.</span>
                                    </div>
                                </div>
                                {outfit.description !== null && outfit.description !== '' && (
                                    <div>
                                        <p className={`text-muted mb-0`}>{outfit.description}</p>
                                    </div>
                                )}
                            </div>
                        ))}
                    </Col>
                    {tour.parameters.outfit_rent_note !== undefined && <Col xs={12}>{tour.parameters.outfit_rent_note.value}</Col>}
                </Row>
            </React.Fragment>
        );
    }
}

export default TourOutfitsRent;
