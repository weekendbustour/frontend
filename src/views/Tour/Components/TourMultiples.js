import React, {Component} from 'react';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import {tourTypes} from '../../../prop/Types/Tour';
import Container from "react-bootstrap/Container";
import TourCard from "../../../containers/Tour/Card/Card";
import {TITLE_TOURS_DUPLICATE} from "../../../lib/titles";
import TitleMain from "../../../containers/Layout/TitleMain";

class TourMultiples extends Component {
    static propTypes = {
        tour: tourTypes,
    };
    constructor(props) {
        super(props);
        this.state = {
            tour: props.tour,
        };
    }
    render() {
        const {tour} = this.state;
        return (
            <React.Fragment>
                <Container className={`mb-5 mt-5`}>
                    <TitleMain title={`ЭТОТ ТУР НА ДРУГИЕ ДАТЫ`} model={TITLE_TOURS_DUPLICATE}/>
                </Container>
                <Container>
                    <Row className={`awesome-card cards`}>
                        {tour.multiples.length !== 0 &&
                        tour.multiples.map((tour, key) => (
                            <Col xs={`12`} sm={`12`} md={`6`} lg={`4`} className={`mb-3`} key={key}>
                                <TourCard tour={tour}/>
                            </Col>
                        ))}
                    </Row>
                </Container>
            </React.Fragment>
        );
    }
}

export default TourMultiples;
