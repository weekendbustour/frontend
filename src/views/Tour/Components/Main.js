import React, {Component} from 'react';
import Badge from 'react-bootstrap/Badge';
import 'react-dates/initialize';
import TourSocial from '../../../containers/Tour/Components/Social/Social';
import {connect} from 'react-redux';
import {isMobile} from 'react-device-detect';
import FsLightbox from 'fslightbox-react';
import {fsLightboxHelper} from '../../../lib/fsLightboxHelper';
import {tourTypes} from '../../../prop/Types/Tour';
import {TooltipQuestion} from '../../../containers/Layout/Tooltip/Question';

/**
 * toggler: E.a.bool.isRequired,
 sources: E.a.array,
 customSources: E.a.array,
 slide: E.a.number,
 source: E.a.string,
 sourceIndex: E.a.number,
 onOpen: E.a.func,
 onClose: E.a.func,
 onInit: E.a.func,
 onShow: E.a.func,
 disableLocalStorage: E.a.bool,
 types: E.a.array,
 type: E.a.string,
 videosPosters: E.a.array,
 maxYoutubeVideoDimensions: E.a.object,
 loadOnlyCurrentSource: E.a.bool,
 slideDistance: E.a.number,
 openOnMount: E.a.bool
 */

class Main extends Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        tour: tourTypes,
    };

    constructor(props) {
        super(props);
        this.state = {
            toggleBanner: false,
            tour: props.tour,
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.setToggleImageBanner = this.setToggleImageBanner.bind(this);
        this.setToggleImageCard = this.setToggleImageCard.bind(this);
        this.handleClearTitles = this.handleClearTitles.bind(this);
    }

    setToggleImageBanner() {
        const {toggleBanner} = this.state;
        this.setState({toggleBanner: !toggleBanner});
    }
    setToggleImageCard(state) {
        this.setState({toggleCard: state});
    }
    handleClearTitles() {
        const {tour} = this.state;
        fsLightboxHelper.clearTitles();
        fsLightboxHelper.addCaption([tour.image.banner_caption]);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {tour, toggleBanner} = this.state;
        if (!this.shouldComponentRender()) return null;
        return (
            <React.Fragment>
                <div className={`page__tale--img`}>
                    <div className={`page__tale--background`} style={{backgroundImage: 'url(' + tour.image.banner + ')'}} onClick={this.setToggleImageBanner} />
                    <div className={`page__tale--logo __centered __100px`}>
                        <div className={`__logo`}>
                            <img src={tour.organization.image.logo} alt="" />
                        </div>
                    </div>
                    {tour.tag !== null && (
                        <div className="page__tale--tag">
                            <Badge
                                variant="default"
                                className={[``, tour.tag.color ? `colored` : ``]}
                                style={{backgroundColor: tour.tag.color, borderColor: tour.tag.color}}
                            >
                                {tour.tag.title}
                            </Badge>
                        </div>
                    )}
                    <div className="page__tale--date day-month">
                        <div>
                            <span>{tour.start_at_days}</span>
                            <span>{tour.start_at_month}</span>
                        </div>
                    </div>
                    <div className={`page__tale--socials __centered __100px`}>
                        <TourSocial tour={tour} variant={`light-dark`} size={isMobile ? `md` : `md`} role={`page`}/>
                    </div>
                    {tour.cancellation !== null && (
                        <div className={`page__tale--cancellation type-is-${tour.cancellation.type}`}>
                            <div className={`cancellation--block`}>
                                <h6 className={`mb-0`}>
                                    {tour.cancellation.title}&#160;{tour.cancellation.tooltip !== null && <TooltipQuestion title={tour.cancellation.tooltip} />}
                                </h6>
                                {tour.cancellation.description !== null && <p className={`mb-0`}>{tour.cancellation.description}</p>}
                            </div>
                        </div>
                    )}
                </div>
                <div className={`flight-box-custom ${isMobile ? 'is-mobile' : 'is-desktop'}`}>
                    <FsLightbox
                        type="image"
                        toggler={toggleBanner}
                        sources={[tour.image.banner]}
                        slide={0}
                        thumbs={[tour.image.banner]}
                        onOpen={this.handleClearTitles}
                    />
                </div>
                {/*<div className={`flight-box-custom ${isMobile ? 'is-mobile' : 'is-desktop'}`}>*/}
                {/*    <FsLightbox*/}
                {/*        type="image"*/}
                {/*        toggler={toggleCard}*/}
                {/*        sources={[tour.image.original.card]}*/}
                {/*        slide={0}*/}
                {/*        thumbs={[tour.image.original.card]}*/}
                {/*        className={`flight-box-custom`}*/}
                {/*        onOpen={this.handleClearTitles}*/}
                {/*    />*/}
                {/*</div>*/}
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Main);
