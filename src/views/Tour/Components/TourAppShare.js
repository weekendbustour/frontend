import React, {Component} from 'react';
import Container from 'react-bootstrap/Container';
import {connect} from 'react-redux';
import {getDataAbout, getDataAboutPending} from '../../../store/reducers/Data';
import * as PropTypes from "prop-types";
import {ReactSVG} from "react-svg";
import svgVk from "../../../assets/svg/social/vk.svg";
import svgInstagram from "../../../assets/svg/social/instagram.svg";

class TourAppShare extends Component {
    static propTypes = {
        about: PropTypes.object,
        pending: PropTypes.bool,
    };
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        const {about, pending} = this.props;
        return (
            <React.Fragment>
                {!pending &&
                <div className="grid--news-container is-app-share shadow-banner-- mb-0" style={{backgroundImage: `url(${about.share.image})`, marginBottom: '-80px'}}>
                    <Container>
                        <div className={`banner`}>
                            <div className="banner-text">
                                <h3>{about.share.title}</h3>
                                <p>{about.share.description}</p>
                                <div>
                                    <p>И подписывайтесь на нас в социальных сетях:</p>
                                    <div className={`d-flex align-items-center h-100`}>
                                        <a href={process.env.REACT_APP_SOCIAL_VK} role={`button`} className="social mr-3" target={`_blank`}>
                                            <ReactSVG src={svgVk} className={`svg-social d-inline`} />
                                        </a>
                                        <a href={process.env.REACT_APP_SOCIAL_INSTAGRAM} role={`button`} className="social" target={`_blank`}>
                                            <ReactSVG src={svgInstagram} className={`svg-social d-inline`} />
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Container>
                </div>
                }
                {/*<div className={`pre-footer`}/>*/}
            </React.Fragment>
        );
    }
}
const mapStateToProps = state => ({
    about: getDataAbout(state),
    pending: getDataAboutPending(state),
});
const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(TourAppShare);
