import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import {connect} from 'react-redux';
import {TITLE_ATTRACTIONS_TOUR} from '../../../lib/titles';
import TitleMain from '../../../containers/Layout/TitleMain';
import Card from "../../../containers/Attraction/Card/Card";
import {tourTypes} from "../../../prop/Types/Tour";

class TourAttractions extends React.Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        tour: tourTypes,
    };

    constructor(props) {
        super(props);
        this.state = {
            tour: props.tour,
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {tour} = this.props;
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <Container>
                    <Row className={`mb-5 mt-5`}>
                        <TitleMain title={`Достопримечательности`} model={TITLE_ATTRACTIONS_TOUR} role={`h2`} />
                    </Row>
                    <Row>
                        {tour.attractions.map((attraction, key) => (
                            <Col xs={12} sm={6} md={6} lg={4} xl={3} key={key}>
                                <Card attraction={attraction}/>
                            </Col>
                        ))}
                    </Row>
                </Container>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({});
export default connect(mapStateToProps)(TourAttractions);
