import React, {Component} from 'react';
import Badge from 'react-bootstrap/Badge';
import {tourTypes} from '../../../prop/Types/Tour';

class TourCategories extends Component {
    static propTypes = {
        tour: tourTypes,
    };
    constructor(props) {
        super(props);
        this.state = {
            tour: props.tour,
        };
    }

    render() {
        const {tour} = this.state;
        return (
            <React.Fragment>
                <div>
                    {tour.categories.length !== 0 &&
                        tour.categories.map((category, key) => (
                            <Badge
                                variant="default"
                                className={[`mr-1 badge-link`, category.color ? `colored` : ``]}
                                style={{borderColor: category.color, color: category.color}}
                                key={key}
                            >
                                <span>{category.title}</span>
                                {/*<Link to={`/search?extended=1&category_id%5B%5D=${category.id}`}></Link>*/}
                            </Badge>
                        ))}
                    {/*{tour.categories.length !== 0 &&*/}
                    {/*    tour.categories.map((category, key) => (*/}
                    {/*        <Tooltip title="Найти еще" theme={`light`} position={`bottom`} size={`small`} key={key} animateFill={false}>*/}
                    {/*            <Badge*/}
                    {/*                variant="default"*/}
                    {/*                className={[`mr-1 badge-link`, category.color ? `colored` : ``]}*/}
                    {/*                style={{borderColor: category.color, color: category.color}}*/}
                    {/*            >*/}
                    {/*                <Link to={`/search?extended=1&category_id%5B%5D=${category.id}`}>{category.title}</Link>*/}
                    {/*            </Badge>*/}
                    {/*        </Tooltip>*/}
                    {/*    ))}*/}
                </div>
            </React.Fragment>
        );
    }
}

export default TourCategories;
