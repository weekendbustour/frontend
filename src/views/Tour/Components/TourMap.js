import React, {Component} from 'react';
import {FullscreenControl, GeolocationControl, Map, RouteButton, TypeSelector, YMaps, ZoomControl, ObjectManager} from 'react-yandex-maps';
import {tourTypes} from "../../../prop/Types/Tour";

/**
 * https://tech.yandex.com/maps/jsapi/doc/2.1/ref/reference/ObjectManager-docpage/
 *
 * https://tech.yandex.com/maps/jsapi/doc/2.1/ref/reference/Placemark-docpage/
 * balloonContentHeader
 * balloonContentBody
 * balloonContentFooter
 */
class TourMap extends Component {
    static propTypes = {
        tour: tourTypes,
    };
    constructor(props) {
        super(props);
        this.state = {
            tour: props.tour,
        };
    }

    /**
     *
     */
    propertiesByLocation(location, features, type) {
        let properties = {};
        if (location.title !== null && location.description !== null) {
            properties.balloonContentBody = location.title;
            properties.balloonContentFooter = location.description;
        } else if (location.title !== null && location.description === null) {
            properties.balloonContentBody = location.title;
        } else if (location.title === null && location.description !== null) {
            properties.balloonContentFooter = location.description;
        }
        if (location.placeholder !== null) {
            properties.hintContent = location.placeholder;
        }
        features.push({
            type: 'Feature',
            id: location.id,
            geometry: {
                type: 'Point',
                coordinates: location.point,
            },
            options: {
                preset: type === 'attraction' ? "islands#blueObservationCircleIcon" : '',
            },
            properties: properties,
        });
        return features;
    }

    render() {
        const self = this;
        const {tour} = this.state;
        const direction = tour.direction;
        const attractions = tour.attractions;
        let features = [];
        features = self.propertiesByLocation(direction.location, features, 'direction');
        // eslint-disable-next-line
        attractions.map((attraction, key) => {
            features = self.propertiesByLocation(attraction.location, features, 'attraction');
        });
        console.log(features);
        return (
            <React.Fragment>
                {/*<Row>*/}
                {/*    <Col xs={12} md={12} style={{marginTop: '30px'}}>*/}
                {/*        */}
                {/*    </Col>*/}
                {/*</Row>*/}
                <div className={`mt-5`}>
                    <YMaps>
                        <div>
                            <Map
                                defaultState={{
                                    center: direction.location.point,
                                    zoom: direction.location.zoom,
                                    controls: [],
                                    //behaviors: ['ScrollZoom'],
                                }}
                                width={`100%`}
                                height={`300px`}
                            >
                                <ObjectManager
                                    options={{
                                        clusterize: false,
                                        gridSize: 32,
                                    }}
                                    objects={{
                                        openBalloonOnClick: true,
                                        //preset: 'islands#greenDotIcon',
                                    }}
                                    clusters={
                                        {
                                            //preset: 'islands#redClusterIcons',
                                        }
                                    }
                                    // filter={object => object.id % 2 === 0}
                                    defaultFeatures={features}
                                    modules={['objectManager.addon.objectsBalloon', 'objectManager.addon.objectsHint']}
                                />
                                {/*{article.locations.map((location, key) => (*/}
                                {/*    <Placemark geometry={location.point} key={key}/>*/}
                                {/*))}*/}
                                <ZoomControl options={{float: 'right'}} />
                                <TypeSelector options={{float: 'right'}} />
                                <GeolocationControl options={{float: 'left'}} />
                                <RouteButton options={{float: 'right'}} />
                                <FullscreenControl />
                            </Map>
                        </div>
                    </YMaps>
                </div>
            </React.Fragment>
        );
    }
}

export default TourMap;
