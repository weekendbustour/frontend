import React, {Component} from 'react';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import {tourTypes} from '../../../prop/Types/Tour';
import ReactMarkdown from "../../../plugins/ReactMarkdown";

class TourPrepayment extends Component {
    static propTypes = {
        tour: tourTypes,
    };
    constructor(props) {
        super(props);
        this.state = {
            tour: props.tour,
        };
    }
    render() {
        const {tour} = this.state;
        return (
            <React.Fragment>
                <Row>
                    {/*<Col xs={12} md={12} className={``}>*/}
                    {/*    <h3>Предоплата</h3>*/}
                    {/*</Col>*/}
                    <Col xs={12} md={12} className={`mb-1`}>
                        <div>
                            Предоплата: <b>{tour.price.prepayment}р.</b>
                        </div>
                        {tour.price.prepayment_description !== null &&
                        <div>
                            <p className={`text-muted`}>
                                {tour.price.prepayment_description}
                            </p>
                        </div>
                        }
                    </Col>
                </Row>
                {tour.parameters.prepayment_cancellation !== undefined &&
                <Row>
                    <Col xs={12} md={12} className={``}>
                        <h5>{tour.parameters.prepayment_cancellation.title}</h5>
                        <ReactMarkdown source={tour.parameters.prepayment_cancellation.value}/>
                    </Col>
                </Row>
                }
            </React.Fragment>
        );
    }
}

export default TourPrepayment;
