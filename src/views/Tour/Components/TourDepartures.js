import React, {Component} from 'react';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import {tourTypes} from '../../../prop/Types/Tour';

class TourDepartures extends Component {
    static propTypes = {
        tour: tourTypes,
    };
    constructor(props) {
        super(props);
        this.state = {
            tour: props.tour,
        };
    }
    render() {
        const {tour} = this.state;
        return (
            <React.Fragment>
                <Row className={`mb-3`}>
                    <Col xs={12} md={12}>
                        <h3>Отправления</h3>
                    </Col>
                    <Col>
                        {tour.departures.map((departure, key) => departure.departure_at_date !== null && (
                            <div key={key} className={`mb-1`}>
                                <div className={`d-flex`}>
                                    <div>
                                        <span className={`font-weight-bold`}>{departure.departure_at_date}</span>
                                        {departure.departure_at_hourly === 1 &&
                                        <span>
                                            &#160;в <span className={`font-weight-bold`}>{departure.departure_at_time}</span>
                                        </span>
                                        }
                                    </div>
                                    <div>
                                        &#160;из {departure.title}
                                    </div>
                                </div>
                                {departure.description !== null && departure.description !== '' &&
                                <div>
                                    <p className={`text-muted mb-0`}>
                                        {departure.description}
                                    </p>
                                </div>
                                }
                            </div>
                        ))}
                    </Col>
                </Row>
            </React.Fragment>
        );
    }
}

export default TourDepartures;
