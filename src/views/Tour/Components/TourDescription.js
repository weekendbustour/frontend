import React, {Component} from 'react';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import ReactMarkdown from '../../../plugins/ReactMarkdown';
import {tourTypes} from '../../../prop/Types/Tour';

class TourDescription extends Component {
    static propTypes = {
        tour: tourTypes,
    };
    constructor(props) {
        super(props);
        this.state = {
            tour: props.tour,
        };
    }
    render() {
        const {tour} = this.state;
        return (
            <React.Fragment>
                <Row className={`mb-3`}>
                    <Col xs={12} md={12}>
                        <h3>Описание</h3>
                        <ReactMarkdown source={tour.description} />
                    </Col>
                </Row>
            </React.Fragment>
        );
    }
}

export default TourDescription;
