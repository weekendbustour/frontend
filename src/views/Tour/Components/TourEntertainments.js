import React, {Component} from 'react';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import {tourTypes} from '../../../prop/Types/Tour';
import EntertainmentCardModal from "../../../containers/Modal/Entertainment/EntertainmentCardModal";
import Card from "../../../containers/Entertainment/Card/Card";

class TourEntertainments extends Component {
    static propTypes = {
        tour: tourTypes,
    };
    constructor(props) {
        super(props);
        this.state = {
            tour: props.tour,
        };
    }
    render() {
        const {tour} = this.state;
        return (
            <React.Fragment>
                <Row className={`mb-3`}>
                    <Col xs={12} md={12} className={``}>
                        <h3>Развлечения</h3>
                        <p>
                            Развлечения тура носят информативный характер, чтобы ознакомится в общем о данном развлечении.
                        </p>
                    </Col>
                    <Col xs={12}>
                        <Row className={`mb-3`}>
                            {tour.entertainments.map((entertainment, key) => (
                                <Col xs={12} sm={6} md={6} lg={4} xl={3} key={key}>
                                    <Card entertainment={entertainment}/>
                                </Col>
                            ))}
                        </Row>
                        <EntertainmentCardModal />
                    </Col>
                </Row>
            </React.Fragment>
        );
    }
}

export default TourEntertainments;
