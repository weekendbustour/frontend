import React, {Component} from 'react';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import {tourTypes} from '../../../prop/Types/Tour';
import ReactMarkdown from "../../../plugins/ReactMarkdown";

class TourSignature extends Component {
    static propTypes = {
        tour: tourTypes,
    };
    constructor(props) {
        super(props);
        this.state = {
            tour: props.tour,
        };
    }
    render() {
        const {tour} = this.state;
        return (
            <React.Fragment>
                <Row>
                    <Col xs={12}>
                        <hr className={`mt-0`}/>
                        <div>
                            <ReactMarkdown source={tour.organization.signature} />
                        </div>
                    </Col>
                </Row>
            </React.Fragment>
        );
    }
}

export default TourSignature;
