import React, {Component} from 'react';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import {tourTypes} from '../../../prop/Types/Tour';
import ReactMarkdown from "../../../plugins/ReactMarkdown";

class TourReservation extends Component {
    static propTypes = {
        tour: tourTypes,
    };
    constructor(props) {
        super(props);
        this.state = {
            tour: props.tour,
        };
    }
    render() {
        const {tour} = this.state;
        return (
            <React.Fragment>
                <Row className={`mb-3`}>
                    <Col xs={12} md={12} className={``}>
                        <h3>Как забронировать?</h3>
                    </Col>
                    <Col xs={12}>
                        <ReactMarkdown source={tour.parameters.reservation.value} />
                    </Col>
                </Row>
            </React.Fragment>
        );
    }
}

export default TourReservation;
