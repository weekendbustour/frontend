import React, {Component} from 'react';
import Cleave from 'cleave.js/react';

const cleave = {
    init(target, value) {
        value = value.replace(/[^0-9.]/g, '');
        // если начинается с +8 900 ... то сделать +7 900 ..
        // чтобы автокомплит срабатывал верно
        if (value.startsWith('89')) {
            value = '79' + value.slice(2);
            console.log('target.val(value)');
            //target.val(value);
            return;
        }
        //self.destroy();
        let char;
        let options;
        // для трех символов
        if (value.length >= 3) {
            char = value.substr(0, 3);
            if (cleave.three.check(char)) {
                options = cleave.three.cleave();
                console.log('3', options);
                return;
            }
        }
        // для двух введенных
        if (value.length >= 2) {
            char = value.substr(0, 2);
            if (cleave.two.check(char)) {
                options = cleave.two.cleave();
                console.log('2', options);
                return;
            }
        }
        // для двух введенных
        if (value.length >= 1) {
            char = value.substr(0, 1);
            if (cleave.one.check(char)) {
                options = cleave.one.cleave();
                console.log('1', options);
                return;
            }
        }
        console.log(options);
    },

    one: {
        chars: ['1', '7'],
        check(chars) {
            const self = this;
            return self.chars.includes(chars);
        },
        cleave() {
            return {
                blocks: [0, 1, 0, 3, 0, 3, 2, 4], // в конце должно быть 2, с запасом на 3ий вариант
                delimiters: ['+', ' ', '(', ')', ' ', '-', '-'],
                numericOnly: true,
            };
        },
    },
    two: {
        chars: [
            '20',
            '27',
            '28',
            '30',
            '31',
            '32',
            '33',
            '34',
            '36',
            '39',
            '40',
            '41',
            '43',
            '44',
            '45',
            '46',
            '47',
            '48',
            '49',
            '51',
            '52',
            '53',
            '54',
            '55',
            '56',
            '57',
            '58',
            '60',
            '61',
            '62',
            '63',
            '64',
            '65',
            '66',
            '73',
            '74',
            '76',
            '77',
            '78', //'79',
            '81',
            '82',
            '83',
            '84',
            '86', //'89',
            '90',
            '91',
            '92',
            '93',
            '94',
            '95',
            '98',
        ],
        check(chars) {
            const self = this;
            return self.chars.includes(chars);
        },
        cleave() {
            return {
                blocks: [0, 2, 0, 3, 0, 3, 2, 2],
                delimiters: ['+', ' ', '(', ')', ' ', '-', '-'],
                numericOnly: true,
                maxLength: 20,
            };
        },
    },
    three: {
        chars: [
            '21*',
            '22*',
            '23*',
            '24*',
            '25*',
            '26*',
            '29*',
            '35*',
            '37*',
            '38*',
            '42*',
            '50*',
            '59*',
            '67*',
            '68*',
            '69*',
            '80*',
            '85*',
            '87*',
            '88*',
            '96*',
            '97*',
            '99*',
        ],
        check(chars) {
            const self = this;
            if (self.chars.includes(chars)) {
                return true;
            }
            let char = chars.substr(0, 2) + '*';
            return self.chars.includes(char);
        },
        cleave() {
            return {
                blocks: [0, 3, 0, 3, 0, 3, 2, 2],
                delimiters: ['+', ' ', '(', ')', ' ', '-', '-'],
                numericOnly: true,
            };
        },
    },
    four: {
        chars: [
            '1242',
            '1246',
            '1264',
            '1268',
            '1284',
            '1340',
            '1345',
            '1441',
            '1473',
            '1649',
            '1658',
            '1664',
            '1670',
            '1671',
            '1684',
            '1721',
            '1758',
            '1767',
            '1784',
            '1787',
            '1809',
            '1829',
            '1849',
            '1868',
            '1869',
            '1876',
            '1939',
        ],
        check(chars) {
            const self = this;
            let char = chars.substr(0, 2) + '*';
            console.log(char);
            return self.chars.includes(char);
        },
        cleave() {
            return {
                blocks: [0, 3, 0, 3, 0, 3, 2, 2],
                delimiters: ['+', ' ', '(', ')', ' ', '-', '-'],
                numericOnly: true,
            };
        },
    },
};

class CleavePhone extends Component {
    constructor(props) {
        super(props);
        this.state = {
            phoneRawValue: '',
            cleaveOptions: {
                blocks: [1],
                numericOnly: true,
            },
        };
        this.onPhoneChange = this.onPhoneChange.bind(this);
    }
    onPhoneChange(event) {
        console.log(event.target.rawValue);
        const options = cleave.init(null, event.target.rawValue);
        this.setState({
            phoneRawValue: event.target.rawValue,
            cleaveOptions: options,
        });
        console.log(this.state.phoneRawValue);
        console.log(this.state.cleaveOptions);
    }
    render() {
        const {cleaveOptions} = this.state;
        return (
            <React.Fragment>
                <Cleave
                    className="form-control"
                    placeholder="Телефон"
                    options={cleaveOptions}
                    key={JSON.stringify(cleaveOptions)}
                    onChange={this.onPhoneChange}
                />
            </React.Fragment>
        );
    }
}

export default CleavePhone;
