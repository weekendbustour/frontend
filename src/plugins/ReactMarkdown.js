import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import * as PropTypes from 'prop-types';
const Markdown = require('react-markdown');
const breaks = require('remark-breaks');
const htmlParser = require('react-markdown/plugins/html-parser');

const parseHtml = htmlParser({
    isValidNode: node => node.type !== 'script',
});

class ReactMarkdown extends Component {
    static propTypes = {
        source: PropTypes.string,
    };

    constructor(props) {
        super(props);
        this.state = {
            source: props.source,
        };
    }
    render() {
        const {source} = this.state;
        return (
            <React.Fragment>
                <div className={`markdown-container`}>
                    <Markdown source={source} plugins={[breaks]} escapeHtml={false} astPlugins={[parseHtml]}/>
                </div>
            </React.Fragment>
        );
    }
}

export default withRouter(ReactMarkdown);
