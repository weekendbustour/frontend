import React from 'react';
const Index = React.lazy(() => import('./views/Index/Index'));
const Tours = React.lazy(() => import('./views/Tours/Tours'));
const Tour = React.lazy(() => import('./views/Tour/Tour'));
const DevTourCard = React.lazy(() => import('./views/Dev/Tour/DevTourCard'));
const Contacts = React.lazy(() => import('./views/Contacts/Contacts'));
const About = React.lazy(() => import('./views/About/About'));
const Category = React.lazy(() => import('./views/Category/Category'));
const FavouritesTours = React.lazy(() => import('./views/Account/Favourites/Tours'));
const FavouritesOrganizations = React.lazy(() => import('./views/Account/Favourites/Organizations'));
const FavouritesDirections = React.lazy(() => import('./views/Account/Favourites/Directions'));
const Reservations = React.lazy(() => import('./views/Account/Reservations/Reservations'));
const SubscriptionsDirections = React.lazy(() => import('./views/Account/Subscriptions/Directions'));
const SubscriptionsOrganizations = React.lazy(() => import('./views/Account/Subscriptions/Organizations'));
const Account = React.lazy(() => import('./views/Account/Account'));
const Organizations = React.lazy(() => import('./views/Organizations/Organizations'));
const Organization = React.lazy(() => import('./views/Organization/Organization'));
const OrganizationArticles = React.lazy(() => import('./views/Organization/OrganizationArticles'));
const Directions = React.lazy(() => import('./views/Directions/Directions'));
const Direction = React.lazy(() => import('./views/Direction/Direction'));
const DirectionAttractions = React.lazy(() => import('./views/Direction/DirectionAttractions'));
const Login = React.lazy(() => import('./views/Auth/Login'));
const Reset = React.lazy(() => import('./views/Auth/Reset'));
const Register = React.lazy(() => import('./views/Auth/Register'));
const Search = React.lazy(() => import('./views/Search/Search'));
//const Report = React.lazy(() => import('./views/Help/Report'));
//const Offer = React.lazy(() => import('./views/Help/Offer'));
//const Rating = React.lazy(() => import('./views/Help/Rating'));
const Help = React.lazy(() => import('./views/Help/Help'));
const News = React.lazy(() => import('./views/News/News'));
const Articles = React.lazy(() => import('./views/Articles/Articles'));
const Article = React.lazy(() => import('./views/Article/Article'));
const Attractions = React.lazy(() => import('./views/Attractions/Attractions'));
const Attraction = React.lazy(() => import('./views/Attraction/Attraction'));
const NewsItem = React.lazy(() => import('./views/NewsItem/NewsItem'));
const Name = React.lazy(() => import('./views/Name/Name'));

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
    {path: '/', exact: true, name: 'Index', component: Index},

    /**
     * Туры
     */
    {path: '/tours', exact: true, name: 'Tours', component: Tours},
    {path: '/tour/:id/:name', exact: false, name: 'Tour', component: Tour},

    /**
     * Организации
     */
    {path: '/organizations', exact: true, name: 'Organizations', component: Organizations},
    {path: '/organization/:id/articles', exact: false, name: 'OrganizationArticles', component: OrganizationArticles},
    {path: '/organization/:id/:name', exact: false, name: 'Organization', component: Organization},

    /**
     * Направления
     */
    {path: '/directions', exact: true, name: 'Directions', component: Directions},
    {path: '/direction/:id/attractions', exact: false, name: 'DirectionAttractions', component: DirectionAttractions},
    {path: '/direction/:id/:name', exact: false, name: 'Direction', component: Direction},

    /**
     * Страницы
     */
    {path: '/contacts', exact: true, name: 'Contacts', component: Contacts},
    {path: '/about', exact: true, name: 'About', component: About},
    {path: '/privacy', exact: true, name: 'Privacy', component: About},
    {path: '/terms', exact: true, name: 'Terms', component: About},
    {path: '/search', exact: true, name: 'Search', component: Search},

    /**
     * Новости
     */
    {path: '/news', exact: true, name: 'News', component: News},
    {path: '/news/:id/:name', exact: false, name: 'NewsItem', component: NewsItem},

    /**
     * Статьи
     */
    {path: '/articles', exact: true, name: 'Articles', component: Articles},
    {path: '/article/:id/:name', exact: false, name: 'Article', component: Article},

    /**
     * Достопримечательности
     */
    {path: '/attractions', exact: true, name: 'Attractions', component: Attractions},
    {path: '/attraction/:id/:name', exact: false, name: 'Attraction', component: Attraction},

    /**
     * Категории
     */
    {path: '/category/:id/:name', exact: false, name: 'Category', component: Category},

    /**
     * Аутентификация
     */
    {path: '/auth/login', exact: true, name: 'Login', component: Login},
    {path: '/auth/register', exact: true, name: 'Register', component: Register},
    {path: '/auth/logout', exact: true, name: 'Logout', component: Index},
    {path: '/auth/reset', exact: true, name: 'Reset', component: Reset},
    {path: '/auth/vkontakte/code', exact: true, name: 'VkCode', component: Index},
    {path: '/auth/vkontakte/wall', exact: true, name: 'VkCode', component: Index},
    {path: '/auth/facebook/callback', exact: true, name: 'FacebookCallback', component: Index},

    /**
     * Личный кабинет
     */
    {path: '/lk/account', exact: true, name: 'Account', component: Account},

    /**
     * Бронирования
     */
    {path: '/lk/reservations', exact: true, name: 'Reservations', component: Reservations},

    /**
     * Избранные
     */
    {path: '/lk/favourites', exact: true, name: 'Favourites', component: FavouritesTours},
    {path: '/lk/favourites/tours', exact: true, name: 'FavouritesTours', component: FavouritesTours},
    {path: '/lk/favourites/organizations', exact: true, name: 'FavouritesOrganizations', component: FavouritesOrganizations},
    {path: '/lk/favourites/directions', exact: true, name: 'FavouritesDirections', component: FavouritesDirections},

    /**
     * Подписки
     */
    {path: '/lk/subscriptions', exact: true, name: 'Subscriptions', component: SubscriptionsDirections},
    {path: '/lk/subscriptions/directions', exact: true, name: 'SubscriptionsDirections', component: SubscriptionsDirections},
    {path: '/lk/subscriptions/organizations', exact: true, name: 'SubscriptionsOrganizations', component: SubscriptionsOrganizations},

    /**
     * Помощь
     */
    {path: '/help', exact: true, name: 'Help', component: Help},


    /**
     * Организаторы
     */
    {path: '/:name', exact: true, name: 'Name', component: Name},
    //{path: '/help/report', exact: true, name: 'Report', component: Report},
    //{path: '/help/offer', exact: true, name: 'Offer', component: Offer},
    //{path: '/help/rating', exact: true, name: 'Rating', component: Rating},

    {path: '/dev/tour/:id/card', exact: false, name: 'DevTourCard', component: DevTourCard},
];

export default routes;
