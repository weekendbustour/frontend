import * as PropTypes from 'prop-types';
import {organizationTypes} from './Organization';

export const tourCancellationTypes = PropTypes.shape({
    type: PropTypes.string,
    title: PropTypes.string,
    description: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(null)]),
    start_at_format: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(null)]),
    finish_at_format: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(null)]),
});

export const tourTimelineTypes = PropTypes.shape({
    id: PropTypes.number,
    type: PropTypes.string,
    images: PropTypes.array,
    time_description: PropTypes.string,
    start_at_time: PropTypes.string,
    finish_at_time: PropTypes.string,
    preview_description: PropTypes.string,
});

export const tourTimelinesSorted = PropTypes.arrayOf(tourTimelineTypes);

export const tourPriceTypes = PropTypes.oneOfType([
    PropTypes.shape({
        discount: PropTypes.number,
        value: PropTypes.number,
        value_description: PropTypes.string,
        discount_description: PropTypes.string,
        prepayment: PropTypes.number,
        prepayment_description: PropTypes.string,
    }),
    PropTypes.instanceOf(null),
]);

export const tourTimelinesTypes = PropTypes.objectOf(tourTimelinesSorted);

export const tourTypes = PropTypes.shape({
    id: PropTypes.number,
    price: tourPriceTypes,
    title: PropTypes.string,

    categories: PropTypes.array,
    parameters: PropTypes.shape({
        outfit_rent_note: PropTypes.object,
    }),
    image: PropTypes.shape({
        images: PropTypes.array,
        images_captions: PropTypes.array,
        banner_has: PropTypes.bool,
        banner: PropTypes.string,
        banner_captions: PropTypes.array,
    }),
    parameters_view: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
    published_at_format: PropTypes.string,
    organization: organizationTypes,
    cancellation: tourCancellationTypes,
    announcement: PropTypes.object,
    meta_tag: PropTypes.object,
    multiples: PropTypes.array,
    outfits_rent: PropTypes.array,
});

export const toursTypes = PropTypes.arrayOf(tourTypes);
