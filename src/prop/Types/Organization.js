import * as PropTypes from 'prop-types';

export const organizationTypes = PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    title: PropTypes.string,
});

export const organizationsTypes = PropTypes.arrayOf(organizationTypes);
