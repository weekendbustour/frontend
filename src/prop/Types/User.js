import * as PropTypes from 'prop-types';
import {commentTypes} from './Comment';
import {favouritesTypes} from './Favourite';
// eslint-disable-next-line import/named
import {subscriptionsTypes} from './Subscription';

export const userTypes = PropTypes.shape({
    id: PropTypes.number,
    first_name: PropTypes.string,
    last_name: PropTypes.string,
    second_name: PropTypes.string,
    email: PropTypes.string,
    phone: PropTypes.string,
    image: PropTypes.string,
    subscriber: PropTypes.bool,
    isAdmin: PropTypes.bool,
    birthday_at: PropTypes.string,
});

export const userCommentsTypes = PropTypes.arrayOf(commentTypes);

export const userFavouritesTypes = PropTypes.oneOfType([
    PropTypes.shape({
        news: favouritesTypes,
        directions: favouritesTypes,
        organizations: favouritesTypes,
    }),
    PropTypes.instanceOf(null),
]);

export const userSubscriptionsTypes = PropTypes.oneOfType([
    PropTypes.shape({
        news: subscriptionsTypes,
        directions: subscriptionsTypes,
        organizations: subscriptionsTypes,
    }),
    PropTypes.instanceOf(null),
]);

export const userReservationsTypes = PropTypes.oneOfType([
    PropTypes.shape({
        tours: PropTypes.array,
    }),
    PropTypes.instanceOf(null),
]);
