import * as PropTypes from 'prop-types';

export const promotionTypes = PropTypes.shape({
    id: PropTypes.number,
    title: PropTypes.string,
    description: PropTypes.string,
    url: PropTypes.string,
    images: PropTypes.array,
});

export const promotionsTypes = PropTypes.arrayOf(promotionTypes);

export const promotionsTargetDefaultTypes = PropTypes.shape({
    target: promotionTypes,
    default: promotionTypes,
});
