import * as PropTypes from 'prop-types';

export const entertainmentTypes = PropTypes.shape({
    id: PropTypes.number,
    title: PropTypes.string,
    image: PropTypes.shape({
        default: PropTypes.string,
    }),
    images: PropTypes.array,
    preview_description: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(null)]),
});
