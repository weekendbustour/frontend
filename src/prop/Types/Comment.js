import * as PropTypes from 'prop-types';

export const commentTypes = PropTypes.shape({
    id: PropTypes.number,
    author: PropTypes.string,
    image: PropTypes.string,
    text: PropTypes.string,
    rate: PropTypes.number,
    anon: PropTypes.number,
    rating: PropTypes.number,
    votes: PropTypes.number,
    parent_id: PropTypes.number,
    children: PropTypes.array,
    created_at_format: PropTypes.string,
});

export const commentsTypes = PropTypes.arrayOf(commentTypes);
