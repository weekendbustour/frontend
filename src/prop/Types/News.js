import * as PropTypes from 'prop-types';

export const newsItemTypes = PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    title: PropTypes.string,
});

export const newsTypes = PropTypes.arrayOf(newsItemTypes);
