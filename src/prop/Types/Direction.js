import * as PropTypes from 'prop-types';

export const directionTypes = PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    title: PropTypes.string,
    attractions: PropTypes.shape({
        data: PropTypes.array,
        total: PropTypes.number,
    }),
    image: PropTypes.shape({
        card: PropTypes.string,
        banner: PropTypes.string,
    }),
});

export const directionsTypes = PropTypes.arrayOf(directionTypes);
