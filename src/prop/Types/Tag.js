import * as PropTypes from 'prop-types';

export const tagTypes = PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    title: PropTypes.string,
    color: PropTypes.string,
});
