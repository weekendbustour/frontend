import * as PropTypes from 'prop-types';

export const statisticDataItemTypes = PropTypes.shape({
    count: PropTypes.number,
});

export const statisticDataTypes = PropTypes.shape({
    tours: statisticDataItemTypes,
    organizations: statisticDataItemTypes,
    directions: statisticDataItemTypes,
    articles: statisticDataItemTypes,
    news: statisticDataItemTypes,
    users: statisticDataItemTypes,
});
