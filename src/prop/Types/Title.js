import * as PropTypes from 'prop-types';

export const titleTypes = PropTypes.shape({
    title: PropTypes.string,
    description: PropTypes.string,
    other: PropTypes.object,
    near: PropTypes.object,
    duplicate: PropTypes.object,
});

export const titlesTypes = PropTypes.shape({
    tour: titleTypes,
    organization: titleTypes,
    direction: titleTypes,
    news: titleTypes,
    article: titleTypes,
});
