import * as PropTypes from 'prop-types';

export const articleTypes = PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    title: PropTypes.string,
    with_banner: PropTypes.bool,
});

export const articlesTypes = PropTypes.arrayOf(articleTypes);
