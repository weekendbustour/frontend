import * as PropTypes from 'prop-types';

export const categoryTypes = PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    title: PropTypes.string,
    color: PropTypes.string,
});
