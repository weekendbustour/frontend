import * as PropTypes from "prop-types";

export const paginationTypes = PropTypes.shape({
    total: PropTypes.number,
    page: PropTypes.number,
    limit: PropTypes.number,
});
