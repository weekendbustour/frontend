import * as PropTypes from 'prop-types';

export const attractionTypes = PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    title: PropTypes.string,
    with_banner: PropTypes.bool,
});

export const attractionsTypes = PropTypes.arrayOf(attractionTypes);
