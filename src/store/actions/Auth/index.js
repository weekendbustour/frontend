import {
    FETCH_AUTH_GUEST,
    FETCH_AUTH_LOGIN_ERRORS,
    FETCH_AUTH_LOGIN_PENDING,
    FETCH_AUTH_LOGIN_SUCCESS,
    FETCH_AUTH_REGISTER_ERRORS,
    FETCH_AUTH_REGISTER_PENDING,
    FETCH_AUTH_REGISTER_SUCCESS,
    FETCH_AUTH_RESET_PASSWORD_ERRORS,
    FETCH_AUTH_RESET_PASSWORD_PENDING,
    FETCH_AUTH_RESET_PASSWORD_SUCCESS,
    FETCH_AUTH_USER,
    FETCH_AUTH_LOGIN_SOCIAL_VK_ERRORS,
    FETCH_AUTH_LOGIN_SOCIAL_VK_PENDING,
    FETCH_AUTH_LOGIN_SOCIAL_VK_SUCCESS,
} from '../../constants/Auth';
import {API_URL, axiosBody, headers} from "../../../api/api";
import {toastByError, toastBySuccess} from "../../../lib/toast";
import axios from 'axios';
import {fetchUserSuccess, fetchUserTokenSuccess} from "../../reducers/User";
import {request} from "../../../lib/request";

export function fetchAuthUser(data) {
    return {
        type: FETCH_AUTH_USER,
        user: data,
    };
}

export function fetchAuthGuest(state) {
    return {
        type: FETCH_AUTH_GUEST,
        guest: state,
    };
}

/*
 * ----------------------------------------
 * LOGIN
 * ----------------------------------------
 */

function fetchAuthLoginPending() {
    return {
        type: FETCH_AUTH_LOGIN_PENDING,
    };
}
function fetchAuthLoginSuccess() {
    return {
        type: FETCH_AUTH_LOGIN_SUCCESS,
    };
}
function fetchAuthLoginErrors(errors) {
    return {
        type: FETCH_AUTH_LOGIN_ERRORS,
        errors: errors,
    };
}

export function fetchUserLogin(data) {
    return dispatch => {
        dispatch(fetchAuthLoginPending());
        axios.defaults.withCredentials = true;
        // CSRF COOKIE
        axios.get(API_URL + `/sanctum/csrf-cookie`).then((response) => {
            console.log(response);
            // LOGIN
            axios.post(
                API_URL + `/auth/login`,
                axiosBody({
                    email: data.email,
                    password: data.password,
                })
            ).then(response => {
                const data = response.data.data;
                //const hash = data.hash;
                const token = data.token;
                // GET USER
                axios.get(
                    API_URL + `/user`, {
                        headers: headers(token),
                    },
                ).then(response => {
                    const data = response.data.data;
                    toastBySuccess(response);
                    dispatch(fetchAuthLoginSuccess());
                    dispatch(fetchUserSuccess(data));
                    dispatch(fetchUserTokenSuccess(token));
                    window.location.reload();
                }).catch(error => {
                    toastByError(error);
                    let errors = {};
                    if (error.response !== undefined) {
                        errors = error.response.data.errors
                    }
                    dispatch(fetchAuthLoginErrors(errors));
                });
            }).catch(error => {
                toastByError(error);
                let errors = {};
                if (error.response !== undefined) {
                    errors = error.response.data.errors
                }
                dispatch(fetchAuthLoginErrors(errors));
            });
        }).catch(error => {
            toastByError(error);
            dispatch(fetchAuthLoginErrors({}));
        });
    };
}


/*
 * ----------------------------------------
 * REGISTER
 * ----------------------------------------
 */
function fetchAuthRegisterPending() {
    return {
        type: FETCH_AUTH_REGISTER_PENDING,
    };
}
function fetchAuthRegisterSuccess() {
    return {
        type: FETCH_AUTH_REGISTER_SUCCESS,
    };
}
function fetchAuthRegisterErrors(errors) {
    return {
        type: FETCH_AUTH_REGISTER_ERRORS,
        errors: errors,
    };
}

export function fetchAuthRegister(data) {
    return dispatch => {
        dispatch(fetchAuthRegisterPending());
        axios.defaults.withCredentials = true;
        // CSRF COOKIE
        axios.get(API_URL + `/sanctum/csrf-cookie`).then((response) => {
            console.log(response);
            // LOGIN
            axios.post(
                API_URL + `/auth/register`,
                axiosBody({
                    phone: data.phone,
                    email: data.email,
                    password: data.password,
                })
            ).then(response => {
                const data = response.data.data;
                //const hash = data.hash;
                const token = data.token;
                // GET USER
                axios.get(
                    API_URL + `/user`, {
                        headers: headers(token),
                    },
                ).then(response => {
                    const data = response.data.data;
                    toastBySuccess(response);
                    dispatch(fetchAuthRegisterSuccess());
                    dispatch(fetchUserSuccess(data));
                    dispatch(fetchUserTokenSuccess(token));
                    window.location.reload();
                }).catch(error => {
                    toastByError(error);
                    let errors = {};
                    if (error.response !== undefined) {
                        errors = error.response.data.errors
                    }
                    dispatch(fetchAuthRegisterErrors(errors));
                });
            }).catch(error => {
                toastByError(error);
                let errors = {};
                if (error.response !== undefined) {
                    errors = error.response.data.errors
                }
                dispatch(fetchAuthRegisterErrors(errors));
            });
        }).catch(error => {
            toastByError(error);
            dispatch(fetchAuthRegisterErrors({}));
        });
    };
}

/*
 * ----------------------------------------
 * RESET PASSWORD
 * ----------------------------------------
 */
function fetchAuthResetPasswordPending() {
    return {
        type: FETCH_AUTH_RESET_PASSWORD_PENDING,
    };
}
function fetchAuthResetPasswordSuccess() {
    return {
        type: FETCH_AUTH_RESET_PASSWORD_SUCCESS,
    };
}
function fetchAuthResetPasswordErrors(errors) {
    return {
        type: FETCH_AUTH_RESET_PASSWORD_ERRORS,
        errors: errors,
    };
}
export function fetchAuthForgotPassword(data) {
    return dispatch => {
        dispatch(fetchAuthResetPasswordPending());
        request.post(`/auth/password/email`, {
            email: data.email,
        }).then(response => {
            //const data = response.data.data;
            toastBySuccess(response);
            dispatch(fetchAuthResetPasswordSuccess());
        }).catch(error => {
            toastByError(error);
            let errors = {};
            if (error.response !== undefined) {
                errors = error.response.data.errors
            }
            dispatch(fetchAuthResetPasswordErrors(errors));
        });
    };
}

/*
 * ----------------------------------------
 * AUTH SOCIAL VK
 * ----------------------------------------
 */

export function fetchUserSuccessLogin(data, token) {
    return dispatch => {
        dispatch(fetchAuthLoginSuccess());
        dispatch(fetchUserSuccess(data));
        dispatch(fetchUserTokenSuccess(token));
        window.location.reload();
    };
}

function fetchAuthLoginSocialVkPending() {
    return {
        type: FETCH_AUTH_LOGIN_SOCIAL_VK_PENDING,
    };
}
function fetchAuthLoginSocialVkSuccess() {
    return {
        type: FETCH_AUTH_LOGIN_SOCIAL_VK_SUCCESS,
    };
}
function fetchAuthLoginSocialVkErrors(errors) {
    return {
        type: FETCH_AUTH_LOGIN_SOCIAL_VK_ERRORS,
        errors: errors,
    };
}

export function fetchUserLoginSocialVk(data) {
    return dispatch => {
        dispatch(fetchAuthLoginSocialVkPending());
        axios.defaults.withCredentials = true;
        // CSRF COOKIE
        request.get(`/sanctum/csrf-cookie`).then(response => {
            request.post(`/auth/vkontakte/callback`, data).then(response => {
                const data = response.data.data;
                const token = data.token;
                dispatch(fetchUserTokenSuccess(token));
                // GET USER
                request.get(`/user`).then(response => {
                    const data = response.data.data;
                    toastBySuccess(response);
                    dispatch(fetchAuthLoginSocialVkSuccess());
                    dispatch(fetchUserSuccessLogin(data, token));
                }).catch(error => {
                    toastByError(error);
                    let errors = {};
                    if (error.response !== undefined) {
                        errors = error.response.data.errors
                    }
                    dispatch(fetchAuthLoginSocialVkErrors(errors));
                });
            }).catch(error => {
                toastByError(error);
                dispatch(fetchAuthLoginSocialVkErrors({}));
            });
        }).catch(error => {
            toastByError(error);
            dispatch(fetchAuthLoginSocialVkErrors({}));
        });
    };
}
