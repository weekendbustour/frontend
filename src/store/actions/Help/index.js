import {progressBar} from '../../progress';
import {API_URL, axiosBody} from '../../../api/api';
import {
    FETCH_HELP_REPORT_PENDING,
    FETCH_HELP_REPORT_SUCCESS,
    FETCH_HELP_OFFER_PENDING,
    FETCH_HELP_OFFER_SUCCESS,
    FETCH_HELP_RATING_PENDING,
    FETCH_HELP_RATING_SUCCESS,
} from '../../constants/Help';
import {toastByError, toastBySuccess} from "../../../lib/toast";
import axios from 'axios';

function fetchHelpReportPending() {
    return {
        type: FETCH_HELP_REPORT_PENDING,
    };
}

function fetchHelpReportSuccess() {
    return {
        type: FETCH_HELP_REPORT_SUCCESS,
    };
}

function fetchHelpReportError() {
    return {
        type: FETCH_HELP_REPORT_SUCCESS,
    };
}

function fetchHelpOfferPending() {
    return {
        type: FETCH_HELP_OFFER_PENDING,
    };
}

function fetchHelpOfferSuccess() {
    return {
        type: FETCH_HELP_OFFER_SUCCESS,
    };
}

function fetchHelpOfferError() {
    return {
        type: FETCH_HELP_OFFER_SUCCESS,
    };
}

function fetchHelpRatingPending() {
    return {
        type: FETCH_HELP_RATING_PENDING,
    };
}

export function fetchHelpRatingSuccess(success) {
    return {
        type: FETCH_HELP_RATING_SUCCESS,
        success: success,
    };
}

function fetchHelpRatingError() {
    return {
        type: FETCH_HELP_RATING_SUCCESS,
    };
}

export function fetchHelpReport(data) {
    return dispatch => {
        progressBar.start();
        dispatch(fetchHelpReportPending());
        axios.post(
            API_URL + `/help/report`,
            axiosBody(data)
        ).then(response => {
            progressBar.stop();
            toastBySuccess(response);
            dispatch(fetchHelpReportSuccess());
        }).catch(error => {
            progressBar.stop();
            toastByError(error);
            dispatch(fetchHelpReportError());
        });
    };
}

export function fetchHelpRating(data) {
    return dispatch => {
        progressBar.start();
        dispatch(fetchHelpRatingPending());
        axios.post(
            API_URL + `/help/rating`,
            axiosBody(data)
        ).then(response => {
            progressBar.stop();
            toastBySuccess(response);
            dispatch(fetchHelpRatingSuccess(true));
        }).catch(error => {
            progressBar.stop();
            toastByError(error);
            dispatch(fetchHelpRatingError());
        });
    };
}

export function fetchHelpOffer(data) {
    return dispatch => {
        progressBar.start();
        dispatch(fetchHelpOfferPending());
        axios.post(
            API_URL + `/help/offer`,
            axiosBody(data)
        ).then(response => {
            progressBar.stop();
            toastBySuccess(response);
            dispatch(fetchHelpOfferSuccess());
        }).catch(error => {
            progressBar.stop();
            toastByError(error);
            dispatch(fetchHelpOfferError());
        });
    };
}
