import {progressBar} from '../../progress';
import {API_URL} from '../../../api/api';
import {
    FETCH_DATA_DIRECTIONS_SUCCESS,
    FETCH_DATA_ERROR,
    FETCH_DATA_ORGANIZATIONS_SUCCESS,
    FETCH_DATA_PENDING,
    FETCH_DATA_SEARCH_CATEGORIES_SUCCESS,
    FETCH_DATA_SEARCH_DEPARTURES_SUCCESS,
    FETCH_DATA_SEARCH_DIRECTIONS_SUCCESS,
    FETCH_DATA_SEARCH_ORDERS_SUCCESS,
    FETCH_DATA_SEARCH_ORGANIZATIONS_SUCCESS,
    FETCH_DATA_TOURS_SUCCESS,
    FETCH_DATA_PROMOTIONS_SUCCESS,
    FETCH_DATA_NEWS_SUCCESS, FETCH_DATA_ARTICLES_SUCCESS,
    FETCH_DATA_QUESTIONS_SUCCESS,
    FETCH_DATA_TITLES_SUCCESS,
    FETCH_DATA_ABOUT_SUCCESS,
    FETCH_DATA_STATISTIC_SUCCESS,
} from '../../constants/Data';

function fetchDataPending() {
    return {
        type: FETCH_DATA_PENDING,
    };
}

function fetchDataSearchOrdersSuccess(data) {
    return {
        type: FETCH_DATA_SEARCH_ORDERS_SUCCESS,
        searchOrders: data,
    };
}

function fetchDataSearchDirectionsSuccess(data) {
    return {
        type: FETCH_DATA_SEARCH_DIRECTIONS_SUCCESS,
        searchDirections: data,
    };
}

function fetchDataSearchCategoriesSuccess(data) {
    return {
        type: FETCH_DATA_SEARCH_CATEGORIES_SUCCESS,
        searchCategories: data,
    };
}

function fetchDataSearchOrganizationsSuccess(data) {
    return {
        type: FETCH_DATA_SEARCH_ORGANIZATIONS_SUCCESS,
        searchOrganizations: data,
    };
}

function fetchDataSearchDeparturesSuccess(data) {
    return {
        type: FETCH_DATA_SEARCH_DEPARTURES_SUCCESS,
        searchDepartures: data,
    };
}

function fetchDataToursSuccess(tours) {
    return {
        type: FETCH_DATA_TOURS_SUCCESS,
        tours: tours,
    };
}

function fetchDataDirectionsSuccess(data) {
    return {
        type: FETCH_DATA_DIRECTIONS_SUCCESS,
        directions: data,
    };
}

function fetchDataOrganizationsSuccess(data) {
    return {
        type: FETCH_DATA_ORGANIZATIONS_SUCCESS,
        organizations: data,
    };
}

function fetchDataNewsSuccess(data) {
    return {
        type: FETCH_DATA_NEWS_SUCCESS,
        news: data,
    };
}

function fetchDataArticlesSuccess(data) {
    return {
        type: FETCH_DATA_ARTICLES_SUCCESS,
        articles: data,
    };
}

function fetchDataPromotionsSuccess(data) {
    return {
        type: FETCH_DATA_PROMOTIONS_SUCCESS,
        promotions: data,
    };
}

function fetchDataQuestionsSuccess(data) {
    return {
        type: FETCH_DATA_QUESTIONS_SUCCESS,
        questions: data,
    };
}

function fetchDataTitlesSuccess(data) {
    return {
        type: FETCH_DATA_TITLES_SUCCESS,
        titles: data,
    };
}

function fetchDataAboutSuccess(data) {
    return {
        type: FETCH_DATA_ABOUT_SUCCESS,
        about: data,
    };
}

const fetchDataStatisticSuccess = data => ({
    type: FETCH_DATA_STATISTIC_SUCCESS,
    statistic: data,
});

function fetchDataError(error) {
    return {
        type: FETCH_DATA_ERROR,
        error: error,
    };
}

export function fetchData() {
    return dispatch => {
        progressBar.start();
        //apiData.getData();
        dispatch(fetchDataPending());
        fetch(API_URL + '/data')
            .then(res => res.json())
            .then(res => {
                progressBar.stop();
                const data = res.data;
                if (res.error) {
                    throw res.error;
                }
                dispatch(fetchDataWithoutRequest(data));
            })
            .catch(error => {
                dispatch(fetchDataError(error));
            });
    };
}


export function fetchDataWithoutRequest(data) {
    return dispatch => {
        if (data.search && data.search.orders) {
            dispatch(fetchDataSearchOrdersSuccess(data.search.orders));
        }
        if (data.search && data.search.directions) {
            dispatch(fetchDataSearchDirectionsSuccess(data.search.directions));
        }
        if (data.search && data.search.categories) {
            dispatch(fetchDataSearchCategoriesSuccess(data.search.categories));
        }
        if (data.search && data.search.organizations) {
            dispatch(fetchDataSearchOrganizationsSuccess(data.search.organizations));
        }
        if (data.search && data.search.departures) {
            dispatch(fetchDataSearchDeparturesSuccess(data.search.departures));
        }
        if (data.tours) {
            dispatch(fetchDataToursSuccess(data.tours));
        }
        if (data.directions) {
            dispatch(fetchDataDirectionsSuccess(data.directions));
        }
        if (data.organizations) {
            dispatch(fetchDataOrganizationsSuccess(data.organizations));
        }
        if (data.promotions) {
            dispatch(fetchDataPromotionsSuccess(data.promotions));
        }
        if (data.news) {
            dispatch(fetchDataNewsSuccess(data.news));
        }
        if (data.articles) {
            dispatch(fetchDataArticlesSuccess(data.articles));
        }
        if (data.questions) {
            dispatch(fetchDataQuestionsSuccess(data.questions));
        }
        if (data.titles) {
            dispatch(fetchDataTitlesSuccess(data.titles));
        }
        if (data.about) {
            dispatch(fetchDataAboutSuccess(data.about));
        }
        if (data.statistic) {
            dispatch(fetchDataStatisticSuccess(data.statistic));
        }
    };
}
