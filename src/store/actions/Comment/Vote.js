import {FETCH_COMMENT_VOTE_PENDING_ARRAY, FETCH_COMMENT_VOTE_SUCCESS} from '../../constants/Comment/Vote';
import {getUser} from '../../reducers/User';
import store from '../../index';
import {toastAuthModal, toastByError} from '../../../lib/toast';
import {langAuth} from '../../../lang/auth';
import {fetchModal} from '../../reducers/Modal';
import {MODAL_LOGIN} from '../../../lib/modal';
import {request} from '../../../lib/request';
import {
    fetchCommentsArticleUpdate,
    fetchCommentsAttractionUpdate,
    fetchCommentsDirectionUpdate,
    fetchCommentsNewsUpdate,
    fetchCommentsOrganizationUpdate,
} from '../../reducers/Comments';
import {fetchUserCommentVotesId} from '../../reducers/User/CommentVotes';

function fetchCommentVotePending(pendingName) {
    return {
        type: FETCH_COMMENT_VOTE_PENDING_ARRAY,
        pending: pendingName,
    };
}

export function fetchCommentVoteSuccess(data, pendingName) {
    return {
        type: FETCH_COMMENT_VOTE_SUCCESS,
        comment: data,
        pending: pendingName,
    };
}

export function fetchCommentVote(data, state) {
    const {id, model, model_id} = data;
    const user = getUser(store.getState());
    const pendingName = `${model}-${model_id}-comment-${id}`;
    if (user === null) {
        return dispatch => {
            toastAuthModal(langAuth.access.commentVote);
            dispatch(fetchModal(MODAL_LOGIN));
        };
    }
    return dispatch => {
        dispatch(fetchCommentVotePending(pendingName));
        request
            .post(`/user/comment/${id}/vote`, {
                value: state ? 1 : 0,
            })
            .then(response => {
                const data = response.data;
                dispatch(fetchCommentVoteSuccess(data.comment, pendingName));
                dispatch(fetchUserCommentVotesId());
                if (model === 'direction') {
                    dispatch(fetchCommentsDirectionUpdate(data.comment));
                }
                if (model === 'organization') {
                    dispatch(fetchCommentsOrganizationUpdate(data.comment));
                }
                if (model === 'news') {
                    dispatch(fetchCommentsNewsUpdate(data.comment));
                }
                if (model === 'article') {
                    dispatch(fetchCommentsArticleUpdate(data.comment));
                }
                if (model === 'attraction') {
                    dispatch(fetchCommentsAttractionUpdate(data.comment));
                }
            })
            .catch(error => {
                toastByError(error);
                dispatch(fetchCommentVoteSuccess({}, pendingName));
            });
    };
}
