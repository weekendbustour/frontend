import {progressBar} from '../../progress';
import {
    FETCH_ARTICLE_PENDING,
    FETCH_ARTICLE_ERROR,
    FETCH_ARTICLE_SUCCESS,
} from '../../constants/Article';
import {toastBySuccess} from '../../../lib/toast';
import {request} from "../../../lib/request";
import seo from "../../../lib/seo";

function fetchArticlePending() {
    return {
        type: FETCH_ARTICLE_PENDING,
    };
}

function fetchArticleSuccess(data) {
    seo.article(data);
    return {
        type: FETCH_ARTICLE_SUCCESS,
        article: data,
    };
}

// eslint-disable-next-line
function fetchArticleError(error) {
    return {
        type: FETCH_ARTICLE_ERROR,
        error: error,
    };
}
export function fetchArticle(id) {
    return dispatch => {
        progressBar.start();
        dispatch(fetchArticlePending());
        request.get(`/article/${id}`).then(response => {
            const data = response.data.data;
            progressBar.stop();
            toastBySuccess(response);
            dispatch(fetchArticleSuccess(data));
        });
    };
}
