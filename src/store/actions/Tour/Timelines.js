import {API_URL} from '../../../api/api';
import {FETCH_TOUR_TIMELINES_PENDING, FETCH_TOUR_TIMELINES_ERROR, FETCH_TOUR_TIMELINES_SUCCESS} from '../../constants/Tour/Timelines';
import axios from 'axios';
import {toastByError, toastBySuccess} from '../../../lib/toast';

function fetchTourTimelinesPending() {
    return {
        type: FETCH_TOUR_TIMELINES_PENDING,
    };
}

export function fetchTourTimelinesSuccess(data) {
    return {
        type: FETCH_TOUR_TIMELINES_SUCCESS,
        data: data,
    };
}

function fetchTourTimelinesError(error) {
    return {
        type: FETCH_TOUR_TIMELINES_ERROR,
        error: error,
    };
}

export function fetchTourTimelines(data) {
    return dispatch => {
        //progressBar.start();
        dispatch(fetchTourTimelinesPending());
        axios
            .get(API_URL + `/tour/${data.id}/timelines`)
            .then(response => {
                const data = response.data.data;
                //progressBar.stop();
                toastBySuccess(response);
                dispatch(fetchTourTimelinesSuccess(data));
            })
            .catch(error => {
                //progressBar.stop();
                toastByError(error);
                dispatch(fetchTourTimelinesError());
            });
    };
}
