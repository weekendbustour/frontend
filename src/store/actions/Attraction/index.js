import {progressBar} from '../../progress';
import {
    FETCH_ATTRACTION_PENDING,
    FETCH_ATTRACTION_ERROR,
    FETCH_ATTRACTION_SUCCESS,
} from '../../constants/Attraction';
import {toastBySuccess} from '../../../lib/toast';
import {request} from "../../../lib/request";
import seo from "../../../lib/seo";

function fetchAttractionPending() {
    return {
        type: FETCH_ATTRACTION_PENDING,
    };
}

function fetchAttractionSuccess(data) {
    seo.attraction(data);
    return {
        type: FETCH_ATTRACTION_SUCCESS,
        attraction: data,
    };
}

// eslint-disable-next-line
function fetchAttractionError(error) {
    return {
        type: FETCH_ATTRACTION_ERROR,
        error: error,
    };
}
export function fetchAttraction(id) {
    return dispatch => {
        progressBar.start();
        dispatch(fetchAttractionPending());
        request.get(`/attraction/${id}`).then(response => {
            const data = response.data.data;
            progressBar.stop();
            toastBySuccess(response);
            dispatch(fetchAttractionSuccess(data));
        });
    };
}
