import {progressBar} from '../../progress';
import {
    FETCH_DIRECTIONS_PENDING,
    FETCH_DIRECTIONS_SUCCESS,
    FETCH_DIRECTIONS_PAGINATION,
} from '../../constants/Directions';
import {toastBySuccess} from '../../../lib/toast';
import {request} from "../../../lib/request";

function fetchDirectionsPending() {
    return {
        type: FETCH_DIRECTIONS_PENDING,
    };
}

function fetchDirectionsSuccess(data) {
    return {
        type: FETCH_DIRECTIONS_SUCCESS,
        directions: data,
    };
}
//
// function fetchDirectionsError(error) {
//     return {
//         type: FETCH_DIRECTIONS_ERROR,
//         error: error,
//     };
// }
function fetchDirectionsPagination(data) {
    return {
        type: FETCH_DIRECTIONS_PAGINATION,
        pagination: data,
    };
}

export function fetchDirections(data) {
    return dispatch => {
        progressBar.start();
        dispatch(fetchDirectionsPending());
        request.get(`/directions`, data).then(response => {
            const data = response.data.data;
            progressBar.stop();
            toastBySuccess(response);
            dispatch(fetchDirectionsSuccess(data));
            if (response.data.pagination !== undefined) {
                dispatch(fetchDirectionsPagination(response.data.pagination));
            }
        });
    };
}
