import {FETCH_APP_VIEW_TOURS_SUCCESS} from '../../constants/App/View';

const fetchAppViewToursSuccess = type => ({
    type: FETCH_APP_VIEW_TOURS_SUCCESS,
    data: type,
});

export function fetchAppViewTours(type) {
    return dispatch => {
        dispatch(fetchAppViewToursSuccess(type));
    };
}
