import {FETCH_APP_SEO_SUCCESS} from '../../constants/App/Seo';

const fetchAppSeoSuccess = data => ({
    type: FETCH_APP_SEO_SUCCESS,
    data: data,
});

export function fetchAppSeo(data) {
    return dispatch => {
        dispatch(fetchAppSeoSuccess(data));
    };
}
