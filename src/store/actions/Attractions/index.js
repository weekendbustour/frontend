import {progressBar} from '../../progress';
import {
    FETCH_ATTRACTIONS_PENDING,
    FETCH_ATTRACTIONS_ERROR,
    FETCH_ATTRACTIONS_SUCCESS,
    FETCH_ATTRACTIONS_PAGINATION,
} from '../../constants/Attractions';
import {toastByError, toastBySuccess} from '../../../lib/toast';
import {request} from "../../../lib/request";

function fetchAttractionsPending() {
    return {
        type: FETCH_ATTRACTIONS_PENDING,
    };
}

function fetchAttractionsSuccess(data) {
    return {
        type: FETCH_ATTRACTIONS_SUCCESS,
        attractions: data,
    };
}

function fetchAttractionsError(error) {
    return {
        type: FETCH_ATTRACTIONS_ERROR,
        error: error,
    };
}
function fetchAttractionsPagination(pagination) {
    return {
        type: FETCH_ATTRACTIONS_PAGINATION,
        pagination: pagination,
    };
}

export function fetchAttractions(data) {
    return dispatch => {
        progressBar.start();
        dispatch(fetchAttractionsPending());
        request.get(`/attractions`, data).then(response => {
            const data = response.data.data;
            progressBar.stop();
            toastBySuccess(response);
            dispatch(fetchAttractionsSuccess(data));
            dispatch(fetchAttractionsPagination(response.data.pagination));
        }).catch(error => {
            progressBar.stop();
            toastByError(error);
            dispatch(fetchAttractionsError());
        });
    };
}
