import {progressBar} from '../../progress';
import {toastBySuccess} from '../../../lib/toast';
import seo from '../../../lib/seo';
import {
    FETCH_DIRECTION_PENDING,
    FETCH_DIRECTION_SUCCESS,
    FETCH_DIRECTION_TOURS_SUCCESS,
    FETCH_DIRECTION_TOURS_PENDING,
    FETCH_DIRECTION_TOURS_DATE,
} from '../../constants/Direction/index';
import {request} from "../../../lib/request";

function fetchDirectionPending() {
    return {
        type: FETCH_DIRECTION_PENDING,
    };
}

function fetchDirectionSuccess(direction) {
    seo.direction(direction);
    return {
        type: FETCH_DIRECTION_SUCCESS,
        direction: direction,
    };
}

function fetchDirectionToursDate(date) {
    return {
        type: FETCH_DIRECTION_TOURS_DATE,
        date: date,
    };
}

// function fetchDirectionError(error) {
//     return {
//         type: FETCH_DIRECTION_ERROR,
//         error: error,
//     };
// }

function fetchDirectionToursPending() {
    return {
        type: FETCH_DIRECTION_TOURS_PENDING,
    };
}

function fetchDirectionToursSuccess(data) {
    return {
        type: FETCH_DIRECTION_TOURS_SUCCESS,
        tours: data,
    };
}

export function fetchDirection(id) {
    return dispatch => {
        progressBar.start();
        dispatch(fetchDirectionPending());
        request.get(`/direction/${id}`).then(response => {
            const data = response.data.data;
            progressBar.stop();
            toastBySuccess(response);
            dispatch(fetchDirectionSuccess(data));
        });
    };
}

export function fetchDirectionTours(id, date) {
    return dispatch => {
        progressBar.start();
        dispatch(fetchDirectionToursPending());
        dispatch(fetchDirectionToursDate(date));
        request.get(`/direction/${id}/tours`, {
            date: date,
        }).then(response => {
            const data = response.data.data;
            progressBar.stop();
            toastBySuccess(response);
            dispatch(fetchDirectionToursSuccess(data));
        });
    };
}
