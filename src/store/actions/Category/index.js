import {progressBar} from '../../progress';
import {
    FETCH_CATEGORY_PENDING,
    FETCH_CATEGORY_ERROR,
    FETCH_CATEGORY_SUCCESS,
} from '../../constants/Category';
import {toastBySuccess} from '../../../lib/toast';
import {request} from "../../../lib/request";
import seo from "../../../lib/seo";

function fetchCategoryPending() {
    return {
        type: FETCH_CATEGORY_PENDING,
    };
}

function fetchCategorySuccess(data) {
    seo.article(data);
    return {
        type: FETCH_CATEGORY_SUCCESS,
        category: data,
    };
}

// eslint-disable-next-line
function fetchCategoryError(error) {
    return {
        type: FETCH_CATEGORY_ERROR,
        error: error,
    };
}
export function fetchCategory(id) {
    return dispatch => {
        progressBar.start();
        dispatch(fetchCategoryPending());
        request.get(`/category/${id}`).then(response => {
            const data = response.data.data;
            progressBar.stop();
            toastBySuccess(response);
            dispatch(fetchCategorySuccess(data));
        });
    };
}
