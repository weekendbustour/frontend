import {progressBar} from '../../progress';
import {
    FETCH_ORGANIZATIONS_PENDING,
    FETCH_ORGANIZATIONS_SUCCESS,
    FETCH_ORGANIZATIONS_PAGINATION,
} from '../../constants/Organizations';
import {toastBySuccess} from '../../../lib/toast';
import {request} from "../../../lib/request";

function fetchOrganizationsPending() {
    return {
        type: FETCH_ORGANIZATIONS_PENDING,
    };
}

function fetchOrganizationsSuccess(data) {
    return {
        type: FETCH_ORGANIZATIONS_SUCCESS,
        organizations: data,
    };
}

// function fetchOrganizationsError(error) {
//     return {
//         type: FETCH_ORGANIZATIONS_ERROR,
//         error: error,
//     };
// }
function fetchOrganizationsPagination(data) {
    return {
        type: FETCH_ORGANIZATIONS_PAGINATION,
        pagination: data,
    };
}

export function fetchOrganizations(data) {
    return dispatch => {
        progressBar.start();
        dispatch(fetchOrganizationsPending());
        request.get(`/organizations`, data).then(response => {
            const data = response.data.data;
            progressBar.stop();
            toastBySuccess(response);
            dispatch(fetchOrganizationsSuccess(data));
            if (response.data.pagination !== undefined) {
                dispatch(fetchOrganizationsPagination(response.data.pagination));
            }
        });
    };
}
