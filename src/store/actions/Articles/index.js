import {progressBar} from '../../progress';
import {
    FETCH_ARTICLES_PENDING,
    FETCH_ARTICLES_ERROR,
    FETCH_ARTICLES_SUCCESS,
    FETCH_ARTICLES_PAGINATION,
} from '../../constants/Articles';
import {toastByError, toastBySuccess} from '../../../lib/toast';
import {request} from "../../../lib/request";

function fetchArticlesPending() {
    return {
        type: FETCH_ARTICLES_PENDING,
    };
}

function fetchArticlesSuccess(data) {
    return {
        type: FETCH_ARTICLES_SUCCESS,
        articles: data,
    };
}

function fetchArticlesError(error) {
    return {
        type: FETCH_ARTICLES_ERROR,
        error: error,
    };
}
function fetchArticlesPagination(pagination) {
    return {
        type: FETCH_ARTICLES_PAGINATION,
        pagination: pagination,
    };
}

export function fetchArticles(data) {
    return dispatch => {
        progressBar.start();
        dispatch(fetchArticlesPending());
        request.get(`/articles`, data).then(response => {
            const data = response.data.data;
            progressBar.stop();
            toastBySuccess(response);
            dispatch(fetchArticlesSuccess(data));
            dispatch(fetchArticlesPagination(response.data.pagination));
        }).catch(error => {
            progressBar.stop();
            toastByError(error);
            dispatch(fetchArticlesError());
        });
    };
}
