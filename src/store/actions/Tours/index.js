import {progressBar} from '../../progress';
import {
    FETCH_TOURS_PENDING,
    FETCH_TOURS_SUCCESS,
    FETCH_TOURS_PAGINATION,
} from '../../constants/Tours';
import {toastBySuccess} from '../../../lib/toast';
import {request} from "../../../lib/request";

function fetchToursPending() {
    return {
        type: FETCH_TOURS_PENDING,
    };
}

function fetchToursSuccess(data) {
    return {
        type: FETCH_TOURS_SUCCESS,
        tours: data,
    };
}
//
// function fetchToursError(error) {
//     return {
//         type: FETCH_TOURS_ERROR,
//         error: error,
//     };
// }
function fetchToursPagination(data) {
    return {
        type: FETCH_TOURS_PAGINATION,
        pagination: data,
    };
}

export function fetchTours(data) {
    return dispatch => {
        progressBar.start();
        dispatch(fetchToursPending());
        request.get(`/tours`, data).then(response => {
            const data = response.data.data;
            progressBar.stop();
            toastBySuccess(response);
            dispatch(fetchToursSuccess(data));
            if (response.data.pagination !== undefined) {
                dispatch(fetchToursPagination(response.data.pagination));
            }
        });
    };
}
