import {progressBar} from '../../progress';
import {API_URL, axiosBody} from '../../../api/api';
import {
    FETCH_NEWS_PENDING,
    FETCH_NEWS_ERROR,
    FETCH_NEWS_SUCCESS,
    FETCH_NEWS_PAGINATION,
    FETCH_NEWS_PAGE_SUCCESS, FETCH_NEWS_PAGE_PENDING
} from '../../constants/News';
import axios from 'axios';
import {toastByError, toastBySuccess} from '../../../lib/toast';
import seo from "../../../lib/seo";

function fetchNewsPending() {
    return {
        type: FETCH_NEWS_PENDING,
    };
}

function fetchNewsSuccess(news) {
    return {
        type: FETCH_NEWS_SUCCESS,
        news: news,
    };
}

function fetchNewsPageSuccess(news) {
    seo.newsPage(news);
    return {
        type: FETCH_NEWS_PAGE_SUCCESS,
        news: news,
    };
}

function fetchNewsPagePending() {
    return {
        type: FETCH_NEWS_PAGE_PENDING,
    };
}

function fetchNewsError(error) {
    return {
        type: FETCH_NEWS_ERROR,
        error: error,
    };
}
function fetchNewsPagination(pagination) {
    return {
        type: FETCH_NEWS_PAGINATION,
        pagination: pagination,
    };
}

export function fetchNews(data) {
    return dispatch => {
        progressBar.start();
        dispatch(fetchNewsPending());
        axios.get(API_URL + `/news`, {
            params: axiosBody(data),
        }).then(response => {
            const data = response.data.data;
            progressBar.stop();
            toastBySuccess(response);
            dispatch(fetchNewsSuccess(data));
            dispatch(fetchNewsPagination(response.data.pagination));
        })
        .catch(error => {
            progressBar.stop();
            toastByError(error);
            dispatch(fetchNewsError());
        });
    };
}
export function fetchNewsPage(id) {
    return dispatch => {
        progressBar.start();
        dispatch(fetchNewsPagePending());
        fetch(API_URL + '/news/' + id)
            .then(res => res.json())
            .then(res => {
                progressBar.stop();
                if (res.error) {
                    throw res.error;
                }
                dispatch(fetchNewsPageSuccess(res.data));
                return res.data;
            })
            .catch(error => {

            });
    };
}
