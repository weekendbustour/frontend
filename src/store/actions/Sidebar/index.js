import {FETCH_SIDEBAR} from '../../constants/Sidebar';

function fetchSidebarSuccess(name) {
    return {
        type: FETCH_SIDEBAR,
        sidebar: name,
    };
}

export function fetchSidebar(name) {
    return dispatch => {
        dispatch(fetchSidebarSuccess(name));
    };
}
