import {getUser} from '../../reducers/User';
import store from '../../index';
import {fetchModal} from '../../reducers/Modal';
//import {MODAL_LOGIN} from "../../../lib/modal";
import {MODAL_LOGIN} from '../../../lib/modal';
import {toastBySuccess, toastWarning} from '../../../lib/toast';
import {FETCH_USER_SUBSCRIPTIONS_ID_SUCCESS, FETCH_USER_SUBSCRIPTIONS_PENDING_ARRAY} from '../../constants/User/Subscription';
import {request} from "../../../lib/request";

const fetchUserSubscriptionsPending = pending => ({
    type: FETCH_USER_SUBSCRIPTIONS_PENDING_ARRAY,
    pending,
});

const fetchUserSubscriptionsSuccess = (subscriptions, pending) => ({
    type: FETCH_USER_SUBSCRIPTIONS_ID_SUCCESS,
    subscriptions,
    pending,
});

export function fetchUserSubscribeModel(model, data) {
    const user = getUser(store.getState());
    if (user === null) {
        return dispatch => {
            toastWarning('Только авторизованный пользователь добавлять в Подписки');
            dispatch(fetchModal(MODAL_LOGIN));
        };
    }
    return dispatch => {
        const pendingName = model + '-' + data.id;
        dispatch(fetchUserSubscriptionsPending(pendingName));
        request.post(`/user/subscribe`, {
            id: data.id,
            model: model,
        }).then(response => {
            toastBySuccess(response);
            const data = response.data.data;
            dispatch(fetchUserSubscriptionsSuccess(data, pendingName));
        });
    };
}

export function fetchUserSubscriptionsId() {
    return dispatch => {
        request.get(`/user/subscriptions`).then(response => {
            const data = response.data.data;
            if (data) {
                dispatch(fetchUserSubscriptionsSuccess(data));
            }
        });
    };
}
