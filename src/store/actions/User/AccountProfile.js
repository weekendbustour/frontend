import {
    FETCH_USER_ACCOUNT_PROFILE_SUCCESS,
    FETCH_USER_ACCOUNT_PROFILE_PENDING,
    FETCH_USER_ACCOUNT_IMAGE_SUCCESS,
    FETCH_USER_ACCOUNT_IMAGE_PENDING,
    FETCH_USER_ACCOUNT_IMAGE_DELETE_PENDING,
    FETCH_USER_ACCOUNT_IMAGE_DELETE_SUCCESS, FETCH_USER_ACCOUNT_PROFILE_ERRORS,
} from '../../constants/User/AccountProfile';
import {toastByError, toastBySuccess} from "../../../lib/toast";
import {fetchUser} from "../../reducers/User";
import {fetchModal} from "../../reducers/Modal";
import {request} from "../../../lib/request";

const fetchUserAccountProfilePending = () => ({
    type: FETCH_USER_ACCOUNT_PROFILE_PENDING,
});

const fetchUserAccountProfileSuccess = (data) => ({
    type: FETCH_USER_ACCOUNT_PROFILE_SUCCESS,
    payload: data,
});

const fetchUserAccountProfileErrors = (errors) => ({
    type: FETCH_USER_ACCOUNT_PROFILE_ERRORS,
    errors: errors,
});

const fetchUserAccountImageSuccess = (data) => ({
    type: FETCH_USER_ACCOUNT_IMAGE_SUCCESS,
    payload: data,
});

const fetchUserAccountImagePending = (data) => ({
    type: FETCH_USER_ACCOUNT_IMAGE_PENDING,
});

const fetchUserAccountImageDeleteSuccess = (data) => ({
    type: FETCH_USER_ACCOUNT_IMAGE_DELETE_SUCCESS,
    payload: data,
});

const fetchUserAccountImageDeletePending = (data) => ({
    type: FETCH_USER_ACCOUNT_IMAGE_DELETE_PENDING,
});

export function fetchUserAccountProfile(data) {
    return dispatch => {
        dispatch(fetchUserAccountProfilePending());
        request.post(`/user/account/profile`, data).then(response => {
            const data = response.data.data;
            toastBySuccess(response);
            dispatch(fetchUserAccountProfileSuccess(data));
            dispatch(fetchUser('user'));
        }).catch(error => {
            toastByError(error);
            let errors = {};
            if (error.response !== undefined) {
                errors = error.response.data.errors
            }
            dispatch(fetchUserAccountProfileErrors(errors));
        });
    };
}

export function fetchUserAccountImage(cropData, file) {
    return dispatch => {
        dispatch(fetchUserAccountImagePending());
        let data = new FormData();
        data.append('image', file, file.name);
        data.append('crop[x]', cropData.x);
        data.append('crop[y]', cropData.y);
        data.append('crop[w]', cropData.width);
        data.append('crop[h]', cropData.height);
        data.append('crop[rotate]', cropData.rotate);
        data.append('crop[scaleX]', cropData.scaleX);
        data.append('crop[scaleY]', cropData.scaleY);
        request.postImage(`/user/account/image`, data).then(response => {
            const data = response.data.data;
            toastBySuccess(response);
            dispatch(fetchUserAccountImageSuccess(data));
            dispatch(fetchModal(null));
            dispatch(fetchUser('user'));
        }).catch(response => {
            toastByError(response);
        });
    };
}

export function fetchUserAccountImageDelete() {
    return dispatch => {
        dispatch(fetchUserAccountImageDeletePending());
        request.postImage(`/account/image/delete`, {}).then(response => {
            const data = response.data.data;
            toastBySuccess(response);
            dispatch(fetchUserAccountImageDeleteSuccess(data));
            dispatch(fetchUser('user'));
        });
    };
}
