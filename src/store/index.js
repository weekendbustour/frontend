import {createStore, applyMiddleware} from 'redux';
import thunkMiddleware from 'redux-thunk';
import {composeWithDevTools} from 'redux-devtools-extension';
import {combineReducers} from 'redux';
import progress from 'react-redux-progress/reducer';
import {membersReducer} from './reducers/Members/index';
import {socialsReducer} from './reducers/Socials/index';
import {appSeoReducer} from './reducers/App/Seo';
import {appViewReducer} from './reducers/App/View';
import {toursReducer} from './reducers/Tours/index';
import {categoryReducer} from './reducers/Category/index';
import {tourReducer} from './reducers/Tour/index';
import {newsReducer} from './reducers/News/index';
import {newsPageReducer} from './reducers/NewsPage/index';
import {articlesReducer} from './reducers/Articles/index';
import {articleReducer} from './reducers/Article/index';
import {organizationsReducer} from './reducers/Organizations/index';
import {organizationReducer} from './reducers/Organization/index';
import {directionsReducer} from './reducers/Directions/index';
import {directionReducer} from './reducers/Direction/index';
import {dataReducer} from './reducers/Data/index';
import {commentsReducer} from './reducers/Comments/index';
import {commentVoteReducer} from './reducers/Comment/Vote';
import {searchReducer} from './reducers/Search/index';
import {modalReducer} from './reducers/Modal/index';
import {sidebarReducer} from './reducers/Sidebar/index';
import {userReducer} from './reducers/User/index';
import {userFavouritesReducer} from './reducers/User/Favourites';
import {userFavouriteReducer} from './reducers/User/Favourite';
import {userCommentReducer} from './reducers/User/Comment';
import {userCommentsReducer} from './reducers/User/Comments';
import {userSubscriptionReducer} from './reducers/User/Subscription';
import {userSubscriptionsReducer} from './reducers/User/Subscriptions';
import {authReducer} from './reducers/Auth/index';
import {helpReducer} from './reducers/Help/index';
import {userAccountProfileReducer} from './reducers/User/AccountProfile';
import {userReservationsReducer} from './reducers/User/Reservations';
import {userReservationReducer} from './reducers/User/Reservation';
import {userNotificationReducer} from './reducers/User/Notification';
import {tourTimelinesReducer} from './reducers/Tour/Timelines';
import {userCommentVotesReducer} from "./reducers/User/CommentVotes";
import {attractionReducer} from "./reducers/Attraction";
import {attractionsReducer} from "./reducers/Attractions";

// Root Reducer
const rootReducer = combineReducers({
    appSeo: appSeoReducer,
    appView: appViewReducer,
    auth: authReducer,
    members: membersReducer,
    socials: socialsReducer,
    tour: tourReducer,
    tourTimelines: tourTimelinesReducer,
    tours: toursReducer,
    news: newsReducer,
    newsPage: newsPageReducer,
    articles: articlesReducer,
    article: articleReducer,
    attraction: attractionReducer,
    attractions: attractionsReducer,
    organization: organizationReducer,
    organizations: organizationsReducer,
    direction: directionReducer,
    directions: directionsReducer,
    data: dataReducer,
    category: categoryReducer,
    comments: commentsReducer,
    commentVote: commentVoteReducer,
    search: searchReducer,
    modal: modalReducer,
    sidebar: sidebarReducer,
    user: userReducer,
    help: helpReducer,
    userFavourite: userFavouriteReducer,
    userFavourites: userFavouritesReducer,
    userComment: userCommentReducer,
    userComments: userCommentsReducer,
    userCommentVotes: userCommentVotesReducer,
    userSubscription: userSubscriptionReducer,
    userSubscriptions: userSubscriptionsReducer,
    userReservation: userReservationReducer,
    userReservations: userReservationsReducer,
    userNotification: userNotificationReducer,
    userAccountProfile: userAccountProfileReducer,
});

const rootReducers = combineReducers({
    progress,
    rootReducer,
    // other reducers
});
//const middlewares = [thunk];

//const middlewareEnhancer = applyMiddleware(createLogger(), thunkMiddleware);
const middlewareEnhancer = applyMiddleware(thunkMiddleware);

const store = createStore(rootReducers, composeWithDevTools(middlewareEnhancer));

export default store;
