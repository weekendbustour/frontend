import store from '../store/index';

const startAction = progressId => ({
    type: 'START_ASYNC_ACTION', // your action name
    progressId,
});

const stopAction = progressId => ({
    type: 'STOP_ASYNC_ACTION', // your action name
    progressId,
});

export const progressBar = {
    progressId: null,
    start() {
        if (this.progressId !== null) {
            this.stop();
        }
        this.progressId = store.dispatch(startAsyncAction());
    },
    stop() {
        store.dispatch(stopAsyncAction(this.progressId));
        this.progressId = null;
    },
};

// dispatch thunk action
export function startAsyncAction() {
    return dispatch => {
        const progressId = Math.random()
            .toString(36)
            .substring(7);

        dispatch(startAction(progressId));

        // setTimeout(() => {
        //     dispatch(stopAction(progressId));
        // }, 2000);

        return progressId;
    };
}

export function stopAsyncAction(progressId) {
    return dispatch => {
        dispatch(stopAction(progressId));
    };
}
