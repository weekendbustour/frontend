import {progressBar} from '../../progress';
import {API_URL} from '../../../api/api';

const FETCH_COMMENTS_PENDING = 'FETCH_COMMENTS_PENDING';
const FETCH_COMMENTS_DIRECTION_SUCCESS = 'FETCH_COMMENTS_DIRECTION_SUCCESS';
const FETCH_COMMENTS_ORGANIZATION_SUCCESS = 'FETCH_COMMENTS_ORGANIZATION_SUCCESS';
const FETCH_COMMENTS_NEWS_SUCCESS = 'FETCH_COMMENTS_NEWS_SUCCESS';
const FETCH_COMMENTS_ARTICLE_SUCCESS = 'FETCH_COMMENTS_ARTICLE_SUCCESS';
const FETCH_COMMENTS_ATTRACTION_SUCCESS = 'FETCH_COMMENTS_ATTRACTION_SUCCESS';
const FETCH_COMMENTS_ERROR = 'FETCH_COMMENTS_ERROR';

const FETCH_COMMENTS_NEWS_UPDATE = 'FETCH_COMMENTS_NEWS_UPDATE';
const FETCH_COMMENTS_ORGANIZATION_UPDATE = 'FETCH_COMMENTS_ORGANIZATION_UPDATE';
const FETCH_COMMENTS_DIRECTION_UPDATE = 'FETCH_COMMENTS_DIRECTION_UPDATE';
const FETCH_COMMENTS_ATTRACTION_UPDATE = 'FETCH_COMMENTS_ATTRACTION_UPDATE';
const FETCH_COMMENTS_ARTICLE_UPDATE = 'FETCH_COMMENTS_ARTICLE_UPDATE';

const defaultState = {
    pending: false,
    direction: [],
    organization: [],
    news: [],
    article: [],
    attraction: [],
    error: null,
};

function fetchCommentsPending() {
    return {
        type: FETCH_COMMENTS_PENDING,
    };
}

function fetchCommentsDirectionSuccess(data) {
    return {
        type: FETCH_COMMENTS_DIRECTION_SUCCESS,
        direction: data,
    };
}

function fetchCommentsOrganizationSuccess(data) {
    return {
        type: FETCH_COMMENTS_ORGANIZATION_SUCCESS,
        organization: data,
    };
}

function fetchCommentsNewsSuccess(data) {
    return {
        type: FETCH_COMMENTS_NEWS_SUCCESS,
        news: data,
    };
}

function fetchCommentsArticleSuccess(data) {
    return {
        type: FETCH_COMMENTS_ARTICLE_SUCCESS,
        article: data,
    };
}

function fetchCommentsAttractionSuccess(data) {
    return {
        type: FETCH_COMMENTS_ATTRACTION_SUCCESS,
        attraction: data,
    };
}

function fetchCommentsError(error) {
    return {
        type: FETCH_COMMENTS_ERROR,
        error: error,
    };
}


export function fetchCommentsNewsUpdate(data) {
    return {
        type: FETCH_COMMENTS_NEWS_UPDATE,
        comment: data,
    };
}

export function fetchCommentsOrganizationUpdate(data) {
    return {
        type: FETCH_COMMENTS_ORGANIZATION_UPDATE,
        comment: data,
    };
}

export function fetchCommentsDirectionUpdate(data) {
    return {
        type: FETCH_COMMENTS_DIRECTION_UPDATE,
        comment: data,
    };
}

export function fetchCommentsAttractionUpdate(data) {
    return {
        type: FETCH_COMMENTS_ATTRACTION_UPDATE,
        comment: data,
    };
}

export function fetchCommentsArticleUpdate(data) {
    return {
        type: FETCH_COMMENTS_ARTICLE_UPDATE,
        comment: data,
    };
}

export function fetchComments(id, type) {
    return dispatch => {
        progressBar.start();
        dispatch(fetchCommentsPending());
        fetch(API_URL + '/' + type + '/' + id + '/comments')
            .then(res => res.json())
            .then(res => {
                progressBar.stop();
                const data = res.data;
                console.log(data);
                if (res.error) {
                    throw res.error;
                }
                if (data && type === 'direction') {
                    dispatch(fetchCommentsDirectionSuccess(data));
                }
                if (data && type === 'organization') {
                    dispatch(fetchCommentsOrganizationSuccess(data));
                }
                if (data && type === 'news') {
                    dispatch(fetchCommentsNewsSuccess(data));
                }
                if (data && type === 'article') {
                    dispatch(fetchCommentsArticleSuccess(data));
                }
                if (data && type === 'attraction') {
                    dispatch(fetchCommentsAttractionSuccess(data));
                }
            })
            .catch(error => {
                dispatch(fetchCommentsError(error));
            });
    };
}

export function commentsReducer(state = defaultState, action) {
    switch (action.type) {
        case FETCH_COMMENTS_PENDING:
            return {
                ...state,
                pending: true,
            };
        case FETCH_COMMENTS_DIRECTION_SUCCESS:
            return {
                ...state,
                pending: false,
                direction: action.direction,
            };
        case FETCH_COMMENTS_ORGANIZATION_SUCCESS:
            return {
                ...state,
                pending: false,
                organization: action.organization,
            };
        case FETCH_COMMENTS_NEWS_SUCCESS:
            return {
                ...state,
                pending: false,
                news: action.news,
            };
        case FETCH_COMMENTS_ARTICLE_SUCCESS:
            return {
                ...state,
                pending: false,
                article: action.article,
            };
        case FETCH_COMMENTS_ATTRACTION_SUCCESS:
            return {
                ...state,
                pending: false,
                attraction: action.attraction,
            };
        case FETCH_COMMENTS_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error,
            };
        case FETCH_COMMENTS_NEWS_UPDATE:
            return {
                ...state,
                news: assignComments(action.comment, state.news),
            };
        case FETCH_COMMENTS_ORGANIZATION_UPDATE:
            return {
                ...state,
                organization: assignComments(action.comment, state.organization),
            };
        case FETCH_COMMENTS_DIRECTION_UPDATE:
            return {
                ...state,
                direction: assignComments(action.comment, state.direction),
            };
        case FETCH_COMMENTS_ATTRACTION_UPDATE:
            return {
                ...state,
                attraction: assignComments(action.comment, state.attraction),
            };
        case FETCH_COMMENTS_ARTICLE_UPDATE:
            return {
                ...state,
                article: assignComments(action.comment, state.article),
            };
        default:
            return state;
    }
}

function assignComments(commentUpdate, comments) {
    return comments.map((value, key) => {
        if (value.children.length !== 0) {
            // если есть среди дочерних, то заменит
            let hasInChildren = false;
            // проходимся по дочерним
            const newValue = value.children.map((children, cKey) => {
                if (children.id === commentUpdate.id) {
                    hasInChildren = true;
                    return Object.assign(children, commentUpdate);
                }
                return children;
            });
            if (hasInChildren) {
                value.children = newValue;
                hasInChildren = false;
            }
        }
        if (value.id === commentUpdate.id) {
            return Object.assign(value, commentUpdate);
        }
        return value;
    });
}

export const getCommentsDirection = state => state.rootReducer.comments.direction;
export const getCommentsOrganization = state => state.rootReducer.comments.organization;
export const getCommentsNews = state => state.rootReducer.comments.news;
export const getCommentsArticle = state => state.rootReducer.comments.article;
export const getCommentsAttraction = state => state.rootReducer.comments.attraction;
export const getCommentsPending = state => state.rootReducer.comments.pending;
export const getCommentsError = state => state.rootReducer.comments.error;

//export default {fetchDirections, DirectionsReducer}
