import {
    FETCH_ORGANIZATIONS_PENDING,
    FETCH_ORGANIZATIONS_ERROR,
    FETCH_ORGANIZATIONS_SUCCESS,
    FETCH_ORGANIZATIONS_PAGINATION,
} from '../../constants/Organizations';

const defaultState = {
    pending: false,
    organizations: [],
    error: null,
    pagination: null,
};

export function organizationsReducer(state = defaultState, action) {
    switch (action.type) {
        case FETCH_ORGANIZATIONS_PENDING:
            return {
                ...state,
                pending: true,
            };
        case FETCH_ORGANIZATIONS_SUCCESS:
            return {
                ...state,
                pending: false,
                organizations: action.organizations,
            };
        case FETCH_ORGANIZATIONS_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error,
            };
        case FETCH_ORGANIZATIONS_PAGINATION:
            return {
                ...state,
                pagination: action.pagination,
            };
        default:
            return state;
    }
}

export const getOrganizations = state => state.rootReducer.organizations.organizations;
export const getOrganizationsPending = state => state.rootReducer.organizations.pending;
export const getOrganizationsError = state => state.rootReducer.organizations.error;
// eslint-disable-next-line
export const getOrganizationsPagination = state => state.rootReducer.organizations.pagination;
