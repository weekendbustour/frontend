import {
    FETCH_DATA_DIRECTIONS_SUCCESS,
    FETCH_DATA_ERROR,
    FETCH_DATA_ORGANIZATIONS_SUCCESS,
    FETCH_DATA_PENDING,
    FETCH_DATA_SEARCH_CATEGORIES_SUCCESS,
    FETCH_DATA_SEARCH_DEPARTURES_SUCCESS,
    FETCH_DATA_SEARCH_DIRECTIONS_SUCCESS,
    FETCH_DATA_SEARCH_ORDERS_SUCCESS,
    FETCH_DATA_SEARCH_ORGANIZATIONS_SUCCESS,
    FETCH_DATA_TOURS_SUCCESS,
    FETCH_DATA_ARTICLES_SUCCESS,
    FETCH_DATA_PROMOTIONS_SUCCESS, FETCH_DATA_NEWS_SUCCESS,
    FETCH_DATA_QUESTIONS_SUCCESS, FETCH_DATA_TITLES_SUCCESS,
    FETCH_DATA_ABOUT_SUCCESS,
    FETCH_DATA_STATISTIC_SUCCESS,
} from '../../constants/Data';

const defaultState = {
    pending: false,
    searchOrders: [],
    searchCategories: [],
    searchDirections: [],
    searchOrganizations: [],
    searchDepartures: [],
    tours: [],
    toursPending: false,
    directions: [],
    directionsPending: false,
    organizations: [],
    organizationsPending: false,
    news: [],
    newsPending: false,
    articles: [],
    articlesPending: false,
    promotions: null,
    promotionsPending: false,
    questions: [],
    questionsPending: false,
    titles: {},
    titlesPending: false,
    about: {},
    aboutPending: false,
    statistic: {},
    statisticPending: false,
    error: null,
};

export function dataReducer(state = defaultState, action) {
    switch (action.type) {
        case FETCH_DATA_PENDING:
            return {
                ...state,
                pending: true,
                toursPending: true,
                directionsPending: true,
                organizationsPending: true,
                promotionsPending: true,
                newsPending: true,
                articlesPending: true,
                questionsPending: true,
                titlesPending: true,
                aboutPending: true,
                statisticPending: true,
            };
        case FETCH_DATA_SEARCH_ORDERS_SUCCESS:
            return {
                ...state,
                pending: false,
                searchOrders: action.searchOrders,
            };
        case FETCH_DATA_SEARCH_DIRECTIONS_SUCCESS:
            return {
                ...state,
                pending: false,
                searchDirections: action.searchDirections,
            };
        case FETCH_DATA_SEARCH_ORGANIZATIONS_SUCCESS:
            return {
                ...state,
                pending: false,
                searchOrganizations: action.searchOrganizations,
            };
        case FETCH_DATA_SEARCH_CATEGORIES_SUCCESS:
            return {
                ...state,
                pending: false,
                searchCategories: action.searchCategories,
            };
        case FETCH_DATA_SEARCH_DEPARTURES_SUCCESS:
            return {
                ...state,
                pending: false,
                searchDepartures: action.searchDepartures,
            };
        case FETCH_DATA_TOURS_SUCCESS:
            return {
                ...state,
                toursPending: false,
                tours: action.tours,
            };
        case FETCH_DATA_DIRECTIONS_SUCCESS:
            return {
                ...state,
                directionsPending: false,
                directions: action.directions,
            };
        case FETCH_DATA_ORGANIZATIONS_SUCCESS:
            return {
                ...state,
                organizationsPending: false,
                organizations: action.organizations,
            };
        case FETCH_DATA_PROMOTIONS_SUCCESS:
            return {
                ...state,
                promotionsPending: false,
                promotions: action.promotions,
            };
        case FETCH_DATA_NEWS_SUCCESS:
            return {
                ...state,
                newsPending: false,
                news: action.news,
            };
        case FETCH_DATA_ARTICLES_SUCCESS:
            return {
                ...state,
                articlesPending: false,
                articles: action.articles,
            };
        case FETCH_DATA_QUESTIONS_SUCCESS:
            return {
                ...state,
                questionsPending: false,
                questions: action.questions,
            };
        case FETCH_DATA_TITLES_SUCCESS:
            return {
                ...state,
                titlesPending: false,
                titles: action.titles,
            };
        case FETCH_DATA_ABOUT_SUCCESS:
            return {
                ...state,
                aboutPending: false,
                about: action.about,
            };
        case FETCH_DATA_STATISTIC_SUCCESS:
            return {
                ...state,
                statisticPending: false,
                statistic: action.statistic,
            };
        case FETCH_DATA_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error,
            };
        default:
            return state;
    }
}

export const getDataSearchOrders = state => state.rootReducer.data.searchOrders;
export const getDataSearchDirections = state => state.rootReducer.data.searchDirections;
export const getDataSearchCategories = state => state.rootReducer.data.searchCategories;
export const getDataSearchOrganizations = state => state.rootReducer.data.searchOrganizations;
export const getDataSearchDepartures = state => state.rootReducer.data.searchDepartures;
export const hasDataSearch = state => {
    if (window.project !== undefined) {
        //store.dispatch(fetchDataWithoutRequest(window.project.data));
        return true;
    }
    return (
        state.rootReducer.data.searchDirections.length !== 0 &&
        state.rootReducer.data.searchCategories.length !== 0 &&
        state.rootReducer.data.searchDepartures.length !== 0 &&
        state.rootReducer.data.searchOrganizations.length !== 0
    );
};
export const getDataTours = state => state.rootReducer.data.tours;
export const getDataToursPending = state => state.rootReducer.data.toursPending;
export const getDataDirections = state => state.rootReducer.data.directions;
export const getDataDirectionsPending = state => state.rootReducer.data.directionsPending;
export const getDataOrganizations = state => state.rootReducer.data.organizations;
export const getDataOrganizationsPending = state => state.rootReducer.data.organizationsPending;
export const getDataNews = state => state.rootReducer.data.news;
export const getDataNewsPending = state => state.rootReducer.data.newsPending;
export const getDataArticles = state => state.rootReducer.data.articles;
export const getDataArticlesPending = state => state.rootReducer.data.articlesPending;
export const getDataQuestions = state => state.rootReducer.data.questions;
export const getDataQuestionsPending = state => state.rootReducer.data.questionsPending;
export const getDataTitles = state => state.rootReducer.data.titles;
export const getDataTitlesPending = state => state.rootReducer.data.titlesPending;

export const getDataAbout = state => state.rootReducer.data.about;
export const getDataAboutPending = state => state.rootReducer.data.aboutPending;

export const getDataStatistic = state => state.rootReducer.data.statistic;
export const getDataStatisticPending = state => state.rootReducer.data.statisticPending;

export const getDataPromotions = state => state.rootReducer.data.promotions;
export const getDataPromotionsDefault = state => state.rootReducer.data.promotions.default;
export const getDataPromotionsTarget = state => state.rootReducer.data.promotions.target;
export const getDataPromotionsPending = state => state.rootReducer.data.promotionsPending;
export const getDataPending = state => state.rootReducer.data.pending;
export const getDataError = state => state.rootReducer.data.error;

//export default {fetchDirections, DirectionsReducer}
