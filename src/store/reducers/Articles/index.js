import {FETCH_ARTICLES_PENDING, FETCH_ARTICLES_ERROR, FETCH_ARTICLES_SUCCESS, FETCH_ARTICLES_PAGINATION} from '../../constants/Articles';

const defaultState = {
    pending: false,
    articles: [],
    pagination: null,
    error: null,
};

export function articlesReducer(state = defaultState, action) {
    switch (action.type) {
        case FETCH_ARTICLES_PENDING:
            return {
                ...state,
                pending: true,
                pagination: null,
            };
        case FETCH_ARTICLES_SUCCESS:
            return {
                ...state,
                pending: false,
                articles: action.articles,
            };
        case FETCH_ARTICLES_PAGINATION:
            return {
                ...state,
                pagination: action.pagination,
            };
        case FETCH_ARTICLES_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error,
            };
        default:
            return state;
    }
}

export const getArticles = state => state.rootReducer.articles.articles;
export const getArticlesPending = state => state.rootReducer.articles.pending;
export const getArticlesPagination = state => state.rootReducer.articles.pagination;
export const getArticlesError = state => state.rootReducer.articles.error;
