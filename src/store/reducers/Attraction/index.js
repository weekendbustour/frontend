import {
    FETCH_ATTRACTION_PENDING,
    FETCH_ATTRACTION_SUCCESS,
    FETCH_ATTRACTION_ERROR
} from '../../constants/Attraction';

const defaultState = {
    pending: false,
    attraction: {},
    error: null,
};

export function attractionReducer(state = defaultState, action) {
    switch (action.type) {
        case FETCH_ATTRACTION_PENDING:
            return {
                ...state,
                pending: true,
            };
        case FETCH_ATTRACTION_SUCCESS:
            return {
                ...state,
                pending: false,
                attraction: action.attraction,
            };
        case FETCH_ATTRACTION_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error,
            };
        default:
            return state;
    }
}

export const getAttraction = state => state.rootReducer.attraction.attraction;
export const getAttractionPending = state => state.rootReducer.attraction.pending;
export const getAttractionError = state => state.rootReducer.attraction.error;
