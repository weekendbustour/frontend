import {
    FETCH_ATTRACTIONS_PENDING,
    FETCH_ATTRACTIONS_ERROR,
    FETCH_ATTRACTIONS_SUCCESS,
    FETCH_ATTRACTIONS_PAGINATION
} from '../../constants/Attractions';

const defaultState = {
    pending: false,
    attractions: [],
    pagination: null,
    error: null,
};

export function attractionsReducer(state = defaultState, action) {
    switch (action.type) {
        case FETCH_ATTRACTIONS_PENDING:
            return {
                ...state,
                pending: true,
                pagination: null,
            };
        case FETCH_ATTRACTIONS_SUCCESS:
            return {
                ...state,
                pending: false,
                attractions: action.attractions,
            };
        case FETCH_ATTRACTIONS_PAGINATION:
            return {
                ...state,
                pagination: action.pagination,
            };
        case FETCH_ATTRACTIONS_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error,
            };
        default:
            return state;
    }
}

export const getAttractions = state => state.rootReducer.attractions.attractions;
export const getAttractionsPending = state => state.rootReducer.attractions.pending;
export const getAttractionsPagination = state => state.rootReducer.attractions.pagination;
export const getAttractionsError = state => state.rootReducer.attractions.error;
