import {FETCH_ARTICLE_PENDING, FETCH_ARTICLE_SUCCESS, FETCH_ARTICLE_ERROR} from '../../constants/Article';

const defaultState = {
    pending: false,
    article: {},
    error: null,
};

export function articleReducer(state = defaultState, action) {
    switch (action.type) {
        case FETCH_ARTICLE_PENDING:
            return {
                ...state,
                pending: true,
            };
        case FETCH_ARTICLE_SUCCESS:
            return {
                ...state,
                pending: false,
                article: action.article,
            };
        case FETCH_ARTICLE_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error,
            };
        default:
            return state;
    }
}

export const getArticle = state => state.rootReducer.article.article;
export const getArticlePending = state => state.rootReducer.article.pending;
export const getArticleError = state => state.rootReducer.article.error;
