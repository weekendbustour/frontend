import {FETCH_COMMENT_VOTE_PENDING_ARRAY, FETCH_COMMENT_VOTE_SUCCESS} from "../../constants/Comment/Vote";


const defaultState = {
    pendingArray: [],
    comment: {},
};

export function commentVoteReducer(state = defaultState, action) {
    switch (action.type) {
        case FETCH_COMMENT_VOTE_PENDING_ARRAY:
            const commentVotePendingArray_PENDING = state.pendingArray;
            commentVotePendingArray_PENDING.push(action.pending);
            return {
                ...state,
                comment: {},
                pendingArray: commentVotePendingArray_PENDING,
            };
        case FETCH_COMMENT_VOTE_SUCCESS:
            const commentVotePendingArray_SUCCESS = state.pendingArray;
            let index = commentVotePendingArray_SUCCESS.indexOf(action.pending);
            if (index !== -1) {
                commentVotePendingArray_SUCCESS.splice(index, 1);
            }
            return {
                ...state,
                comment: action.comment,
                pendingArray: commentVotePendingArray_SUCCESS,
            };
        default:
            return state;
    }
}
export const getCommentVotePendingArray = state => state.rootReducer.commentVote.pendingArray;
export const getCommentVoteComment = state => state.rootReducer.commentVote.comment;
