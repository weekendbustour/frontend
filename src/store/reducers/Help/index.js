import {
    FETCH_HELP_REPORT_PENDING,
    FETCH_HELP_REPORT_SUCCESS,
    FETCH_HELP_OFFER_PENDING,
    FETCH_HELP_OFFER_SUCCESS,
    FETCH_HELP_RATING_PENDING,
    FETCH_HELP_RATING_SUCCESS,
} from '../../constants/Help';

const defaultState = {
    pendingReport: false,
    pendingOffer: false,
    pendingRating: false,
    successRating: false,
};

export function helpReducer(state = defaultState, action) {
    switch (action.type) {
        case FETCH_HELP_REPORT_PENDING:
            return {
                ...state,
                pendingReport: true,
            };
        case FETCH_HELP_REPORT_SUCCESS:
            return {
                ...state,
                pendingReport: false,
            };
        case FETCH_HELP_OFFER_PENDING:
            return {
                ...state,
                pendingOffer: true,
            };
        case FETCH_HELP_OFFER_SUCCESS:
            return {
                ...state,
                pendingOffer: false,
            };
        case FETCH_HELP_RATING_PENDING:
            return {
                ...state,
                pendingRating: true,
                successRating: false,
            };
        case FETCH_HELP_RATING_SUCCESS:
            return {
                ...state,
                pendingRating: false,
                successRating: action.success,
            };
        default:
            return state;
    }
}

export const getHelpReportPending = state => state.rootReducer.help.pendingReport;
export const getHelpOfferPending = state => state.rootReducer.help.pendingOffer;
export const getHelpRatingPending = state => state.rootReducer.help.pendingRating;
export const getHelpRatingSuccess = state => state.rootReducer.help.successRating;
