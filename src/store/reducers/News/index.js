import {FETCH_NEWS_PENDING, FETCH_NEWS_ERROR, FETCH_NEWS_SUCCESS, FETCH_NEWS_PAGINATION} from '../../constants/News';

const defaultState = {
    pending: false,
    news: [],
    pagination: null,
    error: null,
};

export function newsReducer(state = defaultState, action) {
    switch (action.type) {
        case FETCH_NEWS_PENDING:
            return {
                ...state,
                pending: true,
                pagination: null,
            };
        case FETCH_NEWS_SUCCESS:
            return {
                ...state,
                pending: false,
                news: action.news,
            };
        case FETCH_NEWS_PAGINATION:
            return {
                ...state,
                pagination: action.pagination,
            };
        case FETCH_NEWS_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error,
            };
        default:
            return state;
    }
}

export const getNews = state => state.rootReducer.news.news;
export const getNewsPending = state => state.rootReducer.news.pending;
export const getNewsPagination = state => state.rootReducer.news.pagination;
export const getNewsError = state => state.rootReducer.news.error;
