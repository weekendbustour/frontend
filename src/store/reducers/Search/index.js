import {progressBar} from '../../progress';
import {API_URL} from '../../../api/api';

const FETCH_SEARCH_PENDING = 'FETCH_SEARCH_PENDING';
const FETCH_SEARCH_SUCCESS = 'FETCH_SEARCH_SUCCESS';
const FETCH_SEARCH_PAGINATION_SUCCESS = 'FETCH_SEARCH_PAGINATION_SUCCESS';
const FETCH_SEARCH_ERROR = 'FETCH_SEARCH_ERROR';

const defaultState = {
    pending: false,
    tours: [],
    pagination: {},
    error: null,
};

function fetchSearchPending() {
    return {
        type: FETCH_SEARCH_PENDING,
        tours: [],
    };
}

function fetchSearchSuccess(data) {
    return {
        type: FETCH_SEARCH_SUCCESS,
        tours: data,
    };
}
function fetchSearchPaginationSuccess(data) {
    return {
        type: FETCH_SEARCH_PAGINATION_SUCCESS,
        pagination: data,
    };
}

function fetchSearchError(error) {
    return {
        type: FETCH_SEARCH_ERROR,
        error: error,
    };
}

export function fetchSearch(data = {}) {
    let urlData = new URLSearchParams(data);
    urlData.forEach(function(value, key) {
        if (key === 'category_id') {
            const array = value.split(',');
            array.map((mapValue, mapKey) => urlData.append('category_id[]', mapValue));
        }
    });
    if (urlData.has('category_id')) {
        urlData.delete('category_id');
    }
    const paras = '?' + urlData.toString();
    console.log(data);
    return dispatch => {
        progressBar.start();
        dispatch(fetchSearchPending());
        fetch(API_URL + '/search' + paras)
            .then(res => res.json())
            .then(res => {
                progressBar.stop();
                if (res.error) {
                    throw res.error;
                }
                if (res.pagination) {
                    dispatch(fetchSearchPaginationSuccess(res.pagination));
                }
                dispatch(fetchSearchSuccess(res.data));
                return res.data;
            })
            .catch(error => {
                progressBar.stop();
                dispatch(fetchSearchError(error));
            });
    };
}

export function searchReducer(state = defaultState, action) {
    switch (action.type) {
        case FETCH_SEARCH_PENDING:
            return {
                ...state,
                pending: true,
                tours: [],
            };
        case FETCH_SEARCH_SUCCESS:
            return {
                ...state,
                pending: false,
                tours: action.tours,
            };
        case FETCH_SEARCH_PAGINATION_SUCCESS:
            return {
                ...state,
                pending: false,
                pagination: action.pagination,
            };
        case FETCH_SEARCH_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error,
            };
        default:
            return state;
    }
}

export const getSearchTours = state => state.rootReducer.search.tours;
export const getSearchPagination = state => state.rootReducer.search.pagination;
export const getSearchPending = state => state.rootReducer.search.pending;
export const getSearchError = state => state.error;
