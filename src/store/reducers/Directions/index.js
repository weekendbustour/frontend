import {FETCH_DIRECTIONS_PENDING, FETCH_DIRECTIONS_ERROR, FETCH_DIRECTIONS_SUCCESS, FETCH_DIRECTIONS_PAGINATION} from '../../constants/Directions';

const defaultState = {
    pending: false,
    directions: [],
    error: null,
    pagination: null,
};

export function directionsReducer(state = defaultState, action) {
    switch (action.type) {
        case FETCH_DIRECTIONS_PENDING:
            return {
                ...state,
                pending: true,
            };
        case FETCH_DIRECTIONS_SUCCESS:
            return {
                ...state,
                pending: false,
                directions: action.directions,
            };
        case FETCH_DIRECTIONS_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error,
            };
        case FETCH_DIRECTIONS_PAGINATION:
            return {
                ...state,
                pagination: action.pagination,
            };
        default:
            return state;
    }
}

export const getDirections = state => state.rootReducer.directions.directions;
export const getDirectionsPending = state => state.rootReducer.directions.pending;
export const getDirectionsError = state => state.rootReducer.directions.error;
export const getDirectionsPagination = state => state.rootReducer.directions.pagination;
