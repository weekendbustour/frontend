import {getUser} from '../User';
import store from '../../index';
import {
    MODAL_ATTRACTION_CARD, MODAL_AUTH_IS_COMING,
    MODAL_COMMENT_CREATE,
    MODAL_COMMENT_RATE_CREATE, MODAL_ENTERTAINMENT_CARD,
    MODAL_LOGIN, MODAL_REGISTER,
    MODAL_TOUR_RESERVATION
} from '../../../lib/modal';
import {toastAuthModal} from "../../../lib/toast";

const FETCH_MODAL_SUCCESS = 'FETCH_MODAL_SUCCESS';
const FETCH_MODAL_TOUR_SUCCESS = 'FETCH_MODAL_TOUR_SUCCESS';
const FETCH_MODAL_ATTRACTION_SUCCESS = 'FETCH_MODAL_ATTRACTION_SUCCESS';
const FETCH_MODAL_ENTERTAINMENT_SUCCESS = 'FETCH_MODAL_ENTERTAINMENT_SUCCESS';

const defaultState = {
    pending: false,
    modal: null,
    attraction: {},
    tour: {},
    entertainment: {},
    error: null,
};

function fetchModalSuccess(data) {
    return {
        type: FETCH_MODAL_SUCCESS,
        modal: data,
    };
}

function fetchModalTourSuccess(name, data) {
    return {
        type: FETCH_MODAL_TOUR_SUCCESS,
        modal: name,
        tour: data,
    };
}

function fetchModalAttractionSuccess(name, data) {
    return {
        type: FETCH_MODAL_ATTRACTION_SUCCESS,
        modal: name,
        attraction: data,
    };
}

function fetchModalEntertainmentSuccess(name, data) {
    return {
        type: FETCH_MODAL_ENTERTAINMENT_SUCCESS,
        modal: name,
        entertainment: data,
    };
}

export function fetchModal(name) {
    if (process.env.REACT_APP_AUTH_ENABLED === 'false') {
        if (name === MODAL_LOGIN || name === MODAL_REGISTER) {
            name = MODAL_AUTH_IS_COMING;
        }
    }
    if (name === MODAL_COMMENT_CREATE) {
        const user = getUser(store.getState());
        if (user === null) {
            return dispatch => {
                toastAuthModal('Только авторизованный пользователь может оставлять комментарии');
                dispatch(fetchModal(MODAL_LOGIN));
            };
        }
    }
    if (name === MODAL_COMMENT_RATE_CREATE) {
        const user = getUser(store.getState());
        if (user === null) {
            return dispatch => {
                toastAuthModal('Только авторизованный пользователь может оставлять комментарии');
                dispatch(fetchModal(MODAL_LOGIN));
            };
        }
    }
    if (name === MODAL_TOUR_RESERVATION) {
        const user = getUser(store.getState());
        if (user === null) {
            return dispatch => {
                toastAuthModal('Только авторизованный пользователь может бронировать тур');
                dispatch(fetchModal(MODAL_LOGIN));
            };
        }
    }
    return dispatch => {
        dispatch(fetchModalSuccess(name));
        return name;
    };
}

export function fetchModalAttraction(data) {
    return dispatch => {
        dispatch(fetchModalAttractionSuccess(MODAL_ATTRACTION_CARD, data));
    };
}

export function fetchModalEntertainment(data) {
    return dispatch => {
        dispatch(fetchModalEntertainmentSuccess(MODAL_ENTERTAINMENT_CARD, data));
    };
}

export function fetchModalTour(data) {
    return dispatch => {
        dispatch(fetchModalTourSuccess('tour', data));
        return data;
    };
}

export function modalReducer(state = defaultState, action) {
    switch (action.type) {
        case FETCH_MODAL_SUCCESS:
            return {
                ...state,
                modal: action.modal,
            };
        case FETCH_MODAL_TOUR_SUCCESS:
            return {
                ...state,
                modal: action.modal,
                tour: action.tour,
            };
        case FETCH_MODAL_ATTRACTION_SUCCESS:
            return {
                ...state,
                modal: action.modal,
                attraction: action.attraction,
            };
        case FETCH_MODAL_ENTERTAINMENT_SUCCESS:
            return {
                ...state,
                modal: action.modal,
                entertainment: action.entertainment,
            };
        default:
            return state;
    }
}

export const getModal = state => state.rootReducer.modal.modal;
export const getModalTour = state => state.rootReducer.modal.tour;
export const getModalAttraction = state => state.rootReducer.modal.attraction;
export const getModalEntertainment = state => state.rootReducer.modal.entertainment;
