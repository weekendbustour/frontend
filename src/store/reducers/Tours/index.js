import {FETCH_TOURS_PENDING, FETCH_TOURS_ERROR, FETCH_TOURS_SUCCESS, FETCH_TOURS_PAGINATION} from '../../constants/Tours';

const defaultState = {
    pending: false,
    tours: [],
    error: null,
    pagination: null,
};

export function toursReducer(state = defaultState, action) {
    switch (action.type) {
        case FETCH_TOURS_PENDING:
            return {
                ...state,
                pending: true,
            };
        case FETCH_TOURS_SUCCESS:
            return {
                ...state,
                pending: false,
                tours: action.tours,
            };
        case FETCH_TOURS_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error,
            };
        case FETCH_TOURS_PAGINATION:
            return {
                ...state,
                pagination: action.pagination,
            };
        default:
            return state;
    }
}

export const getTours = state => state.rootReducer.tours.tours;
export const getToursPending = state => state.rootReducer.tours.pending;
export const getToursError = state => state.rootReducer.tours.error;
export const getToursPagination = state => state.rootReducer.tours.pagination;
