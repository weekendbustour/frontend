import {fetchModal} from '../Modal';
import {toastByError, toastBySuccess} from '../../../lib/toast';
import {request} from "../../../lib/request";

const FETCH_USER_RESERVATIONS_ID_SUCCESS = 'FETCH_USER_RESERVATIONS_ID_SUCCESS';
const FETCH_USER_RESERVATIONS_ID_PENDING = 'FETCH_USER_RESERVATIONS_ID_PENDING';
const FETCH_USER_RESERVATIONS_ID_ERRORS = 'FETCH_USER_RESERVATIONS_ID_ERRORS';

const FETCH_USER_RESERVATIONS_CANCEL_PENDING = 'FETCH_USER_RESERVATIONS_CANCEL_PENDING';
const FETCH_USER_RESERVATIONS_CANCEL_SUCCESS = 'FETCH_USER_RESERVATIONS_CANCEL_SUCCESS';

const defaultState = {
    reservations: null,
    reservationsPendingArray: [],

    errors: {},
    pending: false,
    pendingCancel: false,
};

const fetchUserReservationsCancelPending = pending => ({
    type: FETCH_USER_RESERVATIONS_CANCEL_PENDING,
    pending,
});

const fetchUserReservationsCancelSuccess = (reservations, pending) => ({
    type: FETCH_USER_RESERVATIONS_CANCEL_SUCCESS,
    reservations,
    pending,
});

function fetchUserReservationsIdPending() {
    return {
        type: FETCH_USER_RESERVATIONS_ID_PENDING,
    };
}

export function fetchUserReservationsIdSuccess(data) {
    return {
        type: FETCH_USER_RESERVATIONS_ID_SUCCESS,
        reservations: data,
    };
}

export function fetchUserReservationsIdErrors(data) {
    return {
        type: FETCH_USER_RESERVATIONS_ID_ERRORS,
        errors: data,
    };
}

export function fetchUserReservation(data) {
    return dispatch => {
        dispatch(fetchUserReservationsIdPending());
        request.post(`/user/reservation`, data).then(response => {
            toastBySuccess(response);
            const data = response.data.data;
            dispatch(fetchUserReservationsIdSuccess(data));
            dispatch(fetchModal(null));
        }).catch(response => {
            toastByError(response);
            dispatch(fetchUserReservationsIdErrors({}));
        });
    };
}

export function fetchUserReservationCancel(id) {
    return dispatch => {
        dispatch(fetchUserReservationsCancelPending(id));
        request.post(`/user/reservation/cancel`, {
            id: id,
        }).then(response => {
            toastBySuccess(response);
            const data = response.data.data;
            //dispatch(fetchUserReservationsIdSuccess(data));
            dispatch(fetchUserReservationsCancelSuccess(data, id));
        });
    };
}

export function fetchUserReservationsId() {
    return dispatch => {
        request.get(`/user/reservations`).then(response => {
            const data = response.data.data;
            if (data) {
                dispatch(fetchUserReservationsIdSuccess(data));
            }
        });
    };
}

export function userReservationReducer(state = defaultState, action) {
    switch (action.type) {
        case FETCH_USER_RESERVATIONS_ID_PENDING:
            return {
                ...state,
                pending: true,
            };
        case FETCH_USER_RESERVATIONS_ID_ERRORS:
            return {
                ...state,
                pending: false,
                errors: action.errors,
            };
        case FETCH_USER_RESERVATIONS_ID_SUCCESS:
            return {
                ...state,
                pending: false,
                reservations: action.reservations,
            };
        case FETCH_USER_RESERVATIONS_CANCEL_PENDING:
            let reservationsPendingArray_PENDING = state.reservationsPendingArray;
            reservationsPendingArray_PENDING.push(action.pending);
            return {
                ...state,
                reservationsPendingArray: reservationsPendingArray_PENDING,
            };
        case FETCH_USER_RESERVATIONS_CANCEL_SUCCESS:
            let reservationsPendingArray_SUCCESS = state.reservationsPendingArray;
            let index = reservationsPendingArray_SUCCESS.indexOf(action.pending);
            if (index !== -1) {
                reservationsPendingArray_SUCCESS.splice(index, 1);
            }
            return {
                ...state,
                reservations: action.reservations,
                reservationsPendingArray: reservationsPendingArray_SUCCESS,
            };
        default:
            return state;
    }
}

export const getUserReservations = state => state.rootReducer.userReservation.reservations;
export const getUserReservationsPending = state => state.rootReducer.userReservation.pending;
export const getUserReservationsPendingArray = state => state.rootReducer.userReservation.reservationsPendingArray;
