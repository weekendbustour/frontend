import {request} from "../../../lib/request";

const FETCH_USER_COMMENTS_PENDING = 'FETCH_USER_COMMENTS_PENDING';
const FETCH_USER_COMMENTS_ERROR = 'FETCH_USER_COMMENTS_ERROR';
const FETCH_USER_COMMENTS_SUCCESS = 'FETCH_USER_COMMENTS_SUCCESS';

const defaultState = {
    pending: true,
    comments: {},
    error: null,
};

function fetchUserCommentsPending() {
    return {
        type: FETCH_USER_COMMENTS_PENDING,
    };
}

export function fetchUserCommentsSuccess(data) {
    return {
        type: FETCH_USER_COMMENTS_SUCCESS,
        comments: data,
    };
}

//
// function fetchUserCommentsError(error) {
//     return {
//         type: FETCH_USER_COMMENTS_ERROR,
//         error: error,
//     };
// }

export function fetchUserComments(user) {
    return dispatch => {
        dispatch(fetchUserCommentsPending());
        request.get(`/user/comments`, {
            extended: 1,
        }).then(response => {
            const data = response.data.data;
            dispatch(fetchUserCommentsSuccess(data));
        });
    };
}

export function fetchUserCommentsId() {
    return dispatch => {
        request.get(`/user/comments`).then(response => {
            const data = response.data.data;
            if (data) {
                dispatch(fetchUserCommentsSuccess(data));
            }
        });
    };
}

export function userCommentsReducer(state = defaultState, action) {
    switch (action.type) {
        case FETCH_USER_COMMENTS_PENDING:
            return {
                ...state,
                pending: true,
            };
        case FETCH_USER_COMMENTS_SUCCESS:
            return {
                ...state,
                pending: false,
                comments: action.comments,
            };
        case FETCH_USER_COMMENTS_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error,
            };
        default:
            return state;
    }
}
export const getUserComments = state => state.rootReducer.userComments.comments;
export const getUserCommentsPending = state => state.rootReducer.userComments.pending;
export const getUserCommentsTours = state => state.rootReducer.userComments.comments.tours;
export const getUserCommentsOrganizations = state => state.rootReducer.userComments.comments.organizations;
export const getUserCommentsDirections = state => state.rootReducer.userComments.comments.directions;
export const getUserCommentsNews = state => state.rootReducer.userComments.comments.news;
