import {progressBar} from '../../progress';
import {toastByRes, toastByError, toastAuthModal} from '../../../lib/toast';
import {fetchModal} from '../Modal';
import {fetchComments} from '../Comments';
import {getUser} from "./index";
import store from "../../index";
import {MODAL_LOGIN} from "../../../lib/modal";
import {request} from "../../../lib/request";
import {langAuth} from "../../../lang/auth";

const FETCH_USER_COMMENT_PENDING = 'FETCH_USER_COMMENT_PENDING';
const FETCH_USER_COMMENT_ERROR = 'FETCH_USER_COMMENT_ERROR';
const FETCH_USER_COMMENT_SUCCESS = 'FETCH_USER_COMMENT_SUCCESS';
const FETCH_USER_COMMENT_VOTE_COUNT = 'FETCH_USER_COMMENT_VOTE_COUNT';

const defaultState = {
    pending: false,
    success: false,
    error: null,
    comment: {
        vote: null,
        id: 0,
    },
    clear: false,
};

function fetchUserCommentPending() {
    return {
        type: FETCH_USER_COMMENT_PENDING,
    };
}

function fetchUserCommentSuccess(success) {
    return {
        type: FETCH_USER_COMMENT_SUCCESS,
        success: success,
    };
}

function fetchUserCommentVoteCount(comment) {
    return {
        type: FETCH_USER_COMMENT_VOTE_COUNT,
        comment: comment,
    };
}

function fetchUserCommentError(error) {
    return {
        type: FETCH_USER_COMMENT_ERROR,
        error: error,
    };
}

export function fetchUserComment(data) {
    const {id, model} = data;
    const user = getUser(store.getState());
    if (user === null) {
        return dispatch => {
            toastAuthModal(langAuth.access.comment);
            dispatch(fetchModal(MODAL_LOGIN));
        };
    }
    return dispatch => {
        dispatch(fetchUserCommentPending());
        request.post(`/user/comment`, data).then(response => {
            //const data = response.data;
            dispatch(fetchUserCommentSuccess(true));
            toastByRes(response);
            dispatch(fetchModal(null));
            dispatch(fetchComments(id, model));
        }).catch(error => {
            toastByError(error);
            dispatch(fetchUserCommentError({}));
            const data = error.response.data;
            console.log(data);
        });
    };
}

export function fetchUserCommentVote(data, state) {
    const {id, model, model_id} = data;
    const user = getUser(store.getState());
    if (user === null) {
        return dispatch => {
            toastAuthModal(langAuth.access.commentVote);
            dispatch(fetchModal(MODAL_LOGIN));
        };
    }
    return dispatch => {
        dispatch(fetchUserCommentPending());
        request.post(`/user/comment/${id}/vote`, {
            value: state ? 1 : 0,
        }).then(response => {
            progressBar.stop();
            const data = response.data;
            if (data.comment) {
                dispatch(fetchUserCommentVoteCount(data.comment));
            }
            dispatch(fetchComments(model_id, model));
            console.log(data);
        }).catch(error => {
            progressBar.stop();
            toastByError(error);
            const data = error.response.data;
            if (data.comment) {
                dispatch(fetchUserCommentVoteCount(data.comment));
            }
            console.log(data);
        });
    };
}

export function userCommentReducer(state = defaultState, action) {
    switch (action.type) {
        case FETCH_USER_COMMENT_PENDING:
            return {
                ...state,
                pending: true,
            };
        case FETCH_USER_COMMENT_SUCCESS:
            return {
                ...state,
                pending: false,
                success: action.success,
            };
        case FETCH_USER_COMMENT_VOTE_COUNT:
            return {
                ...state,
                pending: false,
                comment: action.comment,
            };
        case FETCH_USER_COMMENT_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error,
            };
        default:
            return state;
    }
}
export const getUserCommentVote = state => state.rootReducer.userComment.comment;
export const getUserCommentSuccess = state => state.rootReducer.userComment.success;
export const getUserCommentPending = state => state.rootReducer.userComment.pending;
export const getUserCommentClear = state => state.rootReducer.userComment.clear;
