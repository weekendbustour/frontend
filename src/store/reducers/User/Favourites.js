import {request} from "../../../lib/request";

const FETCH_USER_FAVOURITES_LIST_PENDING = 'FETCH_USER_FAVOURITES_LIST_PENDING';
const FETCH_USER_FAVOURITES_LIST_ERROR = 'FETCH_USER_FAVOURITES_LIST_ERROR';
const FETCH_USER_FAVOURITES_LIST_SUCCESS = 'FETCH_USER_FAVOURITES_LIST_SUCCESS';

const defaultState = {
    pending: true,
    favourites: {},
    error: null,
};

function fetchUserFavouritesListPending() {
    return {
        type: FETCH_USER_FAVOURITES_LIST_PENDING,
    };
}

function fetchUserFavouritesListSuccess(data) {
    console.log(data);
    return {
        type: FETCH_USER_FAVOURITES_LIST_SUCCESS,
        favourites: data,
    };
}
//
// function fetchUserFavouritesListError(error) {
//     return {
//         type: FETCH_USER_FAVOURITES_LIST_ERROR,
//         error: error,
//     };
// }

export function fetchUserFavourites() {
    return dispatch => {
        dispatch(fetchUserFavouritesListPending());
        request.get(`/user/favourites`, {
            extended: 1,
        }).then(response => {
            const data = response.data.data;
            dispatch(fetchUserFavouritesListSuccess(data));
        });
    };
}

export function userFavouritesReducer(state = defaultState, action) {
    switch (action.type) {
        case FETCH_USER_FAVOURITES_LIST_PENDING:
            return {
                ...state,
                pending: true,
            };
        case FETCH_USER_FAVOURITES_LIST_SUCCESS:
            return {
                ...state,
                pending: false,
                favourites: action.favourites,
            };
        case FETCH_USER_FAVOURITES_LIST_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error,
            };
        default:
            return state;
    }
}
export const getUserFavourites = state => state.rootReducer.userFavourites.favourites;
export const getUserFavouritesPending = state => state.rootReducer.userFavourites.pending;
export const getUserFavouritesTours = state => state.rootReducer.userFavourites.favourites.tours;
export const getUserFavouritesOrganizations = state => state.rootReducer.userFavourites.favourites.organizations;
export const getUserFavouritesDirections = state => state.rootReducer.userFavourites.favourites.directions;
export const getUserFavouritesAttractions = state => state.rootReducer.userFavourites.favourites.attractions;
