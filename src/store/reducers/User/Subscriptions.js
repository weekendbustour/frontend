import {request} from "../../../lib/request";

const FETCH_USER_SUBSCRIPTIONS_PENDING = 'FETCH_USER_SUBSCRIPTIONS_PENDING';
const FETCH_USER_SUBSCRIPTIONS_ERROR = 'FETCH_USER_SUBSCRIPTIONS_ERROR';
const FETCH_USER_SUBSCRIPTIONS_SUCCESS = 'FETCH_USER_SUBSCRIPTIONS_SUCCESS';

const defaultState = {
    pending: false,
    subscriptions: {},
    error: null,
};

function fetchUserSubscriptionsPending() {
    return {
        type: FETCH_USER_SUBSCRIPTIONS_PENDING,
    };
}

export function fetchUserSubscriptionsSuccess(subscriptions) {
    return {
        type: FETCH_USER_SUBSCRIPTIONS_SUCCESS,
        subscriptions: subscriptions,
    };
}
//
// function fetchUserSubscriptionsError(error) {
//     return {
//         type: FETCH_USER_SUBSCRIPTIONS_ERROR,
//         error: error,
//     };
// }

export function fetchUserSubscriptions(user) {
    return dispatch => {
        dispatch(fetchUserSubscriptionsPending());
        request.get(`/user/subscriptions`, {
            extended: 1,
        }).then(response => {
            const data = response.data.data;
            dispatch(fetchUserSubscriptionsSuccess(data));
        });
    };
}

export function userSubscriptionsReducer(state = defaultState, action) {
    switch (action.type) {
        case FETCH_USER_SUBSCRIPTIONS_PENDING:
            return {
                ...state,
                pending: true,
            };
        case FETCH_USER_SUBSCRIPTIONS_SUCCESS:
            return {
                ...state,
                pending: false,
                subscriptions: action.subscriptions,
            };
        case FETCH_USER_SUBSCRIPTIONS_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error,
            };
        default:
            return state;
    }
}
export const getUserSubscriptions = state => state.rootReducer.userSubscriptions.subscriptions;
export const getUserSubscriptionsSuccess = state => state.rootReducer.userSubscriptions.success;
export const getUserSubscriptionsPending = state => state.rootReducer.userSubscriptions.pending;
export const getUserSubscriptionsOrganizations = state => state.rootReducer.userSubscriptions.subscriptions.organizations;
export const getUserSubscriptionsDirections = state => state.rootReducer.userSubscriptions.subscriptions.directions;
