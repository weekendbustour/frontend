import {fetchModal} from '../Modal';
import {toastBySuccess} from '../../../lib/toast';
import {request} from "../../../lib/request";

const FETCH_USER_NOTIFICATION_ID_SUCCESS = 'FETCH_USER_NOTIFICATION_ID_SUCCESS';
const FETCH_USER_NOTIFICATION_ID_PENDING = 'FETCH_USER_NOTIFICATION_ID_PENDING';

const defaultState = {
    notifications: null,
    pending: false,
};

function fetchUserNotificationsIdPending() {
    return {
        type: FETCH_USER_NOTIFICATION_ID_PENDING,
    };
}

export function fetchUserNotificationsIdSuccess(data) {
    return {
        type: FETCH_USER_NOTIFICATION_ID_SUCCESS,
        notifications: data,
    };
}

export function fetchUserNotification(data) {
    return dispatch => {
        dispatch(fetchUserNotificationsIdPending());
        request.post(`/user/notification`, data).then(response => {
            toastBySuccess(response);
            const data = response.data.data;
            dispatch(fetchUserNotificationsIdSuccess(data));
            dispatch(fetchModal(null));
        });
    };
}

export function fetchUserNotificationsId() {
    return dispatch => {
        request.get(`/user/notifications`).then(response => {
            const data = response.data.data;
            if (data) {
                dispatch(fetchUserNotificationsIdSuccess(data));
            }
        });
    };
}

export function userNotificationReducer(state = defaultState, action) {
    switch (action.type) {
        case FETCH_USER_NOTIFICATION_ID_PENDING:
            return {
                ...state,
                pending: true,
            };
        case FETCH_USER_NOTIFICATION_ID_SUCCESS:
            return {
                ...state,
                pending: false,
                notifications: action.notifications,
            };
        default:
            return state;
    }
}

export const getUserNotification = state => state.rootReducer.userNotification.notifications;
export const getUserNotificationPending = state => state.rootReducer.userNotification.pending;
