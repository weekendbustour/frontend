import {API_URL, body, headers} from '../../../api/api';
import {toast} from 'react-toastify';
import store from '../../index';
import {fetchModal} from '../Modal';
import {toastByRes} from '../../../lib/toast';
import {getUser} from './index';
import {MODAL_LOGIN} from '../../../lib/modal';
import {request} from "../../../lib/request";

const FETCH_USER_FAVOURITES_ID_SUCCESS = 'FETCH_USER_FAVOURITES_ID_SUCCESS';
const FETCH_USER_FAVOURITES_PENDING = 'FETCH_USER_FAVOURITES_PENDING';

const defaultState = {
    favourites: null,
    favouritesPendingArray: [],
};

export function fetchUserFavouritesPending(pendingName) {
    return {
        type: FETCH_USER_FAVOURITES_PENDING,
        pending: pendingName,
    };
}

export function fetchUserFavouritesSuccess(data, pendingName) {
    return {
        type: FETCH_USER_FAVOURITES_ID_SUCCESS,
        favourites: data,
        pending: pendingName,
    };
}

export function fetchUserFavourite(model, data) {
    const user = getUser(store.getState());
    if (user === null) {
        return dispatch => {
            toast('Только авторизованный пользователь может добавлять в Изранное', {
                type: toast.TYPE.WARNING,
                containerId: 'B',
                autoClose: 5000,
            });
            dispatch(fetchModal(MODAL_LOGIN));
        };
    }
    return dispatch => {
        //progressBar.start();
        const pendingName = model + '-' + data.id;
        dispatch(fetchUserFavouritesPending(pendingName));
        fetch(API_URL + '/user/favourite', {
            method: 'POST',
            body: body({
                id: data.id,
                model: model,
            }),
            headers: headers(),
        })
            .then(res => res.json())
            .then(res => {
                //progressBar.stop();
                const data = res.data;
                if (res.error) {
                    throw res.error;
                }
                toastByRes(res);
                if (data) {
                    dispatch(fetchUserFavouritesSuccess(data, pendingName));
                }
                return data;
            })
            .catch(error => {});
    };
}

export function fetchUserFavouritesId() {
    return dispatch => {
        request.get(`/user/favourites`).then(response => {
            const data = response.data.data;
            if (data) {
                dispatch(fetchUserFavouritesSuccess(data));
            }
        });
    };
}

export function userFavouriteReducer(state = defaultState, action) {
    switch (action.type) {
        case FETCH_USER_FAVOURITES_PENDING:
            let favouritesPendingArray_PENDING = state.favouritesPendingArray;
            favouritesPendingArray_PENDING.push(action.pending);
            return {
                ...state,
                favouritesPendingArray: favouritesPendingArray_PENDING,
            };
        case FETCH_USER_FAVOURITES_ID_SUCCESS:
            let favouritesPendingArray_SUCCESS = state.favouritesPendingArray;
            let index = favouritesPendingArray_SUCCESS.indexOf(action.pending);
            if (index !== -1) {
                favouritesPendingArray_SUCCESS.splice(index, 1);
            }
            return {
                ...state,
                favourites: action.favourites,
                favouritesPendingArray: favouritesPendingArray_SUCCESS,
            };
        default:
            return state;
    }
}

export const getUserFavourites = state => state.rootReducer.userFavourite.favourites;
export const getUserFavouriteDirections = state => {
    if (state.rootReducer.userFavourite.favourites === null) {
        return [];
    }
    return state.rootReducer.userFavourite.favourites.directions;
};

export const getUserFavouritesPendingArray = state => state.rootReducer.userFavourite.favouritesPendingArray;
