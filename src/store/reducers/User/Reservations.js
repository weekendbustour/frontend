import {request} from "../../../lib/request";

const FETCH_USER_RESERVATIONS_PENDING = 'FETCH_USER_RESERVATIONS_PENDING';
const FETCH_USER_RESERVATIONS_ERROR = 'FETCH_USER_RESERVATIONS_ERROR';
const FETCH_USER_RESERVATIONS_SUCCESS = 'FETCH_USER_RESERVATIONS_SUCCESS';

const defaultState = {
    pending: false,
    reservations: {},
    error: null,
};

function fetchUserReservationsPending() {
    return {
        type: FETCH_USER_RESERVATIONS_PENDING,
    };
}

export function fetchUserReservationsSuccess(data) {
    return {
        type: FETCH_USER_RESERVATIONS_SUCCESS,
        data: data,
    };
}

// function fetchUserReservationsError(error) {
//     return {
//         type: FETCH_USER_RESERVATIONS_ERROR,
//         error: error,
//     };
// }

export function fetchUserReservations() {
    return dispatch => {
        dispatch(fetchUserReservationsPending());
        request.get(`/user/reservations`, {
            extended: 1,
        }).then(response => {
            const data = response.data.data;
            dispatch(fetchUserReservationsSuccess(data));
        });
    };
}

export function userReservationsReducer(state = defaultState, action) {
    switch (action.type) {
        case FETCH_USER_RESERVATIONS_PENDING:
            return {
                ...state,
                pending: true,
            };
        case FETCH_USER_RESERVATIONS_SUCCESS:
            return {
                ...state,
                pending: false,
                reservations: action.data,
            };
        case FETCH_USER_RESERVATIONS_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error,
            };
        default:
            return state;
    }
}
export const getUserReservations = state => state.rootReducer.userReservations.reservations;
export const getUserReservationsSuccess = state => state.rootReducer.userReservations.success;
export const getUserReservationsPending = state => state.rootReducer.userReservations.pending;
export const getUserReservationsTours = state => state.rootReducer.userReservations.reservations.tours;
