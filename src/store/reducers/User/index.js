import store from '../../index';
import {fetchAuthGuest} from '../../actions/Auth';
import {fetchUserFavouritesId} from './Favourite';
import {fetchUserCommentsId} from './Comments';
import {fetchUserSubscriptionsId} from './../../actions/User/Subscription';
import {fetchUserSubscriptionsSuccess} from './../User/Subscriptions';
import {fetchUserFavouritesSuccess} from './../User/Favourite';
import {fetchUserCommentsSuccess} from './../User/Comments';

import {fetchUserReservationsSuccess} from "./Reservations";
import {fetchUserReservationsId} from "./Reservation";
import {request} from "../../../lib/request";
import {fetchUserCommentVotesId} from "./CommentVotes";
import {toastByError} from "../../../lib/toast";

const FETCH_USER_SUCCESS = 'FETCH_USER_SUCCESS';
const FETCH_USER_PENDING = 'FETCH_USER_PENDING';
//const FETCH_USER_PENDING_LOGIN = 'FETCH_USER_PENDING_LOGIN';
//const FETCH_USER_PENDING_REGISTER = 'FETCH_USER_PENDING_REGISTER';
//const FETCH_USER_PENDING_FORGOT_PASSWORD = 'FETCH_USER_PENDING_FORGOT_PASSWORD';
const FETCH_USER_ERROR = 'FETCH_USER_ERROR';
const FETCH_USER_RELOAD = 'FETCH_USER_RELOAD';
//const FETCH_USER_ERRORS_LOGIN = 'FETCH_USER_ERRORS_LOGIN';
//const FETCH_USER_ERRORS_REGISTER = 'FETCH_USER_ERRORS_REGISTER';
//const FETCH_USER_ERRORS_FORGOT_PASSWORD = 'FETCH_USER_ERRORS_FORGOT_PASSWORD';
//const FETCH_USER_SUCCESS_FORGOT_PASSWORD = 'FETCH_USER_SUCCESS_FORGOT_PASSWORD';
const FETCH_USER_HASH_SUCCESS = 'FETCH_USER_HASH_SUCCESS';
const FETCH_USER_TOKEN_SUCCESS = 'FETCH_USER_TOKEN_SUCCESS';

export const LOCAL_STORAGE_HASH_NAME = 'LOCAL_STORAGE_HASH_NAME';
export const LOCAL_STORAGE_TOKEN_NAME = 'LOCAL_STORAGE_TOKEN_NAME';
export const LOCAL_STORAGE_USER_NAME = 'LOCAL_STORAGE_USER_NAME';

const defaultState = {
    pending: false,
    user: null,
    subscriptions: null,
    hash: null,
    error: null,
    //pendingLogin: false,
    //pendingRegister: false,
    //pendingForgotPassword: false,
    //errors: {
    //    login: null,
    //    register: null,
    //    forgotPassword: null,
    //},
    reload: false,
};

function fetchUserPending() {
    return {
        type: FETCH_USER_PENDING,
    };
}

// function fetchUserPendingLogin() {
//     return {
//         type: FETCH_USER_PENDING_LOGIN,
//     };
// }
// function fetchUserPendingForgotPassword() {
//     return {
//         type: FETCH_USER_PENDING_FORGOT_PASSWORD,
//     };
// }
//
// function fetchUserPendingRegister() {
//     return {
//         type: FETCH_USER_PENDING_REGISTER,
//     };
// }

export function fetchUserSuccess(data) {
    localStorage.setItem(LOCAL_STORAGE_USER_NAME, JSON.stringify(data));
    return {
        type: FETCH_USER_SUCCESS,
        user: data,
    };
}
//
// function fetchUserSubscriptionsSuccess(data) {
//     console.log(data);
//     return {
//         type: FETCH_USER_SUBSCRIPTIONS_ID_SUCCESS,
//         subscriptions: data
//     }
// // }
// function fetchUserHashSuccess(data) {
//     localStorage.setItem(LOCAL_STORAGE_HASH_NAME, data);
//     return {
//         type: FETCH_USER_HASH_SUCCESS,
//         hash: data,
//     };
// }

export function fetchUserTokenSuccess(data) {
    localStorage.setItem(LOCAL_STORAGE_TOKEN_NAME, data);
    return {
        type: FETCH_USER_TOKEN_SUCCESS,
        token: data,
    };
}
//
// function fetchUserError(error) {
//     return {
//         type: FETCH_USER_ERROR,
//         error: error,
//     };
// }

// function fetchUserErrorsLogin(errors) {
//     return {
//         type: FETCH_USER_ERRORS_LOGIN,
//         errors: errors,
//     };
// }
//
// function fetchUserErrorsRegister(errors) {
//     return {
//         type: FETCH_USER_ERRORS_REGISTER,
//         errors: errors,
//     };
// }
//
// function fetchUserErrorsForgotPassword(errors) {
//     return {
//         type: FETCH_USER_ERRORS_FORGOT_PASSWORD,
//         errors: errors,
//     };
// }
// function fetchUserSuccessForgotPassword() {
//     return {
//         type: FETCH_USER_SUCCESS_FORGOT_PASSWORD,
//     };
// }
//
// function fetchUserReload(state) {
//     return {
//         type: FETCH_USER_RELOAD,
//         reload: state,
//     };
// }

export function fetchUser(type = 'all') {
    const token = localStorage.getItem(LOCAL_STORAGE_TOKEN_NAME);
    //const hash = localStorage.getItem(LOCAL_STORAGE_HASH_NAME);
    //const user = localStorage.getItem(LOCAL_STORAGE_USER_NAME);
    if (token !== null && token !== 'undefined') {
        return dispatch => {
            dispatch(fetchUserPending());
            request.get(`/user`, {}).then(response => {
                const data = response.data.data;
                dispatch(fetchUserSuccess(data));
                dispatch(fetchAuthGuest(false));
                if (getUserReload(store.getState())) {
                    window.location.reload();
                }
                if (type === 'all') {
                    dispatch(fetchUserFavouritesId(data));
                    dispatch(fetchUserSubscriptionsId(data));
                    dispatch(fetchUserCommentsId(data));
                    dispatch(fetchUserReservationsId(data));
                    dispatch(fetchUserCommentVotesId(data));
                }
            }).catch(response => {
                toastByError(response);
                dispatch(fetchUserLogout());
            });
        };
    } else {
        return dispatch => {};
    }
}
export function fetchUserWithoutRequest(data) {
    return dispatch => {
        dispatch(fetchUserSuccess(data));
        dispatch(fetchAuthGuest(false));
        dispatch(fetchUserFavouritesSuccess(data.favourites));
        dispatch(fetchUserSubscriptionsSuccess(data.subscriptions));
        dispatch(fetchUserCommentsSuccess(data.comments));
        dispatch(fetchUserReservationsSuccess(data.reservations));
    };
}

//
// export function fetchUserLogin(data) {
//     return dispatch => {
//         dispatch(fetchUserPendingLogin());
//         axios.defaults.withCredentials = true;
//         // CSRF COOKIE
//         axios
//             .get(API_URL + `/sanctum/csrf-cookie`)
//             .then((response) => {
//                 console.log(response);
//                 // LOGIN
//                 axios.post(
//                     API_URL + `/auth/login`,
//                     axiosBody({
//                         email: data.email,
//                         password: data.password,
//                     })
//                 ).then(response => {
//                     const data = response.data.data;
//                     //const hash = data.hash;
//                     const token = data.token;
//                     // GET USER
//                     axios.get(
//                         API_URL + `/user`, {
//                             headers: headers(token),
//                         },
//                     ).then(response => {
//                         const data = response.data.data;
//                         toastBySuccess(response);
//                         dispatch(fetchUserSuccess(data));
//                         //dispatch(fetchUserHashSuccess(hash));
//                         dispatch(fetchUserTokenSuccess(token));
//                         window.location.reload();
//                     }).catch(error => {
//                         toastByError(error);
//                         let errors = {};
//                         if (error.response !== undefined) {
//                             errors = error.response.data.errors
//                         }
//                         dispatch(fetchUserErrorsLogin(errors));
//                     });
//                     // toastBySuccess(response);
//                     // dispatch(fetchUserHashSuccess(data.hash));
//                     // dispatch(fetchUserTokenSuccess(data.token));
//                     // dispatch(fetchUserReload(true));
//                     // dispatch(fetchUser());
//                 }).catch(error => {
//                     toastByError(error);
//                     let errors = {};
//                     if (error.response !== undefined) {
//                         errors = error.response.data.errors
//                     }
//                     dispatch(fetchUserErrorsLogin(errors));
//                 });
//             }).catch(error => {
//                 toastByError(error);
//                 dispatch(fetchUserErrorsLogin({}));
//             });
//     };
// }
// export function fetchUserForgotPassword(data) {
//     return dispatch => {
//         dispatch(fetchUserPendingForgotPassword());
//         request.post(`/auth/password/email`, {
//             email: data.email,
//         }).then(response => {
//             const data = response.data.data;
//             toastBySuccess(response);
//             dispatch(fetchUserSuccessForgotPassword());
//         }).catch(error => {
//             toastByError(error);
//             dispatch(fetchUserSuccessForgotPassword());
//             if (error.response.data.errors !== undefined) {
//                 dispatch(fetchUserErrorsForgotPassword(error.response.data.errors));
//             }
//         });
//     };
// }
// export function fetchUserRegister(data) {
//     return dispatch => {
//         dispatch(fetchUserPendingRegister());
//         axios.post(
//             API_URL + `/auth/register`,
//             axiosBody({
//                 phone: data.phone,
//                 email: data.email,
//                 password: data.password,
//             })
//         ).then(response => {
//             const data = response.data.data;
//             toastBySuccess(response);
//             dispatch(fetchUserHashSuccess(data.hash));
//             dispatch(fetchUserTokenSuccess(data.token));
//             dispatch(fetchUserReload(true));
//             dispatch(fetchUser());
//         }).catch(error => {
//             toastByError(error);
//             dispatch(fetchUserErrorsRegister(error.response.data.errors));
//         });
//     };
// }

export function fetchUserLogout() {
    return dispatch => {
        //localStorage.removeItem(LOCAL_STORAGE_HASH_NAME);
        localStorage.removeItem(LOCAL_STORAGE_USER_NAME);
        localStorage.removeItem(LOCAL_STORAGE_TOKEN_NAME);
        window.location.href = '/';
    };
}

export function userReducer(state = defaultState, action) {
    switch (action.type) {
        case FETCH_USER_PENDING:
            return {
                ...state,
                pending: true,
            };
        // case FETCH_USER_PENDING_LOGIN:
        //     return {
        //         ...state,
        //         pendingLogin: true,
        //         errors: Object.assign(state.errors, {
        //             login: null,
        //         }),
        //     };
        // case FETCH_USER_PENDING_REGISTER:
        //     return {
        //         ...state,
        //         pendingRegister: true,
        //         errors: Object.assign(state.errors, {
        //             register: null,
        //         }),
        //     };
        // case FETCH_USER_PENDING_FORGOT_PASSWORD:
        //     return {
        //         ...state,
        //         pendingForgotPassword: true,
        //         errors: Object.assign(state.errors, {
        //             forgotPassword: null,
        //         }),
        //     };
        // case FETCH_USER_SUCCESS_FORGOT_PASSWORD:
        //     return {
        //         ...state,
        //         pendingForgotPassword: false,
        //         errors: Object.assign(state.errors, {
        //             forgotPassword: null,
        //         }),
        //     };
        case FETCH_USER_SUCCESS:
            return {
                ...state,
                user: action.user,
            };
        case FETCH_USER_HASH_SUCCESS:
            return {
                ...state,
                hash: action.hash,
            };
        case FETCH_USER_TOKEN_SUCCESS:
            return {
                ...state,
                token: action.token,
            };
        case FETCH_USER_ERROR:
            return {
                ...state,
                error: action.error,
            };
        case FETCH_USER_RELOAD:
            return {
                ...state,
                reload: action.reload,
            };
        // case FETCH_USER_ERRORS_LOGIN:
        //     return {
        //         ...state,
        //         pendingLogin: false,
        //         errors: Object.assign(state.errors, {
        //             login: action.errors,
        //         }),
        //     };
        // case FETCH_USER_ERRORS_REGISTER:
        //     return {
        //         ...state,
        //         pendingRegister: false,
        //         errors: Object.assign(state.errors, {
        //             register: action.errors,
        //         }),
        //     };
        // case FETCH_USER_ERRORS_FORGOT_PASSWORD:
        //     return {
        //         ...state,
        //         pendingForgotPassword: false,
        //         errors: Object.assign(state.errors, {
        //             forgotPassword: action.errors,
        //         }),
        //     };
        default:
            return state;
    }
}

export const getUser = state => {
    const user = localStorage.getItem(LOCAL_STORAGE_USER_NAME);
    if (user === 'undefined') {
        localStorage.removeItem(LOCAL_STORAGE_USER_NAME);
        //localStorage.removeItem(LOCAL_STORAGE_HASH_NAME);
        localStorage.removeItem(LOCAL_STORAGE_TOKEN_NAME);
        return null;
    }
    if (user !== null) {
        return JSON.parse(user);
    }
    return state.rootReducer.user.user;
};
export const getUserFavouritesTours = state => {
    const user = getUser(state);
    return user !== null ? user.favourites.tours : null;
};
export const getUserIsGuest = state => {
    const user = getUser(state);
    return user === null || user.id === undefined;
};
export const getUserHash = state => state.rootReducer.user.hash;
export const getUserToken = state => state.rootReducer.user.token;
export const getUserReload = state => state.rootReducer.user.reload;
export const getUserSubscriptions = state => state.rootReducer.user.subscriptions;

//export const getUserErrorsLogin = state => state.rootReducer.user.errors.login;
//export const getUserPendingLogin = state => state.rootReducer.user.pendingLogin;
//export const getUserPendingRegister = state => state.rootReducer.user.pendingRegister;
//export const getUserPendingForgotPassword = state => state.rootReducer.user.pendingForgotPassword;
//export const getUserErrorsRegister = state => state.rootReducer.user.errors.register;
//export const getUserErrorsForgotPassword = state => state.rootReducer.user.errors.forgotPassword;
