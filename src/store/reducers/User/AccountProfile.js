import {
    FETCH_USER_ACCOUNT_PROFILE_SUCCESS,
    FETCH_USER_ACCOUNT_PROFILE_PENDING,
    FETCH_USER_ACCOUNT_IMAGE_SUCCESS,
    FETCH_USER_ACCOUNT_IMAGE_PENDING,
    FETCH_USER_ACCOUNT_IMAGE_DELETE_PENDING,
    FETCH_USER_ACCOUNT_IMAGE_DELETE_SUCCESS, FETCH_USER_ACCOUNT_PROFILE_ERRORS,
} from '../../constants/User/AccountProfile';

const defaultState = {
    pending: false,
    pendingImage: false,
    pendingImageDelete: false,

    errors: null,
};

export function userAccountProfileReducer(state = defaultState, action) {
    switch (action.type) {
        case FETCH_USER_ACCOUNT_PROFILE_SUCCESS:
            return {
                ...state,
                pending: false,
                errors: null,
            };
        case FETCH_USER_ACCOUNT_PROFILE_PENDING:
            return {
                ...state,
                pending: true,
                errors: null,
            };
        case FETCH_USER_ACCOUNT_PROFILE_ERRORS:
            return {
                ...state,
                pending: false,
                errors: action.errors,
            };
        case FETCH_USER_ACCOUNT_IMAGE_PENDING:
            return {
                ...state,
                pendingImage: true,
            };
        case FETCH_USER_ACCOUNT_IMAGE_SUCCESS:
            return {
                ...state,
                pendingImage: false,
            };
        case FETCH_USER_ACCOUNT_IMAGE_DELETE_PENDING:
            return {
                ...state,
                pendingImageDelete: true,
            };
        case FETCH_USER_ACCOUNT_IMAGE_DELETE_SUCCESS:
            return {
                ...state,
                pendingImageDelete: false,
            };
        default:
            return state;
    }
}

export const getUserAccountProfilePending = state => state.rootReducer.userAccountProfile.pending;

export const getUserAccountProfileErrors = state => state.rootReducer.userAccountProfile.errors;

export const getUserAccountProfilePendingImage = state => state.rootReducer.userAccountProfile.pendingImage;
export const getUserAccountProfilePendingImageDelete = state => state.rootReducer.userAccountProfile.pendingImageDelete;
