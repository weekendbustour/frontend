import {request} from "../../../lib/request";

const FETCH_USER_COMMENT_VOTES_PENDING = 'FETCH_USER_COMMENT_VOTES_PENDING';
const FETCH_USER_COMMENT_VOTES_ERROR = 'FETCH_USER_COMMENT_VOTES_ERROR';
const FETCH_USER_COMMENT_VOTES_SUCCESS = 'FETCH_USER_COMMENT_VOTES_SUCCESS';

const defaultState = {
    pending: true,
    votes: {},
    error: null,
};

function fetchUserCommentVotesPending() {
    return {
        type: FETCH_USER_COMMENT_VOTES_PENDING,
    };
}

export function fetchUserCommentVotesSuccess(data) {
    return {
        type: FETCH_USER_COMMENT_VOTES_SUCCESS,
        votes: data,
    };
}

//
// function fetchUserCommentsError(error) {
//     return {
//         type: FETCH_USER_COMMENTS_ERROR,
//         error: error,
//     };
// }

export function fetchUserCommentVotes(user) {
    return dispatch => {
        dispatch(fetchUserCommentVotesPending());
        request.get(`/user/votes`, {
            extended: 1,
        }).then(response => {
            const data = response.data.data;
            dispatch(fetchUserCommentVotesSuccess(data));
        });
    };
}

export function fetchUserCommentVotesId() {
    return dispatch => {
        request.get(`/user/votes`).then(response => {
            const data = response.data.data;
            if (data) {
                dispatch(fetchUserCommentVotesSuccess(data));
            }
        });
    };
}

export function userCommentVotesReducer(state = defaultState, action) {
    switch (action.type) {
        case FETCH_USER_COMMENT_VOTES_PENDING:
            return {
                ...state,
                pending: true,
            };
        case FETCH_USER_COMMENT_VOTES_SUCCESS:
            return {
                ...state,
                pending: false,
                votes: action.votes,
            };
        case FETCH_USER_COMMENT_VOTES_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error,
            };
        default:
            return state;
    }
}
export const getUserCommentVotes = state => state.rootReducer.userCommentVotes.votes;
export const getUserCommentVotesPending = state => state.rootReducer.userCommentVotes.pending;
export const getUserCommentVotesUp = state => state.rootReducer.userCommentVotes.votes.up;
export const getUserCommentVotesDown = state => state.rootReducer.userCommentVotes.votes.down;
