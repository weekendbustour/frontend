import {FETCH_USER_SUBSCRIPTIONS_ID_SUCCESS, FETCH_USER_SUBSCRIPTIONS_PENDING_ARRAY} from '../../constants/User/Subscription';

const defaultState = {
    subscriptions: null,
    subscriptionsPendingArray: [],
};

export function userSubscriptionReducer(state = defaultState, action) {
    switch (action.type) {
        case FETCH_USER_SUBSCRIPTIONS_PENDING_ARRAY:
            let subscriptionsPendingArray_PENDING = state.subscriptionsPendingArray;
            subscriptionsPendingArray_PENDING.push(action.pending);
            return {
                ...state,
                subscriptionsPendingArray: subscriptionsPendingArray_PENDING,
            };
        case FETCH_USER_SUBSCRIPTIONS_ID_SUCCESS:
            let subscriptionsPendingArray_SUCCESS = state.subscriptionsPendingArray;
            let index = subscriptionsPendingArray_SUCCESS.indexOf(action.pending);
            if (index !== -1) {
                subscriptionsPendingArray_SUCCESS.splice(index, 1);
            }
            return {
                ...state,
                subscriptions: action.subscriptions,
                subscriptionsPendingArray: subscriptionsPendingArray_SUCCESS,
            };
        default:
            return state;
    }
}

export const getUserSubscriptions = state => state.rootReducer.userSubscription.subscriptions;
export const getUserSubscriptionsPendingArray = state => state.rootReducer.userSubscription.subscriptionsPendingArray;

//
// export function fetchUserSubscription(data, state) {
//     return dispatch => {
//         dispatch(fetchUserSubscriptionPending());
//         axios.post(API_URL + `/user/subscription`, axiosBody({
//             value: state ? 1 : 0,
//             email: data.email ?? '',
//         }))
//         .then(response => {
//             progressBar.stop();
//             toastBySuccess(response);
//             const data = response.data;
//             dispatch(fetchUserSubscriptionSuccess(data));
//             dispatch(fetchUser());
//         })
//         .catch(error => {
//             progressBar.stop();
//             toastByError(error);
//             const data = error.response.data;
//             dispatch(fetchUserSubscriptionError(data));
//         });
//     };
// }
