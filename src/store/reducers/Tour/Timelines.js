import {FETCH_TOUR_TIMELINES_PENDING, FETCH_TOUR_TIMELINES_ERROR, FETCH_TOUR_TIMELINES_SUCCESS} from '../../constants/Tour/Timelines';
const defaultState = {
    pending: false,
    timelines: {},
    error: null,
};

export function tourTimelinesReducer(state = defaultState, action) {
    switch (action.type) {
        case FETCH_TOUR_TIMELINES_PENDING:
            return {
                ...state,
                pending: true,
            };
        case FETCH_TOUR_TIMELINES_SUCCESS:
            return {
                ...state,
                pending: false,
                timelines: action.data,
            };
        case FETCH_TOUR_TIMELINES_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error,
            };
        default:
            return state;
    }
}

export const getTourTimelines = state => state.rootReducer.tourTimelines.timelines;
export const getTourTimelinesPending = state => state.rootReducer.tourTimelines.pending;
export const getTourTimelinesError = state => state.rootReducer.tourTimelines.error;
