import {progressBar} from '../../progress';
import {API_URL} from '../../../api/api';
import seo from '../../../lib/seo';
import {toastByError, toastBySuccess} from "../../../lib/toast";
import axios from 'axios';
import {fetchTourTimelines, fetchTourTimelinesSuccess} from "../../actions/Tour/Timelines";

const FETCH_TOUR_PENDING = 'FETCH_TOUR_PENDING';
const FETCH_TOUR_SUCCESS = 'FETCH_TOUR_SUCCESS';
const FETCH_TOUR_ERROR = 'FETCH_TOUR_ERROR';

const defaultState = {
    pending: false,
    tour: {},
    error: null,
};

function fetchTourPending() {
    return {
        type: FETCH_TOUR_PENDING,
    };
}

function fetchTourSuccess(tour) {
    seo.tour(tour);
    return {
        type: FETCH_TOUR_SUCCESS,
        tour: tour,
    };
}
//
// function fetchTourError(error) {
//     return {
//         type: FETCH_TOUR_ERROR,
//         error: error,
//     };
// }

export function fetchTour(id) {
    return dispatch => {
        progressBar.start();
        dispatch(fetchTourPending());
        axios.get(API_URL + `/tour/${id}`)
            .then(response => {
                const data = response.data.data;
                progressBar.stop();
                toastBySuccess(response);
                dispatch(fetchTourSuccess(data));
                if (data.timelines === undefined) {
                    dispatch(fetchTourTimelines({
                        id: data.id,
                    }));
                } else {
                    dispatch(fetchTourTimelinesSuccess(data.timelines));
                }
            }).catch(error => {
                toastByError(error);
            });
    };
}

export function tourReducer(state = defaultState, action) {
    switch (action.type) {
        case FETCH_TOUR_PENDING:
            return {
                ...state,
                pending: true,
            };
        case FETCH_TOUR_SUCCESS:
            return {
                ...state,
                pending: false,
                tour: action.tour,
            };
        case FETCH_TOUR_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error,
            };
        default:
            return state;
    }
}

export const getTour = state => state.rootReducer.tour.tour;
export const getTourPending = state => state.rootReducer.tour.pending;
export const getTourError = state => state.rootReducer.tour.error;

//export default {fetchOrganizations, OrganizationsReducer}
