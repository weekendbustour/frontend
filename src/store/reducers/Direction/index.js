import {
    FETCH_DIRECTION_PENDING,
    FETCH_DIRECTION_ERROR,
    FETCH_DIRECTION_SUCCESS,
    FETCH_DIRECTION_TOURS_SUCCESS,
    FETCH_DIRECTION_TOURS_PENDING, FETCH_DIRECTION_TOURS_DATE,
} from '../../constants/Direction/index';

const defaultState = {
    pending: false,
    direction: {},
    error: null,

    tours: [],
    toursPending: false,
    toursError: null,
};

export function directionReducer(state = defaultState, action) {
    switch (action.type) {
        case FETCH_DIRECTION_PENDING:
            return {
                ...state,
                pending: true,
            };
        case FETCH_DIRECTION_SUCCESS:
            return {
                ...state,
                pending: false,
                direction: action.direction,
            };
        case FETCH_DIRECTION_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error,
            };
        case FETCH_DIRECTION_TOURS_PENDING:
            return {
                ...state,
                toursPending: true,
            };
        case FETCH_DIRECTION_TOURS_SUCCESS:
            return {
                ...state,
                toursPending: false,
                tours: action.tours,
            };
        case FETCH_DIRECTION_TOURS_DATE:
            return {
                ...state,
                toursDate: action.date,
            };
        default:
            return state;
    }
}

export const getDirection = state => state.rootReducer.direction.direction;
export const getDirectionTours = state => state.rootReducer.direction.tours;
export const getDirectionToursPending = state => state.rootReducer.direction.toursPending;
export const getDirectionPending = state => state.rootReducer.direction.pending;
export const getDirectionError = state => state.rootReducer.direction.error;
export const getDirectionToursDate = state => state.rootReducer.direction.toursDate;
