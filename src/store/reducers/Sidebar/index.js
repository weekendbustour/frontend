import {FETCH_SIDEBAR} from "../../constants/Sidebar";

const defaultState = {
    sidebar: null,
    lockScroll: false,
};

export function sidebarReducer(state = defaultState, action) {
    switch (action.type) {
        case FETCH_SIDEBAR:
            const lockScroll = action.sidebar !== null;
            return {
                ...state,
                sidebar: action.sidebar,
                lockScroll: lockScroll,
            };
        default:
            return state;
    }
}

export const getSidebar = state => state.rootReducer.sidebar.sidebar;
export const getSidebarLockScroll = state => state.rootReducer.sidebar.lockScroll;
