import {FETCH_NEWS_PAGE_PENDING, FETCH_NEWS_PAGE_SUCCESS, FETCH_NEWS_PAGE_ERROR} from '../../constants/News';

const defaultState = {
    pending: false,
    news: {},
    error: null,
};

export function newsPageReducer(state = defaultState, action) {
    switch (action.type) {
        case FETCH_NEWS_PAGE_PENDING:
            return {
                ...state,
                pending: true,
            };
        case FETCH_NEWS_PAGE_SUCCESS:
            return {
                ...state,
                pending: false,
                news: action.news,
            };
        case FETCH_NEWS_PAGE_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error,
            };
        default:
            return state;
    }
}

export const getNewsPage = state => state.rootReducer.newsPage.news;
export const getNewsPagePending = state => state.rootReducer.newsPage.pending;
export const getNewsPageError = state => state.rootReducer.newsPage.error;
