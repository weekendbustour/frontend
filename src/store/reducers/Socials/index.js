import {API_URL} from '../../../api/api';

const FETCH_SOCIALS_PENDING = 'FETCH_SOCIALS_PENDING';
const FETCH_SOCIALS_SUCCESS = 'FETCH_SOCIALS_SUCCESS';
const FETCH_SOCIALS_ERROR = 'FETCH_SOCIALS_ERROR';

const defaultState = {
    pending: false,
    socials: [],
    error: null,
};

function fetchSocialsPending() {
    return {
        type: FETCH_SOCIALS_PENDING,
    };
}

function fetchSocialsSuccess(socials) {
    return {
        type: FETCH_SOCIALS_SUCCESS,
        socials: socials,
    };
}

function fetchSocialsError(error) {
    return {
        type: FETCH_SOCIALS_ERROR,
        error: error,
    };
}

export function fetchSocials() {
    return dispatch => {
        dispatch(fetchSocialsPending());
        fetch(API_URL + '/data')
            .then(res => res.json())
            .then(res => {
                if (res.error) {
                    throw res.error;
                }
                dispatch(fetchSocialsSuccess(res.data.socials));
                //return res.data.socials;
            })
            .catch(error => {
                dispatch(fetchSocialsError(error));
            });
    };
}

export function socialsReducer(state = defaultState, action) {
    switch (action.type) {
        case FETCH_SOCIALS_PENDING:
            return {
                ...state,
                pending: true,
            };
        case FETCH_SOCIALS_SUCCESS:
            return {
                ...state,
                pending: false,
                socials: action.payload,
            };
        case FETCH_SOCIALS_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error,
            };
        default:
            return state;
    }
}

export const getSocials = state => state.socials;
export const getSocialsPending = state => state.pending;
export const getSocialsError = state => state.error;

//export default {fetchSOCIALS, SOCIALSReducer}
