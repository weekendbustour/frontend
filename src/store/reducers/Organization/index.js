import {progressBar} from '../../progress';
import {API_URL, axiosBody} from '../../../api/api';
import axios from 'axios';
import {toastByError, toastBySuccess} from '../../../lib/toast';
import seo from '../../../lib/seo';
import {request} from "../../../lib/request";

const FETCH_ORGANIZATION_PENDING = 'FETCH_ORGANIZATION_PENDING';
const FETCH_ORGANIZATION_SUCCESS = 'FETCH_ORGANIZATION_SUCCESS';
const FETCH_ORGANIZATION_TOURS_SUCCESS = 'FETCH_ORGANIZATION_TOURS_SUCCESS';
const FETCH_ORGANIZATION_TOURS_PENDING = 'FETCH_ORGANIZATION_TOURS_PENDING';
const FETCH_ORGANIZATION_ERROR = 'FETCH_ORGANIZATION_ERROR';
const FETCH_ORGANIZATION_TOURS_DATE = 'FETCH_ORGANIZATION_TOURS_DATE';

const defaultState = {
    pending: false,
    organization: {},
    error: null,

    toursDate: null,
    tours: [],
    toursPending: false,
    toursError: null,
};

function fetchOrganizationPending() {
    return {
        type: FETCH_ORGANIZATION_PENDING,
    };
}

function fetchOrganizationToursPending() {
    return {
        type: FETCH_ORGANIZATION_TOURS_PENDING,
    };
}

function fetchOrganizationSuccess(organization) {
    seo.organization(organization);
    return {
        type: FETCH_ORGANIZATION_SUCCESS,
        organization: organization,
    };
}

function fetchOrganizationToursSuccess(data) {
    return {
        type: FETCH_ORGANIZATION_TOURS_SUCCESS,
        tours: data,
    };
}

// eslint-disable-next-line
function fetchOrganizationError(error) {
    return {
        type: FETCH_ORGANIZATION_ERROR,
        error: error,
    };
}

function fetchOrganizationToursDate(date) {
    return {
        type: FETCH_ORGANIZATION_TOURS_DATE,
        date: date,
    };
}

export function fetchOrganization(id) {
    return dispatch => {
        progressBar.start();
        dispatch(fetchOrganizationPending());
        request.get(`/organization/${id}`)
            .then(response => {
                const data = response.data.data;
                progressBar.stop();
                toastBySuccess(response);
                dispatch(fetchOrganizationSuccess(data));
            });
        //
        // fetch(API_URL + '/organization/' + id)
        //     .then(res => res.json())
        //     .then(res => {
        //         progressBar.stop();
        //         if (res.error) {
        //             throw res.error;
        //         }
        //         dispatch(fetchOrganizationSuccess(res.data));
        //         return res.data;
        //     })
        //     .catch(error => {
        //         dispatch(fetchOrganizationError(error));
        //     });
    };
}
export function fetchOrganizationTours(id, date) {
    return dispatch => {
        progressBar.start();
        dispatch(fetchOrganizationToursPending());
        dispatch(fetchOrganizationToursDate(date));
        request.get(`/organization/${id}/tours`, {
            date: date,
        }).then(response => {
            const data = response.data.data;
            progressBar.stop();
            toastBySuccess(response);
            dispatch(fetchOrganizationToursSuccess(data));
        });
    };
}

export function fetchOrganizationSubscribe(id, state) {
    return dispatch => {
        progressBar.start();
        axios
            .post(
                API_URL + `/organization/${id}/subscribe`,
                axiosBody({
                    value: state ? 1 : 0,
                    id: id,
                    model: 'organization',
                })
            )
            .then(response => {
                progressBar.stop();
                toastBySuccess(response);
            })
            .catch(error => {
                progressBar.stop();
                toastByError(error);
            });
    };
}

export function organizationReducer(state = defaultState, action) {
    switch (action.type) {
        case FETCH_ORGANIZATION_PENDING:
            return {
                ...state,
                pending: true,
            };
        case FETCH_ORGANIZATION_TOURS_PENDING:
            return {
                ...state,
                toursPending: true,
            };
        case FETCH_ORGANIZATION_SUCCESS:
            return {
                ...state,
                pending: false,
                organization: action.organization,
            };
        case FETCH_ORGANIZATION_TOURS_SUCCESS:
            return {
                ...state,
                toursPending: false,
                tours: action.tours,
            };
        case FETCH_ORGANIZATION_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error,
            };
        case FETCH_ORGANIZATION_TOURS_DATE:
            return {
                ...state,
                toursDate: action.date,
            };
        default:
            return state;
    }
}

export const getOrganization = state => state.rootReducer.organization.organization;
export const getOrganizationTours = state => state.rootReducer.organization.tours;
export const getOrganizationToursPending = state => state.rootReducer.organization.toursPending;
export const getOrganizationPending = state => state.rootReducer.organization.pending;
export const getOrganizationError = state => state.rootReducer.organization.error;
export const getOrganizationToursDate = state => state.rootReducer.organization.toursDate;

//export default {fetchOrganizations, OrganizationsReducer}
