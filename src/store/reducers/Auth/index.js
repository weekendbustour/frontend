import {
    FETCH_AUTH_GUEST,
    FETCH_AUTH_LOGIN_ERRORS,
    FETCH_AUTH_LOGIN_PENDING,
    FETCH_AUTH_LOGIN_SUCCESS,
    FETCH_AUTH_REGISTER_ERRORS,
    FETCH_AUTH_REGISTER_PENDING,
    FETCH_AUTH_REGISTER_SUCCESS, FETCH_AUTH_RESET_PASSWORD_ERRORS,
    FETCH_AUTH_RESET_PASSWORD_PENDING,
    FETCH_AUTH_RESET_PASSWORD_SUCCESS,
    FETCH_AUTH_USER,
    FETCH_AUTH_LOGIN_SOCIAL_VK_ERRORS,
    FETCH_AUTH_LOGIN_SOCIAL_VK_PENDING,
    FETCH_AUTH_LOGIN_SOCIAL_VK_SUCCESS,
} from '../../constants/Auth';

const defaultState = {
    user: {},
    guest: true,

    pendingLogin: false,
    pendingRegister: false,
    pendingForgotPassword: false,
    pendingLoginSocialVk: false,
    errors: {
        login: null,
        register: null,
        forgotPassword: null,
        loginSocialVk: null,
    },
};

export function authReducer(state = defaultState, action) {
    switch (action.type) {
        case FETCH_AUTH_GUEST:
            return {
                ...state,
                guest: action.guest,
            };
        case FETCH_AUTH_USER:
            return {
                ...state,
                user: action.user,
            };
        // LOGIN
        case FETCH_AUTH_LOGIN_PENDING:
            return {
                ...state,
                pendingLogin: true,
            };
        case FETCH_AUTH_LOGIN_SUCCESS:
            return {
                ...state,
                pendingLogin: false,
            };
        case FETCH_AUTH_LOGIN_ERRORS:
            return {
                ...state,
                pendingLogin: false,
                errors: Object.assign(state.errors, {
                    login: action.errors,
                }),
            };
        // REGISTER
        case FETCH_AUTH_REGISTER_PENDING:
            return {
                ...state,
                pendingRegister: true,
            };
        case FETCH_AUTH_REGISTER_SUCCESS:
            return {
                ...state,
                pendingRegister: false,
            };
        case FETCH_AUTH_REGISTER_ERRORS:
            return {
                ...state,
                pendingRegister: false,
                errors: Object.assign(state.errors, {
                    register: action.errors,
                }),
            };
        // RESET PASSWORD
        case FETCH_AUTH_RESET_PASSWORD_PENDING:
            return {
                ...state,
                pendingForgotPassword: true,
            };
        case FETCH_AUTH_RESET_PASSWORD_SUCCESS:
            return {
                ...state,
                pendingForgotPassword: false,
            };
        case FETCH_AUTH_RESET_PASSWORD_ERRORS:
            return {
                ...state,
                pendingForgotPassword: false,
                errors: Object.assign(state.errors, {
                    forgotPassword: action.errors,
                }),
            };
        // LOGIN SOCIAL VK
        case FETCH_AUTH_LOGIN_SOCIAL_VK_PENDING:
            return {
                ...state,
                pendingLoginSocialVk: true,
            };
        case FETCH_AUTH_LOGIN_SOCIAL_VK_SUCCESS:
            return {
                ...state,
                pendingLoginSocialVk: false,
            };
        case FETCH_AUTH_LOGIN_SOCIAL_VK_ERRORS:
            return {
                ...state,
                pendingLoginSocialVk: false,
                errors: Object.assign(state.errors, {
                    loginSocialVk: action.errors,
                }),
            };
        default:
            return state;
    }
}
export const getAuth = state => state.rootReducer.auth;
export const getAuthUser = state => state.rootReducer.auth.user;
export const getAuthGuest = state => state.rootReducer.auth.guest;

export const getAuthLoginPending = state => state.rootReducer.auth.pendingLogin;
export const getAuthRegisterPending = state => state.rootReducer.auth.pendingRegister;
export const getAuthForgotPasswordPending = state => state.rootReducer.auth.pendingForgotPassword;
export const getAuthLoginSocialVkPending = state => state.rootReducer.auth.pendingLoginSocialVk;

export const getAuthLoginErrors = state => state.rootReducer.auth.errors.login;
export const getAuthRegisterErrors = state => state.rootReducer.auth.errors.register;
export const getAuthForgotPasswordErrors = state => state.rootReducer.auth.errors.forgotPassword;
export const getAuthLoginSocialVkErrors = state => state.rootReducer.auth.errors.loginSocialVk;

