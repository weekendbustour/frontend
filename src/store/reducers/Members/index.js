const FETCH_MEMBERS_PENDING = 'FETCH_MEMBERS_PENDING';
const FETCH_MEMBERS_SUCCESS = 'FETCH_MEMBERS_SUCCESS';
const FETCH_MEMBERS_ERROR = 'FETCH_MEMBERS_ERROR';

const defaultState = {
    pending: false,
    members: [],
    error: null,
};

function fetchMembersPending() {
    return {
        type: FETCH_MEMBERS_PENDING,
    };
}

function fetchMembersSuccess(members) {
    return {
        type: FETCH_MEMBERS_SUCCESS,
        members: members,
    };
}

function fetchMembersError(error) {
    return {
        type: FETCH_MEMBERS_ERROR,
        error: error,
    };
}

export function fetchMembers() {
    return dispatch => {
        dispatch(fetchMembersPending());
        fetch('https://isamp.ru/middayband/api/data')
            .then(res => res.json())
            .then(res => {
                if (res.error) {
                    throw res.error;
                }
                dispatch(fetchMembersSuccess(res.data.members));
                return res.data.members;
            })
            .catch(error => {
                dispatch(fetchMembersError(error));
            });
    };
}

export function membersReducer(state = defaultState, action) {
    switch (action.type) {
        case FETCH_MEMBERS_PENDING:
            return {
                ...state,
                pending: true,
            };
        case FETCH_MEMBERS_SUCCESS:
            return {
                ...state,
                pending: false,
                members: action.payload,
            };
        case FETCH_MEMBERS_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error,
            };
        default:
            return state;
    }
}

export const getMembers = state => state.members;
export const getMembersPending = state => state.pending;
export const getMembersError = state => state.error;

//export default {fetchMembers, membersReducer}
