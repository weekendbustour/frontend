import {
    FETCH_CATEGORY_PENDING,
    FETCH_CATEGORY_SUCCESS,
    FETCH_CATEGORY_ERROR
} from '../../constants/Category';

const defaultState = {
    pending: false,
    category: {},
    error: null,
};

export function categoryReducer(state = defaultState, action) {
    switch (action.type) {
        case FETCH_CATEGORY_PENDING:
            return {
                ...state,
                pending: true,
            };
        case FETCH_CATEGORY_SUCCESS:
            return {
                ...state,
                pending: false,
                category: action.category,
            };
        case FETCH_CATEGORY_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error,
            };
        default:
            return state;
    }
}

export const getCategory = state => state.rootReducer.category.category;
export const getCategoryPending = state => state.rootReducer.category.pending;
export const getCategoryError = state => state.rootReducer.category.error;
