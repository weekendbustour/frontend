import {FETCH_APP_VIEW_TOURS_SUCCESS} from '../../constants/App/View';
import {VIEW_GRID} from "../../../lib/view";

const defaultState = {
    viewTours: VIEW_GRID,
};
export function appViewReducer(state = defaultState, action) {
    switch (action.type) {
        case FETCH_APP_VIEW_TOURS_SUCCESS:
            return {
                ...state,
                viewTours: action.data,
            };
        default:
            return state;
    }
}
export const getAppViewTours = state => state.rootReducer.appView.viewTours;
