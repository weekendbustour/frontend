import {FETCH_APP_SEO_SUCCESS} from '../../constants/App/Seo';
import {APP_NAME_FULL} from '../../../lib/seo';

const defaultState = {
    data: {
        title: APP_NAME_FULL,
        description: APP_NAME_FULL,
    },
};
export function appSeoReducer(state = defaultState, action) {
    switch (action.type) {
        case FETCH_APP_SEO_SUCCESS:
            return {
                ...state,
                data: action.data,
            };
        default:
            return state;
    }
}
export const getAppSeo = state => state.rootReducer.appSeo.data;
