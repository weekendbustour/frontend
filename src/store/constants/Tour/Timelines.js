export const FETCH_TOUR_TIMELINES_PENDING = 'FETCH_TOUR_TIMELINES_PENDING';
export const FETCH_TOUR_TIMELINES_ERROR = 'FETCH_TOUR_TIMELINES_ERROR';
export const FETCH_TOUR_TIMELINES_SUCCESS = 'FETCH_TOUR_TIMELINES_SUCCESS';
