export const FETCH_ARTICLES_PENDING = 'FETCH_ARTICLES_PENDING';
export const FETCH_ARTICLES_ERROR = 'FETCH_ARTICLES_ERROR';
export const FETCH_ARTICLES_SUCCESS = 'FETCH_ARTICLES_SUCCESS';
export const FETCH_ARTICLES_PAGINATION = 'FETCH_ARTICLES_PAGINATION';

