import React, {useState, useCallback} from 'react';
import * as PropTypes from 'prop-types';
import Input from 'react-phone-number-input/input';

/**
 *
 * @param name
 * @param defaultValue
 * @param validation
 * @param onChange
 * @returns {*}
 * @constructor
 */
const InputPhone = ({name, defaultValue, validation, onChange}) => {
    const [value, setValue] = useState(defaultValue);

    // eslint-disable-next-line react-hooks/exhaustive-deps
    const onChangeCount = useCallback((name, value) => {
        setValue(value);
        onChange(name, value);
    });
    return (
        <React.Fragment>
            <Input defaultCountry="RU" placeholder="Телефон"
                   name={name}
                   value={value}
                   onChange={(value) => onChangeCount(name, value)}
                   className={`form-control ${validation !== null && validation.phone !== undefined ? `is-invalid` : ``}`}
                   required
            />
        </React.Fragment>
    );
};

InputPhone.propTypes = {
    name: PropTypes.string,
    defaultValue: PropTypes.string,
    validation: PropTypes.object,
    onChange: PropTypes.func,
};

export default InputPhone;
