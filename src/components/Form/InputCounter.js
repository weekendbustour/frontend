import Button from 'react-bootstrap/Button';
import React, {useState, useCallback} from 'react';
import * as PropTypes from "prop-types";

/**
 *
 * @param name
 * @param value
 * @param onChange
 * @returns {*}
 * @constructor
 */
const InputCounter = ({name, value, onChange}) => {
    const [count, setCount] = useState(value);

    // eslint-disable-next-line react-hooks/exhaustive-deps
    const onChangeCount = useCallback((name, value) => {
        if (value < 1) {
            return;
        }
        setCount(value);
        onChange(name, value);
    });

    return (
        <React.Fragment>
            <div className="aw-input-count-up input-group">
                <Button variant="primary" size={`sm`} onClick={() => onChangeCount(name, count - 1)}>
                    <i className="fas fa-minus"/>
                </Button>
                <input type="number" step="1" max="" value={count} name={name} className="form-control" onChange={onChange} />
                <Button variant="primary" size={`sm`} onClick={() => onChangeCount(name, count + 1)}>
                    <i className="fas fa-plus"/>
                </Button>
            </div>
        </React.Fragment>
    );
};

InputCounter.propTypes = {
    name: PropTypes.string,
    value: PropTypes.number,
    onChange: PropTypes.func,
};

export default InputCounter;
