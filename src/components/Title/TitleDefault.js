import React from 'react';
import * as PropTypes from 'prop-types';
import Skeleton from 'react-loading-skeleton';

const TitleDefault = ({role = 'h1', title, description = null, pendingDescription = false}) => {
    return (
        <React.Fragment>
            <div className={`wrapper is-role-${role}`}>
                {role === 'h1' && <h1 className="beta uppercase montserrat regular line-after-heading">{title}</h1>}
                {role === 'h2' && <h2 className="beta uppercase montserrat regular line-after-heading">{title}</h2>}
                {role === 'h3' && <h3>{title}</h3>}
                {pendingDescription ? (
                    <Skeleton height={`17px`} width={`200px`} />
                ) : (
                    <div>{description !== null && <p className="delta cardo regular italic">{description}</p>}</div>
                )}
            </div>
        </React.Fragment>
    );
};

TitleDefault.propTypes = {
    role: PropTypes.string,
    title: PropTypes.string,
    description: PropTypes.string,
    pendingDescription: PropTypes.bool,
};

export default TitleDefault;
