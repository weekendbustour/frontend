import React, {Component} from 'react';
import ReactRating from 'react-rating';
import * as PropTypes from "prop-types";

class RatingDefault extends Component {
    static propTypes = {
        rating: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
        readonly: PropTypes.bool,
    };

    constructor(props) {
        super(props);
        this.state = {
            rating: props.rating ?? 0,
            readonly: props.readonly ?? true,
        };
        this.rateChange = this.rateChange.bind(this);
    }

    rateChange(rate) {
        this.setState({
            rating: rate,
            readonly: true,
        });
    }

    render() {
        const {rating} = this.state;
        const {readonly} = this.state;
        return (
            <React.Fragment>
                <ReactRating
                    emptySymbol="far fa-star"
                    fullSymbol="fas fa-star"
                    initialRating={rating}
                    onChange={rate => this.rateChange(rate)}
                    readonly={readonly}
                    className={`rating-default ${readonly ? '__readonly' : ''}`}
                />
            </React.Fragment>
        );
    }
}

export default RatingDefault;
