import React from 'react';
import * as PropTypes from 'prop-types';
import {Link} from "react-router-dom";
import ReactMarkdown from "../../plugins/ReactMarkdown";

const SourceDefault = ({source = null}) => {
    return (
        <React.Fragment>
            {source.organization !== null &&
            <div>
                Организатор:&#160;
                <Link to={`/organization/${source.organization.id}/${source.organization.name}`}>
                    {source.organization.title}
                </Link>
            </div>
            }
            {source.url !== null &&
            <div>
                <a href={source.url} target={`_blank`}>{source.url}</a>
            </div>
            }
            {source.published_at_format !== null &&
            <div>
                Дата публикации:&#160;
                {source.published_at_format}
            </div>
            }
            {source.description !== null &&
            <div>
                <ReactMarkdown source={source.description} />
            </div>
            }
        </React.Fragment>
    );
};

SourceDefault.propTypes = {
    source: PropTypes.object,
};

export default SourceDefault;
