import React from 'react';
import Modal from 'react-bootstrap/Modal';
import store from "../../store";
import {fetchModal} from "../../store/reducers/Modal";
import * as PropTypes from "prop-types";

const ModalDefault = ({name, children, size = `lg`}) => {

    function setShow(name) {
        store.dispatch(fetchModal(name));
    }
    return (
        <Modal
            size={size}
            show={true}
            onHide={() => setShow(null)}
            aria-labelledby="contained-modal-title-vcenter"
            centered
            className={`modal-${name}`}
        >
            <Modal.Body>
                <button type="button" className="modal-button-close close" onClick={() => setShow(null)}>
                    <span aria-hidden="true">×</span>
                    <span className="sr-only">Close</span>
                </button>
                <div>
                    {children}
                </div>
            </Modal.Body>
        </Modal>
    );
};

ModalDefault.propTypes = {
    name: PropTypes.string,
    size: PropTypes.string,
};

export default ModalDefault;
