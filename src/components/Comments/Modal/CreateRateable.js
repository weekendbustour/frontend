import React, {Component} from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import {fetchModal, getModal} from '../../../store/reducers/Modal';
import {connect} from 'react-redux';
import {getUserCommentPending, fetchUserComment, getUserCommentClear} from '../../../store/reducers/User/Comment';
import Rating from 'react-rating';
import {toastError} from '../../../lib/toast';
import {MODAL_COMMENT_RATE_CREATE} from '../../../lib/modal';
import * as PropTypes from 'prop-types';
import ModalDefault from '../../Modal/ModalDefault';

class CreateRateable extends Component {
    _NAME = MODAL_COMMENT_RATE_CREATE;
    _isMounted = false;
    state = {};

    static propTypes = {
        item: PropTypes.object,
        modal: PropTypes.string,
        model: PropTypes.string,
        pending: PropTypes.bool,
        fetchModal: PropTypes.func,
        fetchUserComment: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.state = {
            item: props.item,
            model: props.model,

            text: '',
            good: '',
            bad: '',
            anon: 0,
            rating: 0,
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.setShow = this.setShow.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    setShow(value) {
        const {fetchModal} = this.props;
        fetchModal(value);
    }

    rateChange(rate) {
        this.setState({
            rating: rate,
        });
    }

    handleChange(e) {
        if (e.currentTarget.name === 'anon') {
            this.setState({
                [e.currentTarget.name]: parseInt(e.currentTarget.value),
            });
        } else {
            this.setState({
                [e.currentTarget.name]: e.currentTarget.value,
            });
        }
    }

    handleSubmit(e) {
        e.preventDefault();
        const {fetchUserComment} = this.props;
        const {good, bad, text, anon, model, item, rating} = this.state;
        const data = {
            id: item.id,
            good: good,
            bad: bad,
            text: text,
            anon: anon,
            model: model,
            rating: rating,
        };
        if (rating === 0) {
            toastError('Необходимо поставить вашу оценку');
            return;
        }
        fetchUserComment(data);
    }

    render() {
        const {text, anon, rating, model} = this.state;
        const {modal, pending} = this.props;
        if (!this.shouldComponentRender() || modal !== this._NAME) return null;
        return (
            <React.Fragment>
                <ModalDefault name={this._NAME}>
                    <Form onSubmit={this.handleSubmit} className={`mt-3`}>
                        <h3>Написать отзыв</h3>
                        <hr />
                        <div className={``}>
                            <div className={`mb-3`}>
                                <p className={`mb-0`}>
                                    Оцените <i className="r">*</i>
                                </p>
                                <p className={`text-muted mb-0`}>
                                    И ниже можете описать достоинства и недостатки {model === 'organization' && 'Организатора'}{model === 'direction' && 'Направления'}
                                </p>
                                <Rating
                                    emptySymbol="far fa-star"
                                    fullSymbol="fas fa-star"
                                    initialRating={rating}
                                    onChange={rate => this.rateChange(rate)}
                                    readonly={false}
                                />
                            </div>
                            {/*<p className={`text-muted`}>Вы можете заполнить все 3 поля или любое из них на выбор.</p>*/}
                            {/*<Form.Group>*/}
                            {/*    <Form.Label>Достоинства</Form.Label>*/}
                            {/*    <Form.Control as="textarea" rows="2" placeholder="Достоинства" name="good" value={good} onChange={this.handleChange} />*/}
                            {/*</Form.Group>*/}
                            {/*<Form.Group>*/}
                            {/*    <Form.Label>Недостатки</Form.Label>*/}
                            {/*    <Form.Control as="textarea" rows="2" placeholder="Недостатки" name="bad" value={bad} onChange={this.handleChange} />*/}
                            {/*</Form.Group>*/}
                            <Form.Group>
                                <Form.Label>Комментарий </Form.Label>
                                <Form.Control as="textarea" rows="3" placeholder="Комментарий" name="text" value={text} onChange={this.handleChange} />
                            </Form.Group>
                            <Form.Check
                                type="switch"
                                id="custom-switch"
                                label="Оставить отзыв анонимно"
                                className={`mb-3`}
                                name="anon"
                                value={anon === 1 ? 0 : 1}
                                checked={anon === 1}
                                onChange={this.handleChange}
                            />
                        </div>
                        <hr />
                        <div className={`text-right`}>
                            <Button variant="default" type="button" onClick={() => this.setShow(null)} className={`mr-3`}>
                                Закрыть
                            </Button>
                            <Button variant="primary" type="submit" className={pending ? `is-loading` : ``}>
                                Отправить отзыв
                            </Button>
                        </div>
                    </Form>
                </ModalDefault>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    modal: getModal(state),
    pending: getUserCommentPending(state),
    clear: getUserCommentClear(state),
});
const mapDispatchToProps = dispatch => ({
    fetchModal: name => dispatch(fetchModal(name)),
    fetchUserComment: data => dispatch(fetchUserComment(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CreateRateable);
