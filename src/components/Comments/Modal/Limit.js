import React, {Component} from 'react';
import Modal from 'react-bootstrap/Modal';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import {fetchModal, getModal} from '../../../store/reducers/Modal';
import {connect} from 'react-redux';
import {MODAL_COMMENT_LIMIT} from '../../../lib/modal';
import * as PropTypes from "prop-types";

class Limit extends Component {
    _NAME = MODAL_COMMENT_LIMIT;
    _isMounted = false;
    state = {};

    static propTypes = {
        modal: PropTypes.string,
        fetchModal: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.setShow = this.setShow.bind(this);
    }

    componentDidMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    setShow(value) {
        const {fetchModal} = this.props;
        fetchModal(value);
    }

    render() {
        const {modal} = this.props;
        if (!this.shouldComponentRender() || modal !== this._NAME) return null;
        return (
            <React.Fragment>
                <Modal
                    size="lg"
                    show={true}
                    onHide={() => this.setShow(null)}
                    aria-labelledby="contained-modal-title-vcenter"
                    centered
                    className={`modal-${this._NAME}`}
                >
                    <Modal.Body>
                        <button type="button" className="modal-button-close close" onClick={() => this.setShow(null)}>
                            <span aria-hidden="true">×</span>
                            <span className="sr-only">Close</span>
                        </button>
                        <Row>
                            <Col xs={12}>
                                <div className={`mt-5 mb-5 text-center`}>Вы уже оставляли этому Организатору отзыв</div>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    modal: getModal(state),
});
const mapDispatchToProps = dispatch => ({
    fetchModal: name => dispatch(fetchModal(name)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Limit);
