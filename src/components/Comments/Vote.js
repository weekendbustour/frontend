import React, {Component} from 'react';
import Button from 'react-bootstrap/Button';
import {fetchUserComment, getUserCommentPending, getUserCommentVote} from '../../store/reducers/User/Comment';
import {getUser, getUserIsGuest} from '../../store/reducers/User';
import {connect} from 'react-redux';
import {getCommentVotePendingArray} from '../../store/reducers/Comment/Vote';
import {fetchCommentVote} from '../../store/actions/Comment/Vote';
import * as PropTypes from 'prop-types';
import {isMobile} from 'react-device-detect';
import {getUserCommentVotes} from "../../store/reducers/User/CommentVotes";

class Vote extends Component {
    static propTypes = {
        item: PropTypes.object,
        comment: PropTypes.object,
        model: PropTypes.string,
        votes: PropTypes.shape({
            up: PropTypes.array,
            down: PropTypes.array,
        }),
        commentVotePendingArray: PropTypes.array,
        fetchUserCommentVote: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.state = {
            item: props.item,
            comment: props.comment,
            model: props.model,
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.handleVote = this.handleVote.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    handleVote(comment, state) {
        const {fetchUserCommentVote} = this.props;
        const {model, item} = this.state;
        const data = {
            id: comment.id,
            model: model,
            model_id: item.id,
        };
        fetchUserCommentVote(data, state);
    }

    render() {
        const {commentVotePendingArray, votes} = this.props;
        const {comment, item, model} = this.state;
        if (!this.shouldComponentRender()) return null;
        console.log('Votes Render');
        return (
            <React.Fragment>
                <div
                    className={`d-flex align-items-center d-vote ${isMobile ? '__mobile' : ''} 
                    ${commentVotePendingArray.indexOf(`${model}-${item.id}-comment-${comment.id}`) !== -1 ? 'is-loading' : ''}
                    ${votes.down !== undefined && votes.down.indexOf(comment.id) !== -1 ? 'active-down' : ''}
                    ${votes.up !== undefined && votes.up.indexOf(comment.id) !== -1 ? 'active-up' : ''}
                    `}
                >
                    <Button variant="light-dark" size={`sm`} className={`text-danger`} onClick={() => this.handleVote(comment, false)}>
                        <i className="fas fa-thumbs-down" />
                    </Button>
                    <span className={`ml-1 mr-1 ${comment.votes !== 0 ? (comment.votes < 0 ? 'text-danger' : 'text-primary') : ''}`}>{comment.votes}</span>
                    <Button variant="light-dark" size={`sm`} className={`text-success`} onClick={() => this.handleVote(comment, true)}>
                        <i className="fas fa-thumbs-up" />
                    </Button>
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: getUser(state),
    pending: getUserCommentPending(state),
    userIsGuest: getUserIsGuest(state),
    voteComment: getUserCommentVote(state),
    commentVotePendingArray: getCommentVotePendingArray(state),
    votes: getUserCommentVotes(state),
});

const mapDispatchToProps = dispatch => ({
    fetchUserComment: data => dispatch(fetchUserComment(data)),
    fetchUserCommentVote: (data, value) => dispatch(fetchCommentVote(data, value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Vote);
