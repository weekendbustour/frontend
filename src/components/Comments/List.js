import React, {Component} from 'react';
import CommentItem from './Item';
import * as PropTypes from 'prop-types';
import {commentsTypes} from '../../prop/Types/Comment';

class List extends Component {
    static propTypes = {
        item: PropTypes.object,
        comments: commentsTypes,
        model: PropTypes.string,
        rateable: PropTypes.bool,
    };

    constructor(props) {
        super(props);
        this.state = {
            comments: props.comments,
            item: props.item,
            model: props.model,
            rateable: props.rateable ?? true,
        };
    }

    render() {
        const {comments, item, model, rateable} = this.state;
        return (
            <React.Fragment>
                <div className={`comments-awesome`}>
                    {comments.map(
                        (comment, key) => comment.anon !== 1 && <CommentItem key={key} comment={comment} item={item} model={model} rateable={rateable} />
                    )}
                </div>
            </React.Fragment>
        );
    }
}

export default List;
