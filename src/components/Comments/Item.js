import React, {Component} from 'react';
import Image from 'react-bootstrap/Image';
import Media from 'react-bootstrap/Media';
import RatingDefault from './../Rating/RatingDefault';
import ReadMoreReact from 'read-more-react';
import Button from 'react-bootstrap/Button';
import CommentItem from '../Comments/Item';
import Form from 'react-bootstrap/Form';
import {fetchUserComment, getUserCommentPending} from '../../store/reducers/User/Comment';
import {connect} from 'react-redux';
import Vote from './Vote';
import * as PropTypes from 'prop-types';
import {commentTypes} from '../../prop/Types/Comment';

class Item extends Component {
    static propTypes = {
        item: PropTypes.object,
        comment: commentTypes,
        model: PropTypes.string,
        replyOpen: PropTypes.bool,
        rateable: PropTypes.bool,
        fetchUserComment: PropTypes.func,
        pending: PropTypes.bool,
    };
    constructor(props) {
        super(props);
        this.state = {
            item: props.item,
            comment: props.comment,
            model: props.model,
            replyOpen: props.replyOpen || false,

            /**
             * Выводить ли этому комментарию оставленный рейтинг
             */
            rateable: props.rateable ?? true,
            parentId: null,
            replyId: null,
            replyText: '',
        };
        this.onReplyChange = this.onReplyChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    onReplyChange(state, model) {
        this.setState({
            replyOpen: state,
            replyId: model.id,
            parentId: model.parent_id !== null ? model.parent_id : model.id,
        });
    }

    handleChange(e) {
        this.setState({
            [e.currentTarget.name]: e.currentTarget.value,
        });
    }

    handleSubmit(e) {
        e.preventDefault();
        const {fetchUserComment} = this.props;
        const {replyText, replyId, parentId, item, model} = this.state;
        const data = {
            id: item.id,
            model: model,
            reply_id: replyId,
            parent_id: parentId,
            text: replyText,
        };
        fetchUserComment(data);
    }

    render() {
        const {pending} = this.props;
        const {comment, replyOpen, replyText, rateable, item, model} = this.state;
        return (
            <React.Fragment>
                <Media>
                    <Image src={comment.image} roundedCircle width={64} height={64} alt="Generic placeholder" className="mr-3" />
                    <Media.Body>
                        <div className={`mb-3`}>
                            <div className={`d-flex justify-content-between align-items-center`}>
                                <h5 className={`mb-0`}>{comment.author}</h5>
                                {comment.parent_id === null && rateable && (
                                    <div style={{fontSize: '12px'}}>
                                        <RatingDefault readonly={true} rating={comment.rating} />
                                    </div>
                                )}
                            </div>
                            <div>
                                <span className={`__date`}>{comment.created_at_format}</span>
                            </div>
                            {comment.text !== null && (
                                <div className={`__text`}>
                                    <ReadMoreReact text={comment.text} min={80} ideal={100} max={120} readMoreText="Читать полностью" />
                                </div>
                            )}
                            <div className={`d-flex justify-content-between align-items-center`}>
                                {comment.text !== null ? (
                                    <Button variant="fill-link" size={`sm`} onClick={() => this.onReplyChange(!replyOpen, comment)}>
                                        {!replyOpen ? 'Ответить' : 'Закрыть'}
                                    </Button>
                                ) : (
                                    <div className={`__text`}>
                                        <span className={`text-muted`}>(Комментарий отсутствует)</span>
                                    </div>
                                )}
                                <Vote item={item} model={model} comment={comment} />
                            </div>
                            {replyOpen && (
                                <div className={`mt-1`}>
                                    <Form onSubmit={this.handleSubmit}>
                                        <Form.Group className={`mb-2`}>
                                            <Form.Control
                                                as="textarea"
                                                rows="3"
                                                placeholder="Комментарий"
                                                name="replyText"
                                                value={replyText}
                                                onChange={this.handleChange}
                                            />
                                        </Form.Group>
                                        <Button variant="primary" size={`sm`} type="submit" className={pending ? `is-loading` : ``}>
                                            Ответить
                                        </Button>
                                    </Form>
                                </div>
                            )}
                        </div>
                        {comment.children.length !== 0 &&
                            comment.children.map((childComment, childKey) => (
                                <CommentItem comment={childComment} key={childKey} rateable={false} item={item} model={model} />
                            ))}
                    </Media.Body>
                </Media>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    pending: getUserCommentPending(state),
});
const mapDispatchToProps = dispatch => ({
    fetchUserComment: data => dispatch(fetchUserComment(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Item);
