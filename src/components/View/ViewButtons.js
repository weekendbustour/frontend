import React from 'react';
import * as PropTypes from 'prop-types';
import ButtonGroup from "react-bootstrap/ButtonGroup";
import Button from "react-bootstrap/Button";
import {fetchAppViewTours} from "../../store/actions/App/View";
import { connect } from "react-redux";
import {getAppViewTours} from "../../store/reducers/App/View";
import {VIEW_GRID, VIEW_ROW} from "../../lib/view";
import {TooltipButton} from "../../containers/Layout/Tooltip/Button";
import {isMobile} from 'react-device-detect';

const ViewButtons = ({viewTours, fetchAppViewTours}) => {

    return (
        <React.Fragment>
            {!isMobile &&
            <div className={`views-buttons`}>
                <ButtonGroup aria-label="Basic example">
                    <TooltipButton title={`Переключить вид Плиточный`}>
                        <Button variant="grid-link" size={`sm`} title="Переключить вид Плиточный" className={`${viewTours === VIEW_GRID ? 'active' : ''}`} onClick={() => fetchAppViewTours(VIEW_GRID)}>
                            <i className="fas fa-th-large"/>
                        </Button>
                    </TooltipButton>
                    <TooltipButton title={`Переключить вид Строковый`}>
                        <Button variant="grid-link" size={`sm`} title="Переключить вид Строковый" className={`${viewTours === VIEW_ROW ? 'active' : ''}`} onClick={() => fetchAppViewTours(VIEW_ROW)}>
                            <i className="fas fa-grip-lines"/>
                        </Button>
                    </TooltipButton>
                </ButtonGroup>
            </div>
            }
        </React.Fragment>
    );
};

ViewButtons.propTypes = {
    viewTours: PropTypes.string,
    fetchAppViewTours: PropTypes.func,
};
const mapStateToProps = state => ({
    viewTours: getAppViewTours(state),
});

const mapDispatchToProps = dispatch => ({
    fetchAppViewTours: type => dispatch(fetchAppViewTours(type)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ViewButtons);
