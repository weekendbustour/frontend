import React from 'react';
import {RotateSpinner} from "react-spinners-kit";

const RotateLoader = () => {
    return (
        <React.Fragment>
            <div className={`full-loader`}>
                <RotateSpinner size={30} color="#41b882" loading={true} />
            </div>
        </React.Fragment>
    );
};

RotateLoader.propTypes = {};

export default RotateLoader;
