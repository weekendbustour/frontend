import {LOCAL_STORAGE_TOKEN_NAME} from '../store/reducers/User';

//export const API_URL = 'http://api.laravel.weekendbustour.test/api';
export const API_URL = process.env.REACT_APP_API_URL;

export function body(data) {
    return JSON.stringify(data);
}

export function axiosBody(data) {
    return data;
}

export function headers(token = null) {
    let headers = {
        'content-type': 'application/json',
    };
    if (token === null) {
        token = localStorage.getItem(LOCAL_STORAGE_TOKEN_NAME);
    }
    if (token !== null && token !== 'undefined') {
        headers = {
            'content-type': 'application/json',
            Authorization: 'Bearer ' + token,
        };
    }
    console.log(headers);
    return headers;
}

export function headersImage(token = null) {
    let headers = {
        'content-type': 'multipart/form-data',
    };
    if (token === null) {
        token = localStorage.getItem(LOCAL_STORAGE_TOKEN_NAME);
    }
    if (token !== null && token !== 'undefined') {
        headers = {
            'content-type': 'multipart/form-data',
            Authorization: 'Bearer ' + token,
        };
    }
    console.log(headers);
    return headers;
}
