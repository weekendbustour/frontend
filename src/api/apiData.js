import request from '../lib/helpers/request';

export default {
    getData: () => request('get', '/data'),
    getTour: id => request('get', `tour/${id}`),
};
