const breadcrumbs = {
    index: {
        to: '/',
        title: 'Главная',
    },
    about: {
        to: '/about',
        title: 'О нас',
    },
    search: {
        to: '/search',
        title: 'Поиск',
    },
    tours: {
        to: '/tours',
        title: 'Туры',
    },
    tour: {
        to: '/tour',
        title: '',
        parent: {
            to: '/tours',
            title: 'Туры',
        },
    },
    organizations: {
        to: '/organizations',
        title: 'Организаторы',
    },
    organization: {
        to: '/organization',
        title: 'Организаторы',
    },
    directions: {
        to: '/directions',
        title: 'Направления',
    },
    direction: {
        to: '/direction',
        title: '',
    },

    account: {
        to: '/lk/account',
        title: 'Аккаунт',
    },
    reservations: {
        to: '/lk/reservations',
        title: 'Забронированные туры',
    },
    favouriteTours: {
        to: '/lk/favourites/tours',
        title: 'Туры',
    },
    favouriteOrganizations: {
        to: '/lk/favourites/organizations',
        title: 'Организаторы',
    },
    favouriteDirections: {
        to: '/lk/favourites/directions',
        title: 'Направления',
    },
    favourites: {
        to: '/lk/favourites',
        title: 'Избранное',
    },
    subscriptions: {
        to: '/lk/subscriptions',
        title: 'Подписки',
    },
    contacts: {
        to: '/contacts',
        title: 'Контакты',
    },
    news: {
        to: '/news',
        title: 'Новости',
    },
    newsCategories: {
        to: '/categories',
        title: 'Рубрики Новостей',
    },
    articles: {
        to: '/articles',
        title: 'Статьи',
    },
    attractions: {
        to: '/attractions',
        title: 'Достопримечательности',
    },
    articleCategories: {
        to: '/categories',
        title: 'Рубрики Статей',
    },
    newsPage: {
        to: '/news',
        title: 'Новости',
        parent: {
            to: '/news',
            title: 'Новости',
        },
    },
    help: {
        to: '/help',
        title: 'Помощь',
        offer: {
            to: '/help/offer',
            title: 'Сотрудничество',
            parent: {
                to: '/help',
                title: 'Помощь',
            },
        },
        rating: {
            to: '/help/rating',
            title: 'Оставить отзыв',
            parent: {
                to: '/help',
                title: 'Помощь',
            },
        },
        report: {
            to: '/help/report',
            title: 'Ошибка на сайте',
            parent: {
                to: '/help',
                title: 'Помощь',
            },
        },
    },
};

export default breadcrumbs;
