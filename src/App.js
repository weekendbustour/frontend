import React from 'react';
import {Route, Switch, BrowserRouter} from 'react-router-dom';
import {ToastContainer, toast} from 'react-toastify';
import './App.scss';
import {RotateSpinner} from 'react-spinners-kit';
import ScrollToTop from './plugins/ScrollToTop';

const loading = () => (
    <div className={`full-loader`}>
        <RotateSpinner size={30} color="#41b882" loading={true} />
    </div>
);

// Containers
const AuthLayout = React.lazy(() => import('./containers/Auth/Layout'));
const DevLayout = React.lazy(() => import('./containers/Dev/Layout'));
const Layout = React.lazy(() => import('./containers/Layout'));

function App() {
    return (
        <BrowserRouter>
            <ScrollToTop />
            <React.Suspense fallback={loading()}>
                {/*<div className={`full-loader`}>*/}
                {/*    <RotateSpinner size={30} color="#ccc" loading={true} />*/}
                {/*</div>*/}
                <Switch>
                    <Route exact path="/auth/*" name="Auth" render={props => <AuthLayout {...props} />} />
                    <Route exact path="/dev/*" name="Dev" render={props => <DevLayout {...props} />} />
                    <Route path="/" name="Index" render={props => <Layout {...props} />} />
                </Switch>
            </React.Suspense>
            <ToastContainer
                enableMultiContainer
                containerId={'A'}
                position={toast.POSITION.BOTTOM_CENTER}
                autoClose={100000}
                className={`__toast-awesome`}
                draggablePercent={20}
            />
            <ToastContainer
                enableMultiContainer
                containerId={'B'}
                position={toast.POSITION.TOP_RIGHT}
                autoClose={50000}
                className={`__toast-awesome`}
                draggablePercent={20}
            />
        </BrowserRouter>
    );
}

export default App;
