import React, {Component} from 'react';
import {getModal, fetchModal, getModalEntertainment} from '../../../store/reducers/Modal';
import {connect} from 'react-redux';
import {MODAL_ENTERTAINMENT_CARD} from '../../../lib/modal';
import ModalDefault from '../../../components/Modal/ModalDefault';
import * as PropTypes from 'prop-types';
import Flickity from "react-flickity-component";
import ReactMarkdown from "../../../plugins/ReactMarkdown";
import FsLightbox from "fslightbox-react";
import {entertainmentTypes} from "../../../prop/Types/Entertainment";
const flickityOptions = {
    //asNavFor: '.carousel-main',
    contain: true,
    // pageDots: false,
    setGallerySize: false,
    fullscreen: true,
    lazyLoad: 2,
    groupCells: 3,
};
class EntertainmentCardModal extends Component {
    _NAME = MODAL_ENTERTAINMENT_CARD;
    _isMounted = false;
    state = {};

    static propTypes = {
        modal: PropTypes.string,
        fetchModal: PropTypes.func,
        entertainment: entertainmentTypes,
    };

    constructor(props) {
        super(props);
        this.state = {
            tab: 'info',
            toggle: false,
            slide: 1,
            mapLoaded: false,
        };

        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.setShow = this.setShow.bind(this);
        this.setTab = this.setTab.bind(this);
        this.setLoaded = this.setLoaded.bind(this);
    }

    componentDidMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    setShow(value) {
        const {fetchModal} = this.props;
        fetchModal(value);
    }
    setTab(name) {
        this.setState({
            tab: name,
        });
    }
    setToggle(state, key) {
        this.setState({
            toggle: state,
            slide: key + 1,
        });
    }
    setLoaded() {
        this.setState({
            mapLoaded: true,
        });
    }
    render() {
        const {toggle} = this.state;
        const {slide} = this.state;
        const {modal, entertainment} = this.props;
        if (!this.shouldComponentRender() || modal !== this._NAME) return null;
        return (
            <React.Fragment>
                <ModalDefault name={this._NAME}>
                    <h3>{entertainment.title}</h3>
                    <hr />
                    {entertainment.images.length !== 0 && (
                        <div>
                            <FsLightbox type="image" toggler={toggle} sources={entertainment.images} slide={slide} thumbs={entertainment.images} />
                            <Flickity
                                className={'carousel'} // default ''
                                elementType={'div'} // default 'div'
                                options={flickityOptions} // takes flickity options {}
                                disableImagesLoaded={false} // default false
                                reloadOnUpdate // default false
                                static // default false
                            >
                                {entertainment.images.map((image, key) => (
                                    <img key={key} src={image} onClick={() => this.setToggle(!toggle, key)} alt="" />
                                ))}
                            </Flickity>
                        </div>
                    )}
                    <div>
                        <ReactMarkdown source={entertainment.description} />
                    </div>
                </ModalDefault>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    modal: getModal(state),
    entertainment: getModalEntertainment(state),
});
const mapDispatchToProps = dispatch => ({
    fetchModal: name => dispatch(fetchModal(name)),
});

export default connect(mapStateToProps, mapDispatchToProps)(EntertainmentCardModal);
