import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Modal from 'react-bootstrap/Modal';
import React, {Component} from 'react';
import {getModal, fetchModal} from '../../../store/reducers/Modal';
import {connect} from 'react-redux';
import {MODAL_SUBSCRIPTION} from '../../../lib/modal';
import Subscribe from "../../Layout/Subscribe";
import * as PropTypes from "prop-types";

class SubscriptionModal extends Component {
    _NAME = MODAL_SUBSCRIPTION;
    _isMounted = false;
    state = {};

    static propTypes = {
        modal: PropTypes.string,
        fetchModal: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.state = {};
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.setShow = this.setShow.bind(this);
    }

    componentDidMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    setShow(value) {
        const {fetchModal} = this.props;
        fetchModal(value);
    }

    render() {
        const {modal} = this.props;
        if (!this.shouldComponentRender() || modal !== this._NAME) return null;
        return (
            <React.Fragment>
                <Modal
                    size="lg"
                    show={modal === this._NAME}
                    onHide={() => this.setShow(null)}
                    aria-labelledby="contained-modal-title-vcenter"
                    centered
                    className={`modal-auth`}
                >
                    <Modal.Body>
                        <button type="button" className="modal-button-close close" onClick={() => this.setShow(null)}>
                            <span aria-hidden="true">×</span>
                            <span className="sr-only">Close</span>
                        </button>
                        <Row>
                            <Col xs={12} md={12} lg={{span: 8, offset: 2}} className={`mb-5 mt-5`}>
                                <div className={`d-flex justify-content-center`}>
                                    <Subscribe />
                                </div>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    modal: getModal(state),
});
const mapDispatchToProps = dispatch => ({
    fetchModal: name => dispatch(fetchModal(name)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SubscriptionModal);
