import React, {Component} from 'react';
import {getModal, fetchModal} from '../../../store/reducers/Modal';
import {connect} from 'react-redux';
import {MODAL_TOUR_RESERVATION} from '../../../lib/modal';
import {Button} from 'react-bootstrap';
import Form from 'react-bootstrap/Form';
import InputCounter from '../../../components/Form/InputCounter';
import {fetchUserReservation, getUserReservationsPending} from '../../../store/reducers/User/Reservation';
import store from '../../../store';
import {getTour} from '../../../store/reducers/Tour';
import ModalDefault from '../../../components/Modal/ModalDefault';
import Alert from 'react-bootstrap/Alert';
import {getUser} from '../../../store/reducers/User';
import * as PropTypes from 'prop-types';
import {userTypes} from '../../../prop/Types/User';
import {tourTypes} from '../../../prop/Types/Tour';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

class ReservationModal extends Component {
    _NAME = MODAL_TOUR_RESERVATION;
    _isMounted = false;
    state = {};

    static propTypes = {
        tour: tourTypes,
        user: userTypes,
        modal: PropTypes.string,
        pending: PropTypes.bool,
        fetchModal: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.state = {
            places: 1,
            first_name: props.user !== null ? props.user.first_name : '',
            phone: props.user !== null ? props.user.phone : '',
            email: props.user !== null ? props.user.email : '',
            tab: 'info',
        };

        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.setShow = this.setShow.bind(this);
        this.setTab = this.setTab.bind(this);

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleChangeValue = this.handleChangeValue.bind(this);
    }

    componentDidMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    setShow(value) {
        const {fetchModal} = this.props;
        fetchModal(value);
    }
    setTab(name) {
        this.setState({
            tab: name,
        });
    }

    handleChange(e) {
        this.setState({
            [e.currentTarget.name]: e.currentTarget.value,
        });
    }
    handleChangeValue(name, value) {
        console.log(name, value);
        this.setState({
            [name]: value,
        });
    }

    handleSubmit(e) {
        e.preventDefault();
        const {places, first_name, phone, email} = this.state;
        const {tour} = this.props;
        const data = {
            places: places,
            id: tour.id,
            first_name: first_name,
            email: email,
            phone: phone,
        };
        store.dispatch(fetchUserReservation(data));
    }

    render() {
        const {phone, first_name, email} = this.state;
        const {modal, pending, user} = this.props;
        if (!this.shouldComponentRender() || modal !== this._NAME) return null;
        return (
            <React.Fragment>
                <ModalDefault name={this._NAME}>
                    <Form onSubmit={this.handleSubmit} className={`mt-3`}>
                        <h3>Бронирование тура</h3>
                        <hr />
                        <Alert variant="info">
                            <Alert.Heading>Информация</Alert.Heading>
                            <p className={`mb-0`}>
                                После отправки, заявка будет отправлена Организатору на рассмотрение. После одобрения вы получите все инструкции.
                                <br />
                                Спасибо что используете наш сервис.
                            </p>
                        </Alert>
                        <div className={`p-3 border`}>
                            <h5>Ваша заявка</h5>
                            <hr />
                            <Row>
                                <Col xs={12}>
                                    <Form.Group className={`${user !== null && user.first_name !== '' ? 'is-disabled' : ''}`}>
                                        <Form.Label>
                                            Имя <i className="r">*</i>
                                        </Form.Label>
                                        <Form.Control type="text" placeholder="Имя" value={first_name} name="first_name" onChange={this.handleChange} required />
                                    </Form.Group>
                                </Col>
                            </Row>
                            <Row>
                                <Col xs={12}>
                                    <Form.Group className={`${user !== null && user.email !== '' ? 'is-disabled' : ''}`}>
                                        <Form.Label>
                                            Email <i className="r">*</i>
                                        </Form.Label>
                                        <Form.Control type="email" placeholder="Email" value={email} name="email" onChange={this.handleChange} required />
                                    </Form.Group>
                                </Col>
                            </Row>
                            {/*<Form.Group className={`${user !== null && user.first_name !== '' ? 'is-disabled' : ''}`}>*/}
                            {/*    <Form.Label>*/}
                            {/*        Имя <i className="r">*</i>*/}
                            {/*    </Form.Label>*/}
                            {/*    <Form.Control type="text" placeholder="Имя" value={first_name} name="first_name" onChange={this.handleChange} required />*/}
                            {/*</Form.Group>*/}
                            <Form.Group className={`${user !== null && user.phone !== '' ? 'is-disabled' : ''}`}>
                                <Form.Label>
                                    Телефон <i className="r">*</i>
                                </Form.Label>
                                <Form.Control
                                    placeholder="Телефон"
                                    className={`cleave-mask--phone`}
                                    value={phone}
                                    name="phone"
                                    onChange={this.handleChange}
                                    required
                                />
                            </Form.Group>
                            <Form.Group>
                                <Form.Label>Количество человек</Form.Label>
                                <div>
                                    <InputCounter name={`places`} value={1} onChange={this.handleChangeValue} />
                                </div>
                            </Form.Group>
                        </div>
                        <hr />
                        <div className={`text-right`}>
                            <Button variant="default" type="button" onClick={() => this.setShow(null)} className={`mr-3`}>
                                Закрыть
                            </Button>
                            <Button variant="primary" type={`submit`} className={`${pending ? 'is-loading' : ''} `}>
                                Отправить заявку
                            </Button>
                        </div>
                    </Form>
                </ModalDefault>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: getUser(state),
    tour: getTour(state),
    modal: getModal(state),
    pending: getUserReservationsPending(state),
});
const mapDispatchToProps = dispatch => ({
    fetchModal: name => dispatch(fetchModal(name)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ReservationModal);
