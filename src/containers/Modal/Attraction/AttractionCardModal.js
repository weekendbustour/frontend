import React, {Component} from 'react';
import {getModal, fetchModal, getModalAttraction} from '../../../store/reducers/Modal';
import {connect} from 'react-redux';
import {MODAL_ATTRACTION_CARD} from '../../../lib/modal';
import ModalDefault from '../../../components/Modal/ModalDefault';
import * as PropTypes from 'prop-types';
import Flickity from "react-flickity-component";
import ReactMarkdown from "../../../plugins/ReactMarkdown";
import FsLightbox from "fslightbox-react";
import {
    FullscreenControl,
    GeolocationControl,
    Map,
    Placemark,
    RouteButton,
    TypeSelector, YMaps,
    ZoomControl
} from "react-yandex-maps";
import Skeleton from "react-loading-skeleton";
const flickityOptions = {
    //asNavFor: '.carousel-main',
    contain: true,
    // pageDots: false,
    setGallerySize: false,
    fullscreen: true,
    lazyLoad: 2,
    groupCells: 3,
};
class AttractionCardModal extends Component {
    _NAME = MODAL_ATTRACTION_CARD;
    _isMounted = false;
    state = {};

    static propTypes = {
        modal: PropTypes.string,
        fetchModal: PropTypes.func,
        attraction: PropTypes.object,
    };

    constructor(props) {
        super(props);
        this.state = {
            tab: 'info',
            toggle: false,
            slide: 1,
            mapLoaded: false,
        };

        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.setShow = this.setShow.bind(this);
        this.setTab = this.setTab.bind(this);
        this.setLoaded = this.setLoaded.bind(this);
    }

    componentDidMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    setShow(value) {
        const {fetchModal} = this.props;
        fetchModal(value);
    }
    setTab(name) {
        this.setState({
            tab: name,
        });
    }
    setToggle(state, key) {
        this.setState({
            toggle: state,
            slide: key + 1,
        });
    }
    setLoaded() {
        this.setState({
            mapLoaded: true,
        });
    }
    render() {
        const {toggle} = this.state;
        const {slide, mapLoaded} = this.state;
        const {modal, attraction} = this.props;
        if (!this.shouldComponentRender() || modal !== this._NAME) return null;
        return (
            <React.Fragment>
                <ModalDefault name={this._NAME}>
                    <h3>{attraction.title}</h3>
                    <hr />
                    {attraction.images.length !== 0 && (
                        <div>
                            <FsLightbox type="image" toggler={toggle} sources={attraction.images} slide={slide} thumbs={attraction.images} />
                            <Flickity
                                className={'carousel'} // default ''
                                elementType={'div'} // default 'div'
                                options={flickityOptions} // takes flickity options {}
                                disableImagesLoaded={false} // default false
                                reloadOnUpdate // default false
                                static // default false
                            >
                                {attraction.images.map((image, key) => (
                                    <img key={key} src={image} onClick={() => this.setToggle(!toggle, key)} alt="" />
                                ))}
                            </Flickity>
                        </div>
                    )}
                    <div>
                        <ReactMarkdown source={attraction.description} />
                    </div>
                    {attraction.location !== null &&
                    <div>
                        {!mapLoaded && (
                            <div>
                                <Skeleton count={1} width={`100%`} height={`236px`} />
                            </div>
                        )}
                        <YMaps>
                            <div>
                                <Map
                                    defaultState={{
                                        center: attraction.location.point,
                                        zoom: attraction.location.zoom,
                                        controls: [],
                                        //behaviors: ['ScrollZoom'],
                                    }}
                                    width={`100%`}
                                    onLoad={this.setLoaded}
                                >
                                    <Placemark geometry={attraction.location.point} />
                                    <ZoomControl options={{float: 'right'}} />
                                    <TypeSelector options={{float: 'right'}} />
                                    <GeolocationControl options={{float: 'left'}} />
                                    <RouteButton options={{float: 'right'}} />
                                    <FullscreenControl />
                                </Map>
                            </div>
                        </YMaps>
                    </div>
                    }
                </ModalDefault>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    modal: getModal(state),
    attraction: getModalAttraction(state),
});
const mapDispatchToProps = dispatch => ({
    fetchModal: name => dispatch(fetchModal(name)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AttractionCardModal);
