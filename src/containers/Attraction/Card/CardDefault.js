import React, {Component} from 'react';
import {Link, withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import * as PropTypes from 'prop-types';
import ReadMoreReact from 'read-more-react';
import AttractionCardModal from '../../Modal/Attraction/AttractionCardModal';
import {fetchModalAttraction} from '../../../store/reducers/Modal';

class CardDefault extends Component {
    static propTypes = {
        attraction: PropTypes.object,
        fetchModalAttraction: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.state = {
            attraction: props.attraction,
        };
        this.setShow = this.setShow.bind(this);
    }

    setShow(data) {
        const {fetchModalAttraction} = this.props;
        fetchModalAttraction(data);
    }

    render() {
        const {attraction} = this.state;
        return (
            <React.Fragment>
                <div className="card-inter" style={{backgroundImage: 'url(' + attraction.image.default + ')'}}>
                    <div className="content">
                        <div>
                            <h4 className="title">{attraction.title}</h4>
                            {attraction.preview_description !== null &&
                            <div className="copy">
                                <ReadMoreReact text={attraction.preview_description} min={70} ideal={80} max={100} readMoreText="" />
                            </div>
                            }
                        </div>
                        <button className="btn" onClick={() => this.setShow(attraction)}>
                            Читать подробности
                        </button>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({
    fetchModalAttraction: data => dispatch(fetchModalAttraction(data)),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CardDefault));
