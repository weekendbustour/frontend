import React, {Component} from 'react';
import {Link, withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import * as PropTypes from 'prop-types';
import ReadMoreReact from 'read-more-react';
import {fetchModalAttraction} from '../../../store/reducers/Modal';
import AttractionMedia from "../Components/Social/Media";

class Card extends Component {
    static propTypes = {
        attraction: PropTypes.object,
        fetchModalAttraction: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.state = {
            attraction: props.attraction,
        };
        this.setShow = this.setShow.bind(this);
    }

    setShow(data) {
        const {fetchModalAttraction} = this.props;
        fetchModalAttraction(data);
    }

    render() {
        const {attraction} = this.state;
        return (
            <React.Fragment>
                <div className="card-polo">
                    <div className={`card-polo--image`} style={{backgroundImage: 'url(' + attraction.image.card + ')'}}/>
                    <div className="card-polo--content">
                        <h4 className={`card-polo--title`}>
                            {attraction.title}
                        </h4>
                        <div className={`card-polo--description text-muted`}>
                            {attraction.preview_description !== null &&
                            <ReadMoreReact text={attraction.preview_description} min={50} ideal={60} max={70} readMoreText="Читать полностью" />
                            }
                        </div>
                        <div className={`card-polo--media`}>
                            <div className={`d-flex`}>
                                <AttractionMedia variant={`light-dark`} attraction={attraction} />
                            </div>
                            <div className={`link-continue`}>
                                <Link to={`/attraction/${attraction.id}/${attraction.name}`} className={`btn btn-sm btn-light-dark`}>
                                    Подробнее
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({
    fetchModalAttraction: data => dispatch(fetchModalAttraction(data)),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Card));
