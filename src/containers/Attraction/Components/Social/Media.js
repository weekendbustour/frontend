import React, {Component} from 'react';
import Button from 'react-bootstrap/Button';
import {getUserSubscriptions, getUserSubscriptionsPendingArray} from '../../../../store/reducers/User/Subscription';
import {fetchUserSubscribeModel} from '../../../../store/actions/User/Subscription';
import {fetchUserFavourite, getUserFavourites, getUserFavouritesPendingArray} from '../../../../store/reducers/User/Favourite';
import {connect} from 'react-redux';
import {TooltipButton} from '../../../Layout/Tooltip/Button';
import {isMobile} from 'react-device-detect';
import {scrollTo} from '../../../../lib/scrollTo';
import * as PropTypes from 'prop-types';
import {userFavouritesTypes, userSubscriptionsTypes} from '../../../../prop/Types/User';
import {attractionTypes} from "../../../../prop/Types/Attraction";

class Media extends Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        attraction: attractionTypes,
        variant: PropTypes.string,
        size: PropTypes.string,

        favourites: userFavouritesTypes,
        subscriptions: userSubscriptionsTypes,
        favouritesPending: PropTypes.array,
        subscriptionsPending: PropTypes.array,
        fetchUserFavourite: PropTypes.func,
        fetchUserSubscribeModel: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.state = {
            attraction: props.attraction,
            size: props.size ?? 'sm',
            variant: props.variant ?? 'light',
        };
        this.setFavourite = this.setFavourite.bind(this);
        this.setSubscription = this.setSubscription.bind(this);
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.scrollToComments = this.scrollToComments.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    setFavourite(item) {
        const {fetchUserFavourite} = this.props;
        fetchUserFavourite('attraction', item);
        this.forceUpdate();
    }

    setSubscription(item) {
        const {fetchUserSubscribeModel} = this.props;
        fetchUserSubscribeModel('attraction', item);
        this.forceUpdate();
    }

    scrollToComments() {
        if (document.querySelector('.--container-attraction-comments')) {
            scrollTo.selector('.--container-attraction-comments');
        }
    }

    render() {
        const {favourites, favouritesPending} = this.props;
        const {attraction, variant, role, size} = this.state;
        const liked = favourites !== null ? favourites.attractions.indexOf(attraction.id) !== -1 : false;
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <ul className={`list-group list-group-horizontal ${role === 'page' && isMobile ? '--right' : ''}`}>
                    <li className={`list-group-item`}>
                        <TooltipButton title={`${liked ? 'Убрать из Избранного' : 'Добавить в Избранное'}`}>
                            <Button
                                size={size}
                                variant={variant}
                                className={`${liked ? 'fa-active' : ''} ${favouritesPending.indexOf('attraction-' + attraction.id) !== -1 ? 'is-loading' : ''}`}
                                onClick={() => this.setFavourite(attraction)}
                            >
                                <i className={`${liked ? 'fas' : 'far'} fa-heart`} />
                            </Button>
                        </TooltipButton>
                    </li>
                </ul>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    favourites: getUserFavourites(state),
    subscriptions: getUserSubscriptions(state),
    favouritesPending: getUserFavouritesPendingArray(state),
    subscriptionsPending: getUserSubscriptionsPendingArray(state),
});

const mapDispatchToProps = dispatch => ({
    fetchUserFavourite: (model, data) => dispatch(fetchUserFavourite(model, data)),
    fetchUserSubscribeModel: (model, data) => dispatch(fetchUserSubscribeModel(model, data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Media);
