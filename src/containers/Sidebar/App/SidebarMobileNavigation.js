import React, {Component} from 'react';
import {connect} from 'react-redux';
import {fetchSidebar} from '../../../store/actions/Sidebar';
import {getSidebar, getSidebarLockScroll} from '../../../store/reducers/Sidebar';
import {SIDEBAR_MOBILE_NAVIGATION} from '../../../lib/sidebar';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Sidebar from 'react-sidebar';
import {getDataNews, getDataNewsPending} from '../../../store/reducers/Data';
import ScrollLock from 'react-scrolllock';
import {Button} from 'react-bootstrap';
import * as PropTypes from 'prop-types';
import {Link} from "react-router-dom";
import Nav from "react-bootstrap/Nav";
import {getAppSeo} from "../../../store/reducers/App/Seo";

class SidebarMobileNavigation extends Component {
    _NAME = SIDEBAR_MOBILE_NAVIGATION;
    _isMounted = false;
    state = {};
    _TABS = {
        index: '/',
        search: '/search',
        toursRostov: '/tours',
        toursKrasnodar: '/tours',
        tours: '/tours',
        organizations: '/organizations',
        directions: '/directions',
        news: '/news',
        articles: '/articles',
        contacts: '/contacts',
        help: '/help',
        about: '/about',
    };
    _ALLOWED_TABS = {
        index: ['/'],
        search: ['/search'],
        toursRostov: [],
        toursKrasnodar: [],
        tours: ['/tours'],
        organizations: ['/organizations'],
        directions: ['/directions'],
        news: ['/news'],
        articles: ['/articles'],
        contacts: ['/contacts'],
        help: ['/help'],
        about: ['/about'],
    };

    static propTypes = {
        sidebar: PropTypes.string,
        lockScroll: PropTypes.bool,
        fetchSidebar: PropTypes.func,
        seo: PropTypes.shape({
            title: PropTypes.string,
            route: PropTypes.string,
        }),
    };

    constructor(props) {
        super(props);
        this.state = {};

        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.onSetSidebarOpen = this.onSetSidebarOpen.bind(this);
    }

    componentDidMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    onSetSidebarOpen = name => {
        const {fetchSidebar} = this.props;
        fetchSidebar(name);
    };

    render() {
        const {seo} = this.props;
        const {sidebar, lockScroll} = this.props;
        if (!this.shouldComponentRender()) return null;
        return (
            <React.Fragment>
                <ScrollLock isActive={lockScroll} />
                <nav className={`sidebar-notifications ${sidebar === this._NAME ? 'is-open' : ''}`}>
                    <Sidebar
                        sidebar={''}
                        open={sidebar === this._NAME}
                        onSetOpen={() => this.onSetSidebarOpen(null)}
                        pullRight={true}
                        rootId={`sn-root`}
                        sidebarId={`sn-sidebar`}
                        contentId={`sn-content`}
                        overlayId={`sn-overlay`}
                        styles={{root: null, sidebar: {}, content: {}, overlay: {}, dragHandle: {}}}
                    >
                        {/*<FreeScrollBar>*/}
                        {/*    */}
                        {/*</FreeScrollBar>*/}
                        <div className={`content w-100`}>
                            <div className={`__header mb-3`}>
                                <h5>Меню</h5>
                                <Button variant={`default`} size={`sm`} onClick={() => this.onSetSidebarOpen(null)}>
                                    <i className="fas fa-times" />
                                </Button>
                            </div>
                            <Row>
                                <Col xs={`12`} sm={`12`} md={`12`} lg={`12`} className={`mb-3 aw-tabs-nav-pills`}>
                                    <Nav className="nav-pills flex-column">
                                        <Link to="/" className={`nav-item nav-link ${this._ALLOWED_TABS.index.indexOf(seo.route) !== -1 ? 'active' : ''}`}>
                                            Главная
                                        </Link>
                                        <Link to="/search" className={`nav-item nav-link ${this._ALLOWED_TABS.search.indexOf(seo.route) !== -1 ? 'active' : ''}`}>
                                            Поиск
                                        </Link>
                                        <Link to="/search?departure_id=1" className={`nav-item nav-link ${this._ALLOWED_TABS.toursRostov.indexOf(seo.route) !== -1 ? 'active' : ''}`}>
                                            Туры из Ростова
                                        </Link>
                                        <Link to="/search?departure_id=2" className={`nav-item nav-link ${this._ALLOWED_TABS.toursKrasnodar.indexOf(seo.route) !== -1 ? 'active' : ''}`}>
                                            Туры из Краснодара
                                        </Link>
                                        <Link to="/tours" className={`nav-item nav-link ${this._ALLOWED_TABS.tours.indexOf(seo.route) !== -1 ? 'active' : ''}`}>
                                            Все туры
                                        </Link>
                                        {/*<NavDropdown title="Типы" id="basic-nav-dropdown">*/}
                                        {/*    {categories.map((category, idx) =>*/}
                                        {/*        <NavDropdown.Item href="#action/3.1" key={idx}>{category.title}</NavDropdown.Item>*/}
                                        {/*    )}*/}
                                        {/*</NavDropdown>*/}
                                        <Link to="/organizations" className={`nav-item nav-link ${this._ALLOWED_TABS.organizations.indexOf(seo.route) !== -1 ? 'active' : ''}`}>
                                            Организаторы
                                        </Link>
                                        <Link to="/directions" className={`nav-item nav-link ${this._ALLOWED_TABS.directions.indexOf(seo.route) !== -1 ? 'active' : ''}`}>
                                            Направления
                                        </Link>
                                        <Link to="/news" className={`nav-item nav-link ${this._ALLOWED_TABS.news.indexOf(seo.route) !== -1 ? 'active' : ''}`}>
                                            Новости
                                        </Link>
                                        <Link to="/articles" className={`nav-item nav-link ${this._ALLOWED_TABS.articles.indexOf(seo.route) !== -1 ? 'active' : ''}`}>
                                            Статьи
                                        </Link>
                                        {/*<Link to="/contacts" className={`nav-item nav-link ${this._ALLOWED_TABS.contacts.indexOf(seo.route) !== -1 ? 'active' : ''}`}>*/}
                                        {/*    Контакты*/}
                                        {/*</Link>*/}
                                        <Link to="/help" className={`nav-item nav-link ${this._ALLOWED_TABS.help.indexOf(seo.route) !== -1 ? 'active' : ''}`}>
                                            Помощь
                                        </Link>
                                        <Link to="/about" className={`nav-item nav-link ${this._ALLOWED_TABS.about.indexOf(seo.route) !== -1 ? 'active' : ''}`}>
                                            О нас
                                        </Link>
                                    </Nav>
                                </Col>
                            </Row>
                        </div>
                    </Sidebar>
                </nav>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    seo: getAppSeo(state),
    sidebar: getSidebar(state),
    lockScroll: getSidebarLockScroll(state),
    news: getDataNews(state),
    newsPending: getDataNewsPending(state),
});

const mapDispatchToProps = dispatch => ({
    fetchSidebar: name => dispatch(fetchSidebar(name)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SidebarMobileNavigation);
