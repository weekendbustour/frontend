import React, {Component} from 'react';
import {connect} from 'react-redux';
import {fetchSidebar} from '../../../store/actions/Sidebar';
import {getSidebar, getSidebarLockScroll} from '../../../store/reducers/Sidebar';
import {SIDEBAR_NOTIFICATIONS} from '../../../lib/sidebar';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Sidebar from 'react-sidebar';
import {getDataNews, getDataNewsPending} from '../../../store/reducers/Data';
import Skeleton from 'react-loading-skeleton';
import ScrollLock from 'react-scrolllock';
import {Button} from 'react-bootstrap';
import * as PropTypes from 'prop-types';

class SidebarNotifications extends Component {
    _NAME = SIDEBAR_NOTIFICATIONS;
    _isMounted = false;
    state = {};

    static propTypes = {
        sidebar: PropTypes.string,
        lockScroll: PropTypes.bool,
        fetchSidebar: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.state = {};

        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.onSetSidebarOpen = this.onSetSidebarOpen.bind(this);
    }

    componentDidMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    onSetSidebarOpen = name => {
        const {fetchSidebar} = this.props;
        fetchSidebar(name);
    };

    render() {
        const {sidebar, lockScroll} = this.props;
        if (!this.shouldComponentRender()) return null;
        return (
            <React.Fragment>
                <ScrollLock isActive={lockScroll} />
                <nav className={`sidebar-notifications ${sidebar === this._NAME ? 'is-open' : ''}`}>
                    <Sidebar
                        sidebar={''}
                        open={sidebar === this._NAME}
                        onSetOpen={() => this.onSetSidebarOpen(null)}
                        pullRight={true}
                        rootId={`sn-root`}
                        sidebarId={`sn-sidebar`}
                        contentId={`sn-content`}
                        overlayId={`sn-overlay`}
                        styles={{root: null, sidebar: {}, content: {}, overlay: {}, dragHandle: {}}}
                    >
                        {/*<FreeScrollBar>*/}
                        {/*    */}
                        {/*</FreeScrollBar>*/}
                        <div className={`content`}>
                            <div className={`__header mb-3`}>
                                <h5>Уведомления</h5>
                                <Button variant={`default`} size={`sm`} onClick={() => this.onSetSidebarOpen(null)}>
                                    <i className="fas fa-times" />
                                </Button>
                            </div>
                            <Row>
                                <Col xs={`12`} sm={`12`} md={`12`} lg={`12`} className={`mb-3`}>
                                    <Skeleton height={`70px`} width={`100%`} />
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`12`} lg={`12`} className={`mb-3`}>
                                    <Skeleton height={`70px`} width={`100%`} />
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`12`} lg={`12`} className={`mb-3`}>
                                    <Skeleton height={`70px`} width={`100%`} />
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`12`} lg={`12`} className={`mb-3`}>
                                    <Skeleton height={`70px`} width={`100%`} />
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`12`} lg={`12`} className={`mb-3`}>
                                    <Skeleton height={`70px`} width={`100%`} />
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`12`} lg={`12`} className={`mb-3`}>
                                    <Skeleton height={`70px`} width={`100%`} />
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`12`} lg={`12`} className={`mb-3`}>
                                    <Skeleton height={`70px`} width={`100%`} />
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`12`} lg={`12`} className={`mb-3`}>
                                    <Skeleton height={`70px`} width={`100%`} />
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`12`} lg={`12`} className={`mb-3`}>
                                    <Skeleton height={`70px`} width={`100%`} />
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`12`} lg={`12`} className={`mb-3`}>
                                    <Skeleton height={`70px`} width={`100%`} />
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`12`} lg={`12`} className={`mb-3`}>
                                    <Skeleton height={`70px`} width={`100%`} />
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`12`} lg={`12`} className={`mb-3`}>
                                    <Skeleton height={`70px`} width={`100%`} />
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`12`} lg={`12`} className={`mb-3`}>
                                    <Skeleton height={`70px`} width={`100%`} />
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`12`} lg={`12`} className={`mb-3`}>
                                    <Skeleton height={`70px`} width={`100%`} />
                                </Col>
                                <Col xs={`12`} sm={`12`} md={`12`} lg={`12`} className={`mb-3`}>
                                    <Skeleton height={`70px`} width={`100%`} />
                                </Col>
                            </Row>
                        </div>
                    </Sidebar>
                </nav>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    sidebar: getSidebar(state),
    lockScroll: getSidebarLockScroll(state),
    news: getDataNews(state),
    newsPending: getDataNewsPending(state),
});

const mapDispatchToProps = dispatch => ({
    fetchSidebar: name => dispatch(fetchSidebar(name)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SidebarNotifications);
