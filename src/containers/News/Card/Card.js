import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import ReadMoreReact from 'read-more-react';
import Badge from 'react-bootstrap/Badge';
import {newsItemTypes} from '../../../prop/Types/News';
import * as PropTypes from 'prop-types';
import NewsStatistic from '../Components/NewsStatistic';

class Card extends Component {
    static propTypes = {
        news: newsItemTypes,
        type: PropTypes.string,
    };

    constructor(props) {
        super(props);
        this.state = {
            news: props.news,
            type: props.type || 'default',
        };
    }

    render() {
        const {news, type} = this.state;
        return (
            <React.Fragment>
                <div className={`card__snipe is-${type}`}>
                    <Link to={`/news/${news.id}/${news.name}`}>
                        <div className="card__snipe--background" style={{backgroundImage: 'url(' + news.image.card + ')'}} />
                        {/*<img className="card__snipe--image" src={news.image.card}/>*/}
                        <div className="card__snipe--date">
                            <span className="day">{news.published_at_day}</span>
                            <span className="month">{news.published_at_month}</span>
                        </div>
                        <div className="card__snipe--description">
                            <div className="card__snipe--category">
                                {news.categories.map((category, key) => (
                                    <Badge
                                        key={key}
                                        variant="default"
                                        className={[``, category.color ? `colored` : ``]}
                                        style={{backgroundColor: category.color, borderColor: category.color}}
                                    >
                                        {category.title}
                                    </Badge>
                                ))}
                            </div>
                            <h3>{news.title}</h3>
                            <div>
                                <ReadMoreReact text={news.preview_description} min={70} ideal={80} max={100} readMoreText="Читать полностью" />
                            </div>

                        </div>
                    </Link>
                    <div className="card__snipe--socials">
                        <NewsStatistic news={news} />
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default Card;
