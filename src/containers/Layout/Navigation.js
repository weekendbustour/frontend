import React, {Component} from 'react';
import {Link, withRouter} from 'react-router-dom';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Container from 'react-bootstrap/Container';
import SidebarContainer from '../Layout/Sidebar/Container';

class Navigation extends Component {
    render() {
        return (
            <React.Fragment>
                <Navbar expand="md" variant="dark" bg="primary" className={`d-block d-md-none`}>
                    <Container>
                        <Link to="/" className={`navbar-brand`} style={{padding: 0}}>
                            <h2 className={`mb-0`}>{process.env.REACT_APP_NAME}</h2>
                        </Link>
                        <SidebarContainer />
                    </Container>
                </Navbar>
                <Navbar expand="md" variant="dark" bg="primary" className={`d-none d-md-block`}>
                    <Container>
                        <Nav className="mr-auto">
                            <Link to="/search?departure_id=1" className={`nav-link pl-0`} alt={`Туры из Ростова`}>
                                Туры из Ростова
                            </Link>
                            <Link to="/search?departure_id=2" className={`nav-link`} alt={`Туры из Краснодара`}>
                                Туры из Краснодара
                            </Link>
                            <Link to="/tours" className={`nav-link`} alt={`Все туры`}>
                                Все туры
                            </Link>
                            {/*<NavDropdown title="Типы" id="basic-nav-dropdown">*/}
                            {/*    {categories.map((category, idx) =>*/}
                            {/*        <NavDropdown.Item href="#action/3.1" key={idx}>{category.title}</NavDropdown.Item>*/}
                            {/*    )}*/}
                            {/*</NavDropdown>*/}
                            <Link to="/organizations" className={`nav-link`} alt={`Организаторы`}>
                                Организаторы
                            </Link>
                            <Link to="/directions" className={`nav-link`} alt={`Направления`}>
                                Направления
                            </Link>
                            <Link to="/news" className={`nav-link`} alt={`Новости`}>
                                Новости
                            </Link>
                            <Link to="/articles" className={`nav-link`} alt={`Статьи`}>
                                Статьи
                            </Link>
                            {/*<Link to="/contacts" className={`nav-link`}>*/}
                            {/*    Контакты*/}
                            {/*</Link>*/}
                            {/*<Link to="/help" className={`nav-link`}>*/}
                            {/*    Помощь*/}
                            {/*</Link>*/}
                            <Link to="/about" className={`nav-link`} alt={`О нас`}>
                                О нас
                            </Link>
                        </Nav>
                    </Container>
                </Navbar>
            </React.Fragment>
        );
    }
}

export default withRouter(Navigation);
