import React, {Component} from 'react';
import Container from 'react-bootstrap/Container';
import Jumbotron from 'react-bootstrap/Jumbotron';
import SearchFrom from './SearchFrom';
import {getDataTitles, getDataTitlesPending} from "../../store/reducers/Data";
import {connect} from "react-redux";
import * as PropTypes from "prop-types";
import {titlesTypes} from "../../prop/Types/Title";

class Banner extends Component {
    _isMounted = false;

    static propTypes = {
        titles: titlesTypes,
        titlesPending: PropTypes.bool,
    };

    constructor(props) {
        super(props);
        this.state = {};
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {titlesPending, titles} = this.props;
        if (!this.shouldComponentRender()) return null;
        return (
            <React.Fragment>
                <Jumbotron fluid className={`jumbotron--search`}>
                    <Container>
                        {!titlesPending ? (
                            <div>
                                <h2>{titles.app.title}</h2>
                                <p className={`text-muted`}>
                                    {titles.app.description}
                                </p>
                            </div>
                        ) : (
                            <div>
                                <h2>НАЙДИ СВОЙ НЕЗАБЫВАЕМЫЙ ВЫХОДНОЙ</h2>
                                <p className={`text-muted`}>
                                    У нас можно найти увлекательные автобусные поездки по России у лучших Организаторов
                                </p>
                            </div>
                        )}
                        <SearchFrom />
                    </Container>
                </Jumbotron>
            </React.Fragment>
        );
    }
}
const mapStateToProps = state => ({
    titles: getDataTitles(state),
    titlesPending: getDataTitlesPending(state),
});
export default connect(mapStateToProps)(Banner);
