import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {Button} from 'react-bootstrap';
import {Container} from 'react-bootstrap';
import Nav from 'react-bootstrap/Nav';
import {withRouter} from 'react-router';
import {SIDEBAR_MOBILE_NAVIGATION, SIDEBAR_NOTIFICATIONS} from '../../../lib/sidebar';
import {fetchSidebar} from '../../../store/actions/Sidebar';
import {connect} from 'react-redux';
import * as PropTypes from 'prop-types';

class SidebarContainer extends Component {
    static propTypes = {
        fetchSidebar: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.state = {
            sidebarOpen: false,
        };
        this.onSetSidebarOpen = this.onSetSidebarOpen.bind(this);
        this.onSetSidebarNotificationOpen = this.onSetSidebarNotificationOpen.bind(this);
        this.onSetSidebarMobileNavigationOpen = this.onSetSidebarMobileNavigationOpen.bind(this);
    }

    componentDidMount(): void {
        const self = this;
        self.props.history.listen((location, action) => {
            self.onSetSidebarOpen(false);
        });
    }

    onSetSidebarOpen(open) {
        this.setState({sidebarOpen: open});
    }
    onSetSidebarNotificationOpen = () => {
        const {fetchSidebar} = this.props;
        fetchSidebar(SIDEBAR_NOTIFICATIONS);
    };
    onSetSidebarMobileNavigationOpen = () => {
        const {fetchSidebar} = this.props;
        fetchSidebar(SIDEBAR_MOBILE_NAVIGATION);
    };

    render() {
        const {sidebarOpen} = this.state;
        return (
            <React.Fragment>
                <div className={`root-sidebar ${sidebarOpen ? 'open' : ''}`}>
                    <div className={`w-100 __header`}>
                        <Container>
                            <Link to="/" className={`navbar-brand`}>
                                <img
                                    src="https://q-cf.bstatic.com/static/img/b26logo/booking_logo_retina/22615963add19ac6b6d715a97c8d477e8b95b7ea.png"
                                    width={160}
                                    alt=""
                                />
                            </Link>
                            <Button className={`btn btn-light btn-sm btn-fa-count`} style={{fontSize: '1rem'}} onClick={() => this.onSetSidebarOpen(false)}>
                                <i className="fas fa-times" />
                            </Button>
                        </Container>
                    </div>
                    <Container>
                        <Nav className="text-center w-100 nav-pills nav-justified mt-3">
                            <div className="nav-item">
                                <Link to="/" className={`nav-link`}>
                                    Главная
                                </Link>
                            </div>
                            <div className="nav-item">
                                <Link to="/search" className={`nav-link`}>
                                    Поиск
                                </Link>
                            </div>
                            <div className="nav-item">
                                <Link to="/tours" className={`nav-link`}>
                                    Туры из Ростова
                                </Link>
                            </div>
                            <Link to="/tours" className={`nav-link`}>
                                Туры из Краснодара
                            </Link>
                            <Link to="/tours" className={`nav-link`}>
                                Все туры
                            </Link>
                            {/*<NavDropdown title="Типы" id="basic-nav-dropdown">*/}
                            {/*    {categories.map((category, idx) =>*/}
                            {/*        <NavDropdown.Item href="#action/3.1" key={idx}>{category.title}</NavDropdown.Item>*/}
                            {/*    )}*/}
                            {/*</NavDropdown>*/}
                            <Link to="/organizations" className={`nav-link`}>
                                Организаторы
                            </Link>
                            <Link to="/directions" className={`nav-link`}>
                                Направления
                            </Link>
                            <Link to="/news" className={`nav-link`}>
                                Новости
                            </Link>
                            <Link to="/articles" className={`nav-link`}>
                                Статьи
                            </Link>
                            {/*<Link to="/contacts" className={`nav-link`}>*/}
                            {/*    Контакты*/}
                            {/*</Link>*/}
                            {/*<Link to="/help" className={`nav-link`}>*/}
                            {/*    Помощь*/}
                            {/*</Link>*/}
                            <Link to="/about" className={`nav-link`}>
                                О нас
                            </Link>
                        </Nav>
                    </Container>
                </div>
                <div>
                    {/*<Button onClick={() => this.onSetSidebarNotificationOpen()} className={`btn btn-light btn-sm btn-fa-count mr-3`} style={{fontSize: '1rem'}}>*/}
                    {/*    <i className="far fa-bell" />*/}
                    {/*</Button>*/}
                    <Button onClick={() => this.onSetSidebarMobileNavigationOpen()} className={`btn btn-light btn-sm btn-fa-count`} style={{fontSize: '1rem'}}>
                        <i className="fas fa-bars" />
                    </Button>
                </div>
                {/*<Sidebar sidebar={<SidebarNavigation/>} open={this.state.sidebarOpen} onSetOpen={this.onSetSidebarOpen} styles={{}}*/}
                {/*         contentClassName={`aw-sidebar-content`}*/}
                {/*         rootClassName={`aw-root-sidebar`}*/}
                {/*         sidebarClassName={`aw-sidebar`}*/}
                {/*         overlayClassName={`aw-sidebar-overlay`}*/}
                {/*         pullRight={true}*/}
                {/*>*/}
                {/*    <Button className={`btn btn-light btn-sm btn-fa-count`} style={{fontSize: '1rem'}} onClick={() => this.onSetSidebarOpen(true)}>*/}
                {/*        <i className="fas fa-bars"/>*/}
                {/*    </Button>*/}
                {/*</Sidebar>*/}
            </React.Fragment>
        );
    }
}
const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
    fetchSidebar: name => dispatch(fetchSidebar(name)),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SidebarContainer));
