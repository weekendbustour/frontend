import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import Nav from 'react-bootstrap/Nav';

class Navigation extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <React.Fragment>
                <Nav className="text-center w-100">
                    <Link to="/search?departure_id=1" className={`nav-link`}>
                        Туры из Ростова
                    </Link>
                    <Link to="/search?departure_id=2" className={`nav-link`}>
                        Туры из Краснодара
                    </Link>
                    <Link to="/tours" className={`nav-link`}>
                        Все туры
                    </Link>
                    {/*<NavDropdown title="Типы" id="basic-nav-dropdown">*/}
                    {/*    {categories.map((category, idx) =>*/}
                    {/*        <NavDropdown.Item href="#action/3.1" key={idx}>{category.title}</NavDropdown.Item>*/}
                    {/*    )}*/}
                    {/*</NavDropdown>*/}
                    <Link to="/organizations" className={`nav-link`}>
                        Организаторы
                    </Link>
                    <Link to="/directions" className={`nav-link`}>
                        Направления
                    </Link>
                    {/*<Link to="/contacts" className={`nav-link`}>*/}
                    {/*    Контакты*/}
                    {/*</Link>*/}
                    <Link to="/about" className={`nav-link`}>
                        О нас
                    </Link>
                </Nav>
            </React.Fragment>
        );
    }
}

export default Navigation;
