import React, {Component} from 'react';
import {connect} from 'react-redux';
import * as PropTypes from 'prop-types';
import {getDataTitles, getDataTitlesPending} from '../../store/reducers/Data';
import {titlesTypes} from '../../prop/Types/Title';
import TitleDefault from '../../components/Title/TitleDefault';
import {
    TITLE_TOURS,
    TITLE_TOURS_NEAR,
    TITLE_ORGANIZATIONS,
    TITLE_DIRECTIONS,
    TITLE_ARTICLES,
    TITLE_ARTICLES_OTHER,
    TITLE_NEWS,
    TITLE_NEWS_OTHER,
    TITLE_TOURS_DUPLICATE,
    TITLE_TOURS_RESERVATIONS,
    TITLE_ARTICLES_ORGANIZATION,
    TITLE_ARTICLES_ATTRACTIONS,
    TITLE_ATTRACTIONS,
    TITLE_ATTRACTIONS_DIRECTION,
    TITLE_DIRECTIONS_ATTRACTIONS,
    TITLE_ATTRACTIONS_ARTICLES,
    TITLE_ATTRACTIONS_TOUR,
} from '../../lib/titles';

class TitleMain extends Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        role: PropTypes.string,
        title: PropTypes.string,
        model: PropTypes.string,
        titles: titlesTypes,
        titlesPending: PropTypes.bool,
        item: PropTypes.object,
    };

    constructor(props) {
        super(props);
        this.state = {
            role: props.role ?? 'h1',
            title: props.title,
            model: props.model,
            item: props.item ?? null,
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    title() {}

    description(model) {
        const {titles, titlesPending} = this.props;
        if (titlesPending) {
            return null;
        }
        switch (model) {
            case TITLE_TOURS:
                return titles.tour.description;
            case TITLE_TOURS_NEAR:
                return titles.tour.near.description;
            case TITLE_TOURS_DUPLICATE:
                return titles.tour.duplicate.description;
            case TITLE_TOURS_RESERVATIONS:
                return titles.tour.reservations.description;
            case TITLE_ORGANIZATIONS:
                return titles.organization.description;
            case TITLE_DIRECTIONS:
                return titles.direction.description;
            case TITLE_DIRECTIONS_ATTRACTIONS:
                return titles.direction.attractions.description;
            case TITLE_ARTICLES:
                return titles.article.description;
            case TITLE_ARTICLES_OTHER:
                return titles.article.other.description;
            case TITLE_ARTICLES_ORGANIZATION:
                return titles.article.organization.description;
            case TITLE_ARTICLES_ATTRACTIONS:
                return titles.article.attractions.description;
            case TITLE_NEWS:
                return titles.news.description;
            case TITLE_NEWS_OTHER:
                return titles.news.other.description;
            case TITLE_ATTRACTIONS:
                return titles.attraction.description;
            case TITLE_ATTRACTIONS_DIRECTION:
                return titles.attraction.direction.description;
            case TITLE_ATTRACTIONS_ARTICLES:
                return titles.attraction.articles.description;
            case TITLE_ATTRACTIONS_TOUR:
                return titles.attraction.tour.description;
            default:
                return null;
        }
    }
    render() {
        const {role, title, model, item} = this.state;
        const {titlesPending} = this.props;
        if (!this.shouldComponentRender()) return null;
        const description = this.description(model);
        return (
            <React.Fragment>
                <TitleDefault role={role} title={title} description={`${description}${item !== null ? ' ' + item.title : ''}`} pendingDescription={titlesPending} />
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    titles: getDataTitles(state),
    titlesPending: getDataTitlesPending(state),
});
export default connect(mapStateToProps)(TitleMain);
