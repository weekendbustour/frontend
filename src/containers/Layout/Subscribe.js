import React, {Component} from 'react';
import Form from 'react-bootstrap/Form';
import {connect} from 'react-redux';
import {getUser} from '../../store/reducers/User';
import * as PropTypes from "prop-types";
import {userTypes} from "../../prop/Types/User";

class Subscribe extends Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        email: PropTypes.string,
        pending: PropTypes.bool,
        user: userTypes,
    };

    constructor(props) {
        super(props);
        const {user} = this.props;
        this.state = {
            email: user !== null ? user.email : '',
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    handleChange(e) {
        this.setState({
            [e.currentTarget.name]: e.currentTarget.value,
        });
    }

    handleSubmit(e) {
        e.preventDefault();
        //const {email} = this.state;
        //const data = {email: email,};
        //store.dispatch(fetchUserSubscription(data, true));
    }

    render() {
        const {email} = this.state;
        const {pending, user} = this.props;
        return (
            <React.Fragment>
                <Form onSubmit={this.handleSubmit} className={`modal-newsletter`}>
                    <div className="d-flex justify-content-center icon-box">
                        <i className="far fa-envelope-open" />
                    </div>
                    <div>
                        <h4>Подписаться на новости</h4>
                    </div>
                    <div>
                        <p className={`text-center`}>
                            <span>Подпишитесь на рассылку и мы будем Вам присылать самые лучшие туры</span>
                        </p>
                        <div className={`input-group ${user !== null ? 'is-disabled' : ''}`}>
                            <input
                                type="email"
                                className={`form-control ${user !== null ? 'is-disabled' : ''}`}
                                placeholder="Введите свой email"
                                name="email"
                                value={email}
                                required
                                onChange={this.handleChange}
                            />
                            <span className="input-group-btn">
                                <button type="submit" className={`btn btn-primary ${pending ? 'is-loading' : ''}`}>
                                    Подписаться
                                </button>
                            </span>
                        </div>
                    </div>
                </Form>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: getUser(state),
    //pending: getUserSubscriptionPending(state),
});
const mapDispatchToProps = dispatch => ({
    //fetchUserSubscription: () => dispatch(fetchUserSubscription()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Subscribe);
