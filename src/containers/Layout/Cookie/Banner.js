import React, {Component} from 'react';
import Button from 'react-bootstrap/Button';
import {withCookies} from 'react-cookie';
import * as PropTypes from "prop-types";

class Banner extends Component {
    static propTypes = {
        cookies: PropTypes.object,
    };

    constructor(props) {
        super(props);
        const {cookies} = props;
        this.state = {
            accepted: cookies.get('accepts-cookies') !== undefined ? 1 : 0,
        };
        this.handleAcceptCookie = this.handleAcceptCookie.bind(this);
    }

    handleAcceptCookie() {
        const {cookies} = this.props;
        cookies.set('accepts-cookies', 3600, {path: '/'});
        this.setState({
            accepted: 1,
        });
    }

    render() {
        const {accepted} = this.state;
        return (
            <React.Fragment>
                {!accepted && (
                    <div>
                        <div className="cookie-box cookie-box--hide0">
                            <div className={`__description`}>
                                Продолжая использовать наш сайт, вы даете согласие на обработку файлов cookie, которые обеспечивают правильную работу сайта.
                            </div>
                            <div>
                                <Button variant={`primary`} size={`sm`} onClick={() => this.handleAcceptCookie()}>
                                    Подтверждаю
                                </Button>
                            </div>
                        </div>
                    </div>
                )}
            </React.Fragment>
        );
    }
}

export default withCookies(Banner);
