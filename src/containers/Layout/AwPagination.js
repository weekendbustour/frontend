import React, {Component} from 'react';
import {Link, withRouter} from 'react-router-dom';
import Pagination from 'react-bootstrap/Pagination';
import {connect} from 'react-redux';
import {paginationTypes} from '../../prop/Types/Pagination';
import * as PropTypes from 'prop-types';

class AwPagination extends Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        pagination: paginationTypes,
        url: PropTypes.string,
        onChangePage: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.state = {
            onChangePage: props.onChangePage,
            url: props.url,
            array: [],
        };
        this.handleChangePageNext = this.handleChangePageNext.bind(this);
        this.handleChangePagePrev = this.handleChangePagePrev.bind(this);
        this.handleChangePageCurrent = this.handleChangePageCurrent.bind(this);
    }

    componentDidMount(): void {
        const {location, pagination} = this.props;
        let array = [];
        const len = Math.ceil(pagination.total / pagination.limit);
        let i = 0;
        while (++i <= len) {
            array.push(i);
        }
        // eslint-disable-next-line react/no-did-mount-set-state
        this.setState({array});

        let searchUrl = location.search;
        let urlData = new URLSearchParams(searchUrl);
        console.log(location.pathname, '?' + urlData.toString());
        if (urlData.has('page')) {
            urlData.delete('page');
        }
        urlData.append('page', '');
        // eslint-disable-next-line react/no-did-mount-set-state
        this.setState({
            url: location.pathname + '?' + urlData.toString(),
        });
    }
    handleChangePageNext() {
        const {pagination} = this.props;
        this.props.onChangePage(pagination.page + 1);
    }

    handleChangePagePrev() {
        const {pagination} = this.props;
        this.props.onChangePage(pagination.page - 1);
    }

    handleChangePageCurrent(page) {
        this.props.onChangePage(page);
    }

    render() {
        const {pagination} = this.props;
        const {url} = this.state;
        const {array} = this.state;
        return (
            <React.Fragment>
                <Pagination className={`pagination-primary`}>
                    <li className={`page-item ${pagination.page === 1 ? 'disabled' : ''}`}>
                        {pagination.page === 1 ? (
                            <span className={`page-link disabled`}>
                                <i className="fas fa-angle-left" />
                            </span>
                        ) : (
                            <Link to={`${url}${pagination.page - 1}`} className={`page-link`} onClick={this.handleChangePagePrev}>
                                <i className="fas fa-angle-left" />
                            </Link>
                        )}
                    </li>
                    {array.map((page, key) => (
                        <li className={`page-item ${pagination.page === page ? 'active' : ''}`} key={key}>
                            {pagination.page === page ? (
                                <span className={`page-link disabled`}>{page}</span>
                            ) : (
                                <Link to={`${url}${page}`} className={`page-link`} onClick={() => this.handleChangePageCurrent(page)}>
                                    {page}
                                </Link>
                            )}
                        </li>
                    ))}
                    <li className={`page-item ${pagination.page === Math.ceil(pagination.total / pagination.limit) ? 'disabled' : ''}`}>
                        {pagination.page === Math.ceil(pagination.total / pagination.limit) ? (
                            <span className={`page-link disabled`}>
                                <i className="fas fa-angle-right" />
                            </span>
                        ) : (
                            <Link to={`${url}${pagination.page + 1}`} className={`page-link`} onClick={this.handleChangePageNext}>
                                <i className="fas fa-angle-right" />
                            </Link>
                        )}
                    </li>
                </Pagination>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AwPagination));
