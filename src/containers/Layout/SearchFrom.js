import React from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import InputGroup from 'react-bootstrap/InputGroup';
import Col from 'react-bootstrap/Col';
import {Typeahead} from 'react-bootstrap-typeahead';
import Select from 'react-select';
import 'react-dates/initialize';
import {DateRangePicker} from 'react-dates';
import store from '../../store';
import {getDataSearchCategories, getDataSearchDepartures, getDataSearchDirections, getDataSearchOrganizations} from '../../store/reducers/Data';
import {fetchSearch} from '../../store/reducers/Search';
import {withRouter} from 'react-router-dom';
import queryString from 'query-string';
import {connect} from 'react-redux';
import {isMobile} from 'react-device-detect';
import {scrollTo} from '../../lib/scrollTo';
import * as PropTypes from 'prop-types';
import map from 'lodash/map';
import each from 'lodash/each';
import indexOf from 'lodash/indexOf';

const moment = require('moment');
//const _ = require('lodash');
require('moment/locale/ru');

class SearchFrom extends React.Component {
    _isMounted = false;

    static propTypes = {
        isSearch: PropTypes.bool,
        searchDirections: PropTypes.array,
        searchOrganizations: PropTypes.array,
        searchCategories: PropTypes.array,
        searchDepartures: PropTypes.array,
    };

    constructor(props) {
        super(props);
        const params = queryString.parse(props.location.search);
        let category_id = [];
        // eslint-disable-next-line array-callback-return
        map(params, function(value, key) {
            if (key === 'category_id[]') {
                if (value.isArray) {
                    value.map(valueFilter => category_id.push(parseInt(valueFilter)));
                } else {
                    category_id.push(parseInt(value));
                }
            }
        });
        if (params.start_at !== undefined) {
            params.start_at_moment = moment(params.start_at, 'DD.MM.YYYY');
        }
        if (params.finish_at !== undefined) {
            params.finish_at_moment = moment(params.finish_at, 'DD.MM.YYYY');
        }
        this.state = {
            isSearch: props.isSearch ?? false,
            selectedOption: null,
            date: moment().locale('ru'),
            startDate: params.start_at_moment,
            endDate: params.finish_at_moment,
            search: {
                sale: false,
                directions: [],
                categories: [],
                organizations: [],
            },
            extended: params.extended !== undefined ? parseInt(params.extended) === 1 : false,

            category_id: category_id || null,
            departure_id: params.departure_id || null,
            direction_id: params.direction_id || null,
            organization_id: params.organization_id || null,
            price_from: params.price_from || '',
            price_to: params.price_to || '',
            start_at: params.start_at || '',
            finish_at: params.finish_at || '',
        };
        this.handleDateChange = this.handleDateChange.bind(this);
        this.handleDirectionChange = this.handleDirectionChange.bind(this);
        this.handleOrganizationChange = this.handleOrganizationChange.bind(this);
        this.handleDepartureChange = this.handleDepartureChange.bind(this);
        this.handleCategoryChange = this.handleCategoryChange.bind(this);
        this.handlePriceChange = this.handlePriceChange.bind(this);
        this.handleDatePhoneChange = this.handleDatePhoneChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleSelectChange = this.handleSelectChange.bind(this);
        this.isChanged = this.isChanged.bind(this);
    }

    isChanged() {
        const {category_id, departure_id, direction_id, organization_id, startDate, endDate, price_from, price_to, start_at, finish_at} = this.state;
        if (
            category_id.length !== 0 ||
            departure_id !== null ||
            direction_id !== null ||
            organization_id !== null ||
            price_from !== '' ||
            price_to !== '' ||
            start_at !== '' ||
            finish_at !== '' ||
            startDate !== undefined ||
            endDate !== undefined
        ) {
            return true;
        }
        return false;
    }

    handleChange = selectedOption => {
        this.setState({selectedOption}, () => console.log(`Option selected:`, this.state.selectedOption));
    };

    toggleExtended() {
        const {extended} = this.state;
        this.setState({
            extended: !extended,
        });
        //store.dispatch(startAsyncAction());
    }

    searchClear() {
        this.setState({
            departure_id: null,
            category_id: [],
            direction_id: null,
            organization_id: null,
            price_from: '',
            price_to: '',
            start_at: '',
            finish_at: '',
            startDate: null,
            endDate: null,
        });
    }

    componentWillMount() {
        this._isMounted = true;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    getDataForSearch() {
        const {category_id} = this.state;
        const {departure_id} = this.state;
        const {direction_id} = this.state;
        const {organization_id} = this.state;
        const {startDate} = this.state;
        const {endDate} = this.state;
        const {price_from} = this.state;
        const {price_to} = this.state;
        const {extended} = this.state;
        const {start_at} = this.state;
        const {finish_at} = this.state;

        let data = {};
        if (direction_id !== null && direction_id !== '') {
            data.direction_id = direction_id;
        }
        if (category_id !== null && Object.keys(category_id).length !== 0) {
            data.category_id = category_id;
        }
        if (organization_id !== null && organization_id !== '') {
            data.organization_id = organization_id;
        }
        if (departure_id !== null && departure_id !== '') {
            data.departure_id = departure_id;
        }
        if (startDate !== null && startDate !== undefined && startDate._isValid) {
            data.start_at = startDate.format('DD.MM.YYYY');
        } else if (start_at !== '') {
            data.start_at = start_at;
        }
        if (endDate !== null && endDate !== undefined && endDate._isValid) {
            data.finish_at = endDate.format('DD.MM.YYYY');
        } else if (finish_at !== '') {
            data.finish_at = finish_at;
        }
        if (price_from !== null && price_from !== '') {
            data.price_from = price_from;
        }
        if (price_to !== null && price_to !== '') {
            data.price_to = price_to;
        }
        data.extended = extended ? 1 : 0;
        return data;
    }

    handleDirectionChange(selected) {
        this.setState({
            direction_id: selected !== null ? selected.id : null,
        });
    }

    handleCategoryChange(selected) {
        let array = [];
        each(selected, function(value, key) {
            array.push(value.id);
        });
        this.setState({
            category_id: array,
        });
    }

    handleDepartureChange(selected) {
        this.setState({
            departure_id: selected !== null ? selected.id : null,
        });
    }

    handleOrganizationChange(selected) {
        this.setState({
            organization_id: selected !== null ? selected.id : null,
        });
    }

    onChange = date => this.setState({date});

    handlePriceChange(e) {
        this.setState({[e.currentTarget.name]: e.currentTarget.value});
    }

    handleDatePhoneChange(e) {
        this.setState({[e.currentTarget.name]: e.currentTarget.value});
    }

    handleDateChange(date) {
        this.setState({
            startDate: date.startDate,
            endDate: date.endDate,
        });
    }

    handleSelectChange(e) {
        this.setState({[e.currentTarget.name]: e.currentTarget.value});
    }

    handleSubmit(e) {
        e.preventDefault();
        const {history} = this.props;

        let data = this.getDataForSearch();
        let urlData = new URLSearchParams(data);
        urlData.forEach(function(value, key) {
            //console.log(value, key);
            if (key === 'category_id') {
                const array = value.split(',');
                array.map((mapValue, mapKey) => urlData.append('category_id[]', mapValue));
            }
        });
        if (urlData.has('category_id')) {
            urlData.delete('category_id');
        }
        history.push({
            pathname: '/search',
            search: '?' + urlData.toString(),
        });
        store.dispatch(fetchSearch(data));
        scrollTo.selectorMobile('.breadcrumb');
    }

    onDatesChange = (start, end) =>
        this.setState({
            startDate: start,
            endDate: end,
        });

    render() {
        const {searchDirections, searchOrganizations, searchCategories, searchDepartures} = this.props;
        const {extended, focusedInput} = this.state;
        const {direction_id, organization_id, category_id, departure_id, price_from, price_to} = this.state;
        const {startDate, endDate, start_at, finish_at} = this.state;
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <Form onSubmit={this.handleSubmit}>
                    <Row className={`justify-content-md-center`}>
                        <Col xs={`12`} sm={`12`} md={`3`} lg={`3`}>
                            <Form.Group controlId="exampleForm.ControlSelect1">
                                <Form.Label className={`text-muted ml-2 mb-0`}>Откуда</Form.Label>
                                {isMobile ? (
                                    <Form.Control as="select" name={`departure_id`} value={departure_id ?? 0} onChange={this.handleSelectChange}>
                                        <option disabled={true} value={0}>
                                            Выберите
                                        </option>
                                        {searchDepartures.map((departure, idx) => (
                                            <option key={idx} value={departure.id}>
                                                {departure.label}
                                            </option>
                                        ))}
                                    </Form.Control>
                                ) : (
                                    <Select
                                        classNamePrefix={`react-select`}
                                        className={`react-select-container`}
                                        onChange={this.handleDepartureChange}
                                        options={searchDepartures}
                                        placeholder={`Выберите`}
                                        isClearable={true}
                                        value={searchDepartures.filter(value => {
                                            return value.id === parseInt(departure_id);
                                        })}
                                    />
                                )}
                            </Form.Group>
                        </Col>
                        <Col xs={`12`} sm={`12`} md={`3`} lg={`3`}>
                            <Form.Group controlId="exampleForm.ControlSelect2">
                                <Form.Label className={`text-muted ml-2 mb-0`}>Куда</Form.Label>
                                {isMobile ? (
                                    <Form.Control as="select" name={`direction_id`} value={direction_id ?? 0} onChange={this.handleSelectChange}>
                                        <option disabled={true} value={0}>
                                            Выберите
                                        </option>
                                        <option value={``}>Ничего не выбрано</option>
                                        {searchDirections.map((direction, idx) => (
                                            <option key={idx} value={direction.id}>
                                                {direction.label}
                                            </option>
                                        ))}
                                    </Form.Control>
                                ) : (
                                    <Select
                                        classNamePrefix={`react-select`}
                                        className={`react-select-container`}
                                        onChange={this.handleDirectionChange}
                                        options={searchDirections}
                                        placeholder={`Выберите`}
                                        isClearable={true}
                                        value={searchDirections.filter(value => {
                                            return value.id === parseInt(direction_id);
                                        })}
                                    />
                                )}
                            </Form.Group>
                        </Col>
                        <Col xs={`12`} sm={`12`} md={`3`} lg={`3`}>
                            <Form.Group controlId="exampleForm.ControlSelect3">
                                <Form.Label className={`text-muted ml-2 mb-0`}>Дата</Form.Label>
                                {isMobile ? (
                                    <InputGroup>
                                        <Form.Control type="date" placeholder="Дата С" value={start_at} name="start_at" onChange={this.handleDatePhoneChange} />
                                        <Form.Control
                                            type="date"
                                            placeholder="Дата До"
                                            value={finish_at}
                                            name="finish_at"
                                            onChange={this.handleDatePhoneChange}
                                        />
                                    </InputGroup>
                                ) : (
                                    <DateRangePicker
                                        startDate={startDate} // momentPropTypes.momentObj or null,
                                        startDateId="your_unique_start_date_id" // PropTypes.string.isRequired,
                                        endDate={endDate} // momentPropTypes.momentObj or null,
                                        endDateId="your_unique_end_date_id" // PropTypes.string.isRequired,
                                        onDatesChange={datePickerValue => this.handleDateChange(datePickerValue)} // PropTypes.func.isRequired,
                                        focusedInput={focusedInput} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
                                        onFocusChange={focused => this.setState({focusedInput: focused})} // PropTypes.func.isRequired,
                                        startDatePlaceholderText={`Дата С`}
                                        endDatePlaceholderText={`Дата До`}
                                        firstDayOfWeek={1}
                                        noBorder={true}
                                        small={true}
                                        anchorDirection={`right`}
                                        hideKeyboardShortcutsPanel={true}
                                        // renderMonthText={(month) => {
                                        //     return month.locale('ru').format('MMMM');
                                        // }}
                                        //renderWeekHeaderElement={{}}
                                    />
                                )}
                            </Form.Group>
                        </Col>
                        {extended && isMobile ? (
                            ''
                        ) : (
                            <Col xs={`12`} sm={`12`} md={`3`} lg={`3`} className={`text-left ${isMobile ? 'mb-3' : ''}`}>
                                <Button type="submit" variant="primary" className={`btn-block`} style={{marginTop: '24px'}}>
                                    Найти
                                </Button>
                            </Col>
                        )}
                    </Row>
                    {extended && (
                        <Row>
                            <Col xs={`12`} sm={`12`} md={`6`} lg={`6`} className={``}>
                                <Form.Group>
                                    <Form.Label className={`text-muted ml-2 mb-0`}>Категория</Form.Label>
                                    {isMobile ? (
                                        <Form.Control as="select" name={`category_id`} value={category_id ?? []} onChange={this.handleSelectChange} multiple>
                                            <option disabled={true} value={0}>
                                                Выберите
                                            </option>
                                            <option value={``}>Ничего не выбрано</option>
                                            {searchCategories.map((category, idx) => (
                                                <option key={idx} value={category.id}>
                                                    {category.label}
                                                </option>
                                            ))}
                                        </Form.Control>
                                    ) : (
                                        <Typeahead
                                            onChange={this.handleCategoryChange}
                                            id={`category_typeahead`}
                                            value={category_id}
                                            multiple
                                            labelKey="label"
                                            options={searchCategories}
                                            placeholder={`Выберите`}
                                            clearButton
                                            selected={searchCategories.filter(value => {
                                                return indexOf(category_id, value.id) !== -1;
                                            })}
                                        />
                                    )}
                                </Form.Group>
                            </Col>
                            <Col xs={`12`} sm={`12`} md={`6`} lg={`3`}>
                                <Form.Group>
                                    <Form.Label className={`text-muted ml-2 mb-0`}>Цена</Form.Label>
                                    <InputGroup>
                                        <Form.Control
                                            type="text"
                                            placeholder="От"
                                            className={`cleave-mask--number`}
                                            value={price_from}
                                            name="price_from"
                                            onChange={this.handlePriceChange}
                                            data-length={6}
                                        />
                                        <Form.Control
                                            type="text"
                                            placeholder="До"
                                            className={`cleave-mask--number`}
                                            value={price_to}
                                            name="price_to"
                                            onChange={this.handlePriceChange}
                                            data-length={6}
                                        />
                                    </InputGroup>
                                </Form.Group>
                            </Col>
                        </Row>
                    )}
                    {extended && (
                        <Row>
                            <Col xs={`12`} sm={`12`} md={`6`} lg={`6`}>
                                <Form.Group controlId="exampleForm.ControlSelect1">
                                    <Form.Label className={`text-muted ml-2 mb-0`}>Организатор</Form.Label>
                                    {isMobile ? (
                                        <Form.Control as="select" name={`organization_id`} value={organization_id ?? 0} onChange={this.handleSelectChange}>
                                            <option disabled={true} value={0}>
                                                Выберите
                                            </option>
                                            {searchOrganizations.map((organization, idx) => (
                                                <option key={idx} value={organization.id}>
                                                    {organization.label}
                                                </option>
                                            ))}
                                        </Form.Control>
                                    ) : (
                                        <Select
                                            classNamePrefix={`react-select`}
                                            className={`react-select-container`}
                                            onChange={this.handleOrganizationChange}
                                            options={searchOrganizations}
                                            placeholder={`Выберите`}
                                            isClearable={true}
                                            value={searchOrganizations.filter(value => {
                                                return value.id === parseInt(organization_id);
                                            })}
                                        />
                                    )}
                                </Form.Group>
                            </Col>
                        </Row>
                    )}
                    {extended && (
                        <Row>
                            {/*<Col xs={`12`} sm={`12`} md={`6`} lg={`6`} className={`mb-3`}>*/}
                            {/*    <Form.Group style={{marginBottom: '-7px'}}>*/}
                            {/*        <Button variant="outline-light">*/}
                            {/*            <Form.Check custom inline label="Только со скидкой" type={`checkbox`} id={`custom-inline-primary-1`} />*/}
                            {/*        </Button>*/}
                            {/*    </Form.Group>*/}
                            {/*</Col>*/}
                            {isMobile && (
                                <Col xs={`12`} sm={`12`} md={`6`} lg={`6`} className={`text-left mb-3`}>
                                    <Button type="submit" variant="primary" className={`btn-block`} style={{marginTop: '24px'}}>
                                        Найти
                                    </Button>
                                </Col>
                            )}
                        </Row>
                    )}
                    <Row>
                        <Col className={`text-center text-sm-left`}>
                            <Button variant="light-dark" size={`sm`} className={`mt-2 mb-2`} onClick={() => this.toggleExtended()}>
                                {extended && (
                                    <span>
                                        <i className="fas fa-angle-up" />
                                        &#160; Свернуть поиск
                                    </span>
                                )}
                                {!extended && (
                                    <span>
                                        <i className="fas fa-angle-down" />
                                        &#160; Расширенный поиск
                                    </span>
                                )}
                            </Button>
                            {this.isChanged() && (
                                <Button variant="light-dark" size={`sm`} className={`mt-2 mb-2`} onClick={() => this.searchClear()}>
                                    <i className="fas fa-times" />
                                    &#160; Очистить поиск
                                </Button>
                            )}
                        </Col>
                    </Row>
                </Form>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    searchDirections: getDataSearchDirections(state),
    searchCategories: getDataSearchCategories(state),
    searchOrganizations: getDataSearchOrganizations(state),
    searchDepartures: getDataSearchDepartures(state),
});
export default withRouter(connect(mapStateToProps)(SearchFrom));
