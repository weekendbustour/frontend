import React, {Component} from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Nav from 'react-bootstrap/Nav';
import {Link, withRouter} from 'react-router-dom';
import {getDataSearchCategories, getDataSearchDirections} from '../../store/reducers/Data';
import {connect} from 'react-redux';
import {fetchModal} from '../../store/reducers/Modal';
import {isMobile} from 'react-device-detect';
import {ReactSVG} from 'react-svg';
import svgVk from '../../assets/svg/social/vk.svg';
import svgInstagram from '../../assets/svg/social/instagram.svg';
import logo from "../../assets/logo/v2-152x152.png";

class Footer extends Component {
    _isMounted = false;
    state = {};

    constructor(props) {
        super(props);
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        if (this._isMounted && !isMobile && ['/lk/account'].indexOf(this.props.location.pathname) === -1) {
            return true;
        }
        return false;
    }

    render() {
        if (!this.shouldComponentRender()) return <div style={{height: '100px'}} />;
        return (
            <React.Fragment>
                <footer className="bd-footer text-muted">
                    <div className={`pre-footer`}/>
                    <Container className={`pb-3 pt-3`}>
                        <Row>
                            <Col xs={12} md={12} lg={3} className={`mb-3`}>
                                <Link to="/" className={`navbar-brand logo-header logo-flat`} style={{lineHeight: '.5'}}>
                                    <div className={`d-flex`}>
                                        <div className={`logo-header-container`}>
                                            <img src={logo} width={`40px`} alt=""/>
                                        </div>
                                        <div className={`logo-header-title`}>
                                            <h1>{process.env.REACT_APP_NAME}</h1>
                                            {/*<p>{process.env.REACT_APP_NAME_DESCRIPTION}</p>*/}
                                        </div>
                                    </div>
                                    {/*<h1 className={`mb-0`}>{process.env.REACT_APP_NAME}</h1>*/}
                                    {/*<small style={{fontSize: '12px'}}>{process.env.REACT_APP_NAME_DESCRIPTION}</small>*/}
                                    {/*<img*/}
                                    {/*    src="http://laravel.weekendbustour.test/img/weebus%20lol%20v1.2.png"*/}
                                    {/*    width={160}*/}
                                    {/*    alt=""*/}
                                    {/*/>*/}
                                    {/*<br/>*/}
                                </Link>

                                {/*<Link to="/" className={`navbar-brand d-flex align-items-center h-100`}>*/}
                                {/*    <img src="https://q-cf.bstatic.com/static/img/b26logo/booking_logo_retina/22615963add19ac6b6d715a97c8d477e8b95b7ea.png" width={160} alt=""/>*/}
                                {/*</Link>*/}
                            </Col>
                            <Col xs={12} md={12} lg={6} className={`mb-3`}>
                                <Nav className={`d-flex align-items-center h-100 justify-content-end`}>
                                    {/*<Link to="/help?tab=offer" className={`nav-item mr-3`}>*/}
                                    {/*    Сотрудничество*/}
                                    {/*</Link>*/}
                                    <Link to="/terms" className={`nav-item mr-3`}>
                                        Условия использования сайта
                                    </Link>
                                    <Link to="/help" className={`nav-item mr-3`}>
                                        Помощь
                                    </Link>
                                    <Link to="/about" className={`nav-item mr-3`}>
                                        О нас
                                    </Link>
                                </Nav>
                            </Col>
                            <Col xs={12} md={12} lg={3} className={`mb-3`}>
                                <div className={`d-flex align-items-center h-100`}>
                                    <a href={process.env.REACT_APP_SOCIAL_VK} role={`button`} className="social mr-3" target={`_blank`}>
                                        <ReactSVG src={svgVk} className={`svg-social d-inline`} />
                                    </a>
                                    <a href={process.env.REACT_APP_SOCIAL_INSTAGRAM} role={`button`} className="social" target={`_blank`}>
                                        <ReactSVG src={svgInstagram} className={`svg-social d-inline`} />
                                    </a>
                                </div>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={12} md={12}>
                                <p>
                                    Наша платформа помогает увидеть как можно больше красот, которые находятся рядом. Мы помогаем найти пользователю комфортный для себя тур от качественных организаторов.
                                    <br/>
                                    Если мы нарушаем ваши права, напишите нам на почту <a href="mailto:weekendbustour@yandex.ru">weekendbustour@yandex.ru</a>
                                </p>
                            </Col>
                            {process.env.REACT_APP_YANDEX_RATING_ENABLED === 'true' &&
                            <Col xs={12}>
                                <iframe src={'https://yandex.ru/sprav/widget/rating-badge/187928453495'} title="yandex-sprav" width="150" height="50" frameBorder="0"/>
                            </Col>
                            }
                        </Row>
                        {/*<Row>*/}
                        {/*    <Col xs={12} md={6}>*/}
                        {/*        <p>*/}
                        {/*            <small>*/}
                        {/*                Если мы нарушаем ваши права, напишите нам на почту <a href="mailto:weekendbustour@yandex.ru">weekendbustour@yandex.ru</a>*/}
                        {/*            </small>*/}
                        {/*        </p>*/}
                        {/*    </Col>*/}
                        {/*    <Col xs={12} md={6}>*/}
                        {/*        <Trend*/}
                        {/*            smooth*/}
                        {/*            autoDraw*/}
                        {/*            autoDrawDuration={3000}*/}
                        {/*            autoDrawEasing="ease-out"*/}
                        {/*            data={[0, 2, 5, 9, 5, 10, 3, 5, 0, 0, 1, 8, 2, 9, 0, 0, 2, 3, 9, 6, 14, 3, 5, 0, 0, 1, 8, 2, 9, 0, 7, 3, 2, 6, 0]}*/}
                        {/*            gradient={['#f95858']}*/}
                        {/*            radius={1}*/}
                        {/*            strokeWidth={1}*/}
                        {/*            strokeLinecap={'butt'}*/}
                        {/*            height={50}*/}
                        {/*        />*/}
                        {/*    </Col>*/}
                        {/*</Row>*/}
                    </Container>
                    <Container className={`text-center pb-3 pt-3`}>
                        <small>ALL MATERIAL © WEEKENDBUSTOUR 2020 // ALL RIGHTS RESERVED</small>
                    </Container>
                </footer>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    searchCategories: getDataSearchCategories(state),
    searchDirections: getDataSearchDirections(state),
});
const mapDispatchToProps = dispatch => ({
    fetchModal: (name) => dispatch(fetchModal(name)),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Footer));
