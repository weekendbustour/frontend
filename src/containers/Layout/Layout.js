import React, {Component, Suspense} from 'react';
import {Redirect, Route, Switch} from 'react-router-dom';
import routes from '../../routes';
import Navigation from './Navigation';
import Footer from './Footer';
import Header from './Header';
import MobileTabs from './MobileTabs';
import {ProgressBarProvider} from 'react-redux-progress';
import {connect} from 'react-redux';
import ScrollUpButton from 'react-scroll-up-button';
import {isMobile} from 'react-device-detect';
import {fetchData, fetchDataWithoutRequest} from '../../store/actions/Data';
import {getDataPending, hasDataSearch} from '../../store/reducers/Data';
import {fetchUser, fetchUserTokenSuccess, fetchUserWithoutRequest} from '../../store/reducers/User';
import LoginModal from '../Auth/Modal/LoginModal';
import RegisterModal from '../Auth/Modal/RegisterModal';
import SubscriptionModal from './../Modal/Subscription/SubscriptionModal';
import HtmlHead from './../Html/Head';
import store from '../../store';
import ForgetPasswordModal from '../Auth/Modal/ForgetPasswordModal';
import ReservationModal from '../Modal/Tour/ReservationModal';
import * as PropTypes from 'prop-types';
import SidebarMobileNavigation from '../Sidebar/App/SidebarMobileNavigation';
import {getSidebar} from '../../store/reducers/Sidebar';
import {fetchSidebar} from '../../store/actions/Sidebar';
import AuthIsComingModal from "../Auth/Modal/AuthIsComingModal";

class Layout extends Component {
    _isMounted = false;

    static propTypes = {
        pending: PropTypes.bool,
        isProgressActive: PropTypes.bool,
        hasDataSearch: PropTypes.bool,
        fetchUser: PropTypes.func,
        fetchData: PropTypes.func,
        fetchSidebar: PropTypes.func,
        fetchUserTokenSuccess: PropTypes.func,
        sidebar: PropTypes.string,
    };

    constructor(props) {
        super(props);
        this.state = {};
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentDidUpdate(prevProps) {
        const {sidebar, fetchSidebar} = this.props;
        if (sidebar !== null && prevProps.sidebar !== null) {
            // @todo после загрузки срабатывает опять и закрывается sidebar после клика
            fetchSidebar(null);
        }
    }

    componentDidMount() {
        const {fetchUser, hasDataSearch, fetchData, location, fetchUserTokenSuccess} = this.props;
        if (window.user !== undefined) {
            store.dispatch(fetchUserWithoutRequest(window.user.data));
        } else {
            let urlData = new URLSearchParams(location.search);
            if (urlData.has('token')) {
                fetchUserTokenSuccess(urlData.get('token'));
                window.location.href = '/';
            } else {
                fetchUser();
            }
        }
        if (window.project !== undefined) {
            store.dispatch(fetchDataWithoutRequest(window.project.data));
        }
        if (!hasDataSearch) {
            fetchData();
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {isProgressActive} = this.props;
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <div className={`app`}>
                    <HtmlHead />
                    <ProgressBarProvider isActive={isProgressActive} color="#fff" />
                    <div>
                        <Header />
                        <Navigation />

                        <Suspense fallback={undefined}>
                            <Switch>
                                {routes.map((route, idx) => {
                                    return route.component ? (
                                        <Route
                                            key={idx}
                                            path={route.path}
                                            exact={route.exact}
                                            name={route.name}
                                            render={props => <route.component {...props} />}
                                        />
                                    ) : null;
                                })}
                                <Redirect from="/" to="/" />
                            </Switch>
                        </Suspense>
                        <Footer />
                        <MobileTabs />
                        {/*{!isMobile && (*/}
                        {/*    <ScrollUpButton*/}
                        {/*        ContainerClassName="ScrollUpButton--Custom"*/}
                        {/*        TransitionClassName="ScrollUpButtonTransition--Custom"*/}
                        {/*        styles={{}}*/}
                        {/*        AnimationDuration={200}*/}
                        {/*        ShowAtPosition={1000}*/}
                        {/*    >*/}
                        {/*        <div className="pre-scroll-up-button __left" />*/}
                        {/*        <div className="pre-scroll-up-button __right" />*/}
                        {/*        <span>*/}
                        {/*            <i className="fas fa-angle-up" />*/}
                        {/*            Наверх*/}
                        {/*        </span>*/}
                        {/*    </ScrollUpButton>*/}
                        {/*)}*/}
                        {!isMobile && (
                            <ScrollUpButton
                                ContainerClassName="ScrollUpButton--Custom-2"
                                TransitionClassName="ScrollUpButtonTransition--Custom-2"
                                styles={{}}
                                AnimationDuration={200}
                                ShowAtPosition={1000}
                            >
                                <div className="pre-scroll-up-button">
                                    <i className="fas fa-angle-up" />
                                    <span>Наверх</span>
                                </div>
                            </ScrollUpButton>
                        )}
                        <AuthIsComingModal />
                        <LoginModal />
                        <SubscriptionModal />
                        <ReservationModal />
                        <RegisterModal />
                        <ForgetPasswordModal />
                        {/*<CookieBanner />*/}
                        {/*<SidebarNotifications />*/}
                        <SidebarMobileNavigation />
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    sidebar: getSidebar(state),
    isProgressActive: state.progress.isActive,
    pending: getDataPending(state),
    hasDataSearch: hasDataSearch(state),
});

const mapDispatchToProps = dispatch => ({
    fetchData: () => dispatch(fetchData()),
    fetchUser: () => dispatch(fetchUser()),
    fetchSidebar: name => dispatch(fetchSidebar(name)),
    fetchUserTokenSuccess: token => dispatch(fetchUserTokenSuccess(token)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Layout);
