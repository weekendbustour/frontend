import React, {Component} from 'react';
import {connect} from 'react-redux';
import * as PropTypes from 'prop-types';
import Skeleton from "react-loading-skeleton";

class Title extends Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        role: PropTypes.string,
        title: PropTypes.string,
        description: PropTypes.string,
        pendingDescription: PropTypes.bool,
    };

    constructor(props) {
        super(props);
        this.state = {
            role: props.role ?? 'h1',
            title: props.title,
            description: props.description ?? null,
            pendingDescription: props.pendingDescription ?? false,
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {role, title, description, pendingDescription} = this.state;
        if (!this.shouldComponentRender()) return null;
        return (
            <React.Fragment>
                <div className="wrapper">
                    {role === 'h1' && <h1 className="beta uppercase montserrat regular line-after-heading">{title}</h1>}
                    {role === 'h2' && <h2 className="beta uppercase montserrat regular line-after-heading">{title}</h2>}
                    {pendingDescription ? (
                        <Skeleton height={`21px`} width={`200px`} />
                    ) : (
                        <div>
                            {description !== null && <p className="delta cardo regular italic">{description}</p>}
                        </div>
                    )}
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({});

export default connect(mapStateToProps)(Title);
