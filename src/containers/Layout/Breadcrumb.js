import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import * as PropTypes from 'prop-types';

class Breadcrumb extends Component {
    static propTypes = {
        parent: PropTypes.oneOfType([
            PropTypes.shape({
                to: PropTypes.string,
                title: PropTypes.string,
            }),
            PropTypes.instanceOf(null),
        ]),
        push: PropTypes.oneOfType([
            PropTypes.oneOfType([
                PropTypes.shape({
                    to: PropTypes.string,
                    title: PropTypes.string,
                }),
                PropTypes.shape({
                    title: PropTypes.string,
                }),
            ]),
            PropTypes.instanceOf(null),
        ]),
    };

    constructor(props) {
        super(props);
        this.state = {
            parent: props.parent ?? null,
            push: props.push,
        };
    }
    render() {
        const {parent, push} = this.state;
        return (
            <React.Fragment>
                <nav aria-label="breadcrumb">
                    <ol className="breadcrumb">
                        <li className="breadcrumb-item">
                            <Link to="/">Главная</Link>
                        </li>
                        {parent !== null && (
                            <li className="breadcrumb-item">
                                <Link to={parent.to}>{parent.title}</Link>
                            </li>
                        )}
                        {push !== null && (
                            <li className="breadcrumb-item active">
                                <span className="active">{push.title}</span>
                            </li>
                        )}
                    </ol>
                </nav>
            </React.Fragment>
        );
    }
}

export default Breadcrumb;
