import YandexShare from 'react-yandex-share';
import Button from 'react-bootstrap/Button';
import React from 'react';
import {Tooltip} from 'react-tippy';
import * as PropTypes from 'prop-types';

// Accept children and render it/them
export const TooltipShare = ({title, url, image, description, variant = 'empty-link', size=`sm`}) => (
    <React.Fragment>
        <Tooltip
            theme={`light`}
            position={`top`}
            size={`small`}
            html={
                <div className={`ya-share-container`}>
                    <YandexShare
                        content={{
                            title: title,
                            description: description,
                            url: url,
                            image: image,
                        }}
                        theme={{
                            lang: 'ru',
                            services: 'vkontakte,facebook,twitter,odnoklassniki,pocket,telegram,whatsapp',
                        }}
                    />
                </div>
            }
            // open={true}
            interactive={true}
            animateFill={false}
        >
            <Button size={size} variant={variant}>
                <i className="fas fa-share-alt" />
            </Button>
        </Tooltip>
    </React.Fragment>
);

TooltipShare.propTypes = {
    title: PropTypes.string,
    url: PropTypes.string,
    variant: PropTypes.string,
    size: PropTypes.string,
    image: PropTypes.string,
    description: PropTypes.string,
};
