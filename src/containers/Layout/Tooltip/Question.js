import React from 'react';
import {Tooltip} from 'react-tippy';
import * as PropTypes from 'prop-types';

// Accept children and render it/them
export const TooltipQuestion = ({title}) => (
    <React.Fragment>
        &nbsp;
        <Tooltip title={title} theme={`light`} size={`small`} animateFill={false}>
            <i className="far fa-question-circle" />
        </Tooltip>
        &nbsp;
    </React.Fragment>
);

TooltipQuestion.propTypes = {
    title: PropTypes.string,
};
