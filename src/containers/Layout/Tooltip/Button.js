import React from 'react';
import {Tooltip} from 'react-tippy';
import {isMobile} from 'react-device-detect';
import * as PropTypes from "prop-types";

// Accept children and render it/them
export const TooltipButton = ({title, children}) => (
    <Tooltip title={title} theme={`light`} position={`top`} size={`small`} animateFill={false} disabled={isMobile}>
        {children}
    </Tooltip>
);

TooltipButton.propTypes = {
    title: PropTypes.string,
};
