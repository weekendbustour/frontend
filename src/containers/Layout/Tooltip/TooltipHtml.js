import React from 'react';
import {Tooltip} from 'react-tippy';
import * as PropTypes from 'prop-types';

// Accept children and render it/them
export const TooltipHtml = ({content, text, variation = 'primary'}) => (
    <React.Fragment>
        <Tooltip html={content} theme={`light`} size={`small`} animateFill={false} interactive={true}>
            <span className={`tooltip-text text-${variation}`}>{text}</span>
        </Tooltip>
    </React.Fragment>
);

TooltipHtml.propTypes = {
    content: PropTypes.string,
    text: PropTypes.string,
    variation: PropTypes.string,
};
