import React from 'react';
import {Tooltip} from 'react-tippy';
import * as PropTypes from 'prop-types';

// Accept children and render it/them
export const TooltipText = ({title, text, variation = 'primary'}) => (
    <React.Fragment>
        <Tooltip title={title} theme={`light`} size={`small`} animateFill={false}>
            <span className={`tooltip-text text-${variation}`}>{text}</span>
        </Tooltip>
    </React.Fragment>
);

TooltipText.propTypes = {
    title: PropTypes.string,
    text: PropTypes.string,
    variation: PropTypes.string,
};
