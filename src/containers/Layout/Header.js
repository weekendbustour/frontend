import React from 'react';
import {Link, withRouter} from 'react-router-dom';
import Navbar from 'react-bootstrap/Navbar';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Dropdown from 'react-bootstrap/Dropdown';
import Container from 'react-bootstrap/Container';
import Image from 'react-bootstrap/Image';
import Trend from 'react-trend';
import {Tooltip} from 'react-tippy';
import {fetchModal} from '../../store/reducers/Modal';
import {getUser, fetchUserLogout} from '../../store/reducers/User';
import {connect} from 'react-redux';
import {MODAL_LOGIN, MODAL_REGISTER} from '../../lib/modal';
import {getUserFavourites} from '../../store/reducers/User/Favourite';
import {getUserSubscriptions} from '../../store/reducers/User/Subscription';
import {fetchSidebar} from '../../store/actions/Sidebar';
import * as PropTypes from 'prop-types';
import {userFavouritesTypes, userReservationsTypes, userSubscriptionsTypes, userTypes} from '../../prop/Types/User';
import {getUserReservations} from "../../store/reducers/User/Reservation";
import logo from "../../assets/logo/v2-152x152.png";
import {TooltipButton} from "./Tooltip/Button";

class Header extends React.Component {
    _isMounted = false;

    static propTypes = {
        user: userTypes,
        fetchSidebar: PropTypes.func,
        fetchModal: PropTypes.func,
        fetchUserLogout: PropTypes.func,
        favourites: userFavouritesTypes,
        subscriptions: userSubscriptionsTypes,
        reservations: userReservationsTypes,
    };

    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            sidebarOpen: true,
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.setShow = this.setShow.bind(this);
        this.authLogout = this.authLogout.bind(this);
        this.clickDropDown = this.clickDropDown.bind(this);
        this.onSetSidebarOpen = this.onSetSidebarOpen.bind(this);
    }

    //   you can also use this function on any of your event to open/close the sidebar
    handleSidebarToggle = isOpen => {
        this.setState({isOpen});
    };

    handleClick = itemOptions => {
        /*
            do something with the item you clicked.
            you can also send custom properties of your choice
            in the options array you'll be getting those here
            whenever you click that item
        */
    };

    onSetSidebarOpen = name => {
        const {fetchSidebar} = this.props;
        fetchSidebar(name);
    };

    componentWillMount() {
        this._isMounted = true;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    setShow(value) {
        const {fetchModal} = this.props;
        fetchModal(value);
    }

    authLogout() {
        const {fetchUserLogout} = this.props;
        fetchUserLogout();
    }

    clickDropDown(url) {
        const {history} = this.props;
        history.push(url);
        document.dispatchEvent(new MouseEvent('click'));
    }

    render() {
        const {user, favourites, subscriptions, reservations} = this.props;
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <Navbar expand="lg" variant="dark" bg="primary" className={`d-none d-md-block`}>
                    <Container>
                        <Link to="/" className={`navbar-brand logo-header logo-flat`} style={{lineHeight: '.5'}}>
                            <div className={`d-flex logo-env-${process.env.REACT_APP_ENV}-9`}>
                                <div className={`logo-header-container`}>
                                    <img src={logo} width={`40px`} alt=""/>
                                </div>
                                <div className={`logo-header-title`}>
                                    <h1>{process.env.REACT_APP_NAME}</h1>
                                    <p>{process.env.REACT_APP_NAME_DESCRIPTION}</p>
                                </div>
                            </div>
                            {/*{process.env.REACT_APP_ENV !== 'production' &&*/}
                            {/*<div className={`logo-header-env`}>*/}
                            {/*    {process.env.REACT_APP_ENV}*/}
                            {/*</div>*/}
                            {/*}*/}
                            {/*<h1 className={`mb-0`}>{process.env.REACT_APP_NAME}</h1>*/}
                            {/*<small style={{fontSize: '12px'}}>{process.env.REACT_APP_NAME_DESCRIPTION}</small>*/}
                            {/*<img*/}
                            {/*    src="http://laravel.weekendbustour.test/img/weebus%20lol%20v1.2.png"*/}
                            {/*    width={160}*/}
                            {/*    alt=""*/}
                            {/*/>*/}
                            {/*<br/>*/}
                        </Link>
                        <div style={{height: `40px`}}>
                            <Trend
                                height={40}
                                smooth
                                autoDraw
                                autoDrawDuration={3000}
                                autoDrawEasing="ease-out"
                                data={[0, 2, 5, 9, 5, 10, 3, 5, 0, 0, 1, 8, 2, 9, 0, 0, 2, 3, 9, 6, 14, 3, 5, 0, 0, 1, 8, 2, 9, 0, 7, 3, 2, 6, 0]}
                                gradient={['#fff']}
                                radius={1}
                                strokeWidth={1}
                                strokeLinecap={'butt'}
                            />
                        </div>
                        {/*<Nav className="mr-auto">*/}
                        {/*    <Nav.Link href="#home">*/}
                        {/*        Туры из Ростова*/}
                        {/*    </Nav.Link>*/}
                        {/*    <Nav.Link href="#features">*/}
                        {/*        Туры из Краснодара*/}
                        {/*    </Nav.Link>*/}
                        {/*    <Nav.Link href="#pricing">*/}
                        {/*        Организаторы*/}
                        {/*    </Nav.Link>*/}
                        {/*    <Nav.Link href="#pricing">*/}
                        {/*        О нас*/}
                        {/*    </Nav.Link>*/}
                        {/*</Nav>*/}

                        {user === null && (
                            <Form inline>
                                <Link to={`/search`} className={`btn btn-light btn-sm`}>
                                    {/*<i className="fas fa-search"/>&#160;*/}
                                    Поиск
                                </Link>
                                <Button variant={`light`} size={`sm`} onClick={() => this.setShow(MODAL_LOGIN)}>
                                    Вход
                                </Button>
                                <Button variant={`light`} size={`sm`} onClick={() => this.setShow(MODAL_REGISTER)}>
                                    Регистрация
                                </Button>
                                {/*<Button variant={`light`} onClick={() => this.setShow(true, 'register')}>*/}
                                {/*    Зарегистрироваться*/}
                                {/*</Button>*/}
                                {/*<Button variant={`light`} onClick={() => this.setShow(true, 'login')}>*/}
                                {/*    Войти в аккаунт*/}
                                {/*</Button>*/}
                            </Form>
                        )}
                        {user !== null && (
                            <Form inline>
                                <Link to={`/search`} className={`btn btn-light btn-sm`}>
                                    {/*<i className="fas fa-search"/>&#160;*/}
                                    Поиск
                                </Link>
                                <Dropdown>
                                    <Dropdown.Toggle variant="light" id="dropdown-basic" size="sm" onSelect={() => null}>
                                        <Image src={user.image} roundedCircle width={30} height={30} alt="Generic placeholder" className="mr-3" />
                                    </Dropdown.Toggle>
                                    <Dropdown.Menu className={`dropdown-sm`}>
                                        <Dropdown.Header>
                                            <h6 className={`mb-0`}>{user.first_name}</h6>
                                            <p className={`text-muted mb-0`}>{user.email}</p>
                                        </Dropdown.Header>
                                        <Dropdown.Divider />
                                        <button type={`button`} className={`dropdown-item`} onClick={() => this.clickDropDown('/lk/account')}>
                                            Профиль
                                        </button>
                                        <button type={`button`} className={`dropdown-item`} onClick={() => this.clickDropDown('/lk/reservations')}>
                                            Забронированные туры
                                        </button>
                                        <button type={`button`} className={`dropdown-item`} onClick={() => this.clickDropDown('/lk/favourites')}>
                                            Избранное
                                        </button>
                                        <button type={`button`} className={`dropdown-item`} onClick={() => this.clickDropDown('/lk/subscriptions')}>
                                            Подписки
                                        </button>
                                        <Dropdown.Divider />
                                        <button className={`dropdown-item`} onClick={() => this.authLogout()}>
                                            Выйти
                                        </button>
                                    </Dropdown.Menu>
                                </Dropdown>

                                <Tooltip title="Посмотреть Забронированные туры" theme={`light`} position={`bottom`} size={`small`}>
                                    <Link to={`/lk/reservations`} className={`btn btn-light btn-sm btn-fa-count`} style={{fontSize: '1rem'}}>
                                        <i className="fas fa-bus" />
                                        {reservations !== null && reservations.tours.length !== 0 && (
                                            <span>{reservations.tours.length}</span>
                                        )}
                                    </Link>
                                </Tooltip>
                                <Tooltip title="Посмотреть Избранные" theme={`light`} position={`bottom`} size={`small`}>
                                    <Link to={`/lk/favourites`} className={`btn btn-light btn-sm btn-fa-count`} style={{fontSize: '1rem'}}>
                                        <i className="fas fa-heart" />
                                        {favourites !== null && (favourites.tours.length + favourites.organizations.length + favourites.directions.length) !== 0 && (
                                            <span>{favourites.tours.length + favourites.organizations.length + favourites.directions.length}</span>
                                        )}
                                    </Link>
                                </Tooltip>
                                <Tooltip title="Посмотреть Подписки" theme={`light`} position={`bottom`} size={`small`}>
                                    <Link to={`/lk/subscriptions`} className={`btn btn-light btn-sm btn-fa-count`} style={{fontSize: '1rem'}}>
                                        <i className="far fa-envelope" />
                                        {subscriptions !== null && (subscriptions.organizations.length + subscriptions.directions.length) !== 0 && (
                                            <span>{subscriptions.organizations.length + subscriptions.directions.length}</span>
                                        )}
                                    </Link>
                                </Tooltip>
                                {user.isAdmin &&
                                    <TooltipButton title={`Перейти в админку`}>
                                        <a href="/admin" className={`btn btn-light btn-sm`} style={{fontSize: '1rem'}}>
                                            <i className="fas fa-tools"/>
                                        </a>
                                    </TooltipButton>
                                }
                                {/*<Tooltip title="Уведомления" theme={`light`} position={`bottom`} size={`small`}>*/}
                                {/*    <Button*/}
                                {/*        onClick={() => this.onSetSidebarOpen(SIDEBAR_NOTIFICATIONS)}*/}
                                {/*        className={`btn btn-light btn-sm btn-fa-count`}*/}
                                {/*        style={{fontSize: '1rem'}}*/}
                                {/*    >*/}
                                {/*        <i className="far fa-bell" />*/}
                                {/*    </Button>*/}
                                {/*</Tooltip>*/}
                                {/*<Tooltip title="Выйти" theme={`light`} position={`bottom`} size={`small`}>*/}
                                {/*    <Button variant={`light`} size={`sm`} onClick={() => this.authLogout()}>*/}
                                {/*        Выйти*/}
                                {/*    </Button>*/}
                                {/*</Tooltip>*/}
                            </Form>
                        )}
                    </Container>
                </Navbar>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: getUser(state),
    favourites: getUserFavourites(state),
    subscriptions: getUserSubscriptions(state),
    reservations: getUserReservations(state),
});
const mapDispatchToProps = dispatch => ({
    fetchModal: name => dispatch(fetchModal(name)),
    fetchSidebar: name => dispatch(fetchSidebar(name)),
    fetchUserLogout: () => dispatch(fetchUserLogout()),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Header));
