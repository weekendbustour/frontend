import React, {Component} from 'react';
import {getUser} from '../../store/reducers/User';
import {connect} from 'react-redux';
import {toastInfo} from '../../lib/toast';
import {withRouter} from 'react-router';
import {getAppSeo} from '../../store/reducers/App/Seo';
import * as PropTypes from 'prop-types';
import {userTypes} from '../../prop/Types/User';

class MobileTabs extends Component {
    _isMounted = false;
    _TABS = {
        index: '/',
        search: '/search',
        tours: '/tours',
        favourites: '/lk/favourites',
        account: '/lk/account',
    };
    _ALLOWED_TABS = {
        index: ['/'],
        search: ['/search'],
        tours: ['/tours'],
        favourites: ['/lk/favourites', '/lk/favourites/tours', '/lk/favourites/directions', '/lk/favourites/organizations'],
        account: ['/lk/account'],
    };

    static propTypes = {
        seo: PropTypes.shape({
            title: PropTypes.string,
            route: PropTypes.string,
        }),
        user: userTypes,
    };

    constructor(props) {
        super(props);
        this.state = {
            route: props.history.location.pathname,
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.handleOptionChange = this.handleOptionChange.bind(this);
        this.changeTab = this.changeTab.bind(this);
    }

    handleOptionChange(changeEvent) {
        this.setState({
            route: changeEvent.target.value,
        });
    }

    changeTab(route) {
        const {user, history} = this.props;
        this.setState({
            route: route,
        });
        if (user === null && (route === this._TABS.favourites || route === this._TABS.account)) {
            if (route === this._TABS.favourites) {
                toastInfo('Зайдите под своим аккаунтом чтобы посмотреть Избранное');
            }
            if (route === this._TABS.account) {
                toastInfo('Войдите под своим аккаунтом');
            }
            history.push('/auth/login');
        } else {
            history.push(route);
        }
    }

    componentDidMount() {
        this._isMounted = true;
    }
    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {seo} = this.props;
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <div className="tabs-mobile">
                    <div>
                        <input
                            type="radio"
                            value={this._TABS.index}
                            checked={this._ALLOWED_TABS.index.indexOf(seo.route) !== -1}
                            onChange={() => this.handleOptionChange}
                        />
                        <label htmlFor="tab--mobile--1" onClick={() => this.changeTab(this._TABS.index)}>
                            <span>
                                <i className="fas fa-home" />
                            </span>
                            <span>Главная</span>
                            {/*<div className="wave"/>*/}
                        </label>
                    </div>
                    <div>
                        <input
                            type="radio"
                            value={this._TABS.search}
                            checked={this._ALLOWED_TABS.search.indexOf(seo.route) !== -1}
                            onChange={() => this.handleOptionChange}
                        />
                        <label htmlFor="tab--mobile--2" onClick={() => this.changeTab(this._TABS.search)}>
                            <span>
                                <i className="fas fa-search" />
                            </span>
                            <span>Поиск</span>
                        </label>
                    </div>
                    <div>
                        <input
                            type="radio"
                            value={this._TABS.tours}
                            checked={this._ALLOWED_TABS.tours.indexOf(seo.route) !== -1}
                            onChange={() => this.handleOptionChange}
                        />
                        <label htmlFor="tab--mobile--3" onClick={() => this.changeTab(this._TABS.tours)}>
                            <span>
                                <i className="fas fa-bus" />
                            </span>
                            <span>Туры</span>
                        </label>
                    </div>
                    <div>
                        <input
                            type="radio"
                            value={this._TABS.favourites}
                            checked={this._ALLOWED_TABS.favourites.indexOf(seo.route) !== -1}
                            onChange={() => this.handleOptionChange}
                        />
                        <label htmlFor="tab--mobile--4" onClick={() => this.changeTab(this._TABS.favourites)}>
                            <span>
                                <i className="fas fa-heart" />
                            </span>
                            <span>Избранное</span>
                        </label>
                    </div>
                    <div>
                        <input
                            type="radio"
                            value={this._TABS.account}
                            checked={this._ALLOWED_TABS.account.indexOf(seo.route) !== -1}
                            onChange={() => this.handleOptionChange}
                        />
                        <label htmlFor="tab--mobile--5" onClick={() => this.changeTab(this._TABS.account)}>
                            <span>
                                <i className="fas fa-user" />
                            </span>
                            <span>Мой</span>
                        </label>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}
const mapStateToProps = state => ({
    user: getUser(state),
    seo: getAppSeo(state),
});

export default withRouter(connect(mapStateToProps)(MobileTabs));
