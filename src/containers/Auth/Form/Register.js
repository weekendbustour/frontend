import React, {Component} from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import InputPhone from './../../../components/Form/InputPhone';
import InputGroup from 'react-bootstrap/InputGroup';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {getAuthRegisterPending, getAuthRegisterErrors, getAuthLoginSocialVkPending} from '../../../store/reducers/Auth';
import {fetchAuthRegister, fetchUserLoginSocialVk} from '../../../store/actions/Auth';
import * as PropTypes from 'prop-types';
import VK, {Auth} from 'react-vk';
import RotateLoader from "../../../components/Loader/RotateLoader";

class RegisterForm extends Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        pending: PropTypes.bool,
        pendingLoginSocialVk: PropTypes.bool,
        validation: PropTypes.array,
        fetchAuthRegister: PropTypes.func,
        fetchUserLoginSocialVk: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            phone: '',
            password: '',
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleChangeValue = this.handleChangeValue.bind(this);
        this.handleToCallback = this.handleToCallback.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    handleChange(e) {
        this.setState({
            [e.currentTarget.name]: e.currentTarget.value,
        });
    }

    handleChangeValue(name, value) {
        this.setState({
            [name]: value,
        });
    }

    handleSubmit(e) {
        e.preventDefault();
        const {fetchAuthRegister} = this.props;
        const {email, password, phone} = this.state;
        const data = {
            email: email,
            phone: phone,
            password: password,
        };
        fetchAuthRegister(data);
    }

    handleToCallback(data) {
        const {fetchUserLoginSocialVk} = this.props;
        fetchUserLoginSocialVk(data);
    }

    render() {
        const {validation, pending, pendingLoginSocialVk} = this.props;
        const {email, password} = this.state;
        if (!this.shouldComponentRender()) return null;
        return (
            <React.Fragment>
                {process.env.REACT_APP_AUTH_VK_ONLY === 'true' && (
                    <Form>
                        <h2>Создать аккаунт</h2>
                        <span className={`text-muted`}>чтобы пользоваться платформой было проще</span>
                        <div className={`mb-3 mt-3`}>
                            {process.env.REACT_APP_VKONTAKTE_ENABLED === 'true' && (
                                <div style={{position: 'relative'}}>
                                    <div className={`${pendingLoginSocialVk ? '' : ''}`}>
                                        <VK apiId={process.env.REACT_APP_VKONTAKTE_APP_ID}>
                                            <Auth
                                                options={{
                                                    authUrl: process.env.REACT_APP_VKONTAKTE_REDIRECT_URI,
                                                    onAuth: data => this.handleToCallback(data),
                                                }}
                                            />
                                        </VK>
                                    </div>
                                    {pendingLoginSocialVk &&
                                    <div className={`social-vk-loader`}>
                                        <RotateLoader />
                                    </div>
                                    }
                                </div>
                            )}
                        </div>
                        <span className={`text-muted`}>
                            Входя в аккаунт или создавая новый, вы соглашаетесь с нашим <Link to={`/privacy`}>Положением о конфиденциальности</Link>
                        </span>
                    </Form>
                )}
                {process.env.REACT_APP_AUTH_VK_ONLY !== 'true' && (
                    <Form onSubmit={this.handleSubmit}>
                        <h2>Создать аккаунт</h2>
                        <span className={`text-muted`}>чтобы пользоваться платформой было проще</span>
                        <div className="social-container">
                            {process.env.REACT_APP_VKONTAKTE_ENABLED === 'true' && (
                                <VK apiId={process.env.REACT_APP_VKONTAKTE_APP_ID}>
                                    <Auth options={{authUrl: process.env.REACT_APP_VKONTAKTE_REDIRECT_URI, onAuth: data => this.handleToCallback(data)}} />
                                </VK>
                            )}
                            {/*<div style={{width: '200px', height: '134px', backgroundColor: '#ccc'}}>*/}
                            {/*    */}
                            {/*</div>*/}
                            {/*<a href="/#" role={`button`} className="social mr-1">*/}
                            {/*    <ReactSVG src={svgVk} className={`svg-social d-inline`} />*/}
                            {/*</a>*/}
                            {/*<a href="/#" role={`button`} className="social mr-1">*/}
                            {/*    <ReactSVG src={svgFacebook} className={`svg-social d-inline`} />*/}
                            {/*</a>*/}
                            {/*<a href="/#" role={`button`} className="social">*/}
                            {/*    <ReactSVG src={svgGoogle} className={`svg-social d-inline`} />*/}
                            {/*</a>*/}
                        </div>
                        <span className={`text-muted`}>или создайте под своим email адресом</span>
                        <Form.Group>
                            <InputGroup>
                                <InputGroup.Prepend>
                                    <InputGroup.Text>
                                        <i className="far fa-envelope" />
                                    </InputGroup.Text>
                                </InputGroup.Prepend>
                                <Form.Control
                                    type="email"
                                    placeholder="Email"
                                    name="email"
                                    value={email}
                                    onChange={this.handleChange}
                                    className={[validation !== null && validation.email !== undefined ? `is-invalid` : ``]}
                                    required
                                />
                                {validation !== null && validation.email !== undefined && (
                                    <Form.Control.Feedback type="invalid">{validation.email}</Form.Control.Feedback>
                                )}
                            </InputGroup>
                        </Form.Group>
                        <Form.Group>
                            <InputGroup>
                                <InputGroup.Prepend>
                                    <InputGroup.Text>
                                        <i className="fas fa-phone" />
                                    </InputGroup.Text>
                                </InputGroup.Prepend>
                                {/*<CleavePhone/>*/}
                                <InputPhone name={`phone`} defaultValue={``} validation={validation} onChange={this.handleChangeValue} />
                                {/*<Form.Control*/}
                                {/*    type="text"*/}
                                {/*    placeholder="Телефон"*/}
                                {/*    name="phone"*/}
                                {/*    value={phone}*/}
                                {/*    onChange={this.handleChange}*/}
                                {/*    className={[validation !== null && validation.phone !== undefined ? `is-invalid` : ``]}*/}
                                {/*    required*/}
                                {/*/>*/}
                                {validation !== null && validation.phone !== undefined && (
                                    <Form.Control.Feedback type="invalid">{validation.phone}</Form.Control.Feedback>
                                )}
                            </InputGroup>
                        </Form.Group>
                        <Form.Group>
                            <InputGroup>
                                <InputGroup.Prepend>
                                    <InputGroup.Text>
                                        <i className="fas fa-lock" />
                                    </InputGroup.Text>
                                </InputGroup.Prepend>
                                <Form.Control
                                    type="password"
                                    placeholder="Пароль"
                                    name="password"
                                    value={password}
                                    onChange={this.handleChange}
                                    className={[validation !== null && validation.password !== undefined ? `is-invalid` : ``]}
                                    required
                                />
                                {validation !== null && validation.password !== undefined && (
                                    <Form.Control.Feedback type="invalid">{validation.password}</Form.Control.Feedback>
                                )}
                                {/*<InputGroup.Append>*/}
                                {/*    <InputGroup.Text><i className="fas fa-eye"/></InputGroup.Text>*/}
                                {/*</InputGroup.Append>*/}
                            </InputGroup>
                        </Form.Group>
                        <Button variant="primary" type="submit" className={`auth-button mt-2 mb-3 ${pending ? 'is-loading' : ''}`}>
                            Зарегистрироваться
                        </Button>
                        <span className={`text-muted`}>
                            Входя в аккаунт или создавая новый, вы соглашаетесь с нашим <Link to={`/privacy`}>Положением о конфиденциальности</Link>
                        </span>
                    </Form>
                )}
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    validation: getAuthRegisterErrors(state),
    pending: getAuthRegisterPending(state),
    pendingLoginSocialVk: getAuthLoginSocialVkPending(state),
});

const mapDispatchToProps = dispatch => ({
    fetchAuthRegister: data => dispatch(fetchAuthRegister(data)),
    fetchUserLoginSocialVk: data => dispatch(fetchUserLoginSocialVk(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(RegisterForm);
