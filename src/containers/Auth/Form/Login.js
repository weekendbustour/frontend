import React, {Component} from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import {ReactSVG} from 'react-svg';
import svgFacebook from '../../../assets/svg/social/facebook.svg';
import svgGoogle from '../../../assets/svg/social/google-hangouts.svg';
import svgYandex from '../../../assets/svg/social/yandex.svg';
import InputGroup from 'react-bootstrap/InputGroup';
import {fetchModal} from '../../../store/reducers/Modal';
import {
    getAuthLoginPending,
    getAuthLoginErrors,
    getAuthLoginSocialVkPending
} from '../../../store/reducers/Auth';
import {fetchUserLogin, fetchUserLoginSocialVk} from '../../../store/actions/Auth';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {MODAL_FORGET_PASSWORD} from '../../../lib/modal';
import {isMobile} from 'react-device-detect';
import * as PropTypes from 'prop-types';
import VK, {Auth} from 'react-vk';
import RotateLoader from "../../../components/Loader/RotateLoader";

class LoginForm extends Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        pending: PropTypes.bool,
        pendingLoginSocialVk: PropTypes.bool,
        validation: PropTypes.array,
        fetchModal: PropTypes.func,
        fetchUserLogin: PropTypes.func,
        fetchUserLoginSocialVk: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.setShow = this.setShow.bind(this);
        this.handleToCallback = this.handleToCallback.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    handleChange(e) {
        this.setState({
            [e.currentTarget.name]: e.currentTarget.value,
        });
    }
    setShow(value) {
        const {fetchModal} = this.props;
        fetchModal(value);
    }

    handleSubmit(e) {
        e.preventDefault();
        const {fetchUserLogin} = this.props;
        const {email, password} = this.state;
        const data = {
            email: email,
            password: password,
        };
        fetchUserLogin(data);
    }

    handleToCallback(data) {
        const {fetchUserLoginSocialVk} = this.props;
        fetchUserLoginSocialVk(data);
        // let urlData = new URLSearchParams({
        //     uid: data.uid,
        //     first_name: data.first_name,
        //     last_name: data.last_name,
        //     photo: data.photo,
        //     photo_rec: data.photo_rec,
        //     hash: data.hash,
        // });
        // console.log(data);
        // window.location.href = `${process.env.REACT_APP_VKONTAKTE_REDIRECT_URI}?${urlData.toString()}`;
    }

    render() {
        const {validation, pending, pendingLoginSocialVk} = this.props;
        const {email, password} = this.state;
        if (!this.shouldComponentRender()) return null;
        return (
            <React.Fragment>
                {process.env.REACT_APP_AUTH_VK_ONLY === 'true' && (
                    <Form>
                        <h2>Войти</h2>
                        <div className="social-container">
                            {process.env.REACT_APP_YANDEX_ENABLED === 'true' &&
                            <a href={`/auth/yandex/redirect`} role={`button`} className="social social-circle mr-1" title={`Войти с помощью Яндекс`}>
                                <ReactSVG src={svgYandex} className={`svg-social d-inline`} />
                            </a>
                            }
                            {process.env.REACT_APP_GOOGLE_ENABLED === 'true' &&
                            <a href={`/auth/google/redirect`} role={`button`} className="social social-circle mr-1" title={`Войти с помощью Google`}>
                                <ReactSVG src={svgGoogle} className={`svg-social d-inline`} />
                            </a>
                            }
                            {process.env.REACT_APP_FACEBOOK_ENABLED === 'true' &&
                            <a href={`/auth/facebook/redirect`} role={`button`} className="social social-circle mr-1" title={`Войти с помощью Facebook`}>
                                <ReactSVG src={svgFacebook} className={`svg-social d-inline`} />
                            </a>
                            }
                        </div>
                        <div className={`mb-3 mt-3`}>
                            {process.env.REACT_APP_VKONTAKTE_ENABLED === 'true' && (
                                <div style={{position: 'relative'}}>
                                    <div className={`${pendingLoginSocialVk ? '' : ''}`}>
                                        <VK apiId={process.env.REACT_APP_VKONTAKTE_APP_ID}>
                                            <Auth
                                                options={{
                                                    authUrl: process.env.REACT_APP_VKONTAKTE_REDIRECT_URI,
                                                    onAuth: data => this.handleToCallback(data),
                                                }}
                                            />
                                        </VK>
                                    </div>
                                    {pendingLoginSocialVk &&
                                        <div className={`social-vk-loader`}>
                                            <RotateLoader />
                                        </div>
                                    }
                                </div>
                            )}
                        </div>
                        <span className={`text-muted`}>
                            Входя в аккаунт или создавая новый, вы соглашаетесь с нашим <Link to={`/privacy`}>Положением о конфиденциальности</Link>
                        </span>
                    </Form>
                )}
                {process.env.REACT_APP_AUTH_VK_ONLY !== 'true' && (
                    <Form onSubmit={this.handleSubmit}>
                        <h2>Войти</h2>
                        <div className="social-container">
                            {process.env.REACT_APP_YANDEX_ENABLED === 'true' &&
                            <a href={`/auth/yandex/redirect`} role={`button`} className="social social-circle mr-1" title={`Войти с помощью Яндекс`}>
                                <ReactSVG src={svgYandex} className={`svg-social d-inline`} />
                            </a>
                            }
                            {process.env.REACT_APP_GOOGLE_ENABLED === 'true' &&
                            <a href={`/auth/google/redirect`} role={`button`} className="social social-circle mr-1" title={`Войти с помощью Google`}>
                                <ReactSVG src={svgGoogle} className={`svg-social d-inline`} />
                            </a>
                            }
                            {process.env.REACT_APP_FACEBOOK_ENABLED === 'true' &&
                            <a href={`/auth/facebook/redirect`} role={`button`} className="social social-circle mr-1" title={`Войти с помощью Facebook`}>
                                <ReactSVG src={svgFacebook} className={`svg-social d-inline`} />
                            </a>
                            }
                            {process.env.REACT_APP_VKONTAKTE_ENABLED === 'true' && (
                                <VK apiId={process.env.REACT_APP_VKONTAKTE_APP_ID}>
                                    <Auth options={{authUrl: process.env.REACT_APP_VKONTAKTE_REDIRECT_URI, onAuth: data => this.handleToCallback(data)}} />
                                </VK>
                            )}
                        </div>
                        <span className={`text-muted`}>или используйте аккаунт</span>
                        <Form.Group>
                            <InputGroup>
                                <InputGroup.Prepend>
                                    <InputGroup.Text>
                                        <i className="fas fa-envelope" />
                                    </InputGroup.Text>
                                </InputGroup.Prepend>
                                <Form.Control
                                    type="text"
                                    placeholder="Email"
                                    name="email"
                                    value={email}
                                    onChange={this.handleChange}
                                    className={[validation !== null && validation.email !== undefined ? `is-invalid` : ``]}
                                    required
                                />
                                {validation !== null && validation.email !== undefined && (
                                    <Form.Control.Feedback type="invalid">{validation.email}</Form.Control.Feedback>
                                )}
                            </InputGroup>
                        </Form.Group>
                        <Form.Group>
                            <InputGroup>
                                <InputGroup.Prepend>
                                    <InputGroup.Text>
                                        <i className="fas fa-lock" />
                                    </InputGroup.Text>
                                </InputGroup.Prepend>
                                <Form.Control type="password" placeholder="Пароль" name="password" value={password} onChange={this.handleChange} required />
                            </InputGroup>
                        </Form.Group>
                        <Form.Group>
                            {!isMobile ? (
                                <button className={`btn btn-link forgot-link`} type={`button`} onClick={() => this.setShow(MODAL_FORGET_PASSWORD)}>
                                    Забыли пароль?
                                </button>
                            ) : (
                                <Link to={`/auth/reset`} className={`btn btn-link forgot-link`}>
                                    Забыли пароль?
                                </Link>
                            )}
                        </Form.Group>
                        <Button variant="primary" type="submit" className={`auth-button mb-3 ${pending ? 'is-loading' : ''}`}>
                            Войти
                        </Button>
                        <span className={`text-muted`}>
                            Входя в аккаунт или создавая новый, вы соглашаетесь с нашим <Link to={`/privacy`}>Положением о конфиденциальности</Link>
                        </span>
                    </Form>
                )}
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    validation: getAuthLoginErrors(state),
    pending: getAuthLoginPending(state),
    pendingLoginSocialVk: getAuthLoginSocialVkPending(state),
});

const mapDispatchToProps = dispatch => ({
    fetchModal: name => dispatch(fetchModal(name)),
    fetchUserLogin: data => dispatch(fetchUserLogin(data)),
    fetchUserLoginSocialVk: data => dispatch(fetchUserLoginSocialVk(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);
