import React, {Component} from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import {fetchModal} from '../../../store/reducers/Modal';
import {
    getAuthForgotPasswordPending,
    getAuthForgotPasswordErrors,
} from '../../../store/reducers/Auth';
import {
    fetchAuthForgotPassword,
} from '../../../store/actions/Auth';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {MODAL_LOGIN} from "../../../lib/modal";
import {isMobile} from 'react-device-detect';
import * as PropTypes from "prop-types";

class ForgetPasswordForm extends Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        pending: PropTypes.bool,
        validation: PropTypes.array,
        fetchModal: PropTypes.func,
        fetchAuthForgotPassword: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.state = {
            email: '',
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.setShow = this.setShow.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }
    setShow(value) {
        const {fetchModal} = this.props;
        fetchModal(value);
    }

    handleChange(e) {
        this.setState({
            [e.currentTarget.name]: e.currentTarget.value,
        });
    }

    handleSubmit(e) {
        e.preventDefault();
        const {fetchAuthForgotPassword} = this.props;
        const {email} = this.state;
        const data = {
            email: email,
        };
        fetchAuthForgotPassword(data);
    }

    render() {
        const {validation, pending} = this.props;
        const {email} = this.state;
        if (!this.shouldComponentRender()) return null;
        return (
            <React.Fragment>
                <Form onSubmit={this.handleSubmit}>
                    <h2>Восстановление пароля</h2>
                    <span className={`text-muted`}>На указанный email быдет отправлена ссылка для восстановления пароля</span>
                    <Form.Group>
                        <InputGroup>
                            <InputGroup.Prepend>
                                <InputGroup.Text>
                                    <i className="fas fa-envelope"/>
                                </InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control
                                type="text"
                                placeholder="Email"
                                name="email"
                                value={email}
                                onChange={this.handleChange}
                                className={[validation !== null && validation.email !== undefined ? `is-invalid` : ``]}
                                required
                            />
                            {validation !== null && validation.email !== undefined && (
                                <Form.Control.Feedback type="invalid">{validation.email}</Form.Control.Feedback>
                            )}
                        </InputGroup>
                    </Form.Group>
                    <Form.Group>
                        {!isMobile ? (
                            <button className={`btn btn-link forgot-link`} type={`button`} onClick={() => this.setShow(MODAL_LOGIN)}>
                                Перейти на страницу Входа
                            </button>
                        ) : (
                            <Link to={`/auth/login`} className={`btn btn-link forgot-link`}>
                                Перейти на страницу Входа
                            </Link>
                        )}
                    </Form.Group>
                    <Button variant="primary" type="submit" className={`auth-button mb-3 ${pending ? 'is-loading' : ''}`}>
                        Отправить
                    </Button>
                    <span className={`text-muted`}>
                        Входя в аккаунт или создавая новый, вы соглашаетесь с нашими <Link to={`/about`}>Правилами и условиями</Link> и{' '}
                        <Link to={`/about`}>Положением о конфиденциальности</Link>
                    </span>
                </Form>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    validation: getAuthForgotPasswordErrors(state),
    pending: getAuthForgotPasswordPending(state),
});

const mapDispatchToProps = dispatch => ({
    fetchModal: name => dispatch(fetchModal(name)),
    fetchAuthForgotPassword: data => dispatch(fetchAuthForgotPassword(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ForgetPasswordForm);
