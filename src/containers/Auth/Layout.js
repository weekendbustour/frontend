import React, {Component, Suspense} from 'react';
import {Redirect, Route, Switch} from 'react-router-dom';
import routes from '../../routes';
import {ProgressBarProvider} from 'react-redux-progress';
import {connect} from 'react-redux';
import {fetchUser, fetchUserWithoutRequest, getUser} from '../../store/reducers/User';
import AuthFooter from './Footer';
import HtmlHead from '../Html/Head';
import store from '../../store';
import {fetchData, fetchDataWithoutRequest} from '../../store/actions/Data';
import {hasDataSearch} from '../../store/reducers/Data';
import {fetchModal} from '../../store/reducers/Modal';
import * as PropTypes from 'prop-types';

class Layout extends Component {
    static propTypes = {
        fetchData: PropTypes.func,
        fetchUser: PropTypes.func,
        hasDataSearch: PropTypes.bool,
        isProgressActive: PropTypes.bool,
    };

    constructor(props) {
        super(props);
        this.state = {};
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentDidMount() {
        const {fetchUser, hasDataSearch, fetchData, location} = this.props;
        const hash = location.hash.replace(/#/g, "?");
        let urlData = new URLSearchParams(hash);
        if (urlData.has('access_token')) {
            window.location.href = `${location.pathname}?${urlData.toString()}`;
        }
        if (window.user !== undefined) {
            store.dispatch(fetchUserWithoutRequest(window.user.data));
        } else {
            fetchUser();
        }
        if (window.project !== undefined) {
            store.dispatch(fetchDataWithoutRequest(window.project.data));
        }
        if (!hasDataSearch) {
            fetchData();
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {isProgressActive} = this.props;
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <div className={`app`}>
                    <HtmlHead />
                    <ProgressBarProvider isActive={isProgressActive} color="#fff" />
                    <div>
                        <Suspense fallback={undefined}>
                            <Switch>
                                {routes.map((route, idx) => {
                                    return route.component ? (
                                        <Route
                                            key={idx}
                                            path={route.path}
                                            exact={route.exact}
                                            name={route.name}
                                            render={props => <route.component {...props} />}
                                        />
                                    ) : null;
                                })}
                                <Redirect from="/" to="/auth/login" />
                            </Switch>
                        </Suspense>
                        <AuthFooter />
                        {/*<MobileTabs />*/}
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

//export default Layout;

const mapStateToProps = state => ({
    isProgressActive: state.progress.isActive,
    hasDataSearch: hasDataSearch(state),
    user: getUser(state),
});

const mapDispatchToProps = dispatch => ({
    fetchModal: name => dispatch(fetchModal(name)),
    fetchData: () => dispatch(fetchData()),
    fetchUser: () => dispatch(fetchUser()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Layout);
