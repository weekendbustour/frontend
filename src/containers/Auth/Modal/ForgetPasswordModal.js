import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Modal from 'react-bootstrap/Modal';
import React, {Component} from 'react';
import Button from 'react-bootstrap/Button';
import {getModal, fetchModal} from '../../../store/reducers/Modal';
import {connect} from 'react-redux';
import {MODAL_FORGET_PASSWORD, MODAL_REGISTER} from '../../../lib/modal';
import ForgetPasswordForm from '../Form/ForgetPassword';
import * as PropTypes from 'prop-types';

class ForgetPasswordModal extends Component {
    _NAME = MODAL_FORGET_PASSWORD;
    _isMounted = false;
    state = {};

    static propTypes = {
        modal: PropTypes.string,
        fetchModal: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.state = {};
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.setShow = this.setShow.bind(this);
    }

    componentDidMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    setShow(value) {
        const {fetchModal} = this.props;
        fetchModal(value);
    }

    render() {
        const {modal} = this.props;
        if (!this.shouldComponentRender() || modal !== this._NAME) return null;
        return (
            <React.Fragment>
                <Modal
                    size="lg"
                    show={modal === this._NAME}
                    onHide={() => this.setShow(null)}
                    aria-labelledby="contained-modal-title-vcenter"
                    centered
                    className={`modal-auth`}
                >
                    <Modal.Body>
                        <button type="button" className="modal-button-close close" onClick={() => this.setShow(null)}>
                            <span aria-hidden="true">×</span>
                            <span className="sr-only">Close</span>
                        </button>
                        <Row className={`auth-form`}>
                            <Col>
                                <div className={`auth-form-container`}>
                                    <div className="form-container sign-in-container">
                                        <ForgetPasswordForm />
                                    </div>
                                    <div className="overlay-container">
                                        <div className="overlay">
                                            <div className="overlay-panel overlay-right">
                                                <h1>Добро пожаловать</h1>
                                                <p>Зарегистрируйте аккаунт для того чтобы пользовать платформой легче</p>
                                                <Button variant={`primary-ghost`} className={`auth-button`} onClick={() => this.setShow(MODAL_REGISTER)}>
                                                    Зарегистрироваться
                                                </Button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    modal: getModal(state),
});

const mapDispatchToProps = dispatch => ({
    fetchModal: name => dispatch(fetchModal(name)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ForgetPasswordModal);
