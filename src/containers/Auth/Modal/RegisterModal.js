import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Modal from 'react-bootstrap/Modal';
import React, {Component} from 'react';
import Button from 'react-bootstrap/Button';
import {fetchModal, getModal} from '../../../store/reducers/Modal';
import {connect} from 'react-redux';
import RegisterForm from './../Form/Register';
import {MODAL_LOGIN, MODAL_REGISTER} from '../../../lib/modal';
import * as PropTypes from 'prop-types';

class RegisterModal extends Component {
    _NAME = MODAL_REGISTER;
    _isMounted = false;
    state = {};

    static propTypes = {
        modal: PropTypes.string,
        fetchModal: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            phone: '',
            password: '',
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.setShow = this.setShow.bind(this);
    }

    componentDidMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    setShow(value) {
        const {fetchModal} = this.props;
        fetchModal(value);
    }

    render() {
        const {modal} = this.props;
        if (!this.shouldComponentRender() || modal !== this._NAME) return null;
        return (
            <React.Fragment>
                <Modal
                    size="lg"
                    show={modal === this._NAME}
                    onHide={() => this.setShow(null)}
                    aria-labelledby="contained-modal-title-vcenter"
                    centered
                    className={`modal-auth`}
                >
                    <Modal.Body>
                        <button type="button" className="modal-button-close close" onClick={() => this.setShow(null)}>
                            <span aria-hidden="true">×</span>
                            <span className="sr-only">Close</span>
                        </button>
                        <Row className={`auth-form`}>
                            <Col>
                                <div className={`auth-form-container right-panel-active`}>
                                    <div className="form-container sign-up-container">
                                        <RegisterForm />
                                    </div>
                                    <div className="overlay-container">
                                        <div className="overlay">
                                            <div className="overlay-panel overlay-left">
                                                <h1>Добро пожаловать</h1>
                                                <p>Если у вас уже есть аккаунт, то войдите под своей учетной записью</p>
                                                <Button variant={`primary-ghost`} className={`auth-button`} onClick={() => this.setShow(MODAL_LOGIN)}>
                                                    Войти
                                                </Button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    </Modal.Body>
                </Modal>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    modal: getModal(state),
});

const mapDispatchToProps = dispatch => ({
    fetchModal: name => dispatch(fetchModal(name)),
});

export default connect(mapStateToProps, mapDispatchToProps)(RegisterModal);
