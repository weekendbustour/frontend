import React, {Component} from 'react';
import {getModal} from '../../../store/reducers/Modal';
import {connect} from 'react-redux';
import {MODAL_AUTH_IS_COMING} from '../../../lib/modal';
import * as PropTypes from 'prop-types';
import ModalDefault from "../../../components/Modal/ModalDefault";
import {ReactSVG} from "react-svg";
import svgVk from "../../../assets/svg/social/vk.svg";
import svgInstagram from "../../../assets/svg/social/instagram.svg";

class AuthIsComingModal extends Component {
    _NAME = MODAL_AUTH_IS_COMING;
    _isMounted = false;
    state = {};

    static propTypes = {
        modal: PropTypes.string,
    };

    constructor(props) {
        super(props);
        this.state = {};
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentDidMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {modal} = this.props;
        if (!this.shouldComponentRender() || modal !== this._NAME) return null;
        return (
            <React.Fragment>
                <ModalDefault name={this._NAME} size={`md`}>
                    <h3>Авторизация скоро будет...</h3>
                    <hr />
                    <div>
                        <p className={`text-center mb-0`}>Мы новая платформа и в скором времени дадим доступ к авторизации.</p>
                        <p className={`text-center`}>Подписывайте на нас в соц. сетях</p>
                        <div className={`d-flex align-items-center justify-content-center mb-3`}>
                            <a href={process.env.REACT_APP_SOCIAL_VK} role={`button`} className="social mr-3" target={`_blank`}>
                                <ReactSVG src={svgVk} className={`svg-social d-inline`} />
                            </a>
                            <a href={process.env.REACT_APP_SOCIAL_INSTAGRAM} role={`button`} className="social" target={`_blank`}>
                                <ReactSVG src={svgInstagram} className={`svg-social d-inline`} />
                            </a>
                        </div>
                        <p className={`text-center text-muted mb-0`}>
                            Авторизация даст возможность бронировать туры, подписываться на организатора, оставлять комментарии и многое другое.
                        </p>
                    </div>
                </ModalDefault>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    modal: getModal(state),
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(AuthIsComingModal);
