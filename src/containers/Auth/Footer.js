import React, {Component} from 'react';
import Container from 'react-bootstrap/Container';
import {connect} from 'react-redux';

class Footer extends Component {
    _isMounted = false;
    state = {};
    static propTypes = {};

    constructor(props) {
        super(props);
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <footer className="text-muted fixed-bottom">
                    <Container className={`text-center pb-3 pt-3`}>
                        <small>ALL MATERIAL © MIDDAY 2019 // ALL RIGHTS RESERVED</small>
                    </Container>
                </footer>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Footer);
