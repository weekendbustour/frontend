import React, {Component} from 'react';
import {promotionTypes} from '../../prop/Types/Promotion';
import Container from "react-bootstrap/Container";

class BannerSimple extends Component {
    static propTypes = {
        promotion: promotionTypes,
    };

    constructor(props) {
        super(props);
        this.state = {
            promotion: props.promotion,
        };
    }

    render() {
        const {promotion} = this.state;
        const image = promotion.image.images[0];
        return (
            <React.Fragment>
                <div className="grid--news-container is-app-promotion" style={{backgroundImage: `url(${image})`}}>
                    <Container>
                        <div className={`banner`}>
                            <div className="banner-text">
                                <h3>{promotion.title}</h3>
                                <p>{promotion.description}</p>
                                <a href={promotion.url} className={`btn btn-primary`} target={`_blank`}>
                                    Читать полностью
                                </a>
                            </div>
                        </div>
                    </Container>
                </div>
            </React.Fragment>
        );
    }
}

export default BannerSimple;
