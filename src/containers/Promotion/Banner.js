import React, {Component} from 'react';
import {promotionTypes} from '../../prop/Types/Promotion';

class Banner extends Component {
    static propTypes = {
        promotion: promotionTypes,
    };

    constructor(props) {
        super(props);
        this.state = {
            promotion: props.promotion,
        };
    }

    render() {
        const {promotion} = this.state;
        return (
            <div className="hero hero-400 shadow-banner--">
                <a className={`hero-link`} href={promotion.url} title={`Перейти на страницу`} target={`_blank`}>
                    {promotion.image.images[0] !== undefined && promotion.image.images[1] !== undefined && <div className="hero-one" style={{backgroundImage: 'url(' + promotion.image.images[0] + ')'}} />}
                    {promotion.image.images[0] !== undefined && promotion.image.images[1] !== undefined && <div className="hero-two" style={{backgroundImage: 'url(' + promotion.image.images[1] + ')'}} />}
                    {promotion.image.images[0] !== undefined && promotion.image.images[1] === undefined && <div className="hero-full" style={{backgroundImage: 'url(' + promotion.image.images[0] + ')'}} />}
                    <div className={`hero-title-container`}>
                        <h2 className={`header-title header-primary`}>{promotion.title}</h2>
                        <h4 className={`header-title header-sub`}>{promotion.description}</h4>
                    </div>
                </a>
            </div>
        );
    }
}

export default Banner;
