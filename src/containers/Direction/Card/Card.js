import React, {Component} from 'react';
import DirectionMedia from '../../Direction/Components/Social/Media';
import {Link} from 'react-router-dom';
import Rating from '../../../components/Rating/RatingDefault';
import {directionTypes} from '../../../prop/Types/Direction';

class Card extends Component {
    static propTypes = {
        direction: directionTypes,
    };
    constructor(props) {
        super(props);
        this.state = {
            direction: props.direction,
        };
    }

    render() {
        const {direction} = this.state;
        return (
            <React.Fragment>
                <div className="card__flip">
                    <div className="front" style={{backgroundImage: `url(${direction.image.card})`}}>
                        <div className={`flip--title`}>
                            <h2>{direction.title}</h2>
                        </div>
                        <div className={`flip--rating`}>
                            <div className={`text-center`}>
                                <Rating readonly={true} rating={direction.rating} />
                            </div>
                        </div>
                        <div className={`flip--media`}>
                            <div className={`d-flex`}>
                                <DirectionMedia variant={`light`} direction={direction} />
                            </div>
                            <div className={`link-continue`}>
                                <Link to={`/direction/${direction.id}/${direction.name}`} className={`btn btn-sm btn-light`}>
                                    Подробнее
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default Card;
