import React, {Component} from 'react';
import Button from 'react-bootstrap/Button';
import {getUserSubscriptions, getUserSubscriptionsPendingArray} from '../../../../store/reducers/User/Subscription';
import {fetchUserSubscribeModel} from '../../../../store/actions/User/Subscription';
import {fetchUserFavourite, getUserFavourites, getUserFavouritesPendingArray} from '../../../../store/reducers/User/Favourite';
import {connect} from 'react-redux';
import {TooltipButton} from '../../../Layout/Tooltip/Button';
import {isMobile} from 'react-device-detect';
import {scrollTo} from '../../../../lib/scrollTo';
import * as PropTypes from 'prop-types';
import {userFavouritesTypes, userSubscriptionsTypes} from '../../../../prop/Types/User';
import {directionTypes} from '../../../../prop/Types/Direction';

class Media extends Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        direction: directionTypes,
        variant: PropTypes.string,
        size: PropTypes.string,

        favourites: userFavouritesTypes,
        subscriptions: userSubscriptionsTypes,
        favouritesPending: PropTypes.array,
        subscriptionsPending: PropTypes.array,
        fetchUserFavourite: PropTypes.func,
        fetchUserSubscribeModel: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.state = {
            direction: props.direction,
            size: props.size ?? 'sm',
            variant: props.variant ?? 'light',
        };
        this.setFavourite = this.setFavourite.bind(this);
        this.setSubscription = this.setSubscription.bind(this);
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.scrollToComments = this.scrollToComments.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    setFavourite(item) {
        const {fetchUserFavourite} = this.props;
        fetchUserFavourite('direction', item);
        this.forceUpdate();
    }

    setSubscription(item) {
        const {fetchUserSubscribeModel} = this.props;
        fetchUserSubscribeModel('direction', item);
        this.forceUpdate();
    }

    scrollToComments() {
        if (document.querySelector('.--container-direction-comments')) {
            scrollTo.selector('.--container-direction-comments');
        }
    }

    render() {
        const {favourites, subscriptions, subscriptionsPending, favouritesPending} = this.props;
        const {direction, variant, role, size} = this.state;
        const hasTours = direction.tours.length !== 0;
        const hasToursCount = direction.tours.length;
        const countComments = direction.statistic.comments;
        const liked = favourites !== null ? favourites.directions.indexOf(direction.id) !== -1 : false;
        const subscribed = subscriptions !== null ? subscriptions.directions.indexOf(direction.id) !== -1 : false;
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <ul className={`list-group list-group-horizontal ${role === 'page' && isMobile ? '--left' : ''}`}>
                    <li className={`list-group-item`}>
                        <TooltipButton title={`Количество комментариев`}>
                            <Button
                                size={size}
                                variant={variant}
                                className={[countComments !== 0 ? `text-primary-` : ``]}
                                onClick={() => this.scrollToComments()}
                            >
                                <i className="fas fa-comment-alt" />
                                &#160;
                                <span>{countComments}</span>
                            </Button>
                        </TooltipButton>
                    </li>
                    <li className={`list-group-item`}>
                        <TooltipButton title={hasTours ? `Количество активных туров` : `Нет активных туров`}>
                            <Button size={size} variant={variant} className={[hasTours !== 0 ? `text-primary` : ``]}>
                                <i className="fas fa-bus" />
                                <span>&#160;{hasToursCount}</span>
                            </Button>
                        </TooltipButton>
                    </li>
                </ul>
                <ul className={`list-group list-group-horizontal ${role === 'page' && isMobile ? '--right' : ''}`}>
                    <li className={`list-group-item`}>
                        <TooltipButton title="Добавить в Избранное">
                            <Button
                                size={size}
                                variant={variant}
                                className={`${liked ? 'fa-active' : ''} ${favouritesPending.indexOf('direction-' + direction.id) !== -1 ? 'is-loading' : ''}`}
                                onClick={() => this.setFavourite(direction)}
                            >
                                <i className={`${liked ? 'fas' : 'far'} fa-heart`} />
                            </Button>
                        </TooltipButton>
                    </li>
                    <li className={`list-group-item`}>
                        <TooltipButton title={subscribed ? 'Вы подписаны. Отписаться?' : 'Подписаться на новости'}>
                            <Button
                                size={size}
                                variant={variant}
                                className={`${subscribed ? `text-primary` : ``} ${
                                    subscriptionsPending.indexOf('direction-' + direction.id) !== -1 ? 'is-loading' : ''
                                }`}
                                onClick={() => this.setSubscription(direction)}
                            >
                                <i className="far fa-envelope" />
                            </Button>
                        </TooltipButton>
                    </li>
                </ul>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    favourites: getUserFavourites(state),
    subscriptions: getUserSubscriptions(state),
    favouritesPending: getUserFavouritesPendingArray(state),
    subscriptionsPending: getUserSubscriptionsPendingArray(state),
});

const mapDispatchToProps = dispatch => ({
    fetchUserFavourite: (model, data) => dispatch(fetchUserFavourite(model, data)),
    fetchUserSubscribeModel: (model, data) => dispatch(fetchUserSubscribeModel(model, data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Media);
