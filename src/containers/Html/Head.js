import React, {Component} from 'react';
import {connect} from 'react-redux';
import Helmet from 'react-helmet';
import {getAppSeo} from '../../store/reducers/App/Seo';
import {withRouter} from 'react-router';
import * as PropTypes from 'prop-types';

class Head extends Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        seo: PropTypes.shape({
            title: PropTypes.string,
            description: PropTypes.string,
        }),
    };

    constructor(props) {
        super(props);
        this.state = {};
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {seo} = this.props;
        if (!this.shouldComponentRender()) return null;
        return (
            <React.Fragment>
                <Helmet>
                    <title>{seo.title}</title>
                    <meta name="description" content={seo.description} />
                </Helmet>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    seo: getAppSeo(state),
});
export default withRouter(connect(mapStateToProps)(Head));
