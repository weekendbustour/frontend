import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import ReadMoreReact from 'read-more-react';
import Badge from 'react-bootstrap/Badge';
import * as PropTypes from 'prop-types';
import ArticleStatistic from '../Components/ArticleStatistic';
import {articleTypes} from '../../../prop/Types/Article';
import {TooltipButton} from '../../Layout/Tooltip/Button';

class Card extends Component {
    static propTypes = {
        article: articleTypes,
        type: PropTypes.string,
    };

    constructor(props) {
        super(props);
        this.state = {
            article: props.article,
            type: props.type,
        };
    }

    render() {
        const {article, type} = this.state;
        return (
            <React.Fragment>
                <div className={`card__snipe is-${type}`}>
                    <div className="card__snipe--category">
                        {article.categories.map((category, key) => (
                            <TooltipButton title={`Перейти на страницу Категории`} key={key}>
                                <Link to={`/category/${category.id}/${category.name}`}>
                                    <Badge
                                        variant="default"
                                        className={[``, category.color ? `colored` : ``]}
                                        style={{backgroundColor: category.color, borderColor: category.color}}
                                    >
                                        {category.title}
                                    </Badge>
                                </Link>
                            </TooltipButton>
                        ))}
                    </div>
                    <Link to={`/article/${article.id}/${article.name}`}>
                        <div className="card__snipe--background" style={{backgroundImage: 'url(' + article.image.card + ')'}} />
                        <div className="card__snipe--date">
                            <span className="day">{article.published_at_day}</span>
                            <span className="month">{article.published_at_month}</span>
                        </div>
                        <div className="card__snipe--description">
                            <h3>
                                {article.type === 'top' && `#${article.number} `}
                                {article.title}
                            </h3>
                            <div>
                                {article.preview_description !== null && (
                                    <ReadMoreReact text={article.preview_description} min={70} ideal={80} max={100} readMoreText="Читать полностью" />
                                )}
                            </div>
                            <div className="card__snipe--socials">
                                <ArticleStatistic article={article} />
                            </div>
                        </div>
                    </Link>
                </div>
            </React.Fragment>
        );
    }
}

export default Card;
