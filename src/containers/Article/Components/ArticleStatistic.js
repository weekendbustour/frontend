import React, {Component} from 'react';
import {connect} from 'react-redux';
import * as PropTypes from 'prop-types';
import {TooltipButton} from '../../Layout/Tooltip/Button';
import Button from 'react-bootstrap/Button';
import {articleTypes} from "../../../prop/Types/Article";

class ArticleStatistic extends Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        article: articleTypes,
        variant: PropTypes.string,
        size: PropTypes.string,
    };

    constructor(props) {
        super(props);
        this.state = {
            article: props.article,
            size: props.size || 'sm',
            variant: props.variant || 'light',
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {article, variant, size} = this.state;
        if (!this.shouldComponentRender()) return '';
        console.log('useEffect');
        return (
            <React.Fragment>
                <ul className={`list-group list-group-horizontal`}>
                    <li className={`list-group-item`}>
                        <TooltipButton title={`Количество комментариев`}>
                            <Button size={size} variant={variant}>
                                <i className="fas fa-comments" />
                                &#160;
                                <span>{article.statistic.comments}</span>
                            </Button>
                        </TooltipButton>
                    </li>
                    <li className={`list-group-item`}>
                        <TooltipButton title={`Количество просмотров`}>
                            <Button size={size} variant={variant}>
                                <i className="fas fa-eye" />
                                &#160;
                                <span>{article.statistic.views}</span>
                            </Button>
                        </TooltipButton>
                    </li>
                </ul>
            </React.Fragment>
        );
    }
}
const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(ArticleStatistic);
