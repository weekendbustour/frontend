import React, {Component} from 'react';
import {fetchModal, getModal} from '../../store/reducers/Modal';
import {connect} from 'react-redux';
import {MODAL_SUBSCRIPTION} from '../../lib/modal';
import * as PropTypes from 'prop-types';

class Subscription extends Component {
    static propTypes = {
        fetchModal: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.state = {};
        this.setShow = this.setShow.bind(this);
    }

    setShow(value) {
        const {fetchModal} = this.props;
        fetchModal(value);
    }

    render() {
        return (
            <React.Fragment>
                <div className={`direction--date-placeholder`}>
                    <div className={`text-center pt-5 pb-5`}>
                        <h5>Пока нет статей</h5>
                        <p>Подпишись и будешь в курсе, когда появятся</p>
                        <div>
                            <button type="button" className={`btn btn-primary`} onClick={() => this.setShow(MODAL_SUBSCRIPTION)}>
                                Подписаться
                            </button>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    modal: getModal(state),
});
const mapDispatchToProps = dispatch => ({
    fetchModal: value => dispatch(fetchModal(value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Subscription);
