import React, {Component} from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Skeleton from 'react-loading-skeleton';
import SkeletonArticle from '../../Skeleton/Article/Card';

class Page extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <React.Fragment>
                <Container className={`mb-0 mt-0 page__flip`}>
                    <Row>
                        <Col xs={12}>
                            <div className={`mt-2 mb-2`}>
                                <Skeleton height={`25px`} width={`200px`} />
                            </div>
                        </Col>
                    </Row>
                </Container>
                <div className={`mb-3`}>
                    <Skeleton height={`400px`} width={`100%`} />
                </div>
                <Container>
                    <Row className={`awesome-card cards mt-3`}>
                        <Col xs={`12`} sm={`12`} md={`6`} lg={`4`} className={`mb-3`}>
                            <SkeletonArticle />
                        </Col>
                        <Col xs={`12`} sm={`12`} md={`6`} lg={`8`} className={`mb-3`}>
                            <SkeletonArticle />
                        </Col>
                        {/*<Col xs={`12`} sm={`12`} md={`6`} lg={`4`} className={`mb-3`}>*/}
                        {/*    <SkeletonArticle />*/}
                        {/*</Col>*/}
                        {/*<Col xs={`12`} sm={`12`} md={`6`} lg={`4`} className={`mb-3`}>*/}
                        {/*    <SkeletonNews />*/}
                        {/*</Col>*/}
                    </Row>
                </Container>
            </React.Fragment>
        );
    }
}

export default Page;
