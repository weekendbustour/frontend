import React, {Component} from 'react';
import Skeleton from 'react-loading-skeleton';
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";

class Item extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        return (
            <React.Fragment>
                <Row>
                    <Col xs={12} md={{span: 8, offset: 2}} className={`mt-4`}>
                        <Skeleton count={1} width={`100%`} height={`51px`} />
                        <Skeleton count={1} width={`100%`} height={`51px`} />
                    </Col>
                </Row>
            </React.Fragment>
        );
    }
}

export default Item;
