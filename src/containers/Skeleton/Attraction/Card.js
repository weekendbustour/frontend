import React, {Component} from 'react';
import Skeleton from 'react-loading-skeleton';

class Card extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        return (
            <React.Fragment>
                <div className="card-polo is-skeleton">
                    <div className={`card-polo--image`}/>
                    <div className="card-polo--content">
                        <h4 className={`card-polo--title`}>
                            <Skeleton count={1} width={`100%`} height={`38px`}/>
                        </h4>
                        <div className={`card-polo--description text-muted`}>
                            <Skeleton count={2} width={`100%`} height={`16px`}/>
                        </div>
                        <div className={`card-polo--media`}>
                            <div>
                                <Skeleton count={1} width={`100%`} height={`38px`}/>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default Card;
