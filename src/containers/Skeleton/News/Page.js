import React, {Component} from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Skeleton from 'react-loading-skeleton';

class Page extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <React.Fragment>
                <Container className={`mb-2 mt-0 page__flip`}>
                    <Row>
                        <Col xs={12}>
                            <div className={`mt-2 mb-2`}>
                                <Skeleton height={`25px`} width={`200px`} />
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12}>
                            <Skeleton height={`235px`} width={`100%`} />
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12} md={12} lg={{span: 8, offset: 2}} className={`mt-3`} style={{zIndex: '-1'}}>
                            <div className="page__flip--text-container">
                                <div className={`mb-3`}>
                                    <Skeleton height={`20px`} width={`100px`} />
                                </div>
                                <div className={`mb-1`}>
                                    <Skeleton height={`36px`} width={`300px`} />
                                </div>
                                <div className={`mb-3`}>
                                    <Skeleton height={`20px`} width={`150px`} />
                                </div>
                                <div>
                                    <Skeleton count={3} height={`20px`} width={`100%`} />
                                </div>
                                <div style={{height: '400px'}}/>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </React.Fragment>
        );
    }
}

export default Page;
