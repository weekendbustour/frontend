import React, {Component} from 'react';
import Skeleton from 'react-loading-skeleton';

class Banner extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <React.Fragment>
                <div className={`mt-5 mb-5`}>
                    <Skeleton height={`400px`} width={`100%`} />
                </div>
            </React.Fragment>
        );
    }
}

export default Banner;
