import React, {Component} from 'react';
import Skeleton from 'react-loading-skeleton';
import Media from 'react-bootstrap/Media';

class Item extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <React.Fragment>
                <Media>
                    <div className={`mr-3`}>
                        <Skeleton count={1} width={`64px`} height={`64px`} circle={true} />
                    </div>
                    <Media.Body>
                        <div className={`mb-3`}>
                            <div className={`d-flex justify-content-between`}>
                                <h5>
                                    <Skeleton count={1} width={`150px`} height={`24px`} />
                                </h5>
                                <div style={{fontSize: '12px'}}>
                                    <Skeleton count={1} width={`100px`} height={`24px`} />
                                </div>
                            </div>
                            <div>
                                <span className={`__date`}>
                                    <Skeleton count={1} width={`100px`} height={`14px`} />
                                </span>
                            </div>
                            <div className={`__text`}>
                                <Skeleton count={2} width={`100%`} height={`14px`} />
                            </div>
                            <div className={`d-flex justify-content-between`}>
                                <Skeleton count={1} width={`100px`} height={`28px`} />
                                <Skeleton count={1} width={`100px`} height={`28px`} />
                            </div>
                        </div>
                    </Media.Body>
                </Media>
            </React.Fragment>
        );
    }
}

export default Item;
