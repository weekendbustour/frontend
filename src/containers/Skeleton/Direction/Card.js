import React, {Component} from 'react';
import Skeleton from 'react-loading-skeleton';

class Card extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <React.Fragment>
                <div className="card__flip is-skeleton">
                    <div className="front" style={{backgroundColor: `#fff`}}>
                        <div className={`flip--title`} style={{width: `100%`}}>
                            <h2>
                                <Skeleton height={`40px`} />
                            </h2>
                        </div>
                        <div className={`flip--media d-block`}>
                            <Skeleton count={1} width={`100%`} height={`30px`} />
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default Card;
