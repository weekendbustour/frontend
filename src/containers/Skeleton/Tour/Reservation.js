import React, {Component} from 'react';
import Skeleton from 'react-loading-skeleton';
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

class Reservation extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        return (
            <React.Fragment>
                <div className={`tour-reservation`}>
                    <div className={`tr-info`}>
                        <div>
                            <div className={`mb-3`}>
                                <Skeleton count={1} width={`100px`} height={`26px`} />
                            </div>
                        </div>
                        <div>
                            <Row>
                                <Col xs={12} md={12} className={``}>
                                    <p className={`mb-1`}>
                                        <Skeleton count={1} width={`200px`} height={`26px`} />
                                    </p>
                                    <p className={`mb-1`}>
                                        <Skeleton count={1} width={`100%`} height={`16px`} />
                                    </p>
                                    <p>
                                        <Skeleton count={1} width={`100%`} height={`16px`} />
                                    </p>
                                </Col>
                            </Row>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default Reservation;
