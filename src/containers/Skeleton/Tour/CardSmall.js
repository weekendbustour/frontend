import React, {Component} from 'react';
import Skeleton from 'react-loading-skeleton';

class SkeletonCardSmall extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        return (
            <React.Fragment>
                <div className={`card__tale is-small`}>
                    <div className={`card__tale--container`}>
                        <div className="card__tale--img">
                            <div className="card__tale--date day-month __is-placeholder">
                                <Skeleton count={1} width={`60px`} height={`60px`} circle={true} />
                            </div>
                            <div className="card__tale--socials" style={{marginBottom: `5px`}}>
                                <Skeleton count={1} width={`100px`} height={`26px`} />
                            </div>
                            {/*<div className={`card__tale--organization`}>*/}
                            {/*    <Skeleton count={1} width={`100px`} height={`16px`} circle={true} />*/}
                            {/*</div>*/}
                        </div>
                        <div className="card__tale--info">
                            <div className={`card__tale--title-description-price`}>
                                <div className={`card__tale--title-description`}>
                                    <div className="card__tale--description text-muted">
                                        <Skeleton count={1} width={`100px`} height={`17px`} />
                                    </div>
                                    <h5 className="card__tale--title">
                                        <Skeleton count={1} width={`200px`} height={`24px`} />
                                    </h5>
                                    <div className="card__tale--description text-muted">
                                        <Skeleton count={1} width={`100%`} height={`17px`} />
                                    </div>
                                </div>
                                <div className={`card__tale--price`}>
                                    <div className="__price">
                                        <Skeleton count={1} width={`100px`} height={`26px`} />
                                    </div>
                                    <div>
                                        <Skeleton count={1} width={`100px`} height={`26px`} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default SkeletonCardSmall;
