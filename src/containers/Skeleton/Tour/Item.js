import React, {Component} from 'react';
import Skeleton from 'react-loading-skeleton';

class Item extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        return (
            <React.Fragment>
                <div className={`card card--1`}>
                    <div className="card__info--up" />
                    <div className="card__img">
                        <div className="card__date day-month __is-placeholder">
                            <Skeleton count={1} width={`60px`} height={`60px`} circle={true} />
                        </div>
                        <div className="card__socials" style={{marginRight: `16px`, marginBottom: `5px`}}>
                            <Skeleton count={1} width={`100px`} height={`26px`} />
                        </div>
                    </div>
                    <div className="card__info">
                        <div className={`card__organization`}>
                            <Skeleton count={1} width={`100px`} height={`16px`} />
                        </div>
                        <h5 className="card__title">
                            <Skeleton count={1} width={`200px`} height={`20px`} />
                        </h5>
                        <div className="card__description text-muted">
                            <Skeleton count={1} width={`100%`} height={`17px`} />
                        </div>
                        <div className={`d-flex justify-content-between align-items-center`}>
                            <div className="card__price">
                                <Skeleton count={1} width={`100px`} height={`26px`} />
                            </div>
                            <div>
                                <Skeleton count={1} width={`100px`} height={`26px`} />
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default Item;
