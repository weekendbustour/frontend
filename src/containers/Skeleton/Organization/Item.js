import React, {Component} from 'react';
import Skeleton from 'react-loading-skeleton';

class Item extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        return (
            <React.Fragment>
                <div className="movie_card" id="bright">
                    <div className="info_section">
                        <div className="movie_header">
                            <div className={`card__rating`}>
                                <div className={`text-center`}>
                                    <Skeleton count={1} width={`100px`} height={`20px`} />
                                </div>
                            </div>
                            <div>
                                <div className="d-flex align-items-center">
                                    <div className={`card__logo mb-1 mr-3`} style={{lineHeight: 1}}>
                                        <Skeleton count={1} width={`50px`} height={`50px`} />
                                    </div>
                                    <div>
                                        <h5>
                                            <Skeleton count={1} width={`200px`} height={`20px`} />
                                        </h5>
                                        <div className={`mb-2`}>
                                            <Skeleton count={1} width={`100px`} height={`18px`} />
                                        </div>
                                    </div>
                                </div>
                                <div>{/*<Skeleton count={1} width={`100%`} height={`30px`}/>*/}</div>
                            </div>
                        </div>
                        <p className={`mt-2 pr-5 text-muted`}>
                            <Skeleton count={1} width={`100%`} height={`20px`} />
                        </p>
                        <div className={`card--socials d-block`}>
                            <Skeleton count={1} width={`100%`} height={`30px`} />
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default Item;
