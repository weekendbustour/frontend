import React, {Component} from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Skeleton from 'react-loading-skeleton';

class Page extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <React.Fragment>
                <Container>
                    <Row>
                        <Col xs={12}>
                            <div className={`mt-2 mb-2`}>
                                <Skeleton height={`25px`} width={`200px`} />
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12}>
                            <Skeleton height={`235px`} width={`100%`} />
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12} md={{span: 6, offset: 3}}>
                            <div className={`mb-3 mt-3`}>
                                <Skeleton height={`38px`} />
                            </div>
                            <div className={`text-center mb-3`}>
                                <Skeleton height={`20px`} width={`300px`} />
                            </div>
                        </Col>
                    </Row>
                </Container>
                <div className={`d-flex mb-5`}>
                    <div className={`mr-1`} style={{width: '25%'}}>
                        <Skeleton height={`168px`} />
                    </div>
                    <div className={`mr-1`} style={{width: '25%'}}>
                        <Skeleton height={`168px`} />
                    </div>
                    <div className={`mr-1`} style={{width: '25%'}}>
                        <Skeleton height={`168px`} />
                    </div>
                    <div className={`mr-1`} style={{width: '25%'}}>
                        <Skeleton height={`168px`} />
                    </div>
                </div>
                <Container>
                    <Row>
                        <Col xs={12}>
                            <Skeleton height={`400px`} width={`100%`} />
                        </Col>
                    </Row>
                </Container>
            </React.Fragment>
        );
    }
}

export default Page;
