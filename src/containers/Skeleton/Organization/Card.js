import React, {Component} from 'react';
import Skeleton from 'react-loading-skeleton';

class Card extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        return (
            <React.Fragment>
                <div className="card__movie">
                    <div className="card__movie--section">
                        <div className="card__movie--header">
                            <div className={`card__movie--rating`}>
                                <div className={`text-center`}>
                                    <Skeleton count={1} width={`100px`} height={`20px`} />
                                </div>
                            </div>
                            <div className="card__movie--title">
                                <div className={`card__movie--logo mb-1 mr-3`} style={{lineHeight: 1}}>
                                    <Skeleton count={1} width={`50px`} height={`50px`} />
                                </div>
                                <div>
                                    <h5>
                                        <Skeleton count={1} width={`200px`} height={`20px`} />
                                    </h5>
                                    <div className={`mb-2`}>
                                        <Skeleton count={1} width={`100px`} height={`18px`} />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="card__movie--description">
                            <p className={`mt-2 pr-5 text-muted`}>
                                <Skeleton count={1} width={`100%`} height={`20px`} />
                            </p>
                        </div>
                        <div className={`card__movie--media d-block`}>
                            <Skeleton count={1} width={`100%`} height={`30px`} />
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default Card;
