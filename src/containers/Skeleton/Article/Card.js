import React, {Component} from 'react';
import Skeleton from 'react-loading-skeleton';

class Card extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        return (
            <React.Fragment>
                <div className="card__snipe is-skeleton">
                    <div>
                        <div className="card__snipe--background"/>
                        {/*<img className="card__snipe--image" src={news.image.card}/>*/}
                        <div className="card__snipe--date p-0">
                            <Skeleton count={1} width={`100%`} height={`48px`}/>
                        </div>
                        <div className="card__snipe--description">
                            <Skeleton count={1} width={`100%`} height={`36px`} />
                            <Skeleton count={2} width={`100%`} height={`16px`} />
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default Card;
