import React, {Component, Suspense} from 'react';
import {Redirect, Route, Switch} from 'react-router-dom';
import routes from '../../routes';
import {connect} from 'react-redux';
import * as PropTypes from 'prop-types';

class Layout extends Component {
    _isMounted = false;

    static propTypes = {
        pending: PropTypes.bool,
        isProgressActive: PropTypes.bool,
        hasDataSearch: PropTypes.bool,
        fetchUser: PropTypes.func,
        fetchData: PropTypes.func,
        fetchSidebar: PropTypes.func,
        fetchUserTokenSuccess: PropTypes.func,
        sidebar: PropTypes.string,
    };

    constructor(props) {
        super(props);
        this.state = {};
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <div className={`app`}>
                    <div>
                        <Suspense fallback={undefined}>
                            <Switch>
                                {routes.map((route, idx) => {
                                    return route.component ? (
                                        <Route
                                            key={idx}
                                            path={route.path}
                                            exact={route.exact}
                                            name={route.name}
                                            render={props => <route.component {...props} />}
                                        />
                                    ) : null;
                                })}
                                <Redirect from="/" to="/" />
                            </Switch>
                        </Suspense>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch => ({

});

export default connect(mapStateToProps, mapDispatchToProps)(Layout);
