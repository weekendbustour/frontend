import {TooltipButton} from '../../../Layout/Tooltip/Button';
import Button from 'react-bootstrap/Button';
import React, {useEffect} from 'react';
import store from '../../../../store';
import {fetchUserFavourite} from '../../../../store/reducers/User/Favourite';
import {tourTypes} from "../../../../prop/Types/Tour";
import {userFavouritesTypes} from "../../../../prop/Types/User";
import * as PropTypes from 'prop-types';

const SocialList = ({tour, favourites = null, aPending = [], variant = 'light', size = 'sm', role = 'card'}) => {
    const liked = favourites !== null ? favourites.tours.indexOf(tour.id) !== -1 : false;

    function setFavourite(item) {
        store.dispatch(fetchUserFavourite('tour', item));
    }

    useEffect(() => {
        console.log('useEffect');
    });
    return (
        <React.Fragment>
            <ul className={`list-group list-group-horizontal`}>
                <li className={`list-group-item`}>
                    <TooltipButton title={`${liked ? 'Убрать из Избранного' : 'Добавить в Избранное'}`}>
                        <Button
                            size={size}
                            variant={variant}
                            className={`${liked ? 'fa-active' : ''} ${aPending.indexOf('tour-' + tour.id) !== -1 ? 'is-loading' : ''}`}
                            onClick={() => setFavourite(tour)}
                            aria-label="Добавить в Избранное"
                        >
                            <i className={`${liked ? 'fas' : 'far'} fa-heart`} />
                        </Button>
                    </TooltipButton>
                </li>
            </ul>
        </React.Fragment>
    );
};

SocialList.propTypes = {
    tour: tourTypes,
    favourites: userFavouritesTypes,
    aPending: PropTypes.array,
    variant: PropTypes.string,
    size: PropTypes.string,
    role: PropTypes.string,
};
export default SocialList;
