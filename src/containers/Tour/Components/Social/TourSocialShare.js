import React from 'react';
import * as PropTypes from 'prop-types';
import {tourTypes} from "../../../../prop/Types/Tour";
import {TooltipShare} from "../../../Layout/Tooltip/Share";

// Accept children and render it/them
export const TourSocialShare = ({tour, variant = 'empty-link', size = `sm`}) => (
    <React.Fragment>
        <TooltipShare title={tour.meta_tag.title} url={tour.url} description={tour.preview_description} image={tour.image.banner} variant={variant} size={size}/>
    </React.Fragment>
);

TourSocialShare.propTypes = {
    tour: tourTypes,
    variant: PropTypes.string,
    size: PropTypes.string,
};
