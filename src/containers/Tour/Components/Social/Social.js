import React, {Component} from 'react';
import {connect} from 'react-redux';
import * as PropTypes from 'prop-types';
import {TooltipButton} from '../../../Layout/Tooltip/Button';
import Button from 'react-bootstrap/Button';
import {tourTypes} from '../../../../prop/Types/Tour';
import {userFavouritesTypes} from '../../../../prop/Types/User';
import {
    fetchUserFavourite,
    fetchUserFavouritesPending,
    getUserFavourites,
    getUserFavouritesPendingArray
} from '../../../../store/reducers/User/Favourite';
import {TourSocialShare} from "./TourSocialShare";

class Social extends Component {
    //static whyDidYouRender = true;
    _isMounted = false;
    state = {};

    static propTypes = {
        tour: tourTypes,
        favourites: userFavouritesTypes,
        favouritesPending: PropTypes.array,
        variant: PropTypes.string,
        size: PropTypes.string,
        role: PropTypes.string,
        fetchUserFavourite: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.state = {
            tour: props.tour,
            size: props.size || 'sm',
            variant: props.variant || 'light',
            role: props.role || 'card',
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.handleSetFavourite = this.handleSetFavourite.bind(this);
    }

    handleSetFavourite() {
        const {tour, fetchUserFavourite} = this.props;
        console.log('Tour Socials handleSetFavourite');
        fetchUserFavourite('tour', tour);
        this.forceUpdate();
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {variant, size, role} = this.state;
        const {tour, favourites, favouritesPending} = this.props;
        const liked = favourites !== null ? favourites.tours.indexOf(tour.id) !== -1 : false;
        if (!this.shouldComponentRender()) return '';
        console.log('Tour Socials');
        return (
            <React.Fragment>
                <ul className={`list-group list-group-horizontal`}>
                    <li className={`list-group-item`}>
                        <TooltipButton title={`${liked ? 'Убрать из Избранного' : 'Добавить в Избранное'}`}>
                            <Button
                                size={size}
                                variant={variant}
                                className={`${liked ? 'fa-active' : ''} ${favouritesPending.indexOf('tour-' + tour.id) !== -1 ? 'is-loading' : ''}`}
                                onClick={this.handleSetFavourite}
                                aria-label="Добавить в Избранное"
                            >
                                <i className={`${liked ? 'fas' : 'far'} fa-heart`} />
                            </Button>
                        </TooltipButton>
                        {role !== 'card' &&
                        <TourSocialShare tour={tour} variant={`light-dark`} size={size}/>
                        }
                        {window.isAdmin !== undefined && window.isAdmin &&
                        <TooltipButton title={`Редактировать этот тур`}>
                            <a href={`/admin/tour?id=${tour.id}`} className={`btn btn-${variant} btn-${size}`}>
                                <i className="fas fa-tools"/>
                            </a>
                        </TooltipButton>
                        }
                    </li>
                </ul>
            </React.Fragment>
        );
    }
}
const mapStateToProps = state => ({
    favourites: getUserFavourites(state),
    favouritesPending: getUserFavouritesPendingArray(state),
});
const mapDispatchToProps = dispatch => ({
    fetchUserFavourite: (model, data) => dispatch(fetchUserFavourite(model, data)),
    fetchUserFavouritesPending: (pendingName) => dispatch(fetchUserFavouritesPending(pendingName)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Social);
