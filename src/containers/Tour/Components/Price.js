import React, {Component} from 'react';
import {TooltipQuestion} from '../../Layout/Tooltip/Question';
import {tourPriceTypes} from '../../../prop/Types/Tour';

class Price extends Component {
    static propTypes = {
        price: tourPriceTypes,
    };

    constructor(props) {
        super(props);
        this.state = {
            price: props.price,
        };
    }

    render() {
        const {price} = this.state;
        return (
            <React.Fragment>
                <div>
                    {price.discount !== null ? (
                        <span>
                            {price.discount}р.
                            <span className="card__discount">{price.value}р.</span>
                        </span>
                    ) : (
                        <span>{price.value}р.</span>
                    )}
                    {price.discount_description !== null && <TooltipQuestion title={price.discount_description} />}
                    {price.value_description !== null && <TooltipQuestion title={price.value_description} />}
                </div>
            </React.Fragment>
        );
    }
}

export default Price;
