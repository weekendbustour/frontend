import React, {Component} from 'react';
import {connect} from 'react-redux';
import {helpers} from '../../../lib/helpers/helpers';
import * as PropTypes from 'prop-types';

class ResultFound extends Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        total: PropTypes.number,
    };

    constructor(props) {
        super(props);
        this.state = {
            total: props.total,
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {total} = this.state;
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <p>
                    {helpers.units(total, ['Найден', 'Найдено', 'Найдено'])} <b>{total}</b> {helpers.units(total, ['тур', 'тура', 'туров'])}
                </p>
            </React.Fragment>
        );
    }
}
const mapStateToProps = state => ({});

export default connect(mapStateToProps)(ResultFound);
