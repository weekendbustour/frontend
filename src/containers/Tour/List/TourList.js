import React, {Component} from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import {connect} from 'react-redux';
import * as PropTypes from 'prop-types';
import {VIEW_GRID} from "../../../lib/view";
import {getAppViewTours} from "../../../store/reducers/App/View";
import TourCard from "../Card/Card";
import TourCardSmall from "../Card/CardSmall";
import {toursTypes} from "../../../prop/Types/Tour";

class TourList extends Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        tours: toursTypes,
        toursView: PropTypes.string,
    };

    constructor(props) {
        super(props);
        this.state = {
            tours: props.tours
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {toursView, tours} = this.props;
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                {toursView === VIEW_GRID ? (
                    <Row className={`awesome-card cards`}>
                        {tours.map((tour, key) => (
                            <Col xs={`12`} sm={`12`} md={`6`} lg={`4`} className={`mb-3`} key={key}>
                                <TourCard tour={tour} />
                            </Col>
                        ))}
                    </Row>
                ) : (
                    <Row className={`awesome-card cards`}>
                        {tours.map((tour, key) => (
                            <Col xs={`12`} sm={`12`} md={`12`} lg={`12`} className={`mb-3`} key={key}>
                                <TourCardSmall tour={tour} />
                            </Col>
                        ))}
                    </Row>
                )}
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    toursView: getAppViewTours(state),
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(TourList);
