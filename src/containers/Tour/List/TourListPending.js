import React, {Component} from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import {connect} from 'react-redux';
import SkeletonTour from '../../../containers/Skeleton/Tour/Card';
import * as PropTypes from 'prop-types';
import {VIEW_GRID, VIEW_ROW} from "../../../lib/view";
import {getAppViewTours} from "../../../store/reducers/App/View";
import SkeletonCardSmall from "../../../containers/Skeleton/Tour/CardSmall";

class TourListPending extends Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        toursView: PropTypes.string,
    };

    constructor(props) {
        super(props);
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {toursView} = this.props;
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                {toursView === VIEW_GRID && (
                    <Row className={`awesome-card cards`}>
                        <Col xs={`12`} sm={`12`} md={`6`} lg={`4`} className={`mb-3`}>
                            <SkeletonTour />
                        </Col>
                        <Col xs={`12`} sm={`12`} md={`6`} lg={`4`} className={`mb-3`}>
                            <SkeletonTour />
                        </Col>
                        <Col xs={`12`} sm={`12`} md={`6`} lg={`4`} className={`mb-3`}>
                            <SkeletonTour />
                        </Col>
                        <Col xs={`12`} sm={`12`} md={`6`} lg={`4`} className={`mb-3`}>
                            <SkeletonTour />
                        </Col>
                        <Col xs={`12`} sm={`12`} md={`6`} lg={`4`} className={`mb-3`}>
                            <SkeletonTour />
                        </Col>
                        <Col xs={`12`} sm={`12`} md={`6`} lg={`4`} className={`mb-3`}>
                            <SkeletonTour />
                        </Col>
                    </Row>
                )}
                {toursView === VIEW_ROW && (
                    <Row className={`awesome-card cards`}>
                        <Col xs={`12`} sm={`12`} md={`12`} lg={`12`} className={`mb-3`}>
                            <SkeletonCardSmall/>
                        </Col>
                        <Col xs={`12`} sm={`12`} md={`12`} lg={`12`} className={`mb-3`}>
                            <SkeletonCardSmall/>
                        </Col>
                        <Col xs={`12`} sm={`12`} md={`12`} lg={`12`} className={`mb-3`}>
                            <SkeletonCardSmall/>
                        </Col>
                    </Row>
                )}
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    toursView: getAppViewTours(state),
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(TourListPending);
