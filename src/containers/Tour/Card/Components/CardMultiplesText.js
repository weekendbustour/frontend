import React, {Component} from 'react';
import {Link, withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import {tourTypes} from '../../../../prop/Types/Tour';
import ListGroup from 'react-bootstrap/ListGroup';
import Accordion from 'react-bootstrap/Accordion';
import Button from 'react-bootstrap/Button';
import {CardAnnounce} from "./CardAnnounce";

class CardMultiplesText extends Component {
    _isMounted = false;
    state = {};
    static propTypes = {
        tour: tourTypes,
    };

    constructor(props) {
        super(props);
        this.state = {
            tour: props.tour,
            open: 0,
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.setOpen = this.setOpen.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    setOpen(key) {
        const {open} = this.state;
        this.setState({
            open: open !== key ? key : 0,
        });
    }

    render() {
        const {tour, open} = this.state;
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <div className={`card__tale--multiples`}>
                    <Accordion defaultActiveKey={open}>
                        <Accordion.Toggle as={Button} variant="link" eventKey={1} onClick={() => this.setOpen(1)}>
                            {open !== 1 ? (
                                <span>
                                    Этот тур на другие даты
                                    {/*Еще {tour.multiples.length} {helpers.units(tour.multiples.length, ['предложение', 'предложения', 'предложений'])}*/}
                                </span>
                            ) : (
                                <span>Скрыть другие даты</span>
                            )}
                        </Accordion.Toggle>
                        <Accordion.Collapse eventKey={1}>
                            <ListGroup variant="flush">
                                {tour.multiples.map((item, key) => (
                                    <ListGroup.Item key={key} className={`p-0`}>
                                        <div className={`d-flex justify-content-between align-items-center`}>
                                            <div>
                                                {item.start_at_days} {item.start_at_month}
                                                &#160;&#160;&#160;
                                            </div>
                                            <div>
                                                {item.announcement !== null ? (
                                                    <CardAnnounce tour={item}/>
                                                ) : (
                                                    <Link to={`/tour/${item.id}/${item.name}`} className={`btn btn-sm btn-light-dark`}>
                                                        Подробнее
                                                    </Link>
                                                )}
                                            </div>
                                        </div>
                                    </ListGroup.Item>
                                ))}
                            </ListGroup>
                        </Accordion.Collapse>
                    </Accordion>
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CardMultiplesText));
