import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import {tourTypes} from '../../../../prop/Types/Tour';

class CardDate extends Component {
    _isMounted = false;
    state = {};
    static propTypes = {
        tour: tourTypes,
    };

    constructor(props) {
        super(props);
        this.state = {
            tour: props.tour,
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {tour} = this.state;
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <div className="card__tale--date day-month">
                    <div>
                        <span>{tour.start_at_days}</span>
                        <span>{tour.start_at_month}</span>
                    </div>
                    {/*{tour.start_at_format !== null && <div>{tour.start_at_format || ''}</div>}*/}
                    {/*{tour.finish_at_format !== null && <div>{tour.finish_at_format || ''}</div>}*/}
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CardDate));
