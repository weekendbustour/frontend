import React, {Component} from 'react';
import {Link, withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import TourPrice from './../../Components/Price';
import {tourTypes} from '../../../../prop/Types/Tour';
import {CardAnnounce} from "./CardAnnounce";

class CardPrice extends Component {
    _isMounted = false;
    state = {};
    static propTypes = {
        tour: tourTypes,
    };

    constructor(props) {
        super(props);
        this.state = {
            tour: props.tour,
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {tour} = this.state;
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <div className={`card__tale--price`}>
                    <div className="__price">{tour.price !== null && <TourPrice price={tour.price} />}</div>
                    {tour.announcement !== null ? (
                        <CardAnnounce tour={tour}/>
                    ) : (
                        <div>
                            <Link to={`/tour/${tour.id}/${tour.name}`} className={`btn btn-sm btn-light-dark`}>
                                Подробнее
                            </Link>
                        </div>
                    )}
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CardPrice));
