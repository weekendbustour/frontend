import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import Flickity from 'react-flickity-component';
import {tourTypes} from '../../../../prop/Types/Tour';
let flickityOptions = {
    contain: true,
    setGallerySize: false,
    fullscreen: true,
    lazyLoad: 2,
    groupCells: 1,
    adaptiveHeight: true,
    //pageDots: false,
    //prevNextButtons: false,
    arrowShape: {
        x0: 10,
        x1: 60,
        y1: 50,
        x2: 70,
        y2: 40,
        x3: 30,
    },
};

class CardImage extends Component {
    _isMounted = false;
    state = {};
    static propTypes = {
        tour: tourTypes,
    };

    constructor(props) {
        super(props);
        this.state = {
            tour: props.tour,
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {tour} = this.state;
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <Flickity
                    className={'carousel'} // default ''
                    elementType={'div'} // default 'div'
                    options={flickityOptions} // takes flickity options {}
                    disableImagesLoaded={true} // default false
                    reloadOnUpdate={true} // default false
                    static // default false
                >
                    {tour.image.images.map((image, key) => (
                        <div
                            key={key}
                            className="card__tale--img is-gallery"
                            style={{backgroundImage: 'url(' + image + ')'}}
                            // onClick={() => this.linkToTour()}
                        />
                    ))}
                </Flickity>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CardImage));
