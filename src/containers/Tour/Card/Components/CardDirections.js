import React, {Component} from 'react';
import {Link, withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import {tourTypes} from '../../../../prop/Types/Tour';
import {TooltipButton} from "../../../Layout/Tooltip/Button";

class CardDirections extends Component {
    _isMounted = false;
    state = {};
    static propTypes = {
        tour: tourTypes,
    };

    constructor(props) {
        super(props);
        this.state = {
            tour: props.tour,
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {tour} = this.state;
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <div className={`card__tale--direction`}>
                    <div className={`card__tale--direction--item`}>
                        <TooltipButton title={`Подробнее о Направлении`}>
                            <Link to={`/direction/${tour.direction.id}/${tour.direction.name}`} className={`tooltip-text text-grey`}>
                                <span>{tour.direction.title}</span>
                            </Link>
                        </TooltipButton>
                        {/*<Tooltip title={`Найти еще туры в ${tour.direction.title}`} theme={`light`} position={`bottom`} size={`small`}>*/}
                        {/*    <Link to={`/search?direction_id=${tour.direction.id}&scroll=1`} className={`tooltip-text text-grey`}>*/}
                        {/*        <span>{tour.direction.title}</span>*/}
                        {/*    </Link>*/}
                        {/*</Tooltip>*/}
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({

});
const mapDispatchToProps = dispatch => ({});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CardDirections));
