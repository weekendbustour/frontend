import React from 'react';
import {tourTypes} from "../../../../prop/Types/Tour";
import {TooltipQuestion} from "../../../Layout/Tooltip/Question";

// Accept children and render it/them
export const CardAnnounce = ({tour}) => (
    <React.Fragment>
        <div className={`__announce`}>
            {tour.announcement.title}
            {tour.announcement.tooltip !== null && <TooltipQuestion title={tour.announcement.tooltip} />}
        </div>
    </React.Fragment>
);

CardAnnounce.propTypes = {
    tour: tourTypes,
};
