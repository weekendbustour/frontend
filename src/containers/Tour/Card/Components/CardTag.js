import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import Badge from 'react-bootstrap/Badge';
import {tourTypes} from '../../../../prop/Types/Tour';

class CardTag extends Component {
    _isMounted = false;
    state = {};
    static propTypes = {
        tour: tourTypes,
    };

    constructor(props) {
        super(props);
        this.state = {
            tour: props.tour,
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {tour} = this.state;
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <div className="card__tale--tag">
                    <Badge
                        variant="default"
                        className={[``, tour.tag.color ? `colored` : ``]}
                        style={{backgroundColor: tour.tag.color, borderColor: tour.tag.color}}
                    >
                        {tour.tag.title}
                    </Badge>
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CardTag));
