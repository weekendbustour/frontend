import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import {TooltipQuestion} from '../../../Layout/Tooltip/Question';
import {tourTypes} from '../../../../prop/Types/Tour';

class CardCancellation extends Component {
    _isMounted = false;
    state = {};
    static propTypes = {
        tour: tourTypes,
    };

    constructor(props) {
        super(props);
        this.state = {
            tour: props.tour,
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {tour} = this.state;
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <div className={`card__tale--cancellation type-is-${tour.cancellation.type}`}>
                    <div className={`cancellation--block`}>
                        <div>
                            {tour.cancellation.title}
                            {tour.cancellation.tooltip !== null && <TooltipQuestion title={tour.cancellation.tooltip} />}
                        </div>
                        {tour.cancellation.description !== null && <div className={`text-center`}>{tour.cancellation.description}</div>}
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CardCancellation));
