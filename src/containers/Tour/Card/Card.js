import React, {Component} from 'react';
import {Link, withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import ReadMoreReact from 'read-more-react';
import {isMobile} from 'react-device-detect';
import SocialList from '../Components/Social/Social';
import {tourTypes} from '../../../prop/Types/Tour';
import CardDirections from './Components/CardDirections';
import CardCancellation from './Components/CardCancellation';
import CardPrice from './Components/CardPrice';
import CardTag from './Components/CardTag';
import CardDate from './Components/CardDate';
import CardImage from './Components/CardImage';
import {TooltipButton} from "../../Layout/Tooltip/Button";
import CardMultiplesText from "./Components/CardMultiplesText";

class Card extends Component {
    _isMounted = false;
    state = {};
    static propTypes = {
        tour: tourTypes,
    };

    constructor(props) {
        super(props);
        this.state = {
            tour: props.tour,
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.linkToTour = this.linkToTour.bind(this);
    }

    linkToTour() {
        const {history} = this.props;
        const {tour} = this.state;
        history.push(`/tour/${tour.id}/${tour.name}`);
    }
    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {tour} = this.state;
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <div className={`card__tale`}>
                    <div
                        className={`card__tale--container ${tour.cancellation !== null ? 'is--cancellation' : ''} ${
                            tour.announcement !== null ? 'is--announcement' : ''
                        }`}
                    >
                        <div className="card__tale--up" />
                        <div className={`card_tale--main`}>
                            <div
                                className={`card__tale--gallery-container ${tour.image.images.length === 1 ? '__no-dots' : ''} ${
                                    isMobile ? '__is-mobile' : ''
                                }`}
                            >
                                <CardImage tour={tour} />
                            </div>
                            <CardDate tour={tour} />
                            <div className="card__tale--socials">
                                <SocialList tour={tour} role={`card`}/>
                            </div>
                            {tour.tag !== null && <CardTag tour={tour} />}
                        </div>
                        <div className="card__tale--info">
                            <div className={`card__tale--logo`}>
                                <TooltipButton title={`Перейти на страницу Организатора`}>
                                    <Link to={`/organization/${tour.organization.id}/${tour.organization.name}`}>
                                        <img src={tour.organization.image.logo} alt="" />
                                    </Link>
                                </TooltipButton>
                            </div>
                            {tour.direction !== null && <CardDirections tour={tour} />}
                            <h5 className="card__tale--title">{tour.title}</h5>
                            <div className="card__tale--description text-muted">
                                <ReadMoreReact text={tour.preview_description} min={70} ideal={80} max={100} readMoreText="Читать полностью" />
                            </div>
                            {tour.multiples.length !== 0 &&
                                <CardMultiplesText tour={tour}/>
                            }
                            <CardPrice tour={tour} />
                        </div>
                    </div>
                    {tour.cancellation !== null && <CardCancellation tour={tour} />}
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Card));
