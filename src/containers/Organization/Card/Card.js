import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import OrganizationMedia from './../Components/Social/Media';
import Rating from './../../../components/Rating/RatingDefault';
import OrganizationDepartures from './../Components/Departures';
import {isMobile} from 'react-device-detect';
import ReadMoreReact from 'read-more-react';
import {organizationTypes} from '../../../prop/Types/Organization';

class Card extends Component {
    static propTypes = {
        organization: organizationTypes,
    };

    constructor(props) {
        super(props);
        this.state = {
            organization: props.organization,
        };
    }

    render() {
        const {organization} = this.state;
        return (
            <React.Fragment>
                <div className="card__movie">
                    <div className="card__movie--section">
                        <div className="card__movie--header">
                            <div className={`card__movie--rating`}>
                                <div className={`text-center`}>
                                    <Rating readonly={true} rating={organization.rating} />
                                </div>
                            </div>
                            <div className="card__movie--title">
                                <div className={`card__movie--logo mb-1 mr-3`}>
                                    <img src={organization.image.logo} alt="" />
                                </div>
                                <div>
                                    <h5>{organization.title}</h5>
                                    {organization.departures.length !== 0 && (
                                        <div className={`text-muted`}>
                                            <OrganizationDepartures organization={organization} />
                                        </div>
                                    )}
                                </div>
                            </div>
                        </div>
                        <div className="card__movie--description">
                            <div className={`mt-2 mb-3 pr-5 mr-5 text-muted`}>
                                {isMobile && organization.preview_description !== null ? (
                                    <ReadMoreReact text={organization.preview_description} min={70} ideal={80} max={100} readMoreText="Читать полностью" />
                                ) : (
                                    <span>{organization.preview_description}</span>
                                )}
                            </div>
                        </div>
                        <div className={`card__movie--media`}>
                            <div className={`d-flex`}>
                                <OrganizationMedia organization={organization} />
                            </div>
                            <div className={`link-continue`}>
                                <Link
                                    to={`/organization/${organization.id || 1}/${organization.name || 1}`}
                                    className={`btn btn-sm btn-${isMobile ? 'light-dark' : 'light'}`}
                                >
                                    Подробнее
                                </Link>
                            </div>
                        </div>
                    </div>
                    <div className="card__movie--background" style={{backgroundImage: 'url(' + organization.image.card + ')'}} />
                </div>
            </React.Fragment>
        );
    }
}

export default Card;
