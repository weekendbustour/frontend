import React, {Component} from 'react';
import {organizationTypes} from '../../../prop/Types/Organization';

class Departures extends Component {
    static propTypes = {
        organization: organizationTypes,
    };

    constructor(props) {
        super(props);
        this.state = {
            organization: props.organization,
        };
    }

    render() {
        const {organization} = this.state;
        return (
            <React.Fragment>
                <div className={`mb-0`}>
                    <span>Из&#160;</span>
                    {organization.departures.map((departure, key) => (
                        <span key={key}>
                            <span>{departure.title}</span>
                            &#160;
                        </span>
                    ))}
                </div>
            </React.Fragment>
        );
    }
}

export default Departures;
