import React, {Component} from 'react';
import {connect} from 'react-redux';
import {organizationTypes} from '../../../../prop/Types/Organization';
import {ReactSVG} from 'react-svg';
import svgVk from '../../../../assets/svg/social/vk.svg';
import svgInstagram from '../../../../assets/svg/social/instagram.svg';
import svgWhatsapp from '../../../../assets/svg/social/whatsapp.svg';
import svgYoutube from '../../../../assets/svg/social/youtube.svg';
import svgGmail from '../../../../assets/svg/social/gmail.svg';
import svgGlobe from '../../../../assets/svg/social/globe.svg';
import {TooltipButton} from "../../../Layout/Tooltip/Button";

class Social extends Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        organization: organizationTypes,
    };

    constructor(props) {
        super(props);
        this.state = {
            organization: props.organization,
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {organization} = this.state;
        if (!this.shouldComponentRender()) return '';
        console.log('useEffect');
        return (
            <React.Fragment>
                <ul className={`list-group list-group-horizontal social-container`}>
                    {organization.socials.vkontakte !== undefined && (
                        <li className={`list-group-item`}>
                            <TooltipButton title={`Перейти на страницу в Вконтакте`}>
                                <a href={organization.socials.vkontakte.url} role={`button`} className="social" target={`_blank`}>
                                    <ReactSVG src={svgVk} className={`svg-social d-inline`} />
                                </a>
                            </TooltipButton>
                            {/*<a href={organization.socials.vkontakte.url} className={`btn btn-${variant} btn-${size}`} target={`_blank`}>*/}
                            {/*    <i className="fab fa-vk" />*/}
                            {/*</a>*/}
                        </li>
                    )}
                    {organization.socials.instagram !== undefined && (
                        <li className={`list-group-item`}>
                            <TooltipButton title={`Перейти на страницу в Инстаграм`}>
                                <a href={organization.socials.instagram.url} role={`button`} className="social" target={`_blank`}>
                                    <ReactSVG src={svgInstagram} className={`svg-social d-inline`} />
                                </a>
                            </TooltipButton>
                            {/*<a href={organization.socials.instagram.url} className={`btn btn-${variant} btn-${size}`} target={`_blank`}>*/}
                            {/*    <i className="fab fa-instagram" />*/}
                            {/*</a>*/}
                        </li>
                    )}
                    {organization.socials.whatsapp !== undefined && (
                        <li className={`list-group-item`}>
                            <TooltipButton title={`Перейти в WhatsApp`}>
                                <a href={`https://wa.me/` + organization.socials.whatsapp.url} role={`button`} className="social" target={`_blank`}>
                                    <ReactSVG src={svgWhatsapp} className={`svg-social d-inline`} />
                                </a>
                            </TooltipButton>
                            {/*<a href={organization.socials.instagram.url} className={`btn btn-${variant} btn-${size}`} target={`_blank`}>*/}
                            {/*    <i className="fab fa-instagram" />*/}
                            {/*</a>*/}
                        </li>
                    )}
                    {organization.socials.youtube !== undefined && (
                        <li className={`list-group-item`}>
                            <TooltipButton title={`Перейти на страницу в YouTube`}>
                                <a href={organization.socials.youtube.url} role={`button`} className="social" target={`_blank`}>
                                    <ReactSVG src={svgYoutube} className={`svg-social d-inline`} />
                                </a>
                            </TooltipButton>
                            {/*<a href={organization.socials.instagram.url} className={`btn btn-${variant} btn-${size}`} target={`_blank`}>*/}
                            {/*    <i className="fab fa-instagram" />*/}
                            {/*</a>*/}
                        </li>
                    )}
                    {organization.socials.gmail !== undefined && (
                        <li className={`list-group-item`}>
                            <TooltipButton title={`Написать Email`}>
                                <a href={`mailto:` + organization.socials.gmail.url} role={`button`} className="social" target={`_blank`}>
                                    <ReactSVG src={svgGmail} className={`svg-social d-inline`} />
                                </a>
                            </TooltipButton>
                            {/*<a href={organization.socials.vkontakte.url} className={`btn btn-${variant} btn-${size}`} target={`_blank`}>*/}
                            {/*    <i className="fab fa-vk" />*/}
                            {/*</a>*/}
                        </li>
                    )}
                    {organization.socials.site !== undefined && (
                        <li className={`list-group-item`}>
                            <TooltipButton title={`Перейти на сайт`}>
                                <a href={organization.socials.site.url} role={`button`} className="social" target={`_blank`}>
                                    <ReactSVG src={svgGlobe} className={`svg-social d-inline`} />
                                </a>
                            </TooltipButton>
                            {/*<a href={organization.socials.vkontakte.url} className={`btn btn-${variant} btn-${size}`} target={`_blank`}>*/}
                            {/*    <i className="fab fa-vk" />*/}
                            {/*</a>*/}
                        </li>
                    )}
                </ul>
            </React.Fragment>
        );
    }
}
const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Social);
