import React, {Component} from 'react';
import {connect} from 'react-redux';
import * as PropTypes from 'prop-types';
import {TooltipButton} from '../../../Layout/Tooltip/Button';
import Button from 'react-bootstrap/Button';
import {userFavouritesTypes, userSubscriptionsTypes} from '../../../../prop/Types/User';
import {fetchUserFavourite, getUserFavourites, getUserFavouritesPendingArray} from '../../../../store/reducers/User/Favourite';
import {isMobile} from 'react-device-detect';
import {getUserSubscriptions, getUserSubscriptionsPendingArray} from '../../../../store/reducers/User/Subscription';
import {fetchUserSubscribeModel} from '../../../../store/actions/User/Subscription';
import {organizationTypes} from '../../../../prop/Types/Organization';
import {scrollTo} from '../../../../lib/scrollTo';

class Media extends Component {
    _isMounted = false;
    state = {};

    static propTypes = {
        organization: organizationTypes,
        variant: PropTypes.string,
        size: PropTypes.string,

        favourites: userFavouritesTypes,
        subscriptions: userSubscriptionsTypes,
        favouritesPending: PropTypes.array,
        subscriptionsPending: PropTypes.array,
        fetchUserFavourite: PropTypes.func,
        fetchUserSubscribeModel: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.state = {
            organization: props.organization,
            size: props.size || 'sm',
            variant: props.variant || 'empty-link',
        };
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
        this.setFavourite = this.setFavourite.bind(this);
        this.setSubscription = this.setSubscription.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    setFavourite(item) {
        const {fetchUserFavourite} = this.props;
        fetchUserFavourite('organization', item);
        this.forceUpdate();
    }

    setSubscription(item) {
        const {fetchUserSubscribeModel} = this.props;
        fetchUserSubscribeModel('organization', item);
        this.forceUpdate();
    }

    scrollToComments() {
        if (document.querySelector('.--container-organization-comments')) {
            scrollTo.selector('.--container-organization-comments');
        }
    }

    render() {
        const {favourites, subscriptions, favouritesPending, subscriptionsPending} = this.props;
        const {organization, variant, role, size} = this.state;
        const hasTours = organization.tours.length !== 0;
        const hasToursCount = organization.tours.length;
        const countComments = organization.statistic.comments;
        const liked = favourites !== null ? favourites.organizations.indexOf(organization.id) !== -1 : false;
        const subscribed = subscriptions !== null ? subscriptions.organizations.indexOf(organization.id) !== -1 : false;
        if (!this.shouldComponentRender()) return '';
        return (
            <React.Fragment>
                <ul className={`list-group list-group-horizontal ${role === 'page' && isMobile ? '--left' : ''}`}>
                    <li className={`list-group-item`}>
                        <TooltipButton title={`Количество комментариев`}>
                            <Button
                                size={size}
                                variant={variant}
                                className={[countComments !== 0 ? `text-primary-` : ``]}
                                onClick={() => this.scrollToComments()}
                            >
                                <i className="fas fa-comment-alt" />
                                &#160;
                                <span>{countComments}</span>
                            </Button>
                        </TooltipButton>
                    </li>
                    <li className={`list-group-item`}>
                        <TooltipButton title={hasTours ? `Количество активных туров` : `Нет активных туров`}>
                            <Button size={size} variant={variant} className={[hasTours !== 0 ? `text-primary` : ``]}>
                                <i className="fas fa-bus" />
                                <span>&#160;{hasToursCount}</span>
                            </Button>
                        </TooltipButton>
                    </li>
                </ul>
                <ul className={`list-group list-group-horizontal ${role === 'page' && isMobile ? '--right' : ''}`}>
                    <li className={`list-group-item`}>
                        <TooltipButton title="Добавить в Избранное">
                            <Button
                                size={size}
                                variant={variant}
                                className={`${liked ? 'fa-active' : ''} ${
                                    favouritesPending.indexOf('organization-' + organization.id) !== -1 ? 'is-loading' : ''
                                }`}
                                onClick={() => this.setFavourite(organization)}
                            >
                                <i className={`${liked ? 'fas' : 'far'} fa-heart`} />
                            </Button>
                        </TooltipButton>
                    </li>
                    <li className={`list-group-item`}>
                        <TooltipButton title={subscribed ? 'Вы подписаны. Отписаться?' : 'Подписаться на новости'}>
                            <Button
                                size={size}
                                variant={variant}
                                className={`${subscribed ? `fa-active` : ``} ${
                                    subscriptionsPending.indexOf('organization-' + organization.id) !== -1 ? 'is-loading' : ''
                                }`}
                                onClick={() => this.setSubscription(organization)}
                            >
                                <i className={`far fa-envelope`} />
                            </Button>
                        </TooltipButton>
                    </li>
                </ul>
            </React.Fragment>
        );
    }
}
const mapStateToProps = state => ({
    favourites: getUserFavourites(state),
    subscriptions: getUserSubscriptions(state),
    favouritesPending: getUserFavouritesPendingArray(state),
    subscriptionsPending: getUserSubscriptionsPendingArray(state),
});
const mapDispatchToProps = dispatch => ({
    fetchUserFavourite: (model, data) => dispatch(fetchUserFavourite(model, data)),
    fetchUserSubscribeModel: (model, data) => dispatch(fetchUserSubscribeModel(model, data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Media);
