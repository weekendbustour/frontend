import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import * as PropTypes from 'prop-types';
import ReadMoreReact from 'read-more-react';
import {fetchModalEntertainment} from '../../../store/reducers/Modal';
import {entertainmentTypes} from "../../../prop/Types/Entertainment";

class Card extends Component {
    static propTypes = {
        entertainment: entertainmentTypes,
        fetchModalEntertainment: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.state = {
            entertainment: props.entertainment,
        };
        this.setShow = this.setShow.bind(this);
    }

    setShow(data) {
        const {fetchModalEntertainment} = this.props;
        fetchModalEntertainment(data);
    }

    render() {
        const {entertainment} = this.state;
        return (
            <React.Fragment>
                <div className="card-inter" style={{backgroundImage: 'url(' + entertainment.image.default + ')'}}>
                    <div className="content">
                        <div>
                            <h4 className="title">{entertainment.title}</h4>
                            {entertainment.preview_description !== null &&
                            <div className="copy">
                                <ReadMoreReact text={entertainment.preview_description} min={70} ideal={80} max={100} readMoreText="" />
                            </div>
                            }
                        </div>
                        <button className="btn" onClick={() => this.setShow(entertainment)}>
                            Читать подробности
                        </button>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({
    fetchModalEntertainment: data => dispatch(fetchModalEntertainment(data)),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Card));
