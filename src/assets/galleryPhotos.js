export const photos = [
    {
        src: 'https://source.unsplash.com/2ShvY8Lf6l0/800x599',
        width: 4,
        height: 3,
        alt: 'Архыз',
        caption: {
            title: 'Архыз',
            position: 'center',
            width: 'full',
        },
    },
    {
        src: 'https://source.unsplash.com/Dm-qxdynoEc/800x799',
        width: 1,
        height: 1,
        alt: 'Архыз',
        caption: 'Архыз',
    },
    {
        src: 'https://source.unsplash.com/qDkso9nvCg0/600x799',
        width: 3,
        height: 4,
        alt: 'Архыз',
        caption: 'Архыз',
    },
    {
        src: 'https://source.unsplash.com/iecJiKe_RNg/600x799',
        width: 3,
        height: 4,
        alt: 'Архыз',
        caption: 'Архыз',
    },
    {
        src: 'https://source.unsplash.com/u9cG4cuJ6bU/4927x1000',
        width: 4927,
        height: 1000,
        alt: 'Мезмай',
        caption: 'Мезмай',
    },
    {
        src: 'https://source.unsplash.com/qGQNmBE7mYw/800x599',
        width: 4,
        height: 3,
    },
    {
        src: 'https://source.unsplash.com/NuO6iTBkHxE/800x599',
        width: 4,
        height: 3,
    },
    {
        src: 'https://source.unsplash.com/pF1ug8ysTtY/600x400',
        width: 4,
        height: 3,
    },
    {
        src: 'https://source.unsplash.com/A-fubu9QJxE/800x533',
        width: 4,
        height: 3,
    },
    {
        src: 'https://source.unsplash.com/5P91SF0zNsI/740x494',
        width: 4,
        height: 3,
    },
];
