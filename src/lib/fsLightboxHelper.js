//const _ = require('lodash');
const each =  require('lodash/reduce');

const MarkdownIt = require('markdown-it')({
    html: true,
    linkify: true,
    typographer: true,
    breaks: true,
});

export const fsLightboxHelper = {
    /**
     *
     */
    clearTitles() {
        const next = document.querySelector('.fslightbox-slide-btn-next-container');
        const prev = document.querySelector('.fslightbox-slide-btn-previous-container');
        const fullscreen = document.querySelector('.fslightbox-toolbar-button[title="Enter fullscreen"]');
        const close = document.querySelector('.fslightbox-toolbar-button[title="Close"]');
        if (next !== null) {
            next.title = '';
        }
        if (prev !== null) {
            prev.title = '';
        }
        if (fullscreen !== null) {
            fullscreen.title = '';
        }
        if (close !== null) {
            close.title = '';
        }
    },
    addCaption(captions) {
        console.log(captions);
        if (captions === undefined || captions[0] === undefined) {
            return;
        }
        const container = document.querySelector('.fslightbox-absoluted');
        const children = container.children;
        each(children, function (item, key) {
            const caption = captions[key];
            const div = document.createElement('div');
            div.classList.add('fslightbox-source-caption');

            // TITLE
            if (caption !== undefined && caption.title !== undefined) {
                const title = document.createElement('div');
                title.classList.add('is-title');
                title.innerHTML = caption.title;
                div.appendChild(title);
            }

            // DESCRIPTION
            if (caption !== undefined && caption.description !== undefined) {
                const description = document.createElement('div');
                description.classList.add('is-description');
                description.innerHTML = caption.description;
                div.appendChild(description);
            }

            // SOURCE
            if (caption !== undefined && caption.source !== undefined) {
                const source = document.createElement('div');
                source.classList.add('is-source');
                source.innerHTML = MarkdownIt.render(caption.source);
                div.appendChild(source);
            }
            if (item === undefined) {
                return;
            }
            const inner = item.querySelector('.fslightbox-source-inner');
            if (inner !== null) {
                item.querySelector('.fslightbox-source-inner').appendChild(div);
            }
        });
        // children.map((item) =>
        //     console.log(item)
        // );
    }
};
