import axios from "axios";
import {API_URL, axiosBody, headers, headersImage} from "../api/api";

/**
 *
 * @type {{post(*, *=): *, get(*, *=): *}}
 */
export const request = {

    /**
     *
     * @param url
     * @param data
     * @returns {*}
     */
    post(url, data) {
        return axios.post(API_URL + url, axiosBody(data), {
            headers: headers(),
        });
    },

    /**
     *
     * @param url
     * @param data
     * @returns {*}
     */
    postImage(url, data) {
        return axios.post(API_URL + url, axiosBody(data), {
            headers: headersImage(),
        });
    },

    /**
     *
     * @param url
     * @param data
     * @returns {*}
     */
    get(url, data) {
        if (data === undefined) {
            data = {};
        }
        return axios.get(API_URL + url, {
            params: data,
            headers: headers(),
        });
    }
};
