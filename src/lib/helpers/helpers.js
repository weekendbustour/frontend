//import _ from 'lodash';
import reduce from 'lodash/reduce';

export const helpers = {
    /*
     * функция смены окончаний
     * на вход принимает число и объект со значениями слова в различных склонениях:
     * ['слово', 'слова', 'слов']
     */
    units(num, cases) {
        num = Math.abs(num);

        let word = '';

        if (num.toString().indexOf('.') > -1) {
            word = cases[1];
        } else {
            word = num % 10 === 1 && num % 100 !== 11 ? cases[0] : num % 10 >= 2 && num % 10 <= 4 && (num % 100 < 10 || num % 100 >= 20) ? cases[1] : cases[2];
        }
        return word;
    },

    sum(arr) {
        // returns the sum total of all values in the array
        return reduce(
            arr,
            function(memo, num) {
                return memo + num;
            },
            0
        );
    },

    average(object) {
        const self = this;
        let arr = [];
        object.map(item => arr.push(item.rating));
        if (arr.length === 0) {
            return 0;
        }
        return (self.sum(arr) / arr.length).toFixed(1);
    },

    validateEmail(email) {
        // eslint-disable-next-line
        const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
};
