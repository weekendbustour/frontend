export default (search, category_id) => `?search=${search || ''}&category_id=${category_id || 1}`;
