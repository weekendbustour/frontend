import request from './helpers/request';

export default {
    getMember: id => request('get', `article/${id}`),
};
