import {animateScroll as scroll} from 'react-scroll';
import {isMobile} from 'react-device-detect';

export const scrollTo = {
    /**
     *
     * @param name
     */
    scrollToSelector(name: string) {
        const selector = document.querySelector(name);
        if (selector.length) {
            scroll.scrollTo(selector.offsetTop);
        }
    },

    /**
     *
     * @param name
     */
    scrollToSelectorMobile(name: string) {
        const self = this;
        if (isMobile) {
            self.scrollToSelector(name);
        }
    },

    /**
     *
     * @param name
     * @param options
     */
    selector(name: string, options = null) {
        const selector = document.querySelector(name);
        if (selector !== null) {
            if (options !== null) {
                scroll.scrollTo(selector.offsetTop, options);
            } else {
                scroll.scrollTo(selector.offsetTop);
            }
        }
    },

    /**
     *
     * @param name
     */
    selectorMobile(name: string) {
        const self = this;
        if (isMobile) {
            self.selector(name);
        }
    },

    selectorFast(name: string) {
        const self = this;
        self.selector(name, {
            delay: 0,
            duration: 300,
            smooth: 'easeInOutQuart',
        });
    },
};
