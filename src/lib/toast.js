import {toast} from 'react-toastify';

export function toastSuccess(message) {
    toast(message, {
        type: toast.TYPE.SUCCESS,
        containerId: 'B',
        autoClose: 5000,
    });
}

export function toastError(message) {
    toast(message, {
        type: toast.TYPE.ERROR,
        containerId: 'B',
        autoClose: 5000,
    });
}

export function toastWarning(message) {
    toast(message, {
        type: toast.TYPE.WARNING,
        containerId: 'B',
        autoClose: 5000,
    });
}

export function toastInfo(message) {
    toast(message, {
        type: toast.TYPE.INFO,
        containerId: 'B',
        autoClose: 5000,
    });
}
export function toastByRes(res) {
    const message = res.message;
    if (message) {
        toastSuccess(message);
    }
}
export function toastByError(error) {
    if (error.response === undefined) {
        return;
    }
    const data = error.response.data;
    if (data.redirect) {
        window.location.href = data.redirect;
    }
    // валидация
    if (data.status_code === 422 && data.errors !== undefined) {
        for (let [value] of Object.entries(data.errors)) {
            toastError(data.errors[value][0]);
            //toastError(value[0]);
        }
        return;
    }
    if (data.message !== undefined) {
        toastError(data.message);
    }
}
export function toastBySuccess(response) {
    const data = response.data;
    if (data.message !== undefined) {
        toastSuccess(data.message);
    }
}

export const checkForError = response => {
    if (!response.ok) throw Error(response.statusText);
    return response.json();
};

export function toastAuthModal(text) {
    toast(text, {
        type: toast.TYPE.WARNING,
        containerId: 'B',
        autoClose: 5000,
    });
}
