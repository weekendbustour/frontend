import store from '../store';
import {fetchAppSeo} from '../store/actions/App/Seo';
export const APP_NAME = process.env.REACT_APP_NAME;
export const APP_NAME_FULL = process.env.REACT_APP_NAME_FULL;
export const APP_DESCRIPTION = process.env.REACT_APP_NAME_DESCRIPTION;

export default {
    index: () => {
        const data = {
            title: `${APP_NAME_FULL}`,
            description: `${APP_DESCRIPTION}`,
            route: `/`,
        };
        store.dispatch(fetchAppSeo(data));
    },
    tours: () => {
        const data = {
            title: `Туры | ${APP_NAME}`,
            description: `${APP_DESCRIPTION}`,
            route: `/tours`,
        };
        store.dispatch(fetchAppSeo(data));
    },
    tour: tour => {
        const metaTag = tour.meta_tag;
        const data = {
            title: `${metaTag.title} | ${APP_NAME}`,
            description: `${metaTag.description}`,
            route: `/tour`,
        };
        store.dispatch(fetchAppSeo(data));
    },
    organizations: () => {
        const data = {
            title: `Организаторы | ${APP_NAME}`,
            description: `${APP_DESCRIPTION}`,
            route: `/organizations`,
        };
        store.dispatch(fetchAppSeo(data));
    },
    organization: organization => {
        const metaTag = organization.meta_tag;
        const data = {
            title: `${metaTag.title} | ${APP_NAME}`,
            description: `${metaTag.description}`,
            route: `/organization`,
        };
        store.dispatch(fetchAppSeo(data));
    },
    organizationArticles: organization => {
        const metaTag = organization.meta_tag;
        const data = {
            title: `Статьи написанные организатором ${organization.title} | ${APP_NAME}`,
            description: `${metaTag.description}`,
            route: `/organization`,
        };
        store.dispatch(fetchAppSeo(data));
    },
    directions: () => {
        const data = {
            title: `Направления | ${APP_NAME}`,
            description: `${APP_DESCRIPTION}`,
            route: `/directions`,
        };
        store.dispatch(fetchAppSeo(data));
    },
    direction: direction => {
        const metaTag = direction.meta_tag;
        const data = {
            title: `${metaTag.title} | ${APP_NAME}`,
            description: `${metaTag.description}`,
            route: `/direction`,
        };
        store.dispatch(fetchAppSeo(data));
    },
    directionAttractions: direction => {
        const metaTag = direction.meta_tag;
        const data = {
            title: `Достопримечательности по направлению ${direction.title} | ${APP_NAME}`,
            description: `${metaTag.description}`,
            route: `/direction`,
        };
        store.dispatch(fetchAppSeo(data));
    },
    about: () => {
        const data = {
            title: `О нас | ${APP_NAME}`,
            description: `${APP_DESCRIPTION}`,
            route: `/about`,
        };
        store.dispatch(fetchAppSeo(data));
    },
    privacy: () => {
        const data = {
            title: `Положение о конфиденциальности | ${APP_NAME}`,
            description: `${APP_DESCRIPTION}`,
            route: `/privacy`,
        };
        store.dispatch(fetchAppSeo(data));
    },
    search: () => {
        const data = {
            title: `Поиск | ${APP_NAME}`,
            description: `${APP_DESCRIPTION}`,
            route: `/search`,
        };
        store.dispatch(fetchAppSeo(data));
    },
    news: () => {
        const data = {
            title: `Новости | ${APP_NAME}`,
            description: `${APP_DESCRIPTION}`,
            route: `/news`,
        };
        store.dispatch(fetchAppSeo(data));
    },
    newsPage: news => {
        const data = {
            title: `${news.title} | ${APP_NAME}`,
            description: `${news.preview_description}`,
            route: `/newsPage`,
        };
        store.dispatch(fetchAppSeo(data));
    },
    articles: () => {
        const data = {
            title: `Статьи | ${APP_NAME}`,
            description: `${APP_DESCRIPTION}`,
            route: `/articles`,
        };
        store.dispatch(fetchAppSeo(data));
    },
    article: article => {
        const data = {
            title: `${article.title} | ${APP_NAME}`,
            description: `${article.preview_description}`,
            route: `/article`,
        };
        store.dispatch(fetchAppSeo(data));
    },
    attraction: attraction => {
        const data = {
            title: `${attraction.title} | ${APP_NAME}`,
            description: `${attraction.preview_description}`,
            route: `/attraction`,
        };
        store.dispatch(fetchAppSeo(data));
    },
    attractions: () => {
        const data = {
            title: `Достпримечательности | ${APP_NAME}`,
            description: `${APP_DESCRIPTION}`,
            route: `/attractions`,
        };
        store.dispatch(fetchAppSeo(data));
    },
    help: () => {
        const data = {
            title: `Помощь | ${APP_NAME}`,
            description: `${APP_DESCRIPTION}`,
            route: `/help`,
        };
        store.dispatch(fetchAppSeo(data));
    },
    contacts: () => {
        const data = {
            title: `Контакты | ${APP_NAME}`,
            description: `${APP_DESCRIPTION}`,
            route: `/contacts`,
        };
        store.dispatch(fetchAppSeo(data));
    },
    favourites: () => {
        const data = {
            title: `Избранное | ${APP_NAME}`,
            description: `${APP_DESCRIPTION}`,
            route: `/lk/favourites`,
        };
        store.dispatch(fetchAppSeo(data));
    },
    account: () => {
        const data = {
            title: `Аккаунт | ${APP_NAME}`,
            description: `${APP_DESCRIPTION}`,
            route: `/lk/account`,
        };
        store.dispatch(fetchAppSeo(data));
    },
};
